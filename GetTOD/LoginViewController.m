//  LoginViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "LoginViewController.h"
#import "BaseMapViewController.h"
#import "ServicesListViewController.h"
#import "YourDetailsViewController.h"
#import "GTButton.h"
#import "GTTextField.h"
#import "LoginTextFieldView.h"
#import "AppDelegate.h"

#import "CreatingAccountBlackOverlayViewController.h"
#import "RequestingSupplierBlackOverlayViewController.h"
#import "JobCompleteBlackOverlayViewController.h"
#import "SupplierHomeMapViewController.h"

#import "AFHTTPRequestOperation.h"
#import "AFHTTPSessionManager.h"

#import "Supplier.h"
#import "Province.h"
#import "Entity.h"
#import "Company.h"
#import "Service.h"
#import "City.h"

#import "ImageHelper.h"

#import "AddEditCardDetailsViewController.h"

@interface LoginViewController ()

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topOffset;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *botOffset;
@property (strong, nonatomic) IBOutlet GTButton *signInButton;
@property (strong, nonatomic) IBOutlet UILabel *signUpLabel;

@property (weak, nonatomic) IBOutlet GTButton *createMyAccountButton;
@property (weak, nonatomic) IBOutlet UILabel *signInLabel;

@property (strong, nonatomic) IBOutlet UILabel *forgotPasswordLabel;
@property (strong, nonatomic) IBOutlet UIView *loginFooterView;
@property (strong, nonatomic) IBOutlet UIView *registerFooterView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *headerViewPartner;
@property (strong, nonatomic) IBOutlet UITableView *tableViewFromXib;
@property (strong, nonatomic) IBOutlet UIView *footerBottomView;

@property (strong, nonatomic) LoginTextFieldView *passwordView;
@property (strong, nonatomic) LoginTextFieldView *confirmPasswordView;
@property (strong, nonatomic) LoginTextFieldView *emailAddressView;

@property (strong, nonatomic) GTTextField *email;
@property (strong, nonatomic) GTTextField *password;
@property (strong, nonatomic) GTTextField *confirmPassword;

@property (assign, nonatomic) float yOffset;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    self.tableView = self.tableViewFromXib;
    
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.tableView.frame = self.view.bounds;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.email = [GTTextField new];
    self.email.delegate = self;
    self.email.keyboardType = UIKeyboardTypeEmailAddress;
    self.email.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.email setKeyboardToolbar:self.keyboardToolbar];
    
    self.password = [GTTextField new];
    self.password.secureTextEntry = YES;
    self.password.delegate = self;
    self.password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.password setKeyboardToolbar:self.keyboardToolbar];
    
    [self addFormField:self.email];
    [self addFormField:self.password];
    
    self.emailAddressView = [[LoginTextFieldView alloc] initWithTexField:self.email withPlaceholder:@"Email Address" hasLockIcon:NO];
    self.passwordView = [[LoginTextFieldView alloc] initWithTexField:self.password withPlaceholder:@"Password" hasLockIcon:YES];
    
    [self setupTappableLabels];
    
    if (self.isRegistration)
    {
        self.tableView.tableFooterView = self.registerFooterView;
        self.confirmPassword = [GTTextField new];
        self.confirmPassword.secureTextEntry = YES;
        self.confirmPassword.delegate = self;
        self.confirmPassword.autocapitalizationType = UITextAutocapitalizationTypeNone;
        [self.confirmPassword setKeyboardToolbar:self.keyboardToolbar];
        
        [self addFormField:self.confirmPassword];
        
        self.confirmPasswordView = [[LoginTextFieldView alloc] initWithTexField:self.confirmPassword withPlaceholder:@"Confirm Password" hasLockIcon:YES];
    }
    else
    {
        self.tableView.tableFooterView = self.loginFooterView;
    }
    
#ifdef CLIENT
    self.tableView.tableHeaderView = self.headerView;
#else
    self.tableView.tableHeaderView = self.headerViewPartner;
    [self.signUpLabel removeFromSuperview];// = YES;
    [self.forgotPasswordLabel removeFromSuperview];
    [self.footerBottomView addSubview:self.forgotPasswordLabel];
    
    [self.footerBottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[text]|"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:@{@"text":self.forgotPasswordLabel}]];
    
    [self.footerBottomView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[text]-(>=0)-|"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:@{@"text":self.forgotPasswordLabel}]];
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileImageDownloaded:) name:[NSString stringWithFormat:@"%@-ImageDownloaded",PROFILE_IMAGE_NAME] object:nil];
    [[LocationManager manager] checkIfUserInSouthAfrica:^(BOOL inSA, NSString *message) {}];
}

- (void)checkDone:(id)something
{
    [self hideLoader];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InSACheck" object:nil];
    
    NSNotification *not = (NSNotification*)something;
    NSString *message = [not object];
    
    if (message != nil && ![message isEqualToString:@""] && ![message containsString:@"(null)"])
    {
        [AlertViewManager showAlertWithTitle:@""
                                     message:message
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil
                        buttonSelectionBlock:nil];
    }
    else
    {
        self.signInButton.enabled = YES;
        self.createMyAccountButton.enabled = YES;
        self.signInButton.alpha = 1;
        self.createMyAccountButton.alpha = 1;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkDone:) name:@"InSACheck" object:nil];
    [self reloadTableView];
}


#pragma mark - Private -

- (void)keyboardWasShown:(NSNotification *)notify
{
    NSDictionary* info = [notify userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0); /// 44 ???
    self.tableView.contentInset = contentInsets;
    
    NSIndexPath *path = [self.tableView indexPathForRowAtPoint:[self.tableView convertPoint:self.currentField.center fromView:self.currentField.superview]];
    
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification *)notify
{
    [self reloadTableView];
}


- (void)reloadTableView
{
    CGSize contentSize = self.tableView.contentSize;
    CGSize boundsSize = self.tableView.bounds.size;
    self.yOffset = 0;
    
    if(contentSize.height < boundsSize.height)
    {
        self.yOffset = floorf((boundsSize.height - contentSize.height)/2);
    }
    
    if (IS_IPHONE_4_OR_LESS)
    {
        self.yOffset = self.yOffset - 70;
    }
    else if (IS_IPHONE_5)
    {
        self.yOffset = self.yOffset - 30;
    }
    else
    {
        self.yOffset = self.yOffset - 20;
    }
    
    UIEdgeInsets insets = UIEdgeInsetsMake(self.yOffset, 0, boundsSize.height-self.yOffset-contentSize.height-NAVBAR_HEIGHT, 0);
    self.tableView.contentInset = insets;
    self.tableView.scrollIndicatorInsets = insets;
}

- (void)setupTappableLabels
{
    UITapGestureRecognizer *forgotTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(forgotPassword)];
    forgotTap.numberOfTapsRequired = 1;
    [self.forgotPasswordLabel addGestureRecognizer:forgotTap];
    
    UITapGestureRecognizer *signInTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(goTosignIn)];
    signInTap.numberOfTapsRequired = 1;
    [self.signInLabel addGestureRecognizer:signInTap];
    
#ifdef CLIENT
    UITapGestureRecognizer *signUpTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(goTosignUp)];
    signUpTap.numberOfTapsRequired = 1;
    [self.signUpLabel addGestureRecognizer:signUpTap];
#else
    
#endif
    
}

- (void)profileImageDownloaded:(NSNotification*)notification
{
    [[APIManager manager] updateUserDetails:[SettingsManager manager].firstName
                                    surname:[SettingsManager manager].lastName
                             profilePicture:[notification object]
                             andPhoneNumber:[SettingsManager manager].phoneNumber
                                    success:^(id result) {
                                        [ImageHelper saveImageToDisk:[notification object] withName:PROFILE_IMAGE_NAME];
                                        [SettingsManager manager].profileImageURL = result;
                                        [SettingsManager manager].gotUserDetailsOnce = YES;
                                        [SettingsManager save];
                                        
                                    } failure:^(NSError *error) {
                                        
                                    }];
}

- (void)registerForPushNotifications
{
#if	TARGET_IPHONE_SIMULATOR
    [self navigateToHomeScreen];
#else
    [[APIManager manager] registerForPushNotifications:^(BOOL success) {
        [self hideLoader];
        [self navigateToHomeScreen];
        
    } failure:^(NSError *error) {
        [self hideLoader];
    }];
#endif
}

#pragma mark - Validation -

- (BOOL)isScreenFieldsValid
{
    [super isScreenFieldsValid];
    
    BOOL isValid = YES;
    
    if (self.isRegistration)
    {
        isValid =  [self isEmailValid] && isValid;
        isValid =  [self isPasswordValid] && isValid;
        isValid =  [self isConfirmPasswordValid] && isValid;
    }
    else
    {
        isValid =  [self isEmailValid] && isValid;
        isValid =  [self isPasswordValid] && isValid;
    }
    return isValid;
}


- (BOOL)isEmailValid
{
    if ([Validation isValidEmail:self.email.text])
    {
        [self.emailAddressView dismissError];
        [self dismissScreenError:self.email];
        return YES;
    }
    else
    {
        [self.emailAddressView setError];
        [self setScreenError:self.email message:@"Please enter a valid email address"];
        return NO;
    }
}

- (BOOL)isPasswordValid
{
    if (self.password.text.length >= 8)
    {
        [self.passwordView dismissError];
        [self dismissScreenError:self.password];
        return YES;
    }
    else
    {
        [self.passwordView setError];
        [self setScreenError:self.password message:@"Please enter a password with 8 or more characters"];
        return NO;
    }
    return YES;
}

- (BOOL)isConfirmPasswordValid
{
    if (![self.password.text isEqualToString:self.confirmPassword.text])
    {
        [self.confirmPasswordView setError];
        [self setScreenError:self.confirmPassword message:@"The passwords do not match"];
        return NO;
    }
    else if (self.confirmPassword.text.length < 8)
    {
        [self.confirmPasswordView setError];
        [self setScreenError:self.confirmPassword message:@"Please enter a password with 8 or more characters"];
        return NO;
    }
    else
    {
        [self.confirmPasswordView dismissError];
        [self dismissScreenError:self.confirmPassword];
        return YES;
    }
}


#pragma mark - Textfield -
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    GTTextField *field = (GTTextField*)textField;
    
    if (field == self.email)
    {
        [self isEmailValid];
    }
    else if (field == self.password)
    {
        [self isPasswordValid];
    }
    else if (field == self.confirmPassword)
    {
        [self isConfirmPasswordValid];
    }
}

#pragma mark - TableView -

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LoginTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoginTextFieldCell"];
    
    if (indexPath.row == 0)
    {
        [cell setupCellWithTextFieldView:self.emailAddressView];
    }
    else if (indexPath.row == 1)
    {
        [cell setupCellWithTextFieldView:self.passwordView];
    }
    else if (indexPath.row == 2)
    {
        [cell setupCellWithTextFieldView:self.confirmPasswordView];
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isRegistration)
    {
        return 3;
    }
    else
    {
        return 2;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Actions -
- (IBAction)signIn:(id)sender
{
    [self dismissKeyboard];
    
    if ([self isScreenFieldsValid])
    {
        if ([self.email.text.lowercaseString isEqualToString:@"apple@gettod.com"])
        {
            [self performLogin];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkDone:) name:@"InSACheck" object:nil];
            
            [self showLoader];
            
            [[LocationManager manager] checkIfUserInSouthAfrica:^(BOOL inSA, NSString *message) {
                
                if (inSA)
                {
                    [self performLogin];
                }
                else
                {
                    [AlertViewManager showAlertWithTitle:@""
                                                 message:message
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil
                                    buttonSelectionBlock:nil];
                }
            }];
        }
    }
    else
    {
        [self showScreenError];
    }
}

- (void)performLogin
{
    [self showLoader];
    
    [[APIManager manager] loginWithUsername:self.email.text
                                andPassword:self.password.text
                                    success:^(id result) {
                                        [self hideLoader];
                                        [self registerForPushNotifications];
                                        
                                    } failure:^(NSError *error) {
                                        [self hideLoader];
                                    }];
}

- (void)navigateToHomeScreen
{
#ifdef CLIENT
    [self navigateToServicesList];
#else
    [SettingsManager manager].supplierStatus = SupplierStatusOnline;
    [SettingsManager save];
    
    [[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
        [self hideLoader];
        
        SupplierHomeMapViewController *map = [[SupplierHomeMapViewController alloc] init];
        UINavigationController *nav = self.navigationController;
        [nav popViewControllerAnimated:NO];
        [nav pushViewController:map animated:YES];
    }];
    
#endif
    
}

- (void)navigateToServicesList
{
    [self hideLoader];
    
    ServicesListViewController *list = [[ServicesListViewController alloc] init];
    list.services = [CacheManager servicesFromCache];
    UINavigationController *nav = self.navigationController;
    [nav popViewControllerAnimated:NO];
    [nav pushViewController:list animated:YES];
}

- (void)goTosignUp
{
    LoginViewController *registration = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    registration.isRegistration = YES;
    [SettingsManager manager].isRegistration = YES;
    
    UINavigationController *nav = self.navigationController;
    [self.navigationController popViewControllerAnimated:NO];
    [nav pushViewController:registration animated:YES];
}

- (void)forgotPassword
{
    [self dismissKeyboard];
    
    if ([self isEmailValid])
    {
        [[APIManager manager] resetPasswordWithEmail:self.email.text
                                             success:^(id result) {
                                                 
                                                 [AlertViewManager showAlertWithTitle:@""
                                                                              message:@"Please click the link in the email sent to your address"
                                                                    cancelButtonTitle:@"OK"
                                                                    otherButtonTitles:nil
                                                                 buttonSelectionBlock:nil];
                                                 
                                             } failure:^(NSError *error) {
                                                 
                                             }];
    }
    else
    {
        [self showScreenError];
    }
}

- (IBAction)createAccount:(id)sender
{
    [self dismissKeyboard];
    
    if ([[SettingsManager manager].isInSA isEqualToString:@"YES"])
    {
        if ([self isScreenFieldsValid])
        {
            [self showLoader];
            
            [SettingsManager manager].username = self.email.text;
            [SettingsManager manager].emailAddress = self.email.text;
            [SettingsManager save];
            
            [SettingsManager manager].password = self.password.text;
            
            YourDetailsViewController *yourDetails = [[YourDetailsViewController alloc] init];
            yourDetails.isSettings = NO;
            [self.navigationController pushViewController:yourDetails animated:YES];
        }
        else
        {
            [self showScreenError];
        }
    }
    else
    {
        [self showLoader];
        
        [[LocationManager manager] checkIfUserInSouthAfrica:^(BOOL inSA, NSString *message) {
            
            if (inSA)
            {
                if ([self isScreenFieldsValid])
                {
                    [SettingsManager manager].username = self.email.text;
                    [SettingsManager manager].emailAddress = self.email.text;
                    [SettingsManager save];
                    
                    [SettingsManager manager].password = self.password.text;
                    
                    YourDetailsViewController *yourDetails = [[YourDetailsViewController alloc] init];
                    yourDetails.isSettings = NO;
                    [self.navigationController pushViewController:yourDetails animated:YES];
                }
                else
                {
                    [self showScreenError];
                }
            }
            else
            {
                [AlertViewManager showAlertWithTitle:@""
                                             message:message
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil
                                buttonSelectionBlock:nil];
            }
        }];
    }
}

- (void)goTosignIn
{
    LoginViewController *login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    login.isRegistration = NO;
    [SettingsManager manager].isRegistration = NO;
    
    UINavigationController *nav = self.navigationController;
    [self.navigationController popViewControllerAnimated:NO];
    [nav pushViewController:login animated:YES];
}


@end
