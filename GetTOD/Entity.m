//
//  Entity.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Entity.h"

@implementation Entity

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.entityId = [decoder decodeObjectForKey:@"entityId"];
		self.name = [decoder decodeObjectForKey:@"name"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.entityId forKey:@"entityId"];
	[encoder encodeObject:self.name forKey:@"name"];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nID: %@\nName: %@\n-------------------------------",self.entityId,self.name];
}

@end
