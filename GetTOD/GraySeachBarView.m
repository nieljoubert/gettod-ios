//
//  GraySeachBarView.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/13.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GraySeachBarView.h"
#import "MapSearchModalViewController.h"

@implementation GraySeachBarView

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	
	if (self)
	{
		[self setupBar];
	}
	
	return self;
}


- (id)init
{
	self = [super init];
	
	if (self)
	{
		[self setupBar];
	}
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	
	if (self)
	{
		[self setupBar];
	}
	
	return self;
}

- (void)setupBar
{
	UIView *container = [UIView new];
	container.userInteractionEnabled = YES;
	container.backgroundColor = LIGHT_BACKGROUND_COLOR;
	container.layer.cornerRadius = 5;
	container.layer.masksToBounds = YES;

	UITapGestureRecognizer *single = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchButtonTapped)];
	single.numberOfTapsRequired = 1;
	[container addGestureRecognizer:single];
	
	self.textField = [GTTextField new];
	self.textField.translatesAutoresizingMaskIntoConstraints = YES;
	self.textField.placeholder = @"Search for a town or suburb";
	self.textField.delegate = self;
	self.textField.userInteractionEnabled = NO;

	UIImage *searchImage = [UIImage imageNamed:@"search_icon"];
	
	UIImageView	*search = [[UIImageView	alloc] initWithImage:searchImage];
	search.userInteractionEnabled = YES;
	search.contentMode = UIViewContentModeCenter;
	
	[container addSubview:search];
	[container addSubview:self.textField];
	
	[self addSubview:container];
	
	self.textField.frame = CGRectMake(10+searchImage.size.width+10, 4, SCREEN_WIDTH-searchImage.size.width-60, 36);
	search.frame = CGRectMake(10, 0, searchImage.size.width, 40);
	container.frame = CGRectMake(15, 10, 10+search.frame.size.width+10+self.textField.frame.size.width+10, 40);
	
	self.backgroundColor = [UIColor whiteColor];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	[self.delegate searchTextEntered:(NSString*)textField.text];
	
	return YES;
}

- (void)searchCancelled
{
	
}

- (void)searchButtonTapped
{
	[self.delegate searchButtonTapped];
}

@end
