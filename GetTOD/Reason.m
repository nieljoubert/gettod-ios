//
//  Reason.m
//  getTOD
//
//  Created by Niel Joubert on 2015/05/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Reason.h"

@implementation Reason

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.reasonId = [decoder decodeObjectForKey:@"reasonId"];
		self.reasonDescription = [decoder decodeObjectForKey:@"reasonDescription"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.reasonId forKey:@"reasonId"];
	[encoder encodeObject:self.reasonDescription forKey:@"reasonDescription"];
}

@end
