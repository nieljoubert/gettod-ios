//
//  LoginTextFieldView.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "LoginTextFieldView.h"

@implementation LoginTextFieldView

-(id)initWithTexField:(GTTextField*)textField withPlaceholder:(NSString*)placeholder hasLockIcon:(BOOL)hasLock
{
	self = [super init];
	
	if (self)
	{
		[self setupViewWithTextField:textField withPlaceholder:placeholder hasLockIcon:hasLock];
	}
	
	return self;
}

-(void)setupViewWithTextField:(GTTextField*)textField withPlaceholder:(NSString*)placeholder hasLockIcon:(BOOL)hasLock
{
	self.translatesAutoresizingMaskIntoConstraints = NO;
	self.textField = textField;
	
	self.layer.cornerRadius = 5;
	self.layer.masksToBounds = YES;
	self.backgroundColor = LOGIN_TEXT_BACKGROUND_COLOR;
	
	UIImageView *image = hasLock ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock_with_pipe"]] : [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email_icon_with_pipe"]];
	image.translatesAutoresizingMaskIntoConstraints = NO;
	image.contentMode = UIViewContentModeScaleAspectFit;
	[self addSubview:image];
	
	textField.placeholder = placeholder;
	
	if (hasLock)
	{
		textField.keyboardType = UIKeyboardTypeDefault;
		textField.secureTextEntry = YES;
	}
	else
	{
		textField.keyboardType = UIKeyboardTypeEmailAddress;
	}
	
	[self addSubview:textField];
	
	NSDictionary *views = @{@"image":image,
							@"text":textField};
	
	NSDictionary *metrics = @{@"imageHeight":[NSNumber numberWithFloat:image.frame.size.height],
							  @"imageWidth":[NSNumber numberWithFloat:image.frame.size.width],
							  @"textHeight":[NSNumber numberWithFloat:textField.frame.size.height]};
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-17-[image(==imageWidth)]-16-[text]-|"
															   options:0
															   metrics:metrics
																  views:views]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[image]-1-|"
																 options:0
																 metrics:metrics
																   views:views]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[text]-1-|"
																 options:0
																 metrics:nil
																   views:views]];
}


- (void)setError
{
	self.backgroundColor = ERROR_BACKGROUND_COLOR;
}

- (void)dismissError
{
	self.backgroundColor = LOGIN_TEXT_BACKGROUND_COLOR;
}

@end
