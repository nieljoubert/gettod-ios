//
//  MapSearchModalViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "MapSearchModalViewController.h"
#import "Constants.h"
#import "UIKit+AFNetworking.h"
#import "SearchResultCell.h"
#import "GTSearchBarView.h"
#import "LocationManager.h"

@interface MapSearchModalViewController ()

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) LPGoogleFunctions *googleFunctions;
@property (nonatomic, strong) NSMutableArray *placesList;

@property (nonatomic, strong) NSString *currentText;
@property (weak, nonatomic) IBOutlet UIView *initialOverlay;

@property (strong, nonatomic) IBOutlet UILabel *setLocationLabel;

@end

@implementation MapSearchModalViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self.tableView registerClass:[SearchResultCell class] forCellReuseIdentifier:@"SearchResultCell"];
	
	[[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:DARK_TEXT_COLOR];
	[[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:LIGHT_BACKGROUND_COLOR];
	[[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:FONT_LIGHT size:14]];
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];
	
	self.setLocationLabel.numberOfLines = 0;
	self.setLocationLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Set the location where you require assistance to locate nearby suppliers" attributes:attributes];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self.searchBar becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.placesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	LPPlaceDetails *placeDetails = (LPPlaceDetails *)[self.placesList objectAtIndex:indexPath.row];
	
	SearchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchResultCell"];
	
	[cell setupCellWithPlaceDetails:placeDetails];
	
	return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([self.searchDisplayController isActive])
	{
		[self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
	}
	else
	{
		LPPlaceDetails *details = [self.placesList objectAtIndex:indexPath.row];
		
		[self.delegate locationSelected:details];
		
		[self.tableView deselectRowAtIndexPath:indexPath animated:NO];
		
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 58;
}

#pragma mark - LPGoogleFunctions

- (LPGoogleFunctions *)googleFunctions
{
	if (!_googleFunctions) {
		_googleFunctions = [LPGoogleFunctions new];
		_googleFunctions.googleAPIBrowserKey = GOOGLE_MAPS_BROWSER_API_KEY;
		_googleFunctions.delegate = self;
		_googleFunctions.sensor = YES;
		_googleFunctions.languageCode = @"en";
	}
	return _googleFunctions;
}

- (void)loadPlacesAutocompleteForInput
{
	self.initialOverlay.hidden = YES;
	
	self.searchDisplayController.searchBar.text = self.currentText;
	
	//	CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:-33 longitude:18];
	
	
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		[self.googleFunctions loadPlacesAutocompleteWithDetailsForInput:self.currentText
																 offset:(int)[self.currentText length]
																 radius:200000
															   location:[LPLocation locationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude]
															  placeType:LPGooglePlaceTypeGeocode
													 countryRestriction:nil
														successfulBlock:^(NSArray *placesWithDetails) {
															NSLog(@"successful");
															
															self.placesList = [NSMutableArray arrayWithArray:placesWithDetails];
															
															if ([self.searchDisplayController isActive])
															{
																[self.searchDisplayController.searchResultsTableView reloadData];
															}
															else
															{
																[self.tableView reloadData];
															}
															
														} failureBlock:^(LPGoogleStatus status) {
															
															NSLog(@"Error - %@", [LPGoogleFunctions getGoogleStatus:status]);
															
															self.placesList = [NSMutableArray new];
															
															if ([self.searchDisplayController isActive])
															{
																[self.searchDisplayController.searchResultsTableView reloadData];
															}
															else
															{
																[self.tableView reloadData];
															}
														}];
	}];
}

#pragma mark - LPGoogleFunctions Delegate

- (void)googleFunctionsWillLoadPlacesAutocomplate:(LPGoogleFunctions *)googleFunctions forInput:(NSString *)input
{
	NSLog(@"willLoadPlacesAutcompleteForInput: %@", input);
}

- (void)googleFunctions:(LPGoogleFunctions *)googleFunctions didLoadPlacesAutocomplate:(LPPlacesAutocomplete *)placesAutocomplate
{
	NSLog(@"didLoadPlacesAutocomplete - Delegate");
}

- (void)googleFunctions:(LPGoogleFunctions *)googleFunctions errorLoadingPlacesAutocomplateWithStatus:(LPGoogleStatus)status
{
	NSLog(@"errorLoadingPlacesAutocomplateWithStatus - Delegate: %@", [LPGoogleFunctions getGoogleStatus:status]);
}

#pragma mark - Search Controller
- (void)searchTextEntered:(NSString*)searchText
{
	self.currentText = searchText;
	
	NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
	NSString *trimmedString = [searchText stringByTrimmingCharactersInSet:charSet];
	
	if (![trimmedString isEqualToString:@""])
	{
		[[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadPlacesAutocompleteForInput) object:nil];
		NSLog(@"Cancel");
		[self performSelector:@selector(loadPlacesAutocompleteForInput) withObject:nil afterDelay:1];
	}
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	self.currentText = searchText;

	NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
	NSString *trimmedString = [searchText stringByTrimmingCharactersInSet:charSet];
	
	if (![trimmedString isEqualToString:@""])
	{
		[[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadPlacesAutocompleteForInput) object:nil];
		NSLog(@"Cancel");
		[self performSelector:@selector(loadPlacesAutocompleteForInput) withObject:nil afterDelay:1];
	}
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end