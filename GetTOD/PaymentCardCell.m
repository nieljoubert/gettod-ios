//
//  PaymentCardCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "PaymentCardCell.h"

@interface PaymentCardCell()

@property (nonatomic,strong) UIImageView *dots;
@property (nonatomic,strong) UIImageView *cardImage;
@property (nonatomic,strong) UIImageView *divider;
@property (nonatomic,strong) UIImageView *downArrow;

@end

@implementation PaymentCardCell

- (id)init
{
	self = [super init];
	
	if (self)
	{
	}
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	
	if (self)
	{
	}
	
	return self;
}

- (void)setupCellWithCard:(Card*)card forScreen:(BOOL)forScreen
{
	self.accessoryView = nil;
	self.accessoryType = UITableViewCellAccessoryNone;
	
	if (self.cardImage == nil)
	{
		self.cardImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"visa"]];
		self.cardImage.translatesAutoresizingMaskIntoConstraints = NO;
		self.cardImage.backgroundColor = [UIColor clearColor];
		self.cardImage.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.cardImage];
	}
	
	if (self.divider == nil)
	{
		self.divider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_pipe_card"]];
		self.divider.translatesAutoresizingMaskIntoConstraints = NO;
		self.divider.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.divider];
	}
	
	if (self.nameLabel == nil)
	{
		self.nameLabel = [UILabel new];
		self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.nameLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.nameLabel.backgroundColor = [UIColor clearColor];
//		self.nameLabel.adjustsFontSizeToFitWidth = YES;
		[self.contentView addSubview:self.nameLabel];
	}
	
	if (self.cardNumber == nil)
	{
		self.cardNumber = [UILabel new];
		self.cardNumber.translatesAutoresizingMaskIntoConstraints = NO;
		self.cardNumber.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.cardNumber.backgroundColor = [UIColor clearColor];
		[self.contentView addSubview:self.cardNumber];
	}
	
	if (self.dots == nil)
	{
		self.dots = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_dots"]];
		self.dots.translatesAutoresizingMaskIntoConstraints = NO;
		self.dots.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.dots];
	}
	
	
	NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"image":self.cardImage,
																				 @"div":self.divider,
																				 @"name":self.nameLabel,
																				 @"number":self.cardNumber,
																				 @"dots":self.dots}];
	
	
	NSMutableDictionary *metrics = [NSMutableDictionary dictionaryWithDictionary:@{@"imageW":[NSNumber numberWithFloat:self.cardImage.frame.size.width],
																				   @"divW":[NSNumber numberWithFloat:self.divider.frame.size.width],
																				   @"dotsW":[NSNumber numberWithFloat:self.dots.frame.size.width]}];

	if (forScreen)
	{
		self.nameLabel.font = [UIFont fontWithName:FONT size:16];
		self.cardNumber.font = [UIFont fontWithName:FONT size:16];
		self.dots.image = [UIImage imageNamed:@"dots4"];
		
		if (self.downArrow == nil)
		{
			self.downArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down"]];
			self.downArrow.translatesAutoresizingMaskIntoConstraints = NO;
			self.downArrow.backgroundColor = [UIColor clearColor];
			self.downArrow.contentMode = UIViewContentModeScaleAspectFit;
			[self.contentView addSubview:self.downArrow];
		}
		
		[views addEntriesFromDictionary:@{@"arrow":self.downArrow}];
		[metrics addEntriesFromDictionary:@{@"arrowW":[NSNumber numberWithFloat:self.downArrow.frame.size.width]}];
	}
	
	self.nameLabel.text = card.name;
	self.cardNumber.text = card.lastDigits;
	[self getCardLogo:card.brand];

	
	if (forScreen)
	{
		if (card == nil)
		{
			self.nameLabel.text = @"Tap here to add card";
			self.dots.hidden = YES;
			self.cardNumber.text = @"";
		
			[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[image(==imageW)]-10-[div(==divW)]-10-[name(>=10)]-10-[dots(==dotsW)]-10-[number]-(>=5)-[arrow(==arrowW)]-10-|"
																					 options:0
																					 metrics:metrics
																					   views:views]];
		}
		else
		{
			self.nameLabel.textAlignment = NSTextAlignmentLeft;
			self.dots.hidden = NO;
//		}
			[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[image(==imageW)]-10-[div(==divW)]-10-[name(>=10)]-10-[dots(==dotsW)]-10-[number(==40)]-(>=5)-[arrow(==arrowW)]-10-|"
																				 options:0
																				 metrics:metrics
																				   views:views]];
		}
	
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[arrow]|"
																				 options:0
																				 metrics:metrics
																				   views:views]];
	}
	else
	{
			[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[image(==imageW)]-10-[div(==divW)]-10-[name(>=10)]-10-[dots(==dotsW)]-10-[number(==40)]-(>=10)-|"
																					 options:0
																					 metrics:metrics
																					   views:views]];
	}
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[div]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[name]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
//	if (card != nil)
//	{
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dots]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];

		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[number]|"
																				 options:0
																				 metrics:metrics
																				   views:views]];
//	}
}

- (void)getCardLogo:(NSString*)brand
{
	if ([brand isEqualToString:@"VISA"])
	{
		self.cardImage.image = [UIImage imageNamed:@"visa"];
	}
	else if ([brand isEqualToString:@"MASTERCARD"])
	{
		self.cardImage.image = [UIImage imageNamed:@"mastercard"];
	}
	else
	{
		self.cardImage.image = [UIImage imageNamed:@"plain_card"];
	}
}

@end
