//
//  Service.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Service : NSObject

@property (nonatomic, strong) NSNumber * serviceId;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * pictureActiveURL;
@property (nonatomic, strong) NSString * pictureInactiveURL;
@property (nonatomic, strong) NSString * highlightColor;

@end
