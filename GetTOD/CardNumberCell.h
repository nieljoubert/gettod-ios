//
//  CardNumberCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTextFieldCell.h"
#import "GTTextField.h"

@interface CardNumberCell : GTTextFieldCell <UITextFieldDelegate>

@property (nonatomic, strong) GTTextField *cardNumber;

- (void)setupCellWithCardNumber:(NSString*)number;
- (void)getCardLogo;
- (void)reformatAsCardNumber:(GTTextField *)textField;

@end
