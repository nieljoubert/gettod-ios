//
//  CircularLock.h
//  CirucularLock
//
//  Created by Cem Olcay on 10/06/14.
//  Copyright (c) 2014 studionord. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^animantionCompleted)(void);

@interface ButtonCountDownAnimatedView : UIView 

@property (nonatomic, strong) UIView *innerView;
@property (nonatomic, strong) CAShapeLayer *strokeLayer;
@property (nonatomic, strong) CAShapeLayer *circleLayer;

@property (nonatomic, assign) BOOL clockwise;
@property (nonatomic, strong) UIColor *strokeColor;

@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, assign) CGFloat duration;
@property (nonatomic, assign) CGFloat strokeWidth;

@property (copy) animantionCompleted completed;

- (instancetype)initWithInnerView:(UIView *)innerView
						 duration:(CGFloat)d
					  strokeWidth:(CGFloat)width
					  strokeColor:(UIColor *)strokeColor
						clockwise:(BOOL)clockwise
			  animantionCompleted:(animantionCompleted)animationCompleted;

@end
