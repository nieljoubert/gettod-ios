//
//  NavigationDirectionsViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//
#import "RootViewController.h"
#import "NavigationDistanceTimeView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LPGoogleFunctions.h"

@interface NavigationDirectionsViewController : RootViewController <GMSMapViewDelegate, NavigationDistanceTimeViewDelegate>

@property (weak, nonatomic) IBOutlet NavigationDistanceTimeView *distanceTimeView;

@property (nonatomic, assign) CLLocationCoordinate2D supplierLocationCoordinates;
@property (nonatomic, strong) LPDirections *directions;

@end
