//
//  ChangePasswordViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/17.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@property (nonatomic, strong) GTTextField *currentPassword;
@property (nonatomic, strong) GTTextField *passwordNew;
@property (nonatomic, strong) GTTextField *confirmPassword;

@property (nonatomic, strong) GTTextFieldCell *currentPasswordCell;
@property (nonatomic, strong) GTTextFieldCell *passwordNewCell;
@property (nonatomic, strong) GTTextFieldCell *confirmPasswordCell;
@end

@implementation ChangePasswordViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
	
	self.title = @"Change Password";
	
	self.currentPassword = [GTTextField new];
	self.currentPassword.autocapitalizationType = UITextAutocapitalizationTypeNone;
	[self.currentPassword setKeyboardToolbar:self.keyboardToolbar];
	self.passwordNew = [GTTextField new];
	self.passwordNew.autocapitalizationType = UITextAutocapitalizationTypeNone;
	[self.passwordNew setKeyboardToolbar:self.keyboardToolbar];
	self.confirmPassword = [GTTextField new];
	self.confirmPassword.autocapitalizationType = UITextAutocapitalizationTypeNone;
	[self.confirmPassword setKeyboardToolbar:self.keyboardToolbar];
	
	[self addFormField:self.currentPassword];
	[self addFormField:self.passwordNew];
	[self addFormField:self.confirmPassword];
}

#pragma mark - Private -


#pragma mark - Validation -

- (BOOL)isScreenFieldsValid
{
	[super isScreenFieldsValid];
	
	BOOL isValid = YES;
	
	isValid =  [self isCurrentPasswordValid] && isValid;
	isValid =  [self isNewPasswordValid] && isValid;
	isValid =  [self isConfirmPasswordValid] && isValid;

	return isValid;
}


- (BOOL)isCurrentPasswordValid
{
	if (self.currentPassword.text.length >= 8)
	{
		[self.currentPasswordCell dismissError];
		[self dismissScreenError:self.currentPassword];
		return YES;
	}
	else
	{
		[self.currentPasswordCell setError];
		[self setScreenError:self.currentPassword message:@"Please enter your current password\n(8 or more characters)"];
		return NO;
	}
}

- (BOOL)isNewPasswordValid
{
	if (self.passwordNew.text.length >= 8)
	{
		[self.passwordNewCell dismissError];
		[self dismissScreenError:self.passwordNew];
		return YES;
	}
	else
	{
		[self.passwordNewCell setError];
		[self setScreenError:self.passwordNew message:@"Please enter a password with 8 or more characters"];
		return NO;
	}
}

- (BOOL)isConfirmPasswordValid
{
	if (![self.confirmPassword.text isEqualToString:self.passwordNew.text])
	{
		[self.confirmPasswordCell setError];
		[self setScreenError:self.confirmPassword message:@"The passwords do not match"];
		return NO;
	}
	else if (self.confirmPassword.text.length < 8)
	{
		[self.confirmPasswordCell setError];
		[self setScreenError:self.confirmPassword message:@"Please enter a password with 8 or more characters"];
		return NO;
	}
	else
	{
		[self.confirmPasswordCell dismissError];
		[self dismissScreenError:self.confirmPassword];
		return YES;
	}
}

#pragma mark - Textfield -
- (void)textFieldDidEndEditing:(UITextField *)textField
{
	GTTextField *field = (GTTextField*)textField;
	
	if (field == self.currentPassword)
	{
		[self isCurrentPasswordValid];
	}
	else if (field == self.passwordNew)
	{
		[self isNewPasswordValid];
		[self isConfirmPasswordValid];
	}
	else if (field == self.confirmPassword)
	{
		[self isConfirmPasswordValid];
	}
}

#pragma mark - Actions -
- (void)save
{
	[self.currentField resignFirstResponder];
	
	if ([self isScreenFieldsValid])
	{
		[[APIManager manager] changeCurrentPassword:self.currentPassword.text
									  toNewPassword:self.passwordNew.text
											success:^(id result) {
												
												[self.navigationController popViewControllerAnimated:YES];
												
											} failure:^(NSError *error) {
												
												
											}];

	}
	else
	{
		[self showScreenError];
	}
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
	{
		return 1;
	}
	else
	{
		return 2;
	}
	
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		if (indexPath.row == 0)
		{
			GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
			self.currentPasswordCell = cell;
			
			self.currentPasswordCell.textField = self.currentPassword;
			[self.currentPasswordCell setupCellWithPlaceholder:@"Current password"];
			self.currentPassword.delegate = self;
			self.currentPassword.secureTextEntry = YES;
			
			return self.currentPasswordCell;
		}
	}
	else
	{
		if (indexPath.row == 0)
		{
			GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
			self.passwordNewCell = cell;
			
			self.passwordNewCell.textField = self.passwordNew;
			[self.passwordNewCell setupCellWithPlaceholder:@"New password"];
			self.passwordNew.delegate = self;
			self.passwordNew.secureTextEntry = YES;
			
			return self.passwordNewCell;
		}
		else if (indexPath.row == 1)
		{
			GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
			self.confirmPasswordCell = cell;
			
			self.confirmPasswordCell.textField = self.confirmPassword;
			[self.confirmPasswordCell setupCellWithPlaceholder:@"Confirm new password"];
			self.confirmPassword.delegate = self;
			self.confirmPassword.secureTextEntry = YES;
			
			return self.confirmPasswordCell;
		}
	}
	
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"CURRENT PASSWORD"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"NEW PASSWORD"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

@end
