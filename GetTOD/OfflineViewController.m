//
//  OfflineViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "OfflineViewController.h"
#import "LocationManager.h"
#import "GTButton.h"

@interface OfflineViewController ()

@property (weak, nonatomic) IBOutlet GTButton *goOnlineButton;
@property (weak, nonatomic) IBOutlet UIView *whiteView;

@end

@implementation OfflineViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor blackColor];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (IBAction)goOnline:(id)sender
{
	[SettingsManager manager].supplierStatus = SupplierStatusOnline;
	[SettingsManager save];
	
	[self showLoader];
	
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		[self hideLoader];
		[self.navigationController popViewControllerAnimated:NO];
	}];
}

@end
