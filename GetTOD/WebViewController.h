//
//  WebViewController.h
//  
//
//  Created by Niel Joubert on 2015/09/20.
//
//

#import "RootViewController.h"

@interface WebViewController : RootViewController
@property (strong, nonatomic) NSURLRequest *urlRequestToLoad;
@end
