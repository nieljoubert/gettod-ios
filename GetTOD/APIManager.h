//
//  APIManager.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "API.h"
#import "Constants.h"
#import <CoreLocation/CoreLocation.h>
#import "Card.h"

@interface APIManager : API

+ (APIManager*)manager;

- (void)getProvinces:(SuccessArrayHandler)success failure:(FailureHandler)failure;
- (void)getCompanies:(SuccessArrayHandler)success failure:(FailureHandler)failure;
- (void)getServices:(SuccessArrayHandler)success failure:(FailureHandler)failure;
- (void)getEntities:(SuccessArrayHandler)success failure:(FailureHandler) failure;
- (void)getCities:(SuccessArrayHandler)success failure:(FailureHandler) failure;

// Authenticated Calls

- (void)loginWithUsername:(NSString*)username
			  andPassword:(NSString*)password
				  success:(SuccessHandler)success
				  failure:(FailureHandler)failure;

- (void)registerWithUsername:(NSString*)username
					   email:(NSString*)email
				 andPassword:(NSString*)password
					 success:(SuccessHandler)success
					 failure:(FailureHandler)failure;

- (void)logout:(SuccessTrueFalseHandler)success
	   failure:(FailureHandler)failure;

- (void)activateWithUID:(NSString*)Uid
			   andToken:(NSString*)token
				success:(SuccessHandler)success
				failure:(FailureHandler)failure;

- (void)getMe:(SuccessHandler)success failure:(FailureHandler)failure;
- (void)putMe:(SuccessHandler)success failure:(FailureHandler)failure;

- (void)changeUsername:(NSString*)username
		  withPassword:(NSString*)password
			   success:(SuccessHandler)success
			   failure:(FailureHandler)failure;

- (void)changeCurrentPassword:(NSString*)currentPassword
				toNewPassword:(NSString*)newPassword
					  success:(SuccessHandler)success
					  failure:(FailureHandler)failure;

- (void)resetPasswordWithEmail:(NSString*)email
					   success:(SuccessHandler)success
					   failure:(FailureHandler)failure;

- (void)confirmPasswordResetWithUID:(NSString*)Uid
						   andToken:(NSString*)token
					 andNewPassword:(NSString*)newPassword
							success:(SuccessHandler)success
							failure:(FailureHandler)failure;

- (void)registerForPushNotifications:(SuccessTrueFalseHandler)success
							 failure:(FailureHandler)failure;

- (void)updateUserDetails:(NSString*)name
				  surname:(NSString*)surname
		   profilePicture:(UIImage*)profileImage
		   andPhoneNumber:(NSString*)phonenumber
				  success:(SuccessHandler)success
				  failure:(FailureHandler)failure;

- (void)getUserDetailsWithSuccess:(SuccessHandler)success
						  failure:(FailureHandler)failure;

- (void)getJobForID:(NSNumber*)jobId
			success:(SuccessJobHandler)success
			failure:(FailureHandler)failure;

- (void)performPaymentForJobID:(NSNumber*)jobId
				withCardNumber:(NSString*)number
						   cvv:(NSString*)cvv
				   expiryMonth:(NSString*)month
					expiryYear:(NSString*)year
					   success:(SuccessTrueFalseWithResultHandler)success
					   failure:(FailureHandler)failure;

- (void)registerCreditCard:(Card*)card
				   success:(SuccessHandler)success
				   failure:(FailureHandler)failure;

- (void)deleteCreditCard:(Card*)card
				 success:(SuccessHandler)success
				 failure:(FailureHandler)failure;

- (void)getCreditCards:(SuccessArrayHandler)success
			   failure:(FailureHandler)failure;

- (void)reportProblemForJob:(NSNumber*)jobId
				 andProblem:(NSString*)problem
					success:(SuccessTrueFalseHandler)success
					failure:(FailureHandler)failure;

- (void)referFriendWithEmail:(NSString*)email
					 success:(SuccessHandler)success
					 failure:(FailureHandler)failure;

- (void)getTaskHistory:(SuccessArrayHandler)success
			   failure:(FailureHandler)failure;

// Supplier calls
- (void)getNearbySuppliersFromLocation:(CLLocationCoordinate2D)coordinates
							   andType:(NSNumber*)type
							   success:(SuccessArrayHandler)success
							   failure:(FailureHandler)failure;

- (void)getSupplierSummaryForID:(NSNumber*)supplierID
						success:(SuccessSupplierHandler)success
						failure:(FailureHandler)failure;

- (void)getSupplierLocation:(NSNumber*)supplierID
                    success:(SuccessHandler)success
                    failure:(FailureHandler)failure;

- (void)requestSupplier:(NSNumber*)supplierID
                problem:(Problem*)problem
                isOther:(BOOL)isOther
		 jobCoordinates:(CLLocationCoordinate2D)coordinates
 additionalInstructions:(NSString*)instructions
			  cardToken:(NSString*)cardToken
			 travelTime:(NSString*)time
		 travelDistance:(NSString*)distance
				success:(SuccessJobHandler)success
				failure:(FailureHandler)failure;

- (void)rateSupplierForJob:(NSNumber*)jobID
				 andRating:(NSNumber*)rating
					  success:(SuccessTrueFalseHandler)success
					  failure:(FailureHandler)failure;

- (void)cancelJob:(NSNumber*)jobID
		 withType:(JobCancelType)type
		  success:(SuccessTrueFalseHandler)success
		  failure:(FailureHandler)failure;

- (void)getQuoteForID:(NSNumber*)quoteId
			  success:(SuccessQuoteHandler)success
			  failure:(FailureHandler)failure;

- (void)getQuoteRejectReasonsForServiceID:(NSNumber*)serviceID
								  success:(SuccessArrayHandler)success
								  failure:(FailureHandler)failure;

- (void)acceptQuote:(NSNumber*)quoteID
			success:(SuccessTrueFalseHandler)success
			failure:(FailureHandler)failure;

- (void)rejectQuote:(NSNumber*)quoteID
	   withReasonId:(NSNumber*)reasonID
			success:(SuccessTrueFalseHandler)success
			failure:(FailureHandler)failure;

- (void)setReminderForServiceType:(NSNumber*)serviceID
					 fromLocation:(CLLocationCoordinate2D)coordinates
						  success:(SuccessTrueFalseHandler)success
						  failure:(FailureHandler)failure;


// Supplier Side Calls
- (void)acceptJob:(NSNumber*)jobID
		  success:(SuccessTrueFalseHandler)success
		  failure:(FailureHandler)failure;

- (void)supplierArrivedForJob:(NSNumber*)jobID
					  success:(SuccessHandler)success
					  failure:(FailureHandler)failure;

- (void)setJobCompleted:(NSNumber*)jobID
				success:(SuccessTrueFalseHandler)success
				failure:(FailureHandler)failure;

- (void)createQuoteWithJobID:(NSNumber*)jobID
					  labour:(NSNumber*)labour
				 description:(NSString*)quoteDescription
			 listOfMaterials:(NSArray*)materials
					 success:(SuccessHandler)success
					 failure:(FailureHandler)failure;

- (void)setSupplierLocationCoordinates:(CLLocationCoordinate2D)coordinates
                               bearing:(float)bearing
                             andStatus:(SupplierStatus)status
                               success:(SuccessHandler)success
                               failure:(FailureHandler)failure;

- (void)rateUserForJob:(NSNumber*)jobID
			 andRating:(NSNumber*)rating
			   success:(SuccessTrueFalseHandler)success
			   failure:(FailureHandler)failure;

@end
