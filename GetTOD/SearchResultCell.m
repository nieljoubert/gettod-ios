//
//  SearchResultCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SearchResultCell.h"

@interface SearchResultCell ()

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *subTitle;
@property (nonatomic, strong) UIImageView *marker;

@property (nonatomic, strong) UIView *texts;

@end

@implementation SearchResultCell

- (void)setupCellWithPlaceDetails:(LPPlaceDetails*)placeDetails
{
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.accessoryType = UITableViewCellAccessoryNone;
	
	NSArray *components = placeDetails.addressComponents;
	
	NSString *town = @"";
	NSString *province = @"";
	NSString *country = @"";
	
	for (LPAddressComponent* com in components)
	{
		NSLog(@"Com : %@",com.longName);
		for (NSString *type in com.types)
		{
			NSLog(@"Type : %@",type);
			
			if ([type isEqualToString:@"sublocality"]) // Millerton
			{
				town = com.longName;
			}
			if ([type isEqualToString:@"locality"]) // Cape Town
			{
				//				town = com.longName;
			}
			if ([type isEqualToString:@"administrative_area_level_1"]) // Western Cape
			{
				province = com.longName;
			}
			if ([type isEqualToString:@"country"]) // South Africa
			{
				country = com.longName;
			}
		}
	}
	
	if (self.title == nil)
	{
		self.title = [UILabel new];
		self.title.translatesAutoresizingMaskIntoConstraints = NO;
		self.title.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.title.textColor = DARK_TEXT_COLOR;
		self.title.lineBreakMode = NSLineBreakByTruncatingTail;
	}
	
	if (self.subTitle == nil)
	{
		self.subTitle = [UILabel new];
		self.subTitle.translatesAutoresizingMaskIntoConstraints = NO;
		self.subTitle.font = [UIFont fontWithName:FONT size:12];
		self.subTitle.textColor = GRAY_COLOR;
		self.subTitle.lineBreakMode = NSLineBreakByTruncatingTail;
	}
	
	if (self.texts == nil)
	{
		self.texts = [UIView new];
		self.texts.translatesAutoresizingMaskIntoConstraints = NO;
		[self.texts addSubview:self.title];
		[self.texts addSubview:self.subTitle];
		[self.contentView addSubview:self.texts];
	}
	if (self.marker == nil)
	{
		self.marker = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_marker_gray"]];
		self.marker.translatesAutoresizingMaskIntoConstraints = NO;
		self.marker.contentMode = UIViewContentModeCenter;
		[self.contentView addSubview:self.marker];
	}
	
	self.title.text = placeDetails.name;
	self.subTitle.text = @"";
	
	if (town.length > 0)
	{
		self.subTitle.text = [self.subTitle.text stringByAppendingString:town];
		
		if (province.length > 0)
		{
			self.subTitle.text = [self.subTitle.text stringByAppendingString:[NSString stringWithFormat:@", %@",province]];
		}
	}
	else
	{
		if (province.length > 0)
		{
			self.subTitle.text = [self.subTitle.text stringByAppendingString:province];
		}
	}
	
	NSDictionary *views = @{@"title":self.title, @"marker":self.marker, @"subTitle":self.subTitle, @"texts":self.texts};
	
	
	[self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[title(==subTitle)]|"
																	   options:0
																	   metrics:nil
																		 views:views]];
	
	[self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subTitle(==title)]|"
																	   options:0
																	   metrics:nil
																		 views:views]];
	
	[self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[title][subTitle]-10-|"
																	   options:0
																	   metrics:nil
																		 views:views]];
	
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[marker(==markerW)]-(padding)-[texts]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING, @"markerW":[NSNumber numberWithFloat:self.marker.frame.size.width]}
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[marker(==texts)]|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING, @"markerW":[NSNumber numberWithFloat:self.marker.frame.size.width]}
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[texts]|"
																			 options:0
																			 metrics:nil
																			   views:views]];
}


@end
