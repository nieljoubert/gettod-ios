//
//  GTTextField.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTextField.h"
#import "AlertviewManager.h"

@interface GTTextField ()

@property (nonatomic, assign) BOOL errorVisisble;
@property (nonatomic, strong) UIButton *errorButton;

@end


@implementation GTTextField

-(id)init
{
	self = [super init];
	
	if (self)
	{
		self.font = [UIFont fontWithName:FONT size:16];
		self.backgroundColor = [UIColor clearColor];
		self.translatesAutoresizingMaskIntoConstraints = NO;
		self.autocorrectionType = UITextAutocorrectionTypeNo;
		self.autocapitalizationType = UITextAutocapitalizationTypeWords;
		[self dismissError];
	}
	
	return self;
}

- (void)setupErrorIcon
{
	UIImage *errorImage = [UIImage imageNamed:@"error"];
	
	self.errorButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 17, 15)];
	self.errorButton.center = CGPointMake(self.errorButton.center.x, self.center.y);
	
	[self.errorButton setImage:errorImage forState:UIControlStateNormal];
	[self.errorButton setImage:errorImage forState:UIControlStateSelected];
	[self.errorButton setImage:errorImage forState:UIControlStateHighlighted];
	
	[self.errorButton addTarget:self action:@selector(handleErrorTap) forControlEvents:UIControlEventTouchUpInside];
	[self setRightViewMode:UITextFieldViewModeNever];
	
	self.rightView = self.errorButton;
}

- (void)setErrorTo:(NSString*)text
{
	self.errorTextMessage = text;
	self.textColor = ERROR_TEXT_COLOR;
	[self setupErrorIcon];
	[self setRightViewMode:UITextFieldViewModeAlways];
}

- (void) handleErrorTap
{
	[self showErrorAlert:nil];
}

- (void) showErrorAlert:(AlertViewManagerCompletionBlock) completionBlock
{
	if (self.errorTextMessage)
	{
		[AlertViewManager showAlertWithTitle:@""
									 message:self.errorTextMessage
						   cancelButtonTitle:@"OK"
						   otherButtonTitles:nil
					   buttonSelectionBlock:completionBlock];
	}
}

- (void)dismissError
{
	self.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 17, 15)];
	[self setRightViewMode:UITextFieldViewModeAlways];
	self.textColor = DARK_TEXT_COLOR;
}

- (void)setKeyboardToolbar:(UIToolbar*)toolbar
{
	self.inputAccessoryView = toolbar;
	self.inputAccessoryView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
}

@end
