//
//  ParentMapViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseMapViewController.h"
#import "LocationManager.h"

@interface BaseMapViewController ()

@property (nonatomic, strong) GMSGeocoder *geocoder;
@property (nonatomic, assign) CLLocationCoordinate2D coordinateToSendBack;

@end

@implementation BaseMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.geocoder = [GMSGeocoder geocoder];
	
	[self setupInitialMap];
	
	self.firstRunDone = NO;
	
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		if (success)
		{
			self.currentLocation = location;
			[self setupInitialMap];
		}
		else
		{
			self.mapView = [GMSMapView mapWithFrame:self.view.bounds camera:nil];
			self.mapView.backgroundColor = [UIColor clearColor];
			self.mapView.myLocationEnabled = YES;
			self.mapView.mapType = kGMSTypeNormal;
			self.mapView.accessibilityElementsHidden = NO;
			self.mapView.settings.tiltGestures = YES;
			self.mapView.settings.scrollGestures = YES;
			self.mapView.settings.rotateGestures = YES;
			self.mapView.settings.zoomGestures = YES;
			self.mapView.settings.compassButton = NO;
			self.mapView.settings.myLocationButton = YES;
			self.mapView.delegate = self;
			
			[self addMarkerToLocation:location.coordinate];
			
			[self.delegate mapViewFinishedLoading];
		}
	}];
}

-(void)locationControllerDidUpdateLocation:(CLLocation *)location
{
	self.currentLocation = location;
}

-(void) viewWillDisappear:(BOOL) animated
{
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController])
    {
        if (self.navigationController)
        {
            self.navigationController.delegate = nil;
        }
    }
    
    [self dismissKeyboard];
}

- (void)setupInitialMap
{
	[self getAddressAndUpdateMarker:NO];
	[self.delegate updatedLocation:self.currentLocation];
	
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.currentLocation.coordinate.latitude
															longitude:self.currentLocation.coordinate.longitude
																 zoom:16];
	
	self.mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
	self.mapView.backgroundColor = [UIColor clearColor];
	self.mapView.camera = camera;
	self.mapView.myLocationEnabled = YES;
	self.mapView.mapType = kGMSTypeNormal;
	self.mapView.accessibilityElementsHidden = NO;
	self.mapView.settings.tiltGestures = YES;
	self.mapView.settings.scrollGestures = YES;
	self.mapView.settings.rotateGestures = YES;
	self.mapView.settings.zoomGestures = YES;
	self.mapView.settings.compassButton = NO;
	self.mapView.settings.myLocationButton = YES;
	self.mapView.delegate = self;
	
	[self addMarkerToLocation:self.currentLocation.coordinate];
	
	[self.delegate mapViewFinishedLoading];
}

#pragma mark - Private -
- (void)addMarkerToLocation:(CLLocationCoordinate2D)coordinates
{
	if (self.marker == nil)
	{
		self.marker = [GMSMarker markerWithPosition:coordinates];
		self.marker.icon = [UIImage imageNamed:@"location_marker_gray_large"];
		self.marker.appearAnimation = kGMSMarkerAnimationPop;
		self.marker.flat = NO;
		self.marker.map = self.mapView;
		self.marker.draggable = YES;
		self.marker.map = self.mapView;
	}
	
	self.marker.position = coordinates;
	self.marker.map = self.mapView;

	self.coordinateToSendBack = coordinates;
}

- (void)getAddressAndUpdateMarker:(BOOL)updateMarker
{
	if (updateMarker)
	{
		[self.geocoder reverseGeocodeCoordinate:self.marker.position
							  completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
								  
								  if (response)
								  {
									  GMSAddress *address = response.firstResult;
									  
									  self.marker.title = address.lines[0];
									 
									  if (address.lines.count > 1)
									  {
										  self.marker.snippet = address.lines[1];
									  }
									  
									  [self.mapView setSelectedMarker:self.marker];
									  
									  self.currentAddress = address;
									  
									  [self addressSelected];
								  }
							  }];
	}
	else
	{
		[self.geocoder reverseGeocodeCoordinate:self.currentLocation.coordinate
							  completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
								  
								  if (response)
								  {
									  self.currentAddress = response.firstResult;
									  
									  [self addressSelected];
								  }
							  }];
	}
}

#pragma mark - Mapview Delegate -

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
	[self addMarkerToLocation:position.target];
	self.coordinateToSendBack = position.target;
}

-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
	if (!self.firstRunDone)
	{
		self.firstRunDone = YES;
	}
	else
	{
		[self getAddressAndUpdateMarker:YES];
	}
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
	NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
}

-(void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate
{
	NSLog(@"You long tapped at %f,%f", coordinate.latitude, coordinate.longitude);
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
	return NO;
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{

}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
	[self addressSelected];
}

-(void)addressSelected
{
	[self.delegate addressSelectedFromMap:self.currentAddress withCoordinate:self.coordinateToSendBack];
}

@end
