//
//  CreatingAccountBlackOverlayViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseBlackOverlayViewController.h"

@interface CreatingAccountBlackOverlayViewController : UIViewController //BaseBlackOverlayViewController

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) Card *card;

@end

@protocol CreatingAccountBlackOverlayDelegate <NSObject>
- (void)dismissViewController:(CreatingAccountBlackOverlayViewController*)controller success:(BOOL)success onCard:(BOOL)cardFailed;
@end
