//
//  City.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Province.h"

@interface City : NSObject

@property (nonatomic, strong) NSNumber *cityId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) Province *province;

@end
