//
//  Company.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Company : NSObject

@property (nonatomic, strong) NSNumber * companyId;

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * suburb;
@property (nonatomic, strong) NSString * postCode;
@property (nonatomic, strong) NSString * registeredName;
@property (nonatomic, strong) NSString * registrationNumber;
@property (nonatomic, strong) NSString * contactNumber;
@property (nonatomic, strong) NSString * vatNumber;
@property (nonatomic, assign) BOOL isVatRegistered;
@property (nonatomic, strong) NSNumber * city;
@property (nonatomic, strong) NSNumber * entity;

@end
