//
//  User.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *profilePictureURL;
@property (nonatomic, strong) NSNumber *rating;

@end
