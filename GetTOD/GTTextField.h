//
//  GTTextField.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AlertviewManager.h"

@interface GTTextField : UITextField <UITextFieldDelegate>

@property (nonatomic, strong) NSString *errorTextMessage;

- (void)dismissError;
- (void)showErrorAlert:(AlertViewManagerCompletionBlock) completionBlock;
- (void)setErrorTo:(NSString*)text;
- (void)setKeyboardToolbar:(UIToolbar*)toolbar;

@end
