//
//  SupplierEnRouteViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/05.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"
#import "BaseTableViewBlackOverlayViewController.h"
#import "CancelRequestBlackOverlayViewController.h"
#import "Job.h"

@interface SupplierEnRouteViewController : RootViewController <UITableViewDataSource, UITableViewDelegate, BlackTableViewDelegate,CancelRequestDelegate>

@end
