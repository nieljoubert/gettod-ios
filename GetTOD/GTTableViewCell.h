//
//  GTTableViewCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "UIImageView+AFNetworking.h"

@interface GTTableViewCell : UITableViewCell

@end
