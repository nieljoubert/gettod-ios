//
//  EditEmailAddressViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/17.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"

@interface EditEmailAddressViewController : BaseTableViewController

@end
