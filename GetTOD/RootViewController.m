//
//  RootViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"
#import "UIImage+ImageEffects.h"
#import "UIImage-Helpers.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[[UINavigationBar appearance] setBarTintColor:NAVBAR_COLOR];
	[self.navigationController.navigationBar setTranslucent:YES];
	
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_arrow"] style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];

	[[UIBarButtonItem appearance] setTintColor:PINK_COLOR];
	[[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT size:15], NSFontAttributeName, nil] forState:UIControlStateNormal];
	
	[[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_SEMI_BOLD size:17], NSFontAttributeName, nil]];
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	[self createToolbar];
	
	if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
	{
		self.edgesForExtendedLayout = UIRectEdgeNone;
	}
	
	self.navigationController.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideLoader) name:@"HideLoader" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoaderWithMessageFromDelegate:) name:@"ShowLoaderWithMessage" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) viewWillDisappear:(BOOL) animated
{
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController])
    {
        if (self.navigationController)
        {
            self.navigationController.delegate = nil;
        }
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self dismissKeyboard];
}

-(void) dealloc
{
    if (self.navigationController.delegate == self)
    {
        self.navigationController.delegate = nil;
    }
}

- (void)goBack
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)createToolbar
{
	NSMutableArray *barItems = [[NSMutableArray alloc] init];
	
	UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
	[doneBtn setTintColor:[UIColor whiteColor]];
	UIBarButtonItem *prevBtn = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(goToPreviousField)];
	[prevBtn setTintColor:[UIColor whiteColor]];
	UIBarButtonItem *nextBtn = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(goToNextField)];
	[nextBtn setTintColor:[UIColor whiteColor]];
	UIBarButtonItem *flexiSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
	
	[barItems addObject:prevBtn];
	[barItems addObject:nextBtn];
	[barItems addObject:flexiSpace];
	[barItems addObject:doneBtn];
	
	self.keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
	self.keyboardToolbar.barStyle = UIBarStyleBlackOpaque;
	self.keyboardToolbar.backgroundColor = [UIColor clearColor];
	self.keyboardToolbar.layer.opacity = 1;
	
	[self.keyboardToolbar setTranslucent:NO];
	[self.keyboardToolbar setBarTintColor:PINK_COLOR];
	
	[self.keyboardToolbar setItems:barItems animated:YES];
}

- (void)goToPreviousField
{
	NSInteger index = [self.fields indexOfObject:self.currentField];
	
	if (index != -1 && index > 0)
	{
		GTTextField* field = (GTTextField*)[self.fields objectAtIndex:index-1];
		
		[field becomeFirstResponder];
	}
}

- (void)goToNextField
{
	NSInteger index = [self.fields indexOfObject:self.currentField];
	
	if (index != -1 && index+1 < self.fields.count)
	{
		GTTextField* field = (GTTextField*)[self.fields objectAtIndex:index+1];
		
		[field becomeFirstResponder];
	}
}

- (void)done
{
	[self.currentField resignFirstResponder];
}

#pragma mark - TextField prev/next -
- (void)addFormField:(GTTextField*)formField
{
	if (self.fields == nil)
	{
		self.fields = [NSMutableArray array];
	}
	
	[self.fields addObject:formField];
}

- (void)removeField:(GTTextField*)field
{
	NSInteger index = [self.fields indexOfObject:field];
	
	[self.fields removeObjectAtIndex:index];
}

- (BOOL) isScreenFieldsValid
{
	self.errorField = nil;
	
	return NO;
}

- (void) setScreenError:(GTTextField*)field message:(NSString*)message
{
	if (field != nil)
	{
		[field setErrorTo:message];
		
		if (self.errorField == nil) //only set the first error field
		{
			self.errorField = field;
		}
	}
}

- (void) dismissScreenError:(GTTextField*) field
{
	if (field != nil)
	{
		[field dismissError];
		
		if (field == self.errorField)
		{
			self.errorField = nil;
		}
	}	
}

- (void) showScreenError
{
	if (self.errorField)
	{
		[self.errorField showErrorAlert:^(UIAlertView *alertView, NSInteger buttonIndex) {
			[self.errorField becomeFirstResponder];
		}];
	}
}

- (void)dismissKeyboard
{
	[[UIApplication sharedApplication].keyWindow endEditing:YES];
}

- (void) showLoader
{
	[self.navigationController.navigationBar setUserInteractionEnabled:NO];
	
	if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
	{
		self.navigationController.interactivePopGestureRecognizer.enabled = NO;
	}
	
	self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	self.HUD.opacity = 0.6;
	self.HUD.detailsLabelFont = [UIFont fontWithName:FONT_BOLD size:14];
	[self.HUD show:YES];
}

- (void) showLoaderWithMessage:(NSString*)message
{
	[self showLoader];
	self.HUD.detailsLabelText = message;
}

- (void) showLoaderWithMessageFromDelegate:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    
    self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.HUD.opacity = 0.6;
    self.HUD.detailsLabelFont = [UIFont fontWithName:FONT_BOLD size:14];
    self.HUD.detailsLabelText = [info objectForKey:@"message"];
    [self.HUD show:YES];
}

- (void) hideLoader
{
	[self.navigationController.navigationBar setUserInteractionEnabled:YES];
	
	if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
	{
		self.navigationController.interactivePopGestureRecognizer.enabled = YES;
	}
	
	[self.HUD hide:YES];
	self.HUD.detailsLabelText = nil;
}

- (UIImage*)getBlurredBackgroundImage
{
	UIImage *image = [UIImage screenshot];
	
	// jpeg quality image data
	float quality = .00001f;
	
	// intensity of blurred
	float blurred = 1.0f;
	
	NSData *imageData = UIImageJPEGRepresentation(image, quality);
	UIImage *blurredImage = [[UIImage imageWithData:imageData] blurredImage:blurred];
	
	return blurredImage;
}

@end
