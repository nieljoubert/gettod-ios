//
//  SettingsViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/17.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SettingsViewController.h"
#import "YourDetailsViewController.h"
#import "TaskHistoryViewController.h"
#import "AboutViewController.h"
#import "PaymentViewController.h"
#import "ImageHelper.h"
#import "LoginViewController.h"
#import "LocationManager.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableViewInXib;
@property (weak, nonatomic) IBOutlet UILabel *signOutLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewAccountLabel;

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    self.tableView = self.tableViewInXib;
    [super viewDidLoad];
    
    self.title = @"Settings";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(closeSettings)];
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    
    UITapGestureRecognizer *viewAccountTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(viewAccount)];
    viewAccountTap.numberOfTapsRequired = 1;
    [self.viewAccountLabel addGestureRecognizer:viewAccountTap];
    
    UITapGestureRecognizer *signOutTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(signOut)];
    signOutTap.numberOfTapsRequired = 1;
    [self.signOutLabel addGestureRecognizer:signOutTap];
    
    self.tableView.tableHeaderView = self.headerView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileImageDownloaded:) name:[NSString stringWithFormat:@"%@-ImageDownloaded",PROFILE_IMAGE_NAME] object:nil];
    
    if (![SettingsManager manager].gotUserDetailsOnce && ![SettingsManager manager].loggedInWithFB)
    {
        [[APIManager manager] getUserDetailsWithSuccess:^(id result) {
            
            [SettingsManager manager].gotUserDetailsOnce = YES;
            [SettingsManager save];
            
            if ([SettingsManager manager].profileImageURL)
            {
                __weak SettingsViewController *weakSelf = self;
                
                [self.profileImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[SettingsManager manager].profileImageURL]]
                                             placeholderImage:[UIImage imageNamed:@"default_profile_photo"]
                                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                          
                                                          weakSelf.profileImageView.image = image;
                                                          weakSelf.profileImageView.layer.cornerRadius = weakSelf.profileImageView.frame.size.width/2;
                                                          weakSelf.profileImageView.clipsToBounds = YES;
                                                          [ImageHelper saveImageToDisk:image withName:PROFILE_IMAGE_NAME];
                                                          
                                                      } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                          
                                                          
                                                      }];
            }
            
            self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",[SettingsManager manager].firstName,[SettingsManager manager].lastName];
            self.nameLabel.adjustsFontSizeToFitWidth = YES;
            
        } failure:^(NSError *error) {
            
            
        }];
    }
    else
    {
        [self setValues];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([SettingsManager manager].gotUserDetailsOnce)
    {
        [self setValues];
    }
}

#pragma mark - Private -

- (void)setValues
{
    self.profileImageView.image = [ImageHelper getImageFromDisk:PROFILE_IMAGE_NAME];
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
    self.profileImageView.clipsToBounds = YES;
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",[SettingsManager manager].firstName,[SettingsManager manager].lastName];
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)profileImageDownloaded:(NSNotification*)notification
{
    self.profileImageView.image = [notification object];
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
    self.profileImageView.clipsToBounds = YES;
}

#pragma mark - Actions -
- (void)closeSettings
{
    [self.delegate settingsDismissed:self];
}

- (void)viewAccount
{
    YourDetailsViewController *details = [[YourDetailsViewController alloc] initWithNibName:@"YourDetailsViewController" bundle:nil];
    details.isSettings = YES;
    [self.navigationController pushViewController:details animated:YES];
}

- (void)signOut
{
    [AlertViewManager showAlertWithTitle:@"Signing Out"
                                 message:@"Are you sure you want to sign out?"
                       cancelButtonTitle:@"No"
                       otherButtonTitles:@[@"Yes"]
                    buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        
                        if (buttonIndex == 0) // NO
                        {
                            
                        }
                        else // YES
                        {
                            [self showLoader];
                            
#ifdef CLIENT
                            [self logout];
#else
                            
                            [[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
                                
                                [[APIManager manager] setSupplierLocationCoordinates:location.coordinate
                                                                             bearing:[[SettingsManager manager].currentHeading floatValue]
                                                                           andStatus:SupplierStatusOffline
                                                                             success:^(id result) {
                                                                                 
                                                                                 [self logout];
                                                                                 
                                                                             } failure:^(NSError *error) {
                                                                             }];
                            }];
#endif
                        }
                    }];
}

- (void)logout
{
    [[APIManager manager] logout:^(BOOL success) {
        
        [self hideLoader];
        
        if (success)
        {
            [self dismissViewControllerAnimated:NO completion:nil];
            
            [SettingsManager clear];
            
            [self.navigationController popToRootViewControllerAnimated:NO];
            AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
            [appDelegate startAppWithLoginController];
        }
        else
        {
            [AlertViewManager showAlertWithTitle:@""
                                         message:@"There was a problem logging out. Please try again."
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil
                            buttonSelectionBlock:nil];
        }
        
    } failure:^(NSError *error) {
        [self hideLoader];
    }];
}

#pragma mark - TableView -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTBoldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTBoldTableViewCell"];
    
    if (indexPath.row == 0)
    {
        cell.textLabel.text = @"Task History";
    }
    else if (indexPath.row == 1)
    {
#ifdef CLIENT
        cell.textLabel.text = @"Payment";
#else
        cell.textLabel.text = @"About";
#endif
    }
    else if (indexPath.row == 2)
    {
        cell.textLabel.text = @"About";
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#ifdef CLIENT
    return 3;
#else
    return 2;
#endif
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        TaskHistoryViewController *taskHistory = [[TaskHistoryViewController alloc] init];
        [self.navigationController pushViewController:taskHistory animated:YES];
    }
    else if (indexPath.row == 1)
    {
#ifdef CLIENT
        PaymentViewController *payment = [[PaymentViewController alloc] init];
        [self.navigationController pushViewController:payment animated:YES];
#else
        AboutViewController *about = [[AboutViewController alloc] init];
        [self.navigationController pushViewController:about animated:YES];
#endif
    }
    else if (indexPath.row == 1 || indexPath.row == 2)
    {
#ifdef CLIENT
        if (indexPath.row == 2)
        {
            AboutViewController *about = [[AboutViewController alloc] init];
            [self.navigationController pushViewController:about animated:YES];
        }
#else
        if (indexPath.row == 1)
        {
            AboutViewController *about = [[AboutViewController alloc] init];
            [self.navigationController pushViewController:about animated:YES];
        }
#endif
    }
    
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
