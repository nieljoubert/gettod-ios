//
//  ProblemReportedViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/02.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ProblemReportedViewController.h"

@interface ProblemReportedViewController ()
@property (strong, nonatomic) IBOutlet UILabel *sorryLabel;

@end

@implementation ProblemReportedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.navigationController.navigationBar.hidden = YES;
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];
	
	self.sorryLabel.numberOfLines = 0;
	self.sorryLabel.attributedText = [[NSAttributedString alloc] initWithString:@"We're sorry to hear that things didn't go as expected. Someone will be in touch with you soon to resolve the issue." attributes:attributes];
}

- (void)viewWillDisappear:(BOOL)animated
{
	self.navigationController.navigationBar.hidden = NO;
	[super viewWillDisappear:animated];
}

- (IBAction)okayTapped:(id)sender
{
//	[self.navigationController popToRootViewControllerAnimated:NO];
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
#ifdef CLIENT
    [appDelegate startAppWithServicesList];
#else
    [appDelegate startAppWithPartnerHomeScreen];
#endif
}

@end
