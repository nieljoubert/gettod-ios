//
//  DirectionsDetailView.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTDirection.h"

@interface DirectionsDetailView : UIView

- (void)setupViewWithDirection:(NavigationDirection)direction distanceInMeters:(int)distanceInMeters directionDescription:(NSString*)directionDescription streetName:(NSString*)streetName;
- (void)showLoader;
- (void)hideLoader;

@end
