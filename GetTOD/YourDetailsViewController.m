//
//  YourDetailsViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "YourDetailsViewController.h"
#import "GTTextFieldCell.h"
#import "GTBoldTableViewCell.h"
#import "GTTableSectionHeaderView.h"
#import "GTMultiLineTitleView.h"
#import "GTButton.h"
#import "EditEmailAddressViewController.h"
#import "ChangePasswordViewController.h"
#import "ChangeLocationViewController.h"
#import "AddEditCardDetailsViewController.h"
#import "ImageHelper.h"
#import "LoginViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LocationManager.h"

@interface YourDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIView *profileImageTapView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewFromXib;
@property (weak, nonatomic) IBOutlet UILabel *signOutLabel;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (strong, nonatomic) GTTextField *name;
@property (strong, nonatomic) GTTextField *surname;
@property (strong, nonatomic) GTTextField *phoneNumber;
@property (strong, nonatomic) GTTextFieldCell *nameCell;
@property (strong, nonatomic) GTTextFieldCell *surnameCell;
@property (strong, nonatomic) GTTextFieldCell *phoneNumberCell;

@property (strong, nonatomic) GTBoldTableViewCell *locationCell;
@property (strong, nonatomic) NSString *locationString;

@property (assign, nonatomic) BOOL nameOrSurnameChanged;
@property (assign, nonatomic) BOOL profileImageChanged;

@property (strong, nonatomic) EditEmailAddressViewController *editEmail;
@property (strong, nonatomic) ChangePasswordViewController *changePassword;

@end

@implementation YourDetailsViewController

- (void)viewDidLoad
{
	self.tableView = self.tableViewFromXib;
	
	[super viewDidLoad];
	
	self.tableView.tableHeaderView = self.headerView;
	
	UIBarButtonItem *button;
	
	if (self.isSettings)
	{
		self.title = @"Account Details";
		
		button = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
		
		self.tableView.tableFooterView = self.footerView;
		
		UITapGestureRecognizer *signOutTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(signOut)];
		signOutTap.numberOfTapsRequired = 1;
		[self.signOutLabel addGestureRecognizer:signOutTap];
	}
	else
	{
		GTMultiLineTitleView *title = [[GTMultiLineTitleView alloc] initWithTitle:@"Your Details" andSubTitle:@"Step 2 of 2" smallTitle:NO];
		self.navigationItem.titleView = title;
		
		button = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(next)];//doneWithRegistration)];
		
		self.navigationItem.hidesBackButton = YES;
		self.navigationItem.leftBarButtonItem = nil;
	}
	
	UIImage *image = [ImageHelper getImageFromDisk:PROFILE_IMAGE_NAME];
	
	if (image)
	{
		self.profileImageView.image = image;
		self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
		self.profileImageView.clipsToBounds = YES;
	}
	else
	{
		if ([SettingsManager manager].profileImageURL)
		{
			__weak YourDetailsViewController *weakSelf = self;
			
			[self.profileImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[SettingsManager manager].profileImageURL]]
										 placeholderImage:[UIImage imageNamed:@"default_profile_photo"]
												  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
													  
													  weakSelf.profileImageView.image = image;
													  weakSelf.profileImageView.layer.cornerRadius = weakSelf.profileImageView.frame.size.width/2;
													  weakSelf.profileImageView.clipsToBounds = YES;
													  [ImageHelper saveImageToDisk:image withName:PROFILE_IMAGE_NAME];
													  
												  } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
													  
													  
												  }];
		}
	}
	
	self.navigationItem.rightBarButtonItem = button;
	
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(changeProfileImage)];
	singleTap.numberOfTapsRequired = 1;
	[self.profileImageTapView addGestureRecognizer:singleTap];
	
	self.profileImageView.image = [ImageHelper getImageFromDisk:PROFILE_IMAGE_NAME];
	self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
	self.profileImageView.clipsToBounds = YES;
	
	if ([SettingsManager manager].currentLocation)
	{
		self.locationString = [SettingsManager manager].currentLocation;
	}
	
	self.name = [GTTextField new];
	[self.name setKeyboardToolbar:self.keyboardToolbar];
	self.surname = [GTTextField new];
	[self.surname setKeyboardToolbar:self.keyboardToolbar];
	self.phoneNumber = [GTTextField new];
	self.phoneNumber.keyboardType = UIKeyboardTypePhonePad;
	[self.phoneNumber setKeyboardToolbar:self.keyboardToolbar];
	
	[self addFormField:self.name];
	[self addFormField:self.surname];
	[self addFormField:self.phoneNumber];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
}

#pragma mark - Private -
- (void)updateUserDetails
{
	[[APIManager manager] updateUserDetails:self.name.text
									surname:self.surname.text
							 profilePicture:self.profileImageView.image
							 andPhoneNumber:self.phoneNumber.text
									success:^(id result) {
										
										[SettingsManager manager].profileImageURL = result;
										[SettingsManager manager].gotUserDetailsOnce = YES;
										[SettingsManager save];
										
										[self hideLoader];
										[self.navigationController popViewControllerAnimated:YES];
										
									} failure:^(NSError *error) {
										[self hideLoader];
									}];
	
}

- (void)getAllInfo
{
	//	[DatabaseManager createTables];
	
	[[APIManager manager] getProvinces:^(NSMutableArray *results) {
		//		[DatabaseManager addProvinces:results];
	} failure:^(NSError *error) {}];
	
	[[APIManager manager] getCompanies:^(NSMutableArray *results) {
		//		[DatabaseManager addCompanies:results];
	} failure:^(NSError *error) {}];
	
	[[APIManager manager] getServices:^(NSMutableArray *results) {
		//		[DatabaseManager addServices:results];
	} failure:^(NSError *error) {}];
	
	[[APIManager manager] getEntities:^(NSMutableArray *results) {
		//		[DatabaseManager addEntities:results];
	} failure:^(NSError *error) {}];
	
	[[APIManager manager] getCities:^(NSMutableArray *results) {
		//		[DatabaseManager addCities:results];
	} failure:^(NSError *error) {}];
}

#pragma mark - Validation -

- (BOOL)isScreenFieldsValid
{
	[super isScreenFieldsValid];
	
	BOOL isValid = YES;
	
	isValid = [self isNameValid] && isValid;
	isValid = [self isSurnameValid] && isValid;
	//#ifdef CLIENT
	isValid = [self isPhoneNumberValid] && isValid;
	//#endif
//	isValid = (self.locationString.length > 0) && isValid;
	
	return isValid;
}


- (BOOL)isNameValid
{
	if ([Validation isStringCharactersOnly:self.name.text] && self.name.text.length > 0)
	{
		[self.nameCell dismissError];
		[self dismissScreenError:self.name];
		return YES;
	}
	else
	{
		[self.nameCell setError];
		[self setScreenError:self.name message:@"Please enter a valid name"];
		return NO;
	}
}

- (BOOL)isSurnameValid
{
	if ([Validation isStringCharactersOnly:self.surname.text] && self.surname.text.length > 0)
	{
		[self.surnameCell dismissError];
		[self dismissScreenError:self.surname];
		return YES;
	}
	else
	{
		[self.surnameCell setError];
		[self setScreenError:self.surname message:@"Please enter a valid surname"];
		return NO;
	}
}

- (BOOL)isPhoneNumberValid
{
	if ([Validation isValidPhoneNumber:self.phoneNumber.text] && self.phoneNumber.text.length > 0 && self.phoneNumber.text.length <= 10)
	{
		[self.phoneNumberCell dismissError];
		[self dismissScreenError:self.phoneNumber];
		return YES;
	}
	else
	{
		[self.phoneNumberCell setError];
		[self setScreenError:self.phoneNumber message:@"Please enter a valid mobile number"];
		return NO;
	}
}

#pragma mark - TextField -
-(void)textFieldDidEndEditing:(UITextField *)textField
{
	GTTextField *field = (GTTextField*)textField;
	
	if (field == self.name)
	{
		[self isNameValid];
		[SettingsManager manager].firstName = self.name.text;
	}
	else if (field == self.surname)
	{
		[self isSurnameValid];
		[SettingsManager manager].lastName = self.surname.text;
	}
	else if (field == self.phoneNumber)
	{
		[self isPhoneNumberValid];
		[SettingsManager manager].phoneNumber = self.phoneNumber.text;
	}
	
	self.nameOrSurnameChanged = YES;
}

#pragma mark - Actions -
- (void)signOut
{
	[self dismissKeyboard];
	
	[AlertViewManager showAlertWithTitle:@"Signing Out"
								 message:@"Are you sure you want to sign out?"
					   cancelButtonTitle:@"No"
					   otherButtonTitles:@[@"Yes"]
					buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
						
						if (buttonIndex == 0) // NO
						{
							
						}
						else // YES
						{
							[self showLoader];
							
#ifdef CLIENT
							[self logout];
#else
							[[APIManager manager] setSupplierLocationCoordinates:[SettingsManager manager].currentLocationCoords
                                                                         bearing:[[SettingsManager manager].currentHeading floatValue]
																	   andStatus:SupplierStatusOffline
																		 success:^(id result) {
																			 
																			 [self logout];
																			 
																		 } failure:^(NSError *error) {
																		 }];
#endif
						}
						
					}];
}

- (void)logout
{
	[[APIManager manager] logout:^(BOOL success) {
		[self hideLoader];
		
		[self dismissViewControllerAnimated:NO completion:nil];
		
		[SettingsManager clear];
		
		[self.navigationController popToRootViewControllerAnimated:NO];
		AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
		[appDelegate startAppWithLoginController];
		
		
	} failure:^(NSError *error) {
		[self hideLoader];
	}];
}

- (void)next
{
	[self dismissKeyboard];

	if ([self isScreenFieldsValid])
	{
		[SettingsManager manager].firstName = self.name.text;
		[SettingsManager manager].lastName = self.surname.text;
		[SettingsManager manager].phoneNumber = self.phoneNumber.text;
		
		if (self.locationString)
		{
			[SettingsManager manager].currentLocation = self.locationString;
		}

		[SettingsManager save];

		if (self.profileImageChanged)
		{
			AddEditCardDetailsViewController *addEdit = [[AddEditCardDetailsViewController alloc] init];
			addEdit.isAdding = YES;
			addEdit.isSettings = self.isSettings;
			[self.navigationController pushViewController:addEdit animated:YES];
		}
		else
		{
			[AlertViewManager showAlertWithTitle:@"Profile Image"
										 message:@"Please select a profile image"
							   cancelButtonTitle:@"OK"
							   otherButtonTitles:nil
							buttonSelectionBlock:nil];
		}
	}
	else
	{
		if (self.errorField)
		{
			[self showScreenError];
		}
		else if ([SettingsManager manager].currentLocation == nil)
		{
			[AlertViewManager showAlertWithTitle:@"Location"
										 message:@"Please select your current location"
							   cancelButtonTitle:@"OK"
							   otherButtonTitles:nil
						   buttonSelectionBlock:nil];

		}
	}
}

- (void)save
{
	[self dismissKeyboard];
	
	if ([self isScreenFieldsValid])
	{
		[SettingsManager manager].firstName = self.name.text;
		[SettingsManager manager].lastName = self.surname.text;
		
		//#ifdef CLIENT
		[SettingsManager manager].phoneNumber = self.phoneNumber.text;
		//#else
		//		[SettingsManager manager].phoneNumber = @"";
		//#endif
		if (self.locationString)
		{
			[SettingsManager manager].currentLocation = self.locationString;
		}
		
		[SettingsManager save];
		
		if (self.nameOrSurnameChanged || self.profileImageChanged)
		{
			[self showLoader];
			[self updateUserDetails];
		}
		else
		{
			[self.navigationController popViewControllerAnimated:YES];
		}
	}
	else
	{
		if (self.errorField)
		{
			[self showScreenError];
		}
		else if ([SettingsManager manager].currentLocation == nil)
		{
			[AlertViewManager showAlertWithTitle:@"Location"
										 message:@"Please select your current location"
							   cancelButtonTitle:@"OK"
							   otherButtonTitles:nil
							buttonSelectionBlock:nil];
			
		}
	}
}

- (void)changeProfileImage
{
	[self dismissKeyboard];
	
	IBActionSheet *sheet = [[IBActionSheet alloc] initWithTitle:nil
													   delegate:self
											  cancelButtonTitle:@"Cancel"
										 destructiveButtonTitle:nil
											  otherButtonTitles:@"Take Photo",@"Choose From Library", nil];
	
	[sheet setFont:[UIFont fontWithName:FONT size:18] forButtonAtIndex:0];
	[sheet setFont:[UIFont fontWithName:FONT size:18] forButtonAtIndex:1];
	[sheet setFont:[UIFont fontWithName:FONT size:18] forButtonAtIndex:2];
	
	
	[sheet setButtonTextColor:PINK_COLOR forButtonAtIndex:0];
	[sheet setButtonBackgroundColor:LIGHT_BUTTON_BACKGROUND_COLOR forButtonAtIndex:0];
	
	[sheet setButtonTextColor:PINK_COLOR forButtonAtIndex:1];
	[sheet setButtonBackgroundColor:LIGHT_BUTTON_BACKGROUND_COLOR forButtonAtIndex:1];
	
	[sheet setButtonTextColor:[UIColor whiteColor] forButtonAtIndex:2];
	[sheet setButtonBackgroundColor:PINK_COLOR forButtonAtIndex:2];
	
	[sheet showInView:self.navigationController.view];
}

- (void)dismissViewController:(CreatingAccountBlackOverlayViewController *)controller success:(BOOL)success onCard:(BOOL)cardFailed
{
	if (success)
	{
		AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
		[appDelegate startAppWithServicesList];
	}
	
	[controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIActionSheet -
- (void)actionSheet:(IBActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		[self takePhoto];
	}
	else if (buttonIndex == 1)
	{
		[self choosePhoto];
	}
	else if (buttonIndex == 2)
	{
		
	}
}

- (void)choosePhoto
{
	UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
	imagePickerController.delegate = self;
	imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	imagePickerController.allowsEditing = YES;
	[self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)takePhoto
{
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
		UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
		imagePickerController.delegate = self;
		imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
		imagePickerController.allowsEditing = YES;
		[self presentViewController:imagePickerController animated:YES completion:nil];
	}
}

#pragma mark - ImagePicker -
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	[self dismissViewControllerAnimated:YES completion:nil];
	UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
	
	[ImageHelper saveImageToDisk:image withName:PROFILE_IMAGE_NAME];
	
	self.profileImageView.image = image;
	self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
	self.profileImageView.clipsToBounds = YES;
	
	self.profileImageChanged = YES;
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	if (self.isSettings)
	{
		return 4;
	}
	else
	{
		return 3;
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
	{
		return 2;
	}
	else if (section == 1)
	{
		return 1;
	}
	else if (section == 2)
	{
		return 1;
	}
	else if (section == 3)
	{
		return 2;
	}
	
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		
		if (indexPath.row == 0)
		{
			self.nameCell = cell;
			self.nameCell.textField = self.name;
			[self.nameCell setupCellWithPlaceholder:@"Name"];
			
			if ([SettingsManager manager].firstName)
			{
				self.name.text = [SettingsManager manager].firstName;
			}
			
			self.name.delegate = self;
		}
		else if (indexPath.row == 1)
		{
			self.surnameCell = cell;
			self.surnameCell.textField = self.surname;
			[self.surnameCell setupCellWithPlaceholder:@"Surname"];
			
			if ([SettingsManager manager].lastName)
			{
				self.surname.text = [SettingsManager manager].lastName;
			}
			
			self.surname.delegate = self;
		}
		
		return cell;
	}
	
	else if (indexPath.section == 1)
	{
		//#ifdef CLIENT
		GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		
		if (indexPath.row == 0)
		{
			self.phoneNumberCell = cell;
			self.phoneNumberCell.textField = self.phoneNumber;
			[self.phoneNumberCell setupCellWithPlaceholder:@"Mobile Number"];
			
			if ([SettingsManager manager].phoneNumber)
			{
				self.phoneNumber.text = [SettingsManager manager].phoneNumber;
			}
			
			self.phoneNumber.delegate = self;
		}
		
		return cell;
	}
	else if (indexPath.section == 2)
	{
		//#ifdef CLIENT
		GTTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"GTTableViewCell"];
		
		if (indexPath.row == 0)
		{
			if (self.locationString)
			{
				cell.textLabel.text = self.locationString;
				cell.textLabel.textColor = DARK_TEXT_COLOR;
			}
			else
			{
				cell.textLabel.text = @"Set Location";
				cell.textLabel.textColor = [UIColor lightGrayColor];
			}
		}
		
		return cell;
	}
	
	else if (indexPath.section == 3)
	{
		if (indexPath.row == 0)
		{
			GTTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"GTTableViewCell"];
			cell.textLabel.text = [SettingsManager manager].emailAddress;
			
			return cell;
		}
		else if (indexPath.row == 1)
		{
			GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
			cell.textField = [GTTextField new]; // FAKE
			[cell setupCellWithPlaceholder:@"Password"];
			cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure"]];
			cell.selectionStyle = UITableViewCellSelectionStyleDefault;
			cell.textField.enabled = NO;
			cell.textField.secureTextEntry = YES;
			cell.textField.delegate = self;
			cell.textField.text = @"****";
			
			return cell;
		}
	}
	
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"PERSONAL INFORMATION"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"MOBILE NUMBER"];
	}
	else if (section == 2)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"LOCATION"];
	}
	else if (section == 3)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"CREDENTIALS"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 2 && indexPath.row == 0)
	{
		ChangeLocationViewController *location = [ChangeLocationViewController new];
		location.delegate = self;
		[self.navigationController pushViewController:location animated:YES];
	}
	
	if (indexPath.section == 3)
	{
		if (indexPath.row == 0)
		{
			self.editEmail = [[EditEmailAddressViewController alloc] init];
			[self.navigationController pushViewController:self.editEmail animated:YES];
		}
		else if (indexPath.row == 1)
		{
			self.changePassword = [[ChangePasswordViewController alloc] init];
			[self.navigationController pushViewController:self.changePassword animated:YES];
		}
	}

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - ChangeLocationDelegate -
-(void)locationChanged:(NSString *)newLocationString
{
	self.locationString = newLocationString;
	[self.tableView reloadData];
}



@end
