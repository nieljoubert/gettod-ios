//
//  GTButtonDynamic.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

typedef enum {
	GTButtonTypePink = 0,
	GTButtonTypeWhite = 1
} GTButtonType;

@interface GTButtonDynamic : UIButton

- (id) initWithType: (GTButtonType) buttonType
			 target: (id) target
			 action: (SEL) selector
		  titleText: (NSString*) title;

@end
