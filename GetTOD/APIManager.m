
//
//  APIManager.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "APIManager.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "SettingsManager.h"
#import "CacheManager.h"
#import "ImageHelper.h"
#import "AlertviewManager.h"

#import "Province.h"
#import "City.h"
#import "Supplier.h"
#import "Entity.h"
#import "Company.h"
#import "Service.h"
#import "User.h"
#import "Supplier.h"
#import "Problem.h"
#import "Material.h"
#import "Quote.h"

@implementation APIManager

+ (APIManager*)manager
{
    APIManager *manager = [[APIManager alloc] init];
    return manager;
}

- (id)init
{
    if ((self = [super init]))
    {
        //#ifdef DEBUG
        //		self.path = @"http://52.29.10.204/api";
        //#else
        self.path = @"http://suppliers.gettod.com/api";
        //#endif
    }
    
    return self;
}
///suppliers/register/push/

- (void)registerForPush:(NSString*)udid
                success:(SuccessHandler)success
                failure:(FailureHandler)failure
{
    
}

- (void)getProvinces:(SuccessArrayHandler)success
             failure:(FailureHandler)failure
{
    self.function = @"provinces/?limit=100";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSMutableArray *provinces = [NSMutableArray array];
                                
                                for (NSDictionary *dic in [result objectForKey:@"results"])
                                {
                                    Province *p = [Province new];
                                    p.provinceId = [dic objectForKey:@"id"];
                                    p.name = [dic objectForKey:@"name"];
                                    [provinces addObject:p];
                                }
                                
                                [CacheManager cacheProvinces:provinces];
                                success(provinces);
                                
                            } failure:^(NSError *error) {
                                failure(error);
                            }];
}

- (void)getCompanies:(SuccessArrayHandler)success
             failure:(FailureHandler)failure
{
    self.function = @"companies/?limit=100";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSMutableArray *companies = [NSMutableArray array];
                                
                                NSArray *array = [result objectForKey:@"results"];
                                
                                for (NSDictionary *dic in array)
                                {
                                    Company *c = [Company new];
                                    
                                    c.companyId = [dic objectForKey:@"id"];
                                    c.name = [dic objectForKey:@"name"];
                                    c.address = [dic objectForKey:@"address"];
                                    c.suburb = [dic objectForKey:@"suburb"];
                                    c.postCode = [dic objectForKey:@"postal_code"];
                                    c.registeredName = [dic objectForKey:@"registered_name"];
                                    c.registrationNumber = [dic objectForKey:@"registration_num"];
                                    c.contactNumber = [dic objectForKey:@"contact_number"];
                                    c.vatNumber = [dic objectForKey:@"vat_num"];
                                    c.isVatRegistered = [dic objectForKey:@"vat_registered"];
                                    c.city = [dic objectForKey:@"city"];
                                    c.entity = [dic objectForKey:@"entity_type"];
                                    
                                    [companies addObject:c];
                                }
                                
                                [CacheManager cacheCompanies:companies];
                                success(companies);
                                
                            } failure:^(NSError *error) {
                                failure(error);
                            }];
}

- (void)getServices:(SuccessArrayHandler)success
            failure:(FailureHandler)failure
{
    self.function = @"services/?limit=100";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSMutableArray *services = [NSMutableArray array];
                                
                                for (NSDictionary *dic in [result objectForKey:@"results"])
                                {
                                    Service *service = [Service new];
                                    service.serviceId = [dic objectForKey:@"id"];
                                    service.name = [dic objectForKey:@"name"];
                                    service.pictureActiveURL = [dic objectForKey:@"picture_active"];
                                    service.pictureInactiveURL = [dic objectForKey:@"picture_inactive"];
                                    service.highlightColor = [dic objectForKey:@"hightlight_colour"];
                                    
                                    [ImageHelper downloadImage:service.pictureActiveURL withName:[NSString stringWithFormat:SERVICE_IMAGE_NAME,service.name]];
                                    
                                    [services addObject:service];
                                }
                                
                                //								[DatabaseManager addServices:services];
                                [CacheManager cacheServices:services];
                                
                                if (services.count == 0)
                                {
                                    [self showAlertWithTitle:@""
                                                  andMessage:@"Unable to retrieve the list of services. Please refresh by pulling down."];
                                }
                                
                                success(services);
                                
                            } failure:^(NSError *error) {
                                
                                [self showAlertWithTitle:@""
                                              andMessage:@"Unable to retrieve the list of services. Please refresh by pulling down."];
                                
                                failure(error);
                            }];
}

- (void)getEntities:(SuccessArrayHandler)success
            failure:(FailureHandler) failure
{
    self.function = @"entities/?limit=100";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSMutableArray *entities = [NSMutableArray array];
                                
                                for (NSDictionary *dic in [result objectForKey:@"results"])
                                {
                                    Entity *e = [Entity new];
                                    e.entityId = [dic objectForKey:@"id"];
                                    e.name = [dic objectForKey:@"name"];
                                    [entities addObject:e];
                                }
                                
                                [CacheManager cacheCompanyEntities:entities];
                                success(entities);
                                
                            } failure:^(NSError *error) {
                                failure(error);
                            }];
}

- (void)getCities:(SuccessArrayHandler)success
          failure:(FailureHandler) failure
{
    self.function = @"cities/?limit=100";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSMutableArray *cities = [NSMutableArray array];
                                
                                NSArray *array = [result objectForKey:@"results"];
                                
                                for (NSDictionary *dic in array)
                                {
                                    City *c = [City new];
                                    c.cityId = [dic objectForKey:@"id"];
                                    c.name = [dic objectForKey:@"name"];
                                    c.province = [dic objectForKey:@"province"];
                                    
                                    [cities addObject:c];
                                }
                                
                                [CacheManager cacheCities:cities];
                                success(cities);
                                
                            } failure:^(NSError *error) {
                                failure(error);
                            }];
}

#pragma mark - Authenticated Calls -

- (void)loginWithUsername:(NSString*)username
              andPassword:(NSString*)password
                  success:(SuccessHandler)success
                  failure:(FailureHandler)failure
{
    self.function = @"auth/login";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:username forKey:@"username"];
    [data setObject:password forKey:@"password"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 NSString *token = [result objectForKey:@"auth_token"];
                                 
                                 [SettingsManager manager].authenticationToken = token;
                                 [SettingsManager manager].username = username;
                                 [SettingsManager manager].emailAddress = username;
                                 [SettingsManager save];
                                 
                                 success(result);
                                 
                             } failure:^(NSError *error) {
                                 
                                 [self showAlertWithTitle:@"Login"
                                               andMessage:error.localizedDescription];
                                 failure(error);
                             }];
}

- (void)registerWithUsername:(NSString*)username
                       email:(NSString*)email
                 andPassword:(NSString*)password
                     success:(SuccessHandler)success
                     failure:(FailureHandler)failure
{
    self.function = @"users/create";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:username forKey:@"username"];
    [data setObject:email forKey:@"email"];
    [data setObject:password forKey:@"password"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 if ([result objectForKey:@"error"] != nil)
                                 {
                                     [self showAlertWithTitle:@""
                                                   andMessage:@"User is already registered. Please log in to proceed."];
                                     failure([NSError new]);
                                 }
                                 else
                                 {
                                     NSString *token = [result objectForKey:@"auth_token"];
                                     
                                     [SettingsManager manager].authenticationToken = token;
                                     [SettingsManager manager].username = username;
                                     [SettingsManager manager].emailAddress = email;
                                     [SettingsManager save];
                                     
                                     success(result);
                                 }
                                 
                             } failure:^(NSError *error) {
                                 
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured while registering. Please try again."];
                                 failure(error);
                             }];
}

- (void)logout:(SuccessTrueFalseHandler)success
       failure:(FailureHandler)failure
{
    self.function = @"logout";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:[SettingsManager manager].pushToken forKey:@"registration_id"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(YES);
                             } failure:^(NSError *error) {
                                 
//                                 [self showAlertWithTitle:@""
//                                               andMessage:@"An error occured. Please try again."];
                                 
                                 failure(error);
                             }];
}

- (void)activateWithUID:(NSString*)Uid
               andToken:(NSString*)token
                success:(SuccessHandler)success
                failure:(FailureHandler)failure
{
    self.function = @"auth/activate";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:Uid forKey:@"uid"];
    [data setObject:token forKey:@"token"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(result);
                             } failure:^(NSError *error) {
                                 failure(error);
                             }];
}

- (void)getMe:(SuccessHandler)success
      failure:(FailureHandler)failure
{
    self.function = @"auth/me/";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                success([NSMutableArray arrayWithArray:result]);
                            } failure:^(NSError *error) {
                                failure(error);
                            }];
}

- (void)changeUsername:(NSString*)username
          withPassword:(NSString*)password
               success:(SuccessHandler)success
               failure:(FailureHandler)failure
{
    self.function = @"auth/username";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:username forKey:@"new_username"];
    [data setObject:password forKey:@"current_password"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(result);
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured changing your username. Please try again."];
                                 failure(error);
                             }];
}

- (void)changeCurrentPassword:(NSString*)currentPassword
                toNewPassword:(NSString*)newPassword
                      success:(SuccessHandler)success
                      failure:(FailureHandler)failure
{
    self.function = @"auth/password";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:currentPassword forKey:@"current_password"];
    [data setObject:newPassword forKey:@"new_password"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(result);
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured changing your password. Please try again."];
                                 failure(error);
                             }];
}

- (void)resetPasswordWithEmail:(NSString*)email
                       success:(SuccessHandler)success
                       failure:(FailureHandler)failure
{
    self.function = @"users/resetpassword";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:email forKey:@"email"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(result);
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured changing your password. Please try again."];
                                 failure(error);
                             }];
}

- (void)confirmPasswordResetWithUID:(NSString*)Uid
                           andToken:(NSString*)token
                     andNewPassword:(NSString*)newPassword
                            success:(SuccessHandler)success
                            failure:(FailureHandler)failure
{
    self.function = @"auth/password/reset/confirm";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:Uid forKey:@"uid"];
    [data setObject:token forKey:@"token"];
    [data setObject:newPassword forKey:@"new_password"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(result);
                             } failure:^(NSError *error) {
                                 failure(error);
                             }];
}

- (void)registerForPushNotifications:(SuccessTrueFalseHandler)success
                             failure:(FailureHandler)failure
{
    self.function = @"push/register";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSString * strToken = [UIDevice currentDevice].identifierForVendor.UUIDString;
    
    strToken = [strToken stringByReplacingOccurrencesOfString: @"-" withString: @""];
    
    [data setObject:strToken forKey:@"device_name"];
    [data setObject:strToken forKey:@"device_id"];
    [data setObject:@"iOS" forKey:@"platform"];
    
    [data setObject:[SettingsManager manager].pushToken forKey:@"registration_id"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 [SettingsManager manager].hasRegisteredForPushNotification = YES;
                                 [SettingsManager save];
                                 
                                 success(YES);
                                 
                             } failure:^(NSError *error) {
                                 
                                 [self performPOSTRequestWithData:data
                                                     inJSONFormat:NO
                                                          isAsync:NO
                                                          success:^(id result) {
                                                              
                                                              [SettingsManager manager].hasRegisteredForPushNotification = YES;
                                                              [SettingsManager save];
                                                              
                                                              success(YES);
                                                              
                                                          } failure:^(NSError *error) {
                                                              failure(error);
                                                          }];
                             }];
}

- (void)getUserDetailsWithSuccess:(SuccessHandler)success
                          failure:(FailureHandler)failure
{
    self.function = @"users/update/";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSDictionary *profile = [result objectForKey:@"profile"];
                                
                                [SettingsManager manager].firstName = [[profile objectForKey:@"user"] objectForKey:@"first_name"];
                                [SettingsManager manager].lastName = [[profile objectForKey:@"user"] objectForKey:@"last_name"];
                                [SettingsManager manager].emailAddress = [[profile objectForKey:@"user"] objectForKey:@"email"];
                                
                                if ([[profile objectForKey:@"user"] objectForKey:@"phone_number"])
                                {
                                    [SettingsManager manager].phoneNumber = [[profile objectForKey:@"user"] objectForKey:@"phone_number"];
                                }
                                else
                                {
                                    [SettingsManager manager].phoneNumber = @"";
                                }
                                
                                if ([profile objectForKey:@"picture"] != nil)
                                {
#ifdef CLIENT
                                    [SettingsManager manager].profileImageURL = [NSString stringWithFormat:@"%@/media/%@",BASEURLSTRING,[profile objectForKey:@"picture"]];
#else
                                    [SettingsManager manager].profileImageURL = [NSString stringWithFormat:@"%@%@",BASEURLSTRING,[profile objectForKey:@"picture"]];
#endif
                                }
                                
                                [SettingsManager save];
                                
                                success(result);
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured while retrieving your details. Please try again."];
                                failure(error);
                            }];
}


- (void)updateUserDetails:(NSString*)name
                  surname:(NSString*)surname
           profilePicture:(UIImage*)profileImage
           andPhoneNumber:(NSString*)phonenumber
                  success:(SuccessHandler)success
                  failure:(FailureHandler)failure
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:name forKey:@"name"];
    [data setObject:surname forKey:@"surname"];
    [data setObject:phonenumber forKey:@"phone_number"];
    
    
    //	if (profileImage == nil)
    //	{
    //		self.function = @"users/update";
    //
    //		[self performPOSTRequestWithData:data
    //							inJSONFormat:NO
    //								 success:^(id result) {
    //
    //									 success(result);
    //
    //								 } failure:^(NSError *error) {
    //
    //									 failure(error);
    //
    //								 }];
    //	}
    //	else
    //	{
    self.function = @"/api/users/update";
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BASEURLSTRING]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token %@",[SettingsManager manager].authenticationToken] forHTTPHeaderField:@"Authorization"];
    
    
#ifdef CLIENT
    [manager.requestSerializer setValue:@"CLIENT" forHTTPHeaderField:@"Authorization-Type"];
#else
    [manager.requestSerializer setValue:@"PARTNER" forHTTPHeaderField:@"Authorization-Type"];
#endif
    
    NSData *imageData = UIImageJPEGRepresentation(profileImage, 0.3);
    
    AFHTTPRequestOperation *op = [manager POST:self.function
                                    parameters:data
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         
                         [formData appendPartWithFileData:imageData
                                                     name:@"profile_pic"
                                                 fileName:[NSString stringWithFormat:@"%@_%@_profile.jpg",[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],[surname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]
                                                 mimeType:@"image/jpeg"];
                         
                     } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         
                         [ImageHelper saveImageToDisk:[UIImage imageWithData:imageData] withName:PROFILE_IMAGE_NAME];
                         
                         NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
                         success([responseObject objectForKey:@"picture"]);
                         
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         
                         NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                         
                         [self showAlertWithTitle:@""
                                       andMessage:@"An error occured while updating your details. Please try again."];
                         failure(error);
                         
                     }];
    
    [op start];
    //	}
}

- (void)getJobForID:(NSNumber*)jobId
            success:(SuccessJobHandler)success
            failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/job/%@",jobId];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                success([self createJobFromData:[result objectForKey:@"job"]]);
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured while retrieving job details. Please try again."];
                                failure(error);
                            }];
}

- (void)saveCardData:(Card*)card
             success:(SuccessTrueFalseWithResultHandler)success
             failure:(FailureHandler)failure
{
    self.function = @"cards/register/";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:card.brand forKey:@"paymentBrand"];
    [data setObject:card.number forKey:@"cardNumber"];
    [data setObject:card.cardHolderName forKey:@"cardHolder"];
    [data setObject:card.month forKey:@"expiryMonth"];
    [data setObject:card.year forKey:@"expiryYear"];
    [data setObject:card.cvv forKey:@"cvv"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(YES,result);
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:error.localizedDescription];
                                 failure(error);
                             }];
}

- (void)performPaymentForJobID:(NSNumber*)jobId
                withCardNumber:(NSString*)number
                           cvv:(NSString*)cvv
                   expiryMonth:(NSString*)month
                    expiryYear:(NSString*)year
                       success:(SuccessTrueFalseWithResultHandler)success
                       failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/job/pay/%@",jobId];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:number forKey:@"cardNumber"];
    [data setObject:cvv forKey:@"cardCvv"];
    [data setObject:month forKey:@"cardExpiryMonth"];
    [data setObject:year forKey:@"cardExpiryYear"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(YES,result);
                             } failure:^(NSError *error) {
                                 failure(error);
                             }];
}

//paymentBrand: AMEX
//cardNumber: 377777777777770
//cardHolder: Jane Jones
//expiryMonth: 05
//expiryYear: 2018
//cvv: 1234
//name: Private

- (void)registerCreditCard:(Card*)card
                   success:(SuccessHandler)success
                   failure:(FailureHandler)failure
{
    self.function = @"cards/register/";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:card.brand forKey:@"paymentBrand"];
    [data setObject:card.number forKey:@"cardNumber"];
    [data setObject:card.cardHolderName forKey:@"cardHolder"];
    [data setObject:card.month forKey:@"expiryMonth"];
    [data setObject:card.year forKey:@"expiryYear"];
    [data setObject:card.cvv forKey:@"cvv"];
    [data setObject:card.name forKey:@"name"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success([self convertDataToCard:result]);
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:error.localizedDescription];
                                 failure(error);
                             }];
}

- (void)deleteCreditCard:(Card*)card
                 success:(SuccessHandler)success
                 failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"cards/%d",card.ident];
    
    [self performDELETERequestWithData:nil
                               isAsync:NO
                               success:^(id result) {
                                   
                                   success(result);
                                   
                               } failure:^(NSError *error) {
                                   [self showAlertWithTitle:@""
                                                 andMessage:error.localizedDescription];
                                   failure(error);
                               }];
}

- (void)getCreditCards:(SuccessArrayHandler)success
               failure:(FailureHandler)failure
{
    self.function = @"cards/";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSMutableArray *cards = [NSMutableArray new];
                                
                                for (NSDictionary *dict in result)
                                {
                                    [cards addObject:[self convertDataToCard:dict]];
                                }
                                
                                success(cards);
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:error.localizedDescription];
                                failure(error);
                            }];
}

- (Card*)convertDataToCard:(NSDictionary*)cardDict
{
    Card *card = [Card new];
    card.brand = [cardDict objectForKey:@"brand"];
    card.month = [cardDict objectForKey:@"expiry_month"];
    card.year = [cardDict objectForKey:@"expiry_year"];
    card.ident = [[cardDict objectForKey:@"id"] intValue];
    card.lastDigits = [cardDict objectForKey:@"last_digits"];
    card.name = [cardDict objectForKey:@"name"];
    card.token = [cardDict objectForKey:@"token"];
    
    return card;
}

- (void)reportProblemForJob:(NSNumber*)jobId
                 andProblem:(NSString*)problem
                    success:(SuccessTrueFalseHandler)success
                    failure:(FailureHandler)failure
{
    self.function = @"suppliers/job/problem/";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:jobId forKey:@"job_id"];
    [data setObject:problem forKey:@"problem"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(YES);
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured while reporting the problem. Please try again."];
                                 failure(error);
                             }];
}

- (void)referFriendWithEmail:(NSString*)email
                     success:(SuccessHandler)success
                     failure:(FailureHandler)failure
{
    self.function = @"users/refer";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:email forKey:@"email"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 success(result);
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured while sending referal email. Please try again."];
                                 failure(error);
                             }];
}

-(void)getTaskHistory:(SuccessArrayHandler)success
              failure:(FailureHandler)failure
{
    self.function = @"users/me/history";
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSMutableArray *jobs = [NSMutableArray array];
                                
                                for (NSDictionary *jobDict in [result objectForKey:@"job_history"])
                                {
                                    Job *job = [self createJobFromData:jobDict];
                                    [jobs addObject:job];
                                }
                                
                                success(jobs);
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured while retrieving task history. Please try again."];
                                failure(error);
                            }];
    
}

#pragma mark - Supplier calls -

- (void)getNearbySuppliersFromLocation:(CLLocationCoordinate2D)coordinates
                               andType:(NSNumber*)type
                               success:(SuccessArrayHandler)success
                               failure:(FailureHandler)failure
{
    self.function = @"suppliers/nearby";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:[NSNumber numberWithDouble:coordinates.latitude] forKey:@"lat"];
    [data setObject:[NSNumber numberWithFloat:coordinates.longitude] forKey:@"lng"];
    
    if (type)
    {
        [data setObject:type forKey:@"type"];
    }
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 NSMutableArray *suppliers = [NSMutableArray array];
                                 NSArray *array = result;
                                 
                                 for (NSDictionary *dic in array)
                                 {
                                     [suppliers addObject:[self createSupplierFromDictionary:@{@"supplier":dic}]];
                                     [CacheManager cacheSuppliers:suppliers];
                                 }
                                 
                                 success(suppliers);
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured getting nearby suppliers. Please try again."];
                                 failure(error);
                             }];
}

- (void)getSupplierLocation:(NSNumber*)supplierID
                    success:(SuccessHandler)success
                    failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/location/%@",supplierID];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                success(result);
                                
                            } failure:^(NSError *error) {
//                                [self showAlertWithTitle:@""
//                                              andMessage:@"An error occured while retrieving supplier's location. Please try again."];
                                failure(error);
                            }];
}

- (void)getSupplierSummaryForID:(NSNumber*)supplierID
                        success:(SuccessSupplierHandler)success
                        failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/summary/%@",supplierID];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                if (result)
                                {
                                    success([self createSupplierFromDictionary:result]);//[result objectForKey:@"supplier"]]);
                                }
                                else
                                {
                                    success(nil);
                                }
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured while retrieving supplier details. Please try again."];
                                failure(error);
                            }];
}

- (void)requestSupplier:(NSNumber*)supplierID
                problem:(Problem*)problem
                isOther:(BOOL)isOther
         jobCoordinates:(CLLocationCoordinate2D)coordinates
 additionalInstructions:(NSString*)instructions
              cardToken:(NSString*)cardToken
             travelTime:(NSString*)time
         travelDistance:(NSString*)distance
                success:(SuccessJobHandler)success
                failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/request/%@",supplierID];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:problem.problemId forKey:@"problem"];
    
    if (isOther)
    {
        [data setObject:problem.problemDescription forKey:@"problem_other"];
    }
    
    //	if (coordinates.latitude == 0 || coordinates.longitude == 0)
    //	{
    //		coordinates = CLLocationCoordinate2DMake(-33.854642,18.512265);
    //	}
    
    [data setObject:[NSNumber numberWithFloat:coordinates.latitude] forKey:@"job_lat"];
    [data setObject:[NSNumber numberWithFloat:coordinates.longitude] forKey:@"job_lng"];
    [data setObject:(instructions == nil)?@"":instructions forKey:@"additional_instructions"];
    [data setObject:cardToken forKey:@"card_id"];
    [data setObject:time forKey:@"travel_time"];
    [data setObject:distance forKey:@"travel_distance"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 success([self createJobFromData:result]);
                                 
                             }
                             failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured requesting supplier. Please try again."];
                                 failure(error);
                             }];
}

- (void)rateSupplierForJob:(NSNumber*)jobID
                 andRating:(NSNumber*)rating
                   success:(SuccessTrueFalseHandler)success
                   failure:(FailureHandler)failure
{
    self.function = @"ratings/supplier/";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    //	success(YES);
    
    [data setObject:jobID forKey:@"job_id"];
    [data setObject:rating forKey:@"rating"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 ([[result objectForKey:@"success"] boolValue]) ? success(YES) : success(NO);
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured rating supplier. Please try again."];
                                 failure(error);
                             }];
}

- (void)cancelJob:(NSNumber*)jobID
         withType:(JobCancelType)type
          success:(SuccessTrueFalseHandler)success
          failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/job/cancel/%@",jobID];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:[NSNumber numberWithInteger:type] forKey:@"cancel_type"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:YES
                             success:^(id result) {
                                 
                                 ([[result objectForKey:@"success"] boolValue]) ? success(YES) : success(NO);
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured cancelling the job. Please try again."];
                                 failure(error);
                             }];
}

#pragma mark - Quote Calls -

- (void)getQuoteRejectReasonsForServiceID:(NSNumber*)serviceID
                                  success:(SuccessArrayHandler)success
                                  failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/rejectreasons/%@",serviceID];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSArray *possibleReasons = [result objectForKey:@"possible_reasons"];
                                
                                NSMutableArray *reasons = [NSMutableArray array];
                                
                                for (NSDictionary *reasonDict in possibleReasons)
                                {
                                    Problem *reason = [Problem new];
                                    reason.problemDescription = [reasonDict objectForKey:@"description"];
                                    reason.problemId = [reasonDict objectForKey:@"id"];
                                    [reasons addObject:reason];
                                }
                                
                                success(reasons);
                                
                            } failure:^(NSError *error) {
                                failure(error);
                            }];
}

- (void)acceptQuote:(NSNumber*)quoteID
            success:(SuccessTrueFalseHandler)success
            failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"quotes/accept/%@",quoteID];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                ([[result objectForKey:@"success"] boolValue]) ? success(YES) : success(NO);
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured accepting quote. Please try again."];
                                failure(error);
                            }];
}

- (void)rejectQuote:(NSNumber*)quoteID
       withReasonId:(NSNumber*)reasonID
            success:(SuccessTrueFalseHandler)success
            failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"quotes/reject/%@",quoteID];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:reasonID forKey:@"reject_reason"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 ([[result objectForKey:@"success"] boolValue]) ? success(YES) : success(NO);
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured rejecting quote. Please try again."];
                                 failure(error);
                             }];
}

- (void)getQuoteForID:(NSNumber*)quoteId
              success:(SuccessQuoteHandler)success
              failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"quotes/%@",quoteId];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                NSDictionary *quoteDictionary = [result objectForKey:@"quote"];
                                
                                if (quoteDictionary)
                                {
                                    success([self createQuoteFromDictionary:quoteDictionary]);
                                }
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured retrieving quote details. Please try again."];
                                failure(error);
                            }];
}

- (void)setReminderForServiceType:(NSNumber*)serviceID
                     fromLocation:(CLLocationCoordinate2D)coordinates
                          success:(SuccessTrueFalseHandler)success
                          failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"services/notifyme/%@",serviceID];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:[NSNumber numberWithFloat:coordinates.latitude] forKey:@"lat"];
    [data setObject:[NSNumber numberWithFloat:coordinates.longitude] forKey:@"lng"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 ([[result objectForKey:@"success"] boolValue]) ? success(YES) : success(NO);
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured setting the reminder. Please try again."];
                                 failure(error);
                             }];
}

#pragma mark - Supplier Side Calls -
- (void)acceptJob:(NSNumber*)jobID
          success:(SuccessTrueFalseHandler)success
          failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/job/accept/%@",jobID];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                if ([[result objectForKey:@"success"] boolValue])
                                {
                                    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
                                    [SettingsManager save];
                                    success(YES);
                                }
                                else
                                {
                                    success(NO);
                                }
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured while accepting job. Please try again."];
                                failure(error);
                            }];
}

- (void)setSupplierLocationCoordinates:(CLLocationCoordinate2D)coordinates
                               bearing:(float)bearing
                             andStatus:(SupplierStatus)status
                               success:(SuccessHandler)success
                               failure:(FailureHandler)failure
{
    self.function = @"suppliers/location/";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:[NSNumber numberWithFloat:coordinates.latitude] forKey:@"lat"];
    [data setObject:[NSNumber numberWithFloat:coordinates.longitude] forKey:@"lng"];
    [data setObject:[NSNumber numberWithInteger:status] forKey:@"status"];
    [data setObject:[NSNumber numberWithFloat:bearing] forKey:@"bearing"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 [SettingsManager manager].supplierStatus = [[result objectForKey:@"status"] integerValue];
                                 [SettingsManager manager].currentLocationLat = [result objectForKey:@"lat"];
                                 [SettingsManager manager].currentLocationLng = [result objectForKey:@"lng"];
                                 [SettingsManager manager].currentHeading = [result objectForKey:@"bearing"];
                                 [SettingsManager save];
                                 
                                 success(success);
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured updating your location or status. Please try again."];
                                 failure(error);
                             }];
}

- (void)supplierArrivedForJob:(NSNumber*)jobID
                      success:(SuccessHandler)success
                      failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/arrived/%@",jobID];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                success(result);
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@"" andMessage:@"An error occured while sending arrival notification. Please try again."];
                                failure(error);
                            }];
}

- (void)setJobCompleted:(NSNumber*)jobID
                success:(SuccessTrueFalseHandler)success
                failure:(FailureHandler)failure
{
    self.function = [NSString stringWithFormat:@"suppliers/job/completed/%@",jobID];
    
    [self performGETRequestWithData:nil
                            isAsync:NO
                            success:^(id result) {
                                
                                ([[result objectForKey:@"success"] boolValue]) ? success(YES) : success(NO);
                                
                            } failure:^(NSError *error) {
                                [self showAlertWithTitle:@""
                                              andMessage:@"An error occured while completing the job. Please try again."];
                                failure(error);
                            }];
}

- (void)createQuoteWithJobID:(NSNumber*)jobID
                      labour:(NSNumber*)labour
                 description:(NSString*)quoteDescription
             listOfMaterials:(NSArray*)materials
                     success:(SuccessHandler)success
                     failure:(FailureHandler)failure
{
    self.function = @"suppliers/quote/create/";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:jobID forKey:@"job_id"];
    [data setObject:labour forKey:@"labour_hours"];
    [data setObject:quoteDescription forKey:@"description"];
    
    NSMutableArray *materialsArray = [NSMutableArray array];
    
    for (Material *material in materials)
    {
        NSDictionary *materialDict = @{@"description":material.name, @"unit_price":material.costPerUnit, @"num_units":material.quantity};
        [materialsArray addObject:materialDict];
    }
    
    [data setObject:materialsArray forKey:@"list_items"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:YES
                             isAsync:NO
                             success:^(id result) {
                                 
                                 NSDictionary *quoteDictionary = [result objectForKey:@"quote"];
                                 
                                 if (quoteDictionary)
                                 {
                                     success([self createQuoteFromDictionary:quoteDictionary]);
                                 }
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured while creating quote. Please try again."];
                                 failure(error);
                             }];
}

-(void)rateUserForJob:(NSNumber *)jobID
            andRating:(NSNumber *)rating
              success:(SuccessTrueFalseHandler)success
              failure:(FailureHandler)failure
{
    self.function = @"ratings/user/";
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setObject:jobID forKey:@"job_id"];
    [data setObject:rating forKey:@"rating"];
    
    [self performPOSTRequestWithData:data
                        inJSONFormat:NO
                             isAsync:NO
                             success:^(id result) {
                                 
                                 ([[result objectForKey:@"success"] boolValue]) ? success(YES) : success(NO);
                                 
                             } failure:^(NSError *error) {
                                 [self showAlertWithTitle:@""
                                               andMessage:@"An error occured while rating user. Please try again."];
                                 failure(error);
                             }];
}

#pragma mark - Private -
- (Job*)createJobFromData:(NSDictionary*)jobDict
{
    //	if ([jobDict objectForKey:@"cancelled_at"] != [NSNull null])
    //	{
    //
    //	}
    Job *job = [Job new];
    job.jobId = [jobDict objectForKey:@"id"];
    job.additionalInstructions = [jobDict objectForKey:@"additional_instructions"];
    job.calculatedTime = [jobDict objectForKey:@"calculated_travel_time"];
    job.calculatedDistance = [jobDict objectForKey:@"calculated_travel_distance"];
    job.customerId = [jobDict objectForKey:@"user"];
    job.quotePrice = [jobDict objectForKey:@"quote_price"];
    
    NSArray *cancelReasons = [jobDict objectForKey:@"cancel_reasons"];
    job.cancelReasons = [NSMutableArray array];
    
    for (NSDictionary *reasonDict in cancelReasons)
    {
        Reason *reason = [Reason new];
        reason.reasonDescription = [reasonDict objectForKey:@"description"];
        reason.reasonId = [reasonDict objectForKey:@"id"];
        [job.cancelReasons addObject:reason];
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SS.SSS'Z'"];
    
    job.createdDate = [dateFormatter dateFromString:[jobDict objectForKey:@"created_at"]];
    job.updatedDate = [dateFormatter dateFromString:[jobDict objectForKey:@"updated_at"]];
    
    job.jobLocationLat = [jobDict objectForKey:@"job_location_lat"];
    job.jobLocationLng = [jobDict objectForKey:@"job_location_lng"];
    
    NSDictionary *problemDict = [jobDict objectForKey:@"problem"];
    Problem *prob = [Problem new];
    prob.problemDescription = [problemDict objectForKey:@"description"];
    prob.estimatedPrice = [problemDict objectForKey:@"estimate_price"];
    prob.problemId = [problemDict objectForKey:@"id"];
    
    if (![[jobDict objectForKey:@"problem_other"] isEqualToString:[problemDict objectForKey:@"description"]])
    {
        prob.problemDescription = [jobDict objectForKey:@"problem_other"];
        prob.estimatedPrice = [NSNumber numberWithInt:-1];
    }
    
    job.problem = prob;
    job.supplier = [self createSupplierFromDictionary:jobDict];
    
    NSDictionary *customerDict = [jobDict objectForKey:@"user"];
    
    User *customer = [User new];
    
    customer.name = [NSString stringWithFormat:@"%@ %@",[customerDict objectForKey:@"first_name"],[customerDict objectForKey:@"last_name"]];
    customer.phoneNumber = [customerDict objectForKey:@"phone_number"] ? [customerDict objectForKey:@"phone_number"] : ([SettingsManager manager].phoneNumber) ? [SettingsManager manager].phoneNumber : @"";
    customer.email = [customerDict objectForKey:@"email"];
    customer.rating = [customerDict objectForKey:@"rating"];
    customer.profilePictureURL = [customerDict objectForKey:@"profile_pic"];
    job.customer = customer;
    
    return job;
}

- (Supplier*)createSupplierFromDictionary:(NSDictionary*)dict
{
    NSDictionary *supplierDictionary = [dict objectForKey:@"supplier"];
    
    Supplier *sup = [Supplier new];
    sup.supplierId = [supplierDictionary objectForKey:@"id"];
    sup.city = [CacheManager cityFromCacheWithID:[[supplierDictionary objectForKey:@"city"] intValue]];
    sup.cityOther = [supplierDictionary objectForKey:@"city_other"];
    sup.locationLat = [supplierDictionary objectForKey:@"lat"];
    sup.locationLng = [supplierDictionary objectForKey:@"lng"];
    sup.phoneNumber = [supplierDictionary objectForKey:@"phone_number"];
    sup.profilePicURL = [supplierDictionary objectForKey:@"profile_pic"];
    sup.rating = [supplierDictionary objectForKey:@"user_rating"];
    sup.labourRate = [supplierDictionary objectForKey:@"rate"];
    
    sup.province = [CacheManager provinceFromCacheWithID:[[supplierDictionary objectForKey:@"province"] intValue]];
    sup.service = [CacheManager serviceFromCacheWithID:[[supplierDictionary objectForKey:@"service"] intValue]];
    
    NSDictionary *companyDict = [supplierDictionary objectForKey:@"company"];
    Company *comp = [Company new];
    comp.name = [companyDict objectForKey:@"name"];
    comp.isVatRegistered = [[companyDict objectForKey:@"vat_registered"] boolValue];
    sup.company = comp;
    
    NSDictionary *userDict = [supplierDictionary objectForKey:@"user"];
    User *u = [User new];
    u.email = [userDict objectForKey:@"email"];
    u.name = [NSString stringWithFormat:@"%@ %@",[userDict objectForKey:@"first_name"],[userDict objectForKey:@"last_name"]];
    u.rating = [supplierDictionary objectForKey:@"user_rating"];
    sup.user = u;
    
    NSMutableArray *problems = [NSMutableArray array];
    
    for (NSDictionary *problem in [dict objectForKey:@"possible_problems"])
    {
        Problem *pp = [Problem new];
        pp.problemId = [problem objectForKey:@"id"];
        pp.problemDescription = [problem objectForKey:@"description"];
        pp.estimatedPrice = [problem objectForKey:@"estimate_price"];
        [problems addObject:pp];
    }
    
    sup.possibleProblems = problems;
    
    return sup;
}

- (Quote*)createQuoteFromDictionary:(NSDictionary*)quoteDictionary
{
    Quote *quote = [Quote new];
    quote.quoteID = [quoteDictionary objectForKey:@"id"];
    quote.jobID = [quoteDictionary objectForKey:@"job"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SS.SSS'Z'"];
    
    quote.createdDate = [dateFormatter dateFromString:[quoteDictionary objectForKey:@"created_at"]];
    
    quote.labourRate = [quoteDictionary objectForKey:@"labour_rate"];
    quote.labourHours = [quoteDictionary objectForKey:@"labour_hours"];
    quote.jobDescription = [quoteDictionary objectForKey:@"job_description"];
    
    NSArray *materials = [quoteDictionary objectForKey:@"quote_items"];
    
    quote.materials = [NSMutableArray array];
    
    for (NSDictionary *matDict in materials)
    {
        Material *material = [Material new];
        material.name = [matDict objectForKey:@"description"];
        material.costPerUnit = [matDict objectForKey:@"unit_price"];
        material.quantity = [matDict objectForKey:@"num_units"];
        [quote.materials addObject:material];
    }
    
    NSArray *rejectionReasons = [quoteDictionary objectForKey:@"rejection_reasons"];
    
    quote.rejectReasons = [NSMutableArray array];
    
    for (NSDictionary *reasonDict in rejectionReasons)
    {
        Reason *reason = [Reason new];
        reason.reasonDescription = [reasonDict objectForKey:@"description"];
        reason.reasonId = [reasonDict objectForKey:@"id"];
        [quote.rejectReasons addObject:reason];
    }
    
    return quote;
}

- (void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message
{
//    if ([title isEqualToString:@"Login"])
//    {
        [AlertViewManager showAlertWithTitle:title
                                     message:message
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil
                        buttonSelectionBlock:nil];
//    }
//    else
//    {
//        [AlertViewManager showAlertWithTitle:@""
//                                 message:@"Internet Connectivity lost, please try again"
//                       cancelButtonTitle:@"OK"
//                       otherButtonTitles:nil
//                    buttonSelectionBlock:nil];
//    }
}

@end
