//
//  Handlers.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Supplier.h"
#import "Quote.h"
#import "Job.h"

#ifndef Handlers_h
#define Handlers_h

typedef void (^SuccessHandler)(id result);
typedef void (^SuccessTrueFalseWithResultHandler)(BOOL success, id result);
typedef void (^SuccessImageHandler)(UIImage *image);
typedef void (^SuccessArrayHandler)(NSMutableArray *results);
typedef void (^SuccessImageHandler)(UIImage *image);
typedef void (^SuccessSupplierHandler)(Supplier *supplier);
typedef void (^SuccessLocationHandler)(CLLocationCoordinate2D coordinates);
typedef void (^SuccessTrueFalseHandler)(BOOL success);
typedef void (^SuccessQuoteHandler)(Quote *quote);
typedef void (^SuccessJobHandler)(Job *job);


typedef void (^SuccessPagingArrayHandler)(NSString *prev, NSString *next, NSMutableArray *results);
typedef void (^FailureHandler)(NSError *error);

#endif