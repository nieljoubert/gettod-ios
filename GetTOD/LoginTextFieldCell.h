//
//  LoginTextFieldCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "LoginTextFieldView.h"

@interface LoginTextFieldCell : GTBoldTableViewCell

@property (nonatomic, strong) GTTextField *textField;

- (void)setupCellWithTextFieldView:(LoginTextFieldView*)textfield;

- (void)setErrorTo:(NSString*)text;
- (void)dismissError;

@end
