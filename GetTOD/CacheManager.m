//
//  CacheManager.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CacheManager.h"
#import "SettingsManager.h"
#import "NSData+AES256.h"

static NSFileManager *fileManager = nil;
static NSURL *cachesURL = nil;

NSString *cacheKeyToken = @"b371iu98ey";
NSString *cacheKeyProvinces = @"k0i0tmpnkg";
NSString *cacheKeySuppliers = @"qse1i4q4rz";
NSString *cacheKeyCompanies = @"32vcp9buxt";
NSString *cacheKeyServices = @"2b3xc5a9nh";
NSString *cacheKeyCompanyEntities = @"opvf8ldoq2";
NSString *cacheKeyCities = @"9iwphe5kmh";
NSString *cacheKeyCards = @"l1j1unqolh";
NSString *cacheKeyRejectReasons = @"c482jv09fz";

@implementation CacheManager

+ (void) initialize
{
	fileManager = [[NSFileManager alloc] init];
	
	cachesURL = [fileManager URLForDirectory:NSCachesDirectory
									inDomain:NSUserDomainMask
						   appropriateForURL:nil
									  create:YES
									   error:NULL];
}

+ (NSString*) filePathForRootDirectory: (NSString*) rootDirectory directory: (NSString*) directory fileName: (NSString*) file
{
	NSString *path;
	
	if ([directory isEqualToString:@""])
	{
		path = rootDirectory;
	}
	else
	{
		path = [NSString stringWithFormat:@"%@/%@", rootDirectory, directory];
	}
 
	[fileManager createDirectoryAtPath:path
		   withIntermediateDirectories:YES
							attributes:nil
								 error:NULL];
	
	return [NSString stringWithFormat:@"%@/%@.cache", path, file];
}

+(void)cacheUserToken:(NSString *)token
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"" fileName:cacheKeyToken];
	
	[NSKeyedArchiver archiveRootObject:token toFile:path];
}

+(NSString*)tokenFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"" fileName:cacheKeyToken];
	
	NSString *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = @"";
	}
	
	return cache;
}

+(void)cacheProvinces:(NSArray*)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Provinces" fileName:cacheKeyProvinces];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)provincesFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Provinces" fileName:cacheKeyProvinces];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	
	return cache;
}

+(Province *)provinceFromCacheWithID:(int)ident
{
	NSArray *cache = [CacheManager provincesFromCache];
	
	for (Province* object in cache)
	{
		if ([object.provinceId intValue] == ident)
		{
			return object;
			break;
		}
	}
	
	return nil;
}

+(void)cacheSuppliers:(NSArray*)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Suppliers" fileName:cacheKeySuppliers];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)suppliersFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Suppliers" fileName:cacheKeySuppliers];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	
	return cache;
}

+(NSMutableArray*)suppliersFromCacheForService:(NSNumber*)serviceId
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Suppliers" fileName:cacheKeySuppliers];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	else
	{
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"service.serviceId = %@", serviceId];
		NSArray *filterArray = [cache filteredArrayUsingPredicate:predicate];
		return [filterArray mutableCopy];
	}
	
	return cache;
}





+(void)updateSupplier:(Supplier*)updatedSupplier
{
	NSMutableArray *suppliers = [CacheManager suppliersFromCache];
	
	NSMutableArray *arrayForLoop = [suppliers copy];
	
	for (Supplier *supplier in suppliers)
	{
		if (supplier.supplierId == updatedSupplier.supplierId)
		{
			[suppliers replaceObjectAtIndex:[arrayForLoop indexOfObject:supplier] withObject:updatedSupplier];
			break;
		}
	}
	
	[CacheManager cacheSuppliers:suppliers];
}

+(Supplier *)supplierFromCacheWithID:(int)ident
{
	NSArray *cache = [CacheManager suppliersFromCache];
	
	for (Supplier* object in cache)
	{
		if ([object.supplierId intValue] == ident)
		{
			return object;
			break;
		}
	}
	
	return nil;
}

+(void)cacheCompanies:(NSArray*)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Companies" fileName:cacheKeyCompanies];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)companiesFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Companies" fileName:cacheKeyCompanies];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	
	return cache;
}

+(void)cacheServices:(NSArray*)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Services" fileName:cacheKeyServices];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)servicesFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Services" fileName:cacheKeyServices];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	
	return cache;
}

+(Service *)serviceFromCacheWithID:(int)ident
{
	NSArray *cache = [CacheManager servicesFromCache];
	
	for (Service* object in cache)
	{
		if ([object.serviceId intValue] == ident)
		{
			return object;
			break;
		}
	}
	
	return nil;
}

+(void)cacheCompanyEntities:(NSArray*)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Entities" fileName:cacheKeyCompanyEntities];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)companyEntitiesFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Entities" fileName:cacheKeyCompanyEntities];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	
	return cache;
}

+(Entity *)entityFromCacheWithID:(int)ident
{
	NSArray *cache = [CacheManager companyEntitiesFromCache];
	
	for (Entity* object in cache)
	{
		if ([object.entityId intValue] == ident)
		{
			return object;
			break;
		}
	}
	
	return nil;
}

+(void)cacheCities:(NSArray*)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Cities" fileName:cacheKeyCities];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)citiesFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Cities" fileName:cacheKeyCities];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	
	return cache;
}

+(City *)cityFromCacheWithID:(int)ident
{
	NSArray *cache = [CacheManager citiesFromCache];
	
	for (City* object in cache)
	{
		if ([object.cityId intValue] == ident)
		{
			return object;
			break;
		}
	}
	
	return nil;
}

+(void)cacheCard:(Card *)card
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@".Cards" fileName:cacheKeyCards];
	
	NSMutableArray *cachedCards = [CacheManager cardsFromCache];
	
	BOOL isNew = YES;
	
	for (Card *c in [cachedCards copy])
	{
		NSLog(@"\n%@\n%@",c.number,card.number);
		if ([c.number isEqualToString:card.number])
		{
			isNew = NO;
		}
	}
	
	if (isNew)
	{
		[cachedCards addObject:card];
	}
	
//	NSData *enc = [[NSKeyedArchiver archivedDataWithRootObject:cachedCards] AES256DecryptWithKey:[SettingsManager manager].cardEncryptionKey];
	
	[NSKeyedArchiver archiveRootObject:cachedCards toFile:path];
}

+(void)deleteCard:(Card *)card;
{
	NSMutableArray *cachedCards = [CacheManager cardsFromCache];
	
	for (Card *c in [cachedCards copy])
	{
		if ([c.number isEqualToString:card.number])
		{
			[cachedCards removeObject:c];
		}
	}
	
	[CacheManager cacheCards:cachedCards];
}

+(void)cacheCards:(NSArray *)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@".Cards" fileName:cacheKeyCards];
	
//	NSData *enc = [[NSKeyedArchiver archivedDataWithRootObject:data] AES256DecryptWithKey:[SettingsManager manager].cardEncryptionKey];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)cardsFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@".Cards" fileName:cacheKeyCards];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	NSMutableArray *results = [NSMutableArray array];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
		results = [NSMutableArray array];
	}
	else
	{
//		NSData *dec = [[NSKeyedUnarchiver unarchiveObjectWithFile:path] AES256DecryptWithKey:[SettingsManager manager].cardEncryptionKey];
//		
//		NSMutableArray *decArray = [[NSKeyedUnarchiver unarchiveObjectWithFile:path] AES256DecryptWithKey:[SettingsManager manager].cardEncryptionKey];
		
//		for(Card *object in dec)
//		{
//			Card *card = (Card*)[object AES256DecryptWithKey:[SettingsManager manager].cardEncryptionKey];
//			[results addObject:card];
//		}
	}
	
	
	return cache;
}

+(void)cacheRejectReasons:(NSArray*)data
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Texts" fileName:cacheKeyRejectReasons];
	
	[NSKeyedArchiver archiveRootObject:data toFile:path];
}

+(NSMutableArray*)rejectResonsFromCache
{
	NSString *path = [self filePathForRootDirectory:[cachesURL path] directory:@"Texts" fileName:cacheKeyRejectReasons];
	
	NSMutableArray *cache = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
	
	if (cache == nil)
	{
		cache = [NSMutableArray array];
	}
	
	return cache;
}




@end
