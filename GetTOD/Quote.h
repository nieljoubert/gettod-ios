//
//  Quote.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/24.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Material.h"

@interface Quote : NSObject

@property (nonatomic, strong) NSNumber *quoteID;
@property (nonatomic, strong) NSNumber *jobID;
@property (nonatomic, strong) NSDate *createdDate;
@property (nonatomic, strong) NSNumber *labourRate;
@property (nonatomic, strong) NSNumber *labourHours;
@property (nonatomic, strong) NSString *jobDescription;
@property (nonatomic, strong) NSMutableArray *materials;
@property (nonatomic, strong) NSMutableArray *rejectReasons;

- (NSNumber*)quoteAmount;

@end
