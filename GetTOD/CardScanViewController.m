//
//  CardScanViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CardScanViewController.h"

@interface CardScanViewController ()

@end

@implementation CardScanViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// AS VIEW
	if (![CardIOUtilities canReadCardWithCamera])
	{
		// Hide your "Scan Card" button, or take other appropriate action...
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[CardIOUtilities preload];
	
//	The first time that you create either a CardIOPaymentViewController or a CardIOView, the card.io SDK must load resources, which can result in a noticeable delay. To avoid this delay you may optionally call [CardIOUtilities preload] in advance, so that this resource loading occurs in advance on a background thread.
	
//	When applicationDidEnterBackground: method returns, the snapshot of the application user interface is taken, and it’s used for transition animations and stored in the filesystem. This method should be overridden and all the sensitive information in the user interface should be removed before it returns. This way the snapshot will not contain them.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions - 
- (IBAction)scanCard:(id)sender
{
	CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
	[self presentViewController:scanViewController animated:YES completion:nil];
}

- (IBAction)scanCardAsView:(id)sender
{
	CardIOView *cardIOView = [[CardIOView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
	cardIOView.delegate = self;
	
	[self.view addSubview:cardIOView];
}

- (IBAction)cancelScanCard:(id)sender
{
	//[cardIOView removeFromSuperview];
}

#pragma mark - Delegates -

#pragma mark - CardIO -
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
	NSLog(@"User canceled payment info");
	
	[scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
	NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
	
	[scanViewController dismissViewControllerAnimated:YES completion:nil];
}

//AS View
- (void)cardIOView:(CardIOView *)cardIOView didScanCard:(CardIOCreditCardInfo *)info
{
	if (info)
	{
		NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
	}
	else
	{
		NSLog(@"User cancelled payment info");
	}
	
	[cardIOView removeFromSuperview];
}


@end
