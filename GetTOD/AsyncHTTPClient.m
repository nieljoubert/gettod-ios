//
//  AsyncHTTPClient.m
//  getTOD
//
//  Created by Niel Joubert on 2015/11/24.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AsyncHTTPClient.h"
#import "Constants.h"

@implementation AsyncHTTPClient

+ (AsyncHTTPClient *)sharedAsyncClient
{
    static AsyncHTTPClient *_sharedAsyncClient = nil;
    
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{ _sharedAsyncClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:BASEURLSTRING]]; });
    return _sharedAsyncClient;
}

@end
