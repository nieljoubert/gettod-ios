//
//  SupplierHomeMapViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/06.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SupplierHomeMapViewController.h"
#import "NewRequestViewController.h"
#import "SupplierQuoteViewController.h"
#import "OfflineViewController.h"
#import "ImageHelper.h"

@interface SupplierHomeMapViewController ()

@property (nonatomic, strong) GMSMapView *mapView;

@end

@implementation SupplierHomeMapViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.title = @"";
	
	UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"gear"] style:UIBarButtonItemStylePlain target:self action:@selector(launchSettings)];
	self.navigationItem.leftBarButtonItem = settings;
	self.navigationItem.hidesBackButton = YES;
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self onlineView]];
	
	[self addBottomToolbar];
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animateMap) name:@"LocationUpdated" object:nil];
	
	if ([SettingsManager manager].supplierStatus == SupplierStatusOffline)
	{
		[self launchOfflineViewController:NO];
	}
//	else
//	{
//		[SettingsManager manager].supplierStatus = SupplierStatusOnline;
//		[SettingsManager save];
//	}
	
	[[APIManager manager] getUserDetailsWithSuccess:^(id result) {
		
		[SettingsManager manager].gotUserDetailsOnce = YES;
		[SettingsManager save];
		
		if ([SettingsManager manager].profileImageURL)
		{
			[ImageHelper downloadImage:[SettingsManager manager].profileImageURL withName:PROFILE_IMAGE_NAME];
		}
		
		self.title = [NSString stringWithFormat:@"Hi, %@!",[SettingsManager manager].firstName];
		
	} failure:^(NSError *error) {
		
		
	}];
	
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[SettingsManager manager].currentLocationCoords.latitude
															longitude:[SettingsManager manager].currentLocationCoords.longitude
																 zoom:15];
	
	self.mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, SCREEN_HEIGHT - NAVBAR_HEIGHT - 55) camera:camera];
	self.mapView.backgroundColor = [UIColor clearColor];
	self.mapView.mapType = kGMSTypeNormal;
	self.mapView.myLocationEnabled = YES;
	[self.view addSubview:self.mapView];
	
	self.mapView.accessibilityElementsHidden = NO;
	self.mapView.settings.tiltGestures = YES;
	self.mapView.settings.scrollGestures = YES;
	self.mapView.settings.rotateGestures = YES;
	self.mapView.settings.zoomGestures = YES;
	self.mapView.settings.compassButton = NO;
	self.mapView.settings.myLocationButton = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - Private Methods -
- (void)animateMap
{
	[self.mapView animateToLocation:[SettingsManager manager].currentLocationCoords];
}

- (UIView*)onlineView
{
	UIView *onlineView = [UIView new];
	
	UILabel *title = [UILabel new];
	title.backgroundColor = [UIColor clearColor];
	title.font = [UIFont fontWithName:FONT size:16];
	title.text = @"Online";
	title.textColor = ONLINE_GREEN_COLOR;
	[title sizeToFit];
	
	UIImageView *dot = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"online_dot"]];
	dot.contentMode = UIViewContentModeScaleAspectFit;
	
	[onlineView addSubview:dot];
	[onlineView addSubview:title];
	
	dot.frame = CGRectMake(0, 0, dot.frame.size.width, 20);
	title.frame = CGRectMake(dot.frame.size.width+3, 0, title.frame.size.width, 20);
	
	onlineView.frame = CGRectMake(0, 0, dot.frame.size.width+title.frame.size.width, 20);
	
	return onlineView;
}

- (void)addBottomToolbar
{
	UIView *bottomBar = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-NAVBAR_HEIGHT-55, SCREEN_WIDTH, 55)];
	bottomBar.backgroundColor = [UIColor whiteColor];
	
	UIButton *goOfflineButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	goOfflineButton.titleLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:14];
	goOfflineButton.tintColor = PINK_COLOR;
	[goOfflineButton setTitle:@"Go Offline" forState:UIControlStateNormal];
	[goOfflineButton addTarget:self action:@selector(goOffline) forControlEvents:UIControlEventTouchUpInside];
	
	UIButton *currentLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
	UIImage *locationImage = [UIImage imageNamed:@"my_location"];
	
	[currentLocationButton setImage:locationImage forState:UIControlStateNormal];
	[currentLocationButton addTarget:self action:@selector(gotoCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
	
	[bottomBar addSubview:goOfflineButton];
	[bottomBar addSubview:currentLocationButton];
	
	goOfflineButton.frame = CGRectMake(10, 0, 70, 55);
	currentLocationButton.frame = CGRectMake(SCREEN_WIDTH-locationImage.size.width-10, 10, locationImage.size.width, locationImage.size.height);
	[self.view addSubview:bottomBar];
	
}

- (void)gotoCurrentLocation
{
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		[self.mapView animateToLocation:location.coordinate];
	}];
}

- (void)goOffline
{
	[SettingsManager manager].supplierStatus = SupplierStatusOffline;
	[SettingsManager manager].appState = AppStateOffline;
	[SettingsManager save];
	
	[self showLoader];
	
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		[self hideLoader];
		[self launchOfflineViewController:NO];
	}];
}

- (void)launchOfflineViewController:(BOOL)animated
{
	OfflineViewController *controller = [[OfflineViewController alloc] initWithNibName:@"OfflineViewController" bundle:nil];
	[self.navigationController pushViewController:controller animated:animated];
}

- (void)launchSettings
{
	SettingsViewController *controller = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	controller.delegate = self;
	UINavigationController *settingsNavigationController = [[UINavigationController alloc] initWithRootViewController:controller];
	[Flurry logAllPageViews:settingsNavigationController];
	[self.navigationController presentViewController:settingsNavigationController animated:YES completion:nil];
}

-(void)settingsDismissed:(SettingsViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
}

@end
