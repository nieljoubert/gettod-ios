//
//  GTTableSectionHeaderView.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTTableSectionHeaderView : UIView

- (id)initWithTitle:(NSString*)title;

@end
