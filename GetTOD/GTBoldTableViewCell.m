//
//  GTBoldTableViewCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"

@implementation GTBoldTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
	if (self)
	{
		self.textLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.textLabel.textColor = DARK_TEXT_COLOR;
		self.selectionStyle = UITableViewCellSelectionStyleDefault;
		self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure"]];
	}
	
	return self;
}

@end