//
//  Job.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Supplier.h"
#import "Problem.h"
#import "Quote.h"
#import "Reason.h"

@interface Job : NSObject

@property (nonatomic, strong) NSNumber *jobId;
@property (nonatomic, strong) Supplier *supplier;
@property (nonatomic, strong) Problem *problem;
@property (nonatomic, strong) Quote *quote;
@property (nonatomic, strong) User *customer;
@property (nonatomic, strong) NSNumber *customerId;

@property (nonatomic, strong) NSString *additionalInstructions;
@property (nonatomic, strong) NSNumber *calculatedTime;
@property (nonatomic, strong) NSNumber *calculatedDistance;
@property (nonatomic, strong) NSDate *createdDate;
@property (nonatomic, strong) NSNumber *jobLocationLat;
@property (nonatomic, strong) NSNumber *jobLocationLng;

@property (nonatomic, strong) NSMutableArray *cancelReasons;

- (CLLocationCoordinate2D)jobLocationCoordinates;

@property (nonatomic, strong) NSDate *cancelledAtDate;
@property (nonatomic, strong) NSDate *arrivedAtDate;
@property (nonatomic, strong) NSDate *updatedDate;
@property (nonatomic, strong) NSNumber *quotePrice;





@end
