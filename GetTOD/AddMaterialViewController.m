//
//  AddMaterialViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AddMaterialViewController.h"

@interface AddMaterialViewController ()

@property (nonatomic, strong) GTTextField *amountField;
@property (nonatomic, strong) GTTextField *materialNameField;
@property (nonatomic, strong) GTTextFieldCell *materialNameCell;
@property (nonatomic, strong) CostTableViewCell *amountCell;

@property (nonatomic, assign) int quantity;

@property (assign, nonatomic) NSUInteger maximumFractionDigits;
@property (strong, nonatomic) NSString *decimalSeparator;

@end

@implementation AddMaterialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.materialNameField = [GTTextField new];
	self.materialNameField.delegate = self;
	[self.materialNameField setKeyboardToolbar:self.keyboardToolbar];
	
	[self addFormField:self.materialNameField];
	
	self.navigationController.navigationBarHidden = NO;
	
	self.title = @"Add Material Cost";
	
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancel)];
	
	if (self.material)
	{
		self.quantity = [self.material.quantity intValue];
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
	}
	else
	{
		self.quantity = 1;
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleDone target:self action:@selector(add)];
	}
	
	NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc]init];
	[numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	[numberFormatter setLocale:[NSLocale currentLocale]] ;
	self.maximumFractionDigits = numberFormatter.maximumFractionDigits;
	self.decimalSeparator = numberFormatter.decimalSeparator;
}

- (void)cancel
{
	[self.delegate addMaterialCanceled:self];
}

- (void)add
{
	[self dismissKeyboard];
	
	if ([self isScreenFieldsValid])
	{
		Material *material = [Material new];
		material.costPerUnit = [NSNumber numberWithFloat:[self.amountField.text floatValue]];
		material.name = self.materialNameField.text;
		material.quantity = [NSNumber numberWithInt:self.quantity];
		
		[self.delegate addedMaterial:material wasEditing:NO fromController:self];
	}
	else
	{
		[self showScreenError];
	}
}

- (void)save
{
	[self dismissKeyboard];
	
	if ([self isScreenFieldsValid])
	{
		self.material.costPerUnit = [NSNumber numberWithFloat:[self.amountField.text floatValue]];
		self.material.name = self.materialNameField.text;
		self.material.quantity = [NSNumber numberWithInt:self.quantity];
		
		[self.delegate addedMaterial:self.material wasEditing:YES fromController:self];
	}
	else
	{
		[self showScreenError];
	}
}

#pragma mark - Delegates -
-(void)quantityChanged:(int)quantity
{
	self.quantity = quantity;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
	if (textField == self.materialNameField)
	{
		[self isNameValid];
	}
	else
	{
		[self isAmountValid];
	}
	return TRUE;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
	if (textField == self.amountField)
	{
		if (textField.text.length == 0)
		{
			textField.text = @"0.00";
		}
	}
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if (textField == self.amountField)
	{
		NSMutableString* currentString = [NSMutableString stringWithString:textField.text];
		
		[currentString replaceCharactersInRange:range withString:string];
		
		NSMutableString *newDecimalSeparator = [[self.decimalSeparator stringByReplacingOccurrencesOfString:self.decimalSeparator withString:@"."] mutableCopy];
		
		NSString *amountWithoutSeparator = [[currentString stringByReplacingOccurrencesOfString:newDecimalSeparator withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [currentString length])] mutableCopy];
		
		NSInteger currentValue = [amountWithoutSeparator integerValue];
		
		NSString *format = [NSString stringWithFormat:@"%%.%ldf", (long)self.maximumFractionDigits];
		
		double minorUnitsPerMajor = pow(10, self.maximumFractionDigits);
		
		NSString *newString = [NSString stringWithFormat:format, currentValue/minorUnitsPerMajor];
		
		if (newString.length <= 10)
			textField.text = newString;
		
		return FALSE;
	}
	
	return TRUE;
}

#pragma mark - Validation -

- (BOOL)isScreenFieldsValid
{
	[super isScreenFieldsValid];
	
	BOOL isValid = YES;

	isValid =  [self isNameValid] && isValid;
	isValid =  [self isAmountValid] && isValid;

	return isValid;
}


- (BOOL)isNameValid
{
	if (self.materialNameField.text.length > 0)
	{
		[self.materialNameCell dismissError];
		[self dismissScreenError:self.materialNameField];
		return YES;
	}
	else
	{
		[self.materialNameCell setError];
		[self setScreenError:self.materialNameField message:@"Please enter a material name"];
		return NO;
	}
}

- (BOOL)isAmountValid
{
	if ([self.amountField.text floatValue] > 0.00)
	{
		[self.amountCell dismissError];
		[self dismissScreenError:self.amountField];
		return YES;
	}
	else
	{
		[self.amountCell setError];
		[self setScreenError:self.amountField message:@"Please enter an amount"];
		return NO;
	}
}


#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		self.materialNameCell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		self.materialNameCell.textField = self.materialNameField;
		[self.materialNameCell setupCellWithPlaceholder:@"Material Name"];
		
		if (self.material)
		{
			self.materialNameField.text = self.material.name;
		}
		
		return self.materialNameCell;
	}
	else if (indexPath.section == 1)
	{
		self.amountCell = [tableView dequeueReusableCellWithIdentifier:@"CostTableViewCell"];
		[self.amountCell setupCell];
		
		self.amountField = self.amountCell.amountField;
		self.amountField.delegate = self;
		[self.amountField setKeyboardToolbar:self.keyboardToolbar];
		[self addFormField:self.amountField];
		
		if (self.material)
		{
			self.amountField.text = [NSString stringWithFormat:@"%.2f",[self.material.costPerUnit floatValue]];
		}
		
		return self.amountCell;
	}
	else if (indexPath.section == 2)
	{
		QuantityAdjustmentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuantityAdjustmentCell"];
		[cell setupCellWithSubtitle:NO withQuantity:self.quantity];
		cell.delegate = self;
		return cell;
	}
	
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"MATERIAL"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"COST PER UNIT"];
	}
	else if (section == 2)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"QUANTITY"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		return 50;
	}
	else if (indexPath.section == 1)
	{
		return 50;
	}
	else if (indexPath.section == 2)
	{
		return 67;
	}
	
	return 44;
}
@end
