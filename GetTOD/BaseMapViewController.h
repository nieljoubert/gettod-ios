//
//  ParentMapViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "RootViewController.h"
#import "Constants.h"
#import "LocationManager.h"

@protocol BaseMapViewDelegate;

@interface BaseMapViewController : RootViewController <GMSMapViewDelegate>

@property (nonatomic, strong) GMSMapView *mapView;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) GMSAddress *currentAddress;
@property (nonatomic, strong) GMSMarker *marker;
@property (nonatomic, assign) bool firstRunDone;

- (void)addMarkerToLocation:(CLLocationCoordinate2D)coordinates;

@property(nonatomic, weak) id <BaseMapViewDelegate> delegate;

@end


@protocol BaseMapViewDelegate <NSObject>
-(void) addressSelectedFromMap:(GMSAddress*)address withCoordinate:(CLLocationCoordinate2D)coordinate;
-(void) updatedLocation:(CLLocation*)location;
-(void) mapViewFinishedLoading;

@end
