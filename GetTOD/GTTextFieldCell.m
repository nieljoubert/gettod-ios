//
//  GTTextFieldCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTextFieldCell.h"

@implementation GTTextFieldCell

- (void)setupCellWithPlaceholder:(NSString*)placeholder
{
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	
	self.textField.placeholder = placeholder;
	
	[self.contentView addSubview:self.textField];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[text]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:@{@"text":self.textField}]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[text]|"
																			 options:0
																			 metrics:nil
																			   views:@{@"text":self.textField}]];
}

- (void)setError
{
	self.backgroundColor = ERROR_BACKGROUND_COLOR;
}

- (void)dismissError
{
	self.backgroundColor = [UIColor whiteColor];
}

@end
