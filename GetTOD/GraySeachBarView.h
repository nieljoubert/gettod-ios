//
//  GraySeachBarView.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/13.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTTextField.h"

@protocol GraySeachBarViewDelegate;

@interface GraySeachBarView : UIView <UITextFieldDelegate>

@property(nonatomic, weak) id <GraySeachBarViewDelegate> delegate;
@property(nonatomic, strong) GTTextField *textField;
@end

@protocol GraySeachBarViewDelegate <NSObject>
-(void) searchTextEntered:(NSString*)searchText;
-(void) searchButtonTapped;

@end
