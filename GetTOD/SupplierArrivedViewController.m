//
//  SupplierArrivedViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SupplierArrivedViewController.h"
#import "ClientQuoteViewController.h"

@interface SupplierArrivedViewController ()
@property (weak, nonatomic) IBOutlet UILabel *supplierNameLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) GMSMapView *map;
@end

@implementation SupplierArrivedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
		
	self.title = @"Your Supplier Has Arrived";
	
	self.navigationItem.hidesBackButton = YES;
	self.navigationItem.leftBarButtonItem = nil;
	
	self.bottomView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.bottomView.layer.borderWidth = 0.5;
	
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[SettingsManager manager].currentJob.jobLocationLat doubleValue]
															longitude:[[SettingsManager manager].currentJob.jobLocationLng doubleValue]
																 zoom:16];
	
	self.map = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height-NAVBAR_HEIGHT) camera:camera];
	self.map.backgroundColor = [UIColor clearColor];
	self.map.camera = camera;
	self.map.myLocationEnabled = YES;
	self.map.mapType = kGMSTypeNormal;
	self.map.accessibilityElementsHidden = NO;
	self.map.settings.tiltGestures = YES;
	self.map.settings.scrollGestures = YES;
	self.map.settings.rotateGestures = YES;
	self.map.settings.zoomGestures = YES;
	self.map.settings.compassButton = NO;
	self.map.settings.myLocationButton = YES;
	
	[self.mapView addSubview:self.map];
	
	GMSMarker *marker = [GMSMarker markerWithPosition:[SettingsManager manager].currentJob.jobLocationCoordinates];
	marker.icon = [UIImage imageNamed:@"location_marker_gray_large"];
	marker.appearAnimation = kGMSMarkerAnimationNone;
	marker.flat = YES;
	marker.map = self.map;
	
	self.supplierNameLabel.text = [SettingsManager manager].currentJob.supplier.company.name;
}

- (void) viewWillDisappear:(BOOL) animated
{
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController])
    {
        if (self.navigationController)
        {
            self.navigationController.delegate = nil;
        }
    }
}

-(void) dealloc
{
    if (self.navigationController.delegate == self)
    {
        self.navigationController.delegate = nil;
    }
}

- (IBAction)callSupplier:(id)sender
{
	NSString *cleanedString = [[[SettingsManager manager].currentJob.supplier.phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
	
	// Prompt but return
	NSURL *phoneNumberURL = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:cleanedString]];
	
	// Direct call
	NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
	
	[[UIApplication sharedApplication] openURL:phoneNumberURL];
}


@end
