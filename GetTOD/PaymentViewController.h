//
//  PaymentViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"

@interface PaymentViewController : BaseTableViewController

@property (nonatomic, strong) NSMutableArray *cards;

@end
