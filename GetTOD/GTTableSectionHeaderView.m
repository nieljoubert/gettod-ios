//
//  GTTableSectionHeaderView.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableSectionHeaderView.h"
#import "Constants.h"

@implementation GTTableSectionHeaderView

- (id)initWithTitle:(NSString*)title
{
	self = [super init];
	
	if (self)
	{
		self.backgroundColor = LIGHT_BACKGROUND_COLOR;
		
		UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CELL_PADDING, 0, SCREEN_WIDTH-CELL_PADDING, 24)];
		
		NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[title uppercaseString]];
		[attributedString addAttribute:NSKernAttributeName
								 value:@(1.2)
								 range:NSMakeRange(0,title.length)];
		
		titleLabel.attributedText = attributedString;
		titleLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:8];
		titleLabel.textColor = GRAY_COLOR;
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textAlignment = NSTextAlignmentLeft;
		
		[self addSubview:titleLabel];
		
		self.frame = CGRectMake(0, 0, SCREEN_WIDTH, 24);
		
		
		UIView *topGray = [[UIView alloc] initWithFrame:CGRectMake(0, -0.5, SCREEN_WIDTH, 0.5)];
		topGray.backgroundColor = [UIColor lightGrayColor];
		UIView *botGray = [[UIView alloc] initWithFrame:CGRectMake(0, 24, SCREEN_WIDTH, 0.5)];
		botGray.backgroundColor = [UIColor lightGrayColor];
		
		[self addSubview:topGray];
		[self addSubview:botGray];
	}
	
	return self;
}

@end
