//
//  GTMultiLineTableViewCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/05/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTMultiLineTableViewCell.h"

@interface GTMultiLineTableViewCell ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *multiLineLabel;

@end

@implementation GTMultiLineTableViewCell

- (void)setupCellWithTitle:(NSString*)title timeText:(NSString*)time andText:(NSString*)text
{
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	
	if (self.titleLabel == nil)
	{
		self.titleLabel = [UILabel new];
		self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.titleLabel.font = [UIFont fontWithName:FONT size:16];
		self.titleLabel.textColor = [UIColor whiteColor];
		self.titleLabel.numberOfLines = 1;
		
		[self.contentView addSubview:self.titleLabel];
	}
	
	if (self.timeLabel == nil)
	{
		self.timeLabel = [UILabel new];
		self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.timeLabel.font = [UIFont fontWithName:FONT size:15];
		self.timeLabel.textColor = [UIColor whiteColor];
		[self.contentView addSubview:self.timeLabel];
	}
	
	if (self.multiLineLabel == nil)
	{
		self.multiLineLabel = [UILabel new];
		self.multiLineLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.multiLineLabel.font = [UIFont fontWithName:FONT size:14];
		self.multiLineLabel.textColor = [UIColor whiteColor];
		self.multiLineLabel.numberOfLines = 0;
		self.multiLineLabel.lineBreakMode = NSLineBreakByWordWrapping;
		
		[self.contentView addSubview:self.multiLineLabel];
	}
	
	self.titleLabel.text = title;
	self.timeLabel.text = time;
	self.multiLineLabel.text = text;
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[title]-20-|"
																			 options:0
																			 metrics:nil
																			   views:@{@"title":self.titleLabel}]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[time]-20-|"
																			 options:0
																			 metrics:nil
																			   views:@{@"time":self.timeLabel}]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[text]-20-|"
																			 options:0
																			 metrics:nil
																			   views:@{@"text":self.multiLineLabel}]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[title]-5-[time]-5-[text]-5-|"
																			 options:0
																			 metrics:nil
																			   views:@{@"time":self.timeLabel,@"title":self.titleLabel,@"text":self.multiLineLabel}]];
}

@end
