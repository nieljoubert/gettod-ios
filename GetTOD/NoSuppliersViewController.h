//
//  NoSuppliesViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/05/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"

@interface NoSuppliersViewController : RootViewController

@property (nonatomic, strong) NSNumber *serviceTypeID;

@end
