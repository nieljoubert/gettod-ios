//
//  CardNumberCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CardNumberCell.h"

@interface CardNumberCell()

@property (nonatomic, strong) NSString *previousTextFieldContent;
@property (nonatomic, strong) UITextRange *previousSelection;

@property (nonatomic, strong) UIImageView *cardImage;

@end

@implementation CardNumberCell

- (void)setupCellWithCardNumber:(NSString*)number
{
	self.accessoryView = nil;
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	
	if (self.cardImage == nil)
	{
		self.cardImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"plain_card"]];
		self.cardImage.translatesAutoresizingMaskIntoConstraints = NO;
		self.cardImage.backgroundColor = [UIColor clearColor];
		self.cardImage.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.cardImage];
	}
	
	self.cardNumber.placeholder = @"Credit Card Number";
	self.cardNumber.keyboardType = UIKeyboardTypeNumberPad;
	[self.contentView addSubview:self.cardNumber];
	
	self.cardNumber.text = number;
		
	NSString *cardNumber = [self.cardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
	
	if (cardNumber.length == 16)
	{
		[self getCardLogo];
	}
	
	[self reformatAsCardNumber:self.cardNumber];
	
	NSDictionary *views = @{@"image":self.cardImage,
							@"number":self.cardNumber};
	
	NSDictionary *metrics = @{@"imageW":[NSNumber numberWithFloat:self.cardImage.frame.size.width],
							  @"padding":@CELL_PADDING};

	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[number]-[image(==25)]-padding-|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[number]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
}

- (void)getCardLogo
{
	NSString *stringWithoutDashes = [self.cardNumber.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
	NSString *stringWithoutSpaces = [stringWithoutDashes stringByReplacingOccurrencesOfString:@" " withString:@""];

	NSString *trimmedString = [stringWithoutSpaces stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	NSString *firstNumber = [trimmedString substringToIndex:1];
	
	if ([firstNumber isEqualToString:@"4"])//[visaRegex evaluateWithObject:trimmedString])
	{
		self.cardImage.image = [UIImage imageNamed:@"visa"];
	}
	else if ([firstNumber isEqualToString:@"5"])//[masterRegex evaluateWithObject:trimmedString])
	{
		self.cardImage.image = [UIImage imageNamed:@"mastercard"];
	}
	else
	{
		self.cardImage.image = [UIImage imageNamed:@"plain_card"];
	}
}

#pragma mark - Card Number Formatter -
-(void)reformatAsCardNumber:(GTTextField *)textField
{
	NSUInteger targetCursorPosition = [textField offsetFromPosition:textField.beginningOfDocument
														 toPosition:textField.selectedTextRange.start];

	NSString *cardNumberWithoutSpaces =	[self removeNonDigits:textField.text andPreserveCursorPosition:&targetCursorPosition];

	if ([cardNumberWithoutSpaces length] > 19)
	{
		[textField setText:self.previousTextFieldContent];
		textField.selectedTextRange = self.previousSelection;
		return;
	}

	NSString *cardNumberWithSpaces =
	[self insertSpacesEveryFourDigitsIntoString:cardNumberWithoutSpaces
					  andPreserveCursorPosition:&targetCursorPosition];

	textField.text = cardNumberWithSpaces;
	UITextPosition *targetPosition =
	[textField positionFromPosition:[textField beginningOfDocument]
							 offset:targetCursorPosition];

	[textField setSelectedTextRange:
	 [textField textRangeFromPosition:targetPosition
						   toPosition:targetPosition]
	 ];
}

- (NSString *)removeNonDigits:(NSString *)string
	andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
	NSUInteger originalCursorPosition = *cursorPosition;
	NSMutableString *digitsOnlyString = [NSMutableString new];
	for (NSUInteger i=0; i<[string length]; i++) {
		unichar characterToAdd = [string characterAtIndex:i];
		if (isdigit(characterToAdd)) {
			NSString *stringToAdd =
			[NSString stringWithCharacters:&characterToAdd
									length:1];

			[digitsOnlyString appendString:stringToAdd];
		}
		else {
			if (i < originalCursorPosition) {
				(*cursorPosition)--;
			}
		}
	}

	return digitsOnlyString;
}

- (NSString *)insertSpacesEveryFourDigitsIntoString:(NSString *)string
						  andPreserveCursorPosition:(NSUInteger *)cursorPosition
{
	NSMutableString *stringWithAddedSpaces = [NSMutableString new];
	NSUInteger cursorPositionInSpacelessString = *cursorPosition;
	for (NSUInteger i=0; i<[string length]; i++) {
		if ((i>0) && ((i % 4) == 0)) {
			[stringWithAddedSpaces appendString:@" "];
			if (i < cursorPositionInSpacelessString) {
				(*cursorPosition)++;
			}
		}
		unichar characterToAdd = [string characterAtIndex:i];
		NSString *stringToAdd =
		[NSString stringWithCharacters:&characterToAdd length:1];

		[stringWithAddedSpaces appendString:stringToAdd];
	}

	return stringWithAddedSpaces;
}

@end
