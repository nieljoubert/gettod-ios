//
//  DirectionCollectionViewCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/13.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTDirection.h"

@interface DirectionCollectionViewCell : UICollectionViewCell

- (void)setupViewWithDirection:(GTDirection*)directionObject;

@end
