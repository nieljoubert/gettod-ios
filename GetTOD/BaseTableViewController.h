//
//  BaseTableViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "GTTableViewCell.h"
#import "GTBoldTableViewCell.h"
#import "GTTextFieldCell.h"
#import "GTTableSectionHeaderView.h"
#import "SearchResultCell.h"
#import "TaskHistoryCell.h"
#import "PaymentCardCell.h"
#import "CardNumberCell.h"
#import "ExpiryDateCell.h"
#import "LoginTextFieldCell.h"
#import "Constants.h"
#import "EnRouteCell.h"
#import "QuoteMaterialCell.h"
#import "CostTableViewCell.h"
#import "QuantityAdjustmentCell.h"
#import "AddNewItemCell.h"
#import "ProfileImageAndDetailsCell.h"
#import "GTMultiLineTableViewCell.h"

@interface BaseTableViewController : RootViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *noDataLabel;

@property (nonatomic, strong) UIView *tableFooterView;
@property (nonatomic, assign) CGFloat tableFooterHeight;

- (UIView*) createTableFooterView;

- (void)addNoDataLabelWithText:(NSString*)noDataText;

- (void)keyboardWasShown:(NSNotification *)notify;
- (void)keyboardWillBeHidden:(NSNotification *)notify;
- (void)registerForKeyboardNotifications;
@end
