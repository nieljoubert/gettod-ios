//
//  BaseTableViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationController.navigationBarHidden = NO;
	
	if (self.tableView == nil)
	{
		CGRect frame = [UIScreen mainScreen].bounds;
		frame.origin.y = [UIScreen mainScreen].bounds.origin.y - 1;
		
		self.tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
		self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
	}
	
    self.tableFooterView = [self createTableFooterView];
    
    if (self.tableFooterView)
    {
        [self calculateFooterHeight];
    }
    
	self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.backgroundView = nil;
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"TableViewCell"];
	[self.tableView registerClass:[GTTableViewCell class] forCellReuseIdentifier:@"GTTableViewCell"];
	[self.tableView registerClass:[GTBoldTableViewCell class] forCellReuseIdentifier:@"GTBoldTableViewCell"];
	[self.tableView registerClass:[GTTextFieldCell class] forCellReuseIdentifier:@"TextFieldCell"];
	[self.tableView registerClass:[SearchResultCell class] forCellReuseIdentifier:@"SearchResultCell"];
	[self.tableView registerClass:[TaskHistoryCell class] forCellReuseIdentifier:@"TaskHistoryCell"];
	[self.tableView registerClass:[PaymentCardCell class] forCellReuseIdentifier:@"PaymentCardCell"];
	[self.tableView registerClass:[CardNumberCell class] forCellReuseIdentifier:@"CardNumberCell"];
	[self.tableView registerClass:[ExpiryDateCell class] forCellReuseIdentifier:@"ExpiryDateCell"];
	[self.tableView registerClass:[LoginTextFieldCell class] forCellReuseIdentifier:@"LoginTextFieldCell"];
	[self.tableView registerClass:[EnRouteCell class] forCellReuseIdentifier:@"EnRouteCell"];
	[self.tableView registerClass:[QuoteMaterialCell class] forCellReuseIdentifier:@"QuoteMaterialCell"];
	[self.tableView registerClass:[CostTableViewCell class] forCellReuseIdentifier:@"CostTableViewCell"];
	[self.tableView registerClass:[QuantityAdjustmentCell class] forCellReuseIdentifier:@"QuantityAdjustmentCell"];
	[self.tableView registerClass:[AddNewItemCell class] forCellReuseIdentifier:@"AddNewItemCell"];
	[self.tableView registerClass:[ProfileImageAndDetailsCell class] forCellReuseIdentifier:@"ProfileImageAndDetailsCell"];
	[self.tableView registerClass:[GTMultiLineTableViewCell class] forCellReuseIdentifier:@"GTMultiLineTableViewCell"];
	
	[self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"Footer"];
	[self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"Header"];
	
	[self.view addSubview:self.tableView];
	
	[self registerForKeyboardNotifications];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL) animated
{
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController])
    {
        if (self.navigationController)
        {
            self.navigationController.delegate = nil;
        }
    }
    
    [self dismissKeyboard];
}

- (void)addNoDataLabelWithText:(NSString*)noDataText
{
	if (self.noDataLabel == nil)
	{
		self.noDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, self.tableView.tableHeaderView.frame.size.height+15, SCREEN_WIDTH - 25, 50)];
		self.noDataLabel.font = [UIFont fontWithName:FONT size:15];
		self.noDataLabel.textColor = DARK_TEXT_COLOR;
		self.noDataLabel.textAlignment = NSTextAlignmentCenter;
		self.noDataLabel.numberOfLines = 0;
		self.noDataLabel.lineBreakMode = NSLineBreakByWordWrapping;
		self.noDataLabel.text = noDataText;
		self.noDataLabel.hidden = YES;
		[self.tableView addSubview:self.noDataLabel];
		self.tableView.scrollEnabled = NO;
	}
}

- (UIView*) createTableFooterView
{
    return nil;
}

- (void) calculateFooterHeight
{
    self.tableFooterView.hidden = YES;
    
    [self.view addSubview:self.tableFooterView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[footerView]"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"footerView":self.tableFooterView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[footerView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"footerView":self.tableFooterView}]];
    
    [self.tableFooterView setNeedsLayout];
    [self.tableFooterView layoutIfNeeded];
    
    self.tableFooterHeight = self.tableFooterView.frame.size.height;
    
    [self.tableFooterView removeFromSuperview];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 48;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ((section+1) == tableView.numberOfSections)
    {
        self.tableFooterView = [self createTableFooterView];
        
        if (self.tableFooterView)
        {
            UITableViewHeaderFooterView *headerFooterView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Footer"];
            
            [headerFooterView addSubview:self.tableFooterView];
            
            [headerFooterView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[footer]|"
                                                                                     options:0
                                                                                     metrics:nil
                                                                                       views:@{@"footer": self.tableFooterView}]];
            
            [headerFooterView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[footer]|"
                                                                                     options:0
                                                                                     metrics:nil
                                                                                       views:@{@"footer": self.tableFooterView}]];
            
            return headerFooterView;
        }
        
        return nil;
    }
    
    return nil;
}

- (void)registerForKeyboardNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notify
{
	NSDictionary* info = [notify userInfo];
	CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
	
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0); /// 44 ???
	self.tableView.contentInset = contentInsets;
	self.tableView.scrollIndicatorInsets = contentInsets;
	
	NSIndexPath *path = [self.tableView indexPathForRowAtPoint:[self.tableView convertPoint:self.currentField.center fromView:self.currentField.superview]];
	
	[self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification *)notify
{
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 0, 0);
	self.tableView.contentInset = contentInsets;
	self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)dismissKeyboard
{
	[[UIApplication sharedApplication].keyWindow endEditing:YES];
}

#pragma mark - TextField -

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	
	return TRUE;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	self.currentField = (GTTextField*)textField;
	
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[textField resignFirstResponder];
	
	[self dismissKeyboard];
}

@end
