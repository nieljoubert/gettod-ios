//
//  BaseBlackOverlayViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/25.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface BaseBlackOverlayViewController : RootViewController

@end
