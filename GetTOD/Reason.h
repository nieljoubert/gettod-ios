//
//  Reason.h
//  getTOD
//
//  Created by Niel Joubert on 2015/05/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Reason : NSObject

@property (nonatomic, strong) NSNumber *reasonId;
@property (nonatomic, strong) NSString *reasonDescription;

@end
