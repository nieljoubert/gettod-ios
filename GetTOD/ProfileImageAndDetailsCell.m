//
//  ProfileImageAndDetailsCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ProfileImageAndDetailsCell.h"

@interface ProfileImageAndDetailsCell()

@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *problem;
@property (nonatomic, strong) UILabel *rating;
@property (nonatomic, strong) UIView *ratingView;
@property (nonatomic, strong) UIView *ratingAndProblemView;
@property (nonatomic, strong) UIImageView *profileImageView;
@property (nonatomic, strong) UIImageView *profileImageBorderView;
@property (nonatomic, strong) UIView *profileView;

@end

@implementation ProfileImageAndDetailsCell

- (id)init
{
	self = [super init];
	
	if (self) {}
	
	return self;
}

- (void)setupCellWithJob:(Job*)job forCustomer:(BOOL)isCustomer
{
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	
	if (self.userName == nil)
	{
		self.userName = [UILabel new];
		self.userName.translatesAutoresizingMaskIntoConstraints = NO;
		self.userName.font = [UIFont fontWithName:FONT_SEMI_BOLD size:17];
		self.userName.textColor = DARK_TEXT_COLOR;
		self.userName.numberOfLines = 1;
		self.userName.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.contentView addSubview:self.userName];
	}
	
	self.userName.text = (isCustomer) ? job.customer.name : job.supplier.user.name;
	
	if (self.problem == nil)
	{
		self.problem = [UILabel new];
		self.problem.translatesAutoresizingMaskIntoConstraints = NO;
		self.problem.font = [UIFont fontWithName:FONT size:12];
		self.problem.textColor = GRAY_COLOR;
		self.problem.numberOfLines = 1;
		self.problem.lineBreakMode = NSLineBreakByTruncatingTail;
	}
	
	self.problem.text = [NSString stringWithFormat:@"Problem: %@",job.problem.problemDescription];
	
	if (self.rating == nil)
	{
		self.rating = [UILabel new];
		self.rating.translatesAutoresizingMaskIntoConstraints = NO;
		self.rating.font = [UIFont fontWithName:FONT_BOLD size:9];
		self.rating.textColor = DARK_TEXT_COLOR;
		self.rating.backgroundColor = [UIColor clearColor];
		self.rating.textAlignment = NSTextAlignmentCenter;
	}
	
	self.rating.text = [NSString stringWithFormat:@"%.1f",(isCustomer) ? [job.customer.rating floatValue] : [job.supplier.user.rating floatValue]];
	
	if (self.ratingView == nil)
	{
		self.ratingView = [UIView new];
		self.ratingView.translatesAutoresizingMaskIntoConstraints = NO;
		self.ratingView.backgroundColor = GREEN_COLOR;
		self.ratingView.layer.cornerRadius = 3;
		self.ratingView.layer.masksToBounds = YES;
		[self.ratingView addSubview:self.rating];
	}
	
	if (self.ratingAndProblemView == nil)
	{
		self.ratingAndProblemView = [UIView new];
		self.ratingAndProblemView.translatesAutoresizingMaskIntoConstraints = NO;
		self.ratingAndProblemView.backgroundColor = [UIColor clearColor];
		[self.ratingAndProblemView addSubview:self.ratingView];
		[self.ratingAndProblemView addSubview:self.problem];
		[self.contentView addSubview:self.ratingAndProblemView];
	}
	
	if (self.profileView == nil)
	{
		self.profileView = [UIView new];
		self.profileView.translatesAutoresizingMaskIntoConstraints = NO;
		self.profileView.backgroundColor = [UIColor clearColor];
		[self.contentView addSubview:self.profileView];
	}
	
	if (self.profileImageView == nil)
	{
		self.profileImageView = [UIImageView new];
		self.profileImageView.translatesAutoresizingMaskIntoConstraints = NO;
		self.profileImageView.contentMode = UIViewContentModeScaleAspectFill;
		self.profileImageView.backgroundColor = [UIColor clearColor];
		self.profileImageView.layer.cornerRadius = 48/2;
		self.profileImageView.clipsToBounds = YES;
		[self.profileView addSubview:self.profileImageView];
	}
	
	if (self.profileImageBorderView == nil)
	{
		self.profileImageBorderView = [UIImageView new];
		self.profileImageBorderView.translatesAutoresizingMaskIntoConstraints = NO;
		//		self.profileImageBorderView.contentMode = UIViewContentModeScaleAspectFit;
		self.profileImageBorderView.backgroundColor = [UIColor clearColor];
		self.profileImageBorderView.image = [UIImage imageNamed:@"profile_circle_border"];
		[self.profileView addSubview:self.profileImageBorderView];
	}
	
	NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEURLSTRING,(isCustomer) ? job.customer.profilePictureURL : job.supplier.user.profilePictureURL]]
												  cachePolicy:NSURLRequestReturnCacheDataElseLoad
											  timeoutInterval:60];
	
	__weak ProfileImageAndDetailsCell *weakSelf = self;
	
	[self.profileImageView setImageWithURLRequest:imageRequest
								 placeholderImage:[UIImage imageNamed:@"placeholder"]
										  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
											  
											  weakSelf.profileImageView.image = image;
											  weakSelf.profileImageView.contentMode = UIViewContentModeScaleAspectFill;
											  weakSelf.profileImageView.layer.cornerRadius = 48/2;
											  weakSelf.profileImageView.clipsToBounds = YES;
											  
										  } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
											  
											  
										  }];
	
	NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"name":self.userName,
																				 @"problem":self.problem,
																				 @"rating":self.rating,
																				 @"ratingView":self.ratingView,
																				 @"ratingProb":self.ratingAndProblemView,
																				 @"image":self.profileImageView,
																				 @"profileView":self.profileView,
																				 @"profileBorder":self.profileImageBorderView}];
	
	// Rating and Problem
	[self.ratingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rating]|"
																			options:0
																			metrics:nil
																			  views:views]];
	
	[self.ratingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[rating]|"
																			options:0
																			metrics:nil
																			  views:views]];
	
	
	[self.ratingAndProblemView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ratingView(==25)]-10-[problem]|"
																					  options:0
																					  metrics:nil
																						views:views]];
	
	[self.ratingAndProblemView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ratingView(==17)]|"
																					  options:0
																					  metrics:nil
																						views:views]];
	
	[self.ratingAndProblemView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[problem(14)]|"
																					  options:0
																					  metrics:nil
																						views:views]];
	
	// Profile
	[self.profileView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[profileBorder]|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.profileView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-1-[image]-1-|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.profileView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[image]-1-|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.profileView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[profileBorder]|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	
	// Content
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[profileView(==48)]-10-[name]-15-|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[profileView(==48)]-10-[ratingProb]-15-|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[profileView]-15-|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[name]-5-[ratingProb]-15-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:views]];
}

@end
