//
//  GTMultiLineTitleView.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTMultiLineTitleView.h"
#import "Constants.h"

@implementation GTMultiLineTitleView

- (id)initWithTitle:(NSString*)title andSubTitle:(NSString*)subTitle smallTitle:(BOOL)hasSmallTitle
{
	self = [super init];
	
	if (self)
	{
		self.backgroundColor = [UIColor clearColor];
	
		if (hasSmallTitle)
		{
			UILabel *titleLabel = [UILabel new];
			titleLabel.text = title;
			titleLabel.font = [UIFont fontWithName:@"AdelleSans" size:11];
			titleLabel.textColor = GRAY_COLOR;
			titleLabel.backgroundColor = [UIColor clearColor];
			
			UILabel *subTitleLabel = [UILabel new];
			subTitleLabel.text = subTitle;
			subTitleLabel.font = [UIFont fontWithName:@"AdelleSans-SemiBold" size:18];
			subTitleLabel.textColor = DARK_TEXT_COLOR;
			subTitleLabel.backgroundColor = [UIColor clearColor];
			subTitleLabel.textAlignment = NSTextAlignmentCenter;
			
			[self addSubview:titleLabel];
			[self addSubview:subTitleLabel];

			[titleLabel sizeToFit];
			[subTitleLabel sizeToFit];

			titleLabel.frame = CGRectMake(-(titleLabel.frame.size.width/2), -20, titleLabel.frame.size.width, 20);
			
			subTitleLabel.frame = CGRectMake(-(titleLabel.frame.size.width/2), 0, subTitleLabel.frame.size.width, subTitleLabel.frame.size.height);
			subTitleLabel.center = CGPointMake(titleLabel.center.x, subTitleLabel.center.y);
		}
		else
		{
			UILabel *titleLabel = [UILabel new];
			titleLabel.text = title;
			titleLabel.font = [UIFont fontWithName:@"AdelleSans-SemiBold" size:18];
			titleLabel.textColor = DARK_TEXT_COLOR;
			titleLabel.backgroundColor = [UIColor clearColor];
			
			UILabel *subTitleLabel = [UILabel new];
			subTitleLabel.text = subTitle;
			subTitleLabel.font = [UIFont fontWithName:@"AdelleSans" size:11];
			subTitleLabel.textColor = GRAY_COLOR;
			subTitleLabel.backgroundColor = [UIColor clearColor];
			subTitleLabel.textAlignment = NSTextAlignmentCenter;
			
			[self addSubview:titleLabel];
			[self addSubview:subTitleLabel];
			
			[titleLabel sizeToFit];
			[subTitleLabel sizeToFit];
			
			titleLabel.frame = CGRectMake(-(titleLabel.frame.size.width/2), -15, titleLabel.frame.size.width, 20);
			
			subTitleLabel.frame = CGRectMake(-(titleLabel.frame.size.width/2), 5, titleLabel.frame.size.width, subTitleLabel.frame.size.height);
			subTitleLabel.center = CGPointMake(titleLabel.center.x, subTitleLabel.center.y);
		}
	}
	
	return self;
}

@end