//
//  GTMultiLineTitleView.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GTMultiLineTitleView : UIView

- (id)initWithTitle:(NSString*)title andSubTitle:(NSString*)subTitle smallTitle:(BOOL)hasSmallTitle;

@end
