//
//  ClientQuoteViewController
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ClientQuoteViewController.h"
#import "GTMultiLineTitleView.h"
#import "JobInProgressViewController.h"
#import "GTBoldTableViewCell.h"
#import "CancelationFeedbackViewController.h"
#import "RejectedQuoteViewController.h"
#import "JobCompleteBlackOverlayViewController.h"

@interface ClientQuoteViewController ()

@property (strong, nonatomic) IBOutlet UIView *subtotalView;
@property (strong, nonatomic) IBOutlet UILabel *labourPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *materialsTotalLabel;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UILabel *vatLabel;
@property (weak, nonatomic) IBOutlet UIView *labourContainerView;
@property (weak, nonatomic) IBOutlet UIView *materialsCostContainerView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewFromXib;
@property (nonatomic, strong) UILabel *descriptionTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *labourPerHourLabel;

@property (nonatomic, assign) float quoteTotal;
@property (nonatomic, assign) float vatTotal;
@property (nonatomic, assign) float labourTotal;
@property (nonatomic, assign) float materialsTotal;

@end

@implementation ClientQuoteViewController

- (void)viewDidLoad
{
	self.tableView = self.tableViewFromXib;
	[super viewDidLoad];
	
	self.navigationItem.titleView = [[GTMultiLineTitleView alloc] initWithTitle:@"Quote from" andSubTitle:[SettingsManager manager].currentJob.supplier.user.name smallTitle:YES];
	
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Reject" style:UIBarButtonItemStyleDone target:self action:@selector(rejectQuote)];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Accept" style:UIBarButtonItemStyleDone target:self action:@selector(acceptQuote)];
	
	self.labourContainerView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.labourContainerView.layer.borderWidth = 0.5;
	
	self.materialsCostContainerView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.materialsCostContainerView.layer.borderWidth = 0.5;
	
	self.labourPerHourLabel.text = [NSString stringWithFormat:@"LABOUR AT R%@ / HR",self.quote.labourRate];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = NO;
	[self calculateMaterialTotal];
}

- (void)calculateMaterialTotal
{
	float total = 0.00;
	
	for (Material *mat in self.quote.materials)
	{
		total += [[mat price] floatValue];
	}
	
	self.materialsTotal = total;
	self.materialsTotalLabel.text = [NSString stringWithFormat:@"%.2f",total];
	[self calculateLabourTotal];
}

- (void)calculateLabourTotal
{
	self.labourTotal = [self.quote.labourHours intValue] * [self.quote.labourRate intValue];
	self.labourPriceLabel.text = [NSString stringWithFormat:@"%.2f",self.labourTotal];
	[self calculateQuoteTotal];
}

- (void)calculateQuoteTotal
{
	self.quoteTotal = self.materialsTotal + self.labourTotal;
	self.totalLabel.text = [NSString stringWithFormat:@"%.2f",self.quoteTotal];
	[self calculateVatTotal];
}

- (void)calculateVatTotal
{
	float total = 0.00;
	total += self.materialsTotal + self.labourTotal;
	self.vatTotal = self.quoteTotal * 0.14;
	self.vatLabel.text = [NSString stringWithFormat:@"Inclusive of 14%% VAT at R%.2f",self.vatTotal];
	
	if ([SettingsManager manager].currentJob.supplier.company.isVatRegistered)
	{
		self.vatLabel.hidden = NO;
	}
	else
	{
		self.vatLabel.hidden = YES;
	}
}

- (float)caclulateInfoHeight
{
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, SCREEN_WIDTH-15, 100)];
	label.font = [UIFont fontWithName:FONT size:14];
	label.numberOfLines = 0;
	label.lineBreakMode = NSLineBreakByWordWrapping;
	label.text = self.quote.jobDescription;
	
	CGSize size = [label sizeThatFits:CGSizeMake(SCREEN_WIDTH-15, 1000)];
	return size.height;
}

#pragma mark - Actions -

- (void) acceptQuote
{
	[self showLoader];
	
	[[APIManager manager] acceptQuote:self.quote.quoteID
							  success:^(BOOL success) {
								  
								  [self hideLoader];
								  
								  [SettingsManager manager].appState = AppStateJobInProgress;
								  [SettingsManager save];
								  
								  if (success)
								  {
									  JobInProgressViewController *inProgress = [[JobInProgressViewController alloc] initWithNibName:@"JobInProgressViewController" bundle:nil];
									  [self.navigationController pushViewController:inProgress animated:NO];
								  }
								  
								} failure:^(NSError *error) {
									[self hideLoader];
								}];
	
}

- (void) rejectQuote
{
	BaseTableViewBlackOverlayViewController *rejectList = [[BaseTableViewBlackOverlayViewController alloc] initWithNibName:@"BaseTableViewBlackOverlayViewController" bundle:nil];
	rejectList.listType = BlackOverlayListTypeRejectQuote;
	rejectList.delegate = self;

	[self launchRejectScreenWithReasons:[SettingsManager manager].currentJob.quote.rejectReasons forController:rejectList];
}

- (void)launchRejectScreenWithReasons:(NSArray*)rejectReasons forController:(BaseTableViewBlackOverlayViewController*)rejectList
{
	[SettingsManager manager].appState = AppStateQuoteRejecting;
	[SettingsManager save];
	
	rejectList.cells = [NSMutableArray array];
	
	for (Reason *reason in rejectReasons)
	{
		GTTableViewCell *cell = [GTTableViewCell new];
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.textColor = [UIColor whiteColor];
		cell.accessoryView = nil;
		cell.textLabel.text = reason.reasonDescription;
		
		[rejectList.cells addObject:cell];
	}
	
	rejectList.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	rejectList.backgroundImage = [self getBlurredBackgroundImage];
	
	[self.navigationController presentViewController:rejectList animated:YES completion:nil];
}

#pragma mark - Delegates -

-(void)cellSelected:(int)index WithText:(NSString *)text fromController:(BaseTableViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
	
	if (index == [SettingsManager manager].currentJob.quote.rejectReasons.count)
	{
		[AlertViewManager showAlertWithTitle:@"Other Reason"
									 message:@"Other reason submission not implemented yet.\n\nPlease select an actual reason."
						   cancelButtonTitle:@"OK"
						   otherButtonTitles:nil
						buttonSelectionBlock:nil];
	}
	else
	{
		Reason *reason = [[SettingsManager manager].currentJob.quote.rejectReasons objectAtIndex:index];
		
		[self showLoader];
		
		[[APIManager manager] rejectQuote:self.quote.quoteID
							 withReasonId:reason.reasonId
								  success:^(BOOL success) {
									  
									  [self hideLoader];
									  
									  [SettingsManager manager].appState = AppStateQuoteRejected;
									  [SettingsManager save];
									  
									  if (success)
									  {
										  RejectedQuoteViewController *rejected = [[RejectedQuoteViewController alloc] initWithNibName:@"RejectedQuoteViewController" bundle:nil];
										  [self.navigationController pushViewController:rejected animated:NO];
									  }
									  
								  } failure:^(NSError *error) {
									  [self hideLoader];
								  }];
	}
}

- (void)dismissedController:(BaseTableViewBlackOverlayViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
	{
		return 1;
	}
	else if (section == 1)
	{
		if (self.quote.materials.count == 0)
		{
			return 1;
		}
		else
		{
			return self.quote.materials.count;
		}
	}
	else if (section == 2)
	{
		return 1;
	}
	
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		GTBoldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTBoldTableViewCell"];
		[cell.contentView addSubview:self.subtotalView];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.accessoryView = nil;
		return cell;
	}
	else if (indexPath.section == 1)
	{
		if (self.quote.materials.count == 0)
		{
			GTTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTTableViewCell"];
			UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 58)];
			label.font = [UIFont fontWithName:FONT_SEMI_BOLD size:13];
			label.textColor = PINK_COLOR;
			label.numberOfLines = 1;
			label.textAlignment = NSTextAlignmentCenter;
			label.text = @"No materials added";
			[cell.contentView addSubview:label];
			cell.accessoryType = UITableViewCellAccessoryNone;
			cell.accessoryView = nil;
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			return cell;
		}
		else
		{
			QuoteMaterialCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuoteMaterialCell"];
			[cell setupCellWithMaterial:[self.quote.materials objectAtIndex:indexPath.row]];
			return cell;
		}
	}
	else if (indexPath.section == 2)
	{
		GTBoldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTBoldTableViewCell"];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.accessoryView = nil;
		
		if (self.descriptionTextLabel == nil)
		{
			self.descriptionTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, SCREEN_WIDTH-15, 100)];
			self.descriptionTextLabel.font = [UIFont fontWithName:FONT size:14];
			self.descriptionTextLabel.textColor = DARK_TEXT_COLOR;
			self.descriptionTextLabel.numberOfLines = 0;
			self.descriptionTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
			[cell.contentView addSubview:self.descriptionTextLabel];
		}
		
		self.descriptionTextLabel.text = self.quote.jobDescription;
		
		self.descriptionTextLabel.frame = CGRectMake(15, 15, SCREEN_WIDTH-15, [self caclulateInfoHeight]);
		
		return cell;
	}
		
	
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"SUBTOTAL"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"MATERIALS"];
	}
	else if (section == 2)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"JOB DESCRIPTION"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		return self.subtotalView.frame.size.height;
	}
	else if (indexPath.section == 1)
	{
		return 58;
	}
	else if (indexPath.section == 2)
	{
		return [self caclulateInfoHeight] + 30;
	}
	
	return 44;
}

@end
