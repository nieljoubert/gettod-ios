//
//  BaseTableViewBlackOverlayViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewBlackOverlayViewController.h"
#import "WhitePaymentCardCell.h"
#import "GTMultiLineTableViewCell.h"
#import "APIManager.h"

@interface BaseTableViewBlackOverlayViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) BOOL shouldLaunchNextModal;
@property (nonatomic, strong) NSString *otherTextString;
@property (nonatomic, strong) CAGradientLayer *maskLayer;
@property (strong, nonatomic) IBOutlet UIView *blackOpaqueView;
@end

@implementation BaseTableViewBlackOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.tableView.backgroundColor = [UIColor clearColor];
	
	self.backgroundImageView.image = self.backgroundImage;
	
	NSString *titleString = @"";
	
	if (self.listType == BlackOverlayListTypeRejectQuote)
	{
		titleString = @"REASON FOR REJECTING?";
		[self.button setBackgroundImage:[UIImage imageNamed:@"black_close_button"] forState:UIControlStateNormal];
//		[self addOtherCell];
	}
	else if (self.listType == BlackOverlayListTypeCancelJob)
	{
		titleString = @"REASON FOR CANCELLING?";
		[self.button setBackgroundImage:[UIImage imageNamed:@"black_close_button"] forState:UIControlStateNormal];
//		[self addOtherCell];
	}
	else if (self.listType == BlackOverlayListTypeCards)
	{
		titleString = @"";
		[self.button setBackgroundImage:[UIImage imageNamed:@"black_close_button"] forState:UIControlStateNormal];
	}
	else if (self.listType == BlackOverlayListTypeEnRouteOptions)
	{
		titleString = @"";
		[self.button setBackgroundImage:[UIImage imageNamed:@"black_close_button"] forState:UIControlStateNormal];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTime:) name:@"TimerUpdated" object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopTime) name:@"TimerStopped" object:nil];
	}
	else if (self.listType == BlackOverlayListTypeProblems)
	{
		titleString = @"WHAT IS THE PROBLEM?";
		[self.button setBackgroundImage:[UIImage imageNamed:@"black_close_button"] forState:UIControlStateNormal];
//		[self addOtherCell];
	}
	
	NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[titleString uppercaseString]];
	[attributedString addAttribute:NSKernAttributeName
							 value:@(1.2)
							 range:NSMakeRange(0,titleString.length)];
	
	self.titleLabel.attributedText = attributedString;
    
    [self.tableView registerClass:[GTMultiLineTableViewCell class] forCellReuseIdentifier:@"CancelCellMulti"];
    [self.tableView registerClass:[GTTableViewCell class] forCellReuseIdentifier:@"CancelCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	if (!self.maskLayer)
	{
		self.maskLayer = [CAGradientLayer layer];
		
		//the gradient layer must be positioned at the origin of the view
		int size = 60;
		
		self.maskLayer.frame = CGRectMake(0,
										  self.tableView.frame.origin.y + self.tableView.frame.size.height-size,
										  self.tableView.frame.size.width,
										  size);
		
		UIColor *theColor = [UIColor blackColor];
		
		//build the colors array for the gradient
		NSArray *colors = [NSArray arrayWithObjects:
						   (id)[[theColor colorWithAlphaComponent:0.8f] CGColor],
						   //						   (id)[[theColor colorWithAlphaComponent:0.7f] CGColor],
						   (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
						   //						   (id)[[theColor colorWithAlphaComponent:0.5f] CGColor],
						   (id)[[theColor colorWithAlphaComponent:0.4f] CGColor],
						   //						   (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
						   (id)[[theColor colorWithAlphaComponent:0.2f] CGColor],
						   //						   (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
						   (id)[[UIColor clearColor] CGColor],
						   nil];
		
		//reverse the color array if needed
		if(YES)
		{
			colors = [[colors reverseObjectEnumerator] allObjects];
		}
		
		//apply the colors and the gradient to the view
		self.maskLayer.colors = colors;
		
		[self.blackOpaqueView.layer addSublayer:self.maskLayer];// insertSublayer:self.maskLayer atIndex:0];
	}
}

- (void)viewDidDisappear:(BOOL)animated
{
	//[super viewDidDisappear:animated];
	
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	
//	if (self.listType == BlackOverlayListTypeEnRouteOptions)
//	{
//		if (self.shouldLaunchNextModal)
//		{
//			[self.delegate delayedNotification];
//		}
//	}
}

- (void)viewWillDisappear:(BOOL)animated
{
    
}

- (void)addOtherCell
{
	GTTableViewCell *cell = [GTTableViewCell new];
	cell.backgroundColor = [UIColor clearColor];
	cell.textLabel.textColor = [UIColor whiteColor];
	cell.accessoryView = nil;
	cell.textLabel.text = @"Other";
	
	[self.cells addObject:cell];
}

- (IBAction)buttonAction:(id)sender
{
	[self.delegate dismissedController:self];
	
//	if (self.listType == BlackOverlayListTypeRejectQuote)
//	{
//		[self dismissViewControllerAnimated:YES completion:nil];
//	}
//	else if (self.listType == BlackOverlayListTypeCancelJob)
//	{
//		[self.navigationController popViewControllerAnimated:NO];
//	}
//	else if (self.listType == BlackOverlayListTypeCards)
//	{
//		[self dismissViewControllerAnimated:YES completion:nil];
//	}
//	else if	(self.listType == BlackOverlayListTypeEnRouteOptions)
//	{
//		[self dismissViewControllerAnimated:YES completion:nil];
//	}
//	else if (self.listType == BlackOverlayListTypeProblems)
//	{
//		[self dismissViewControllerAnimated:YES completion:nil];
//	}
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return [self.cells objectAtIndex:indexPath.row];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.cells.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	id view = [self.cells objectAtIndex:indexPath.row];
    NSString *text = ((GTTableViewCell*)view).textLabel.text;
    
    if ( (![text isEqualToString:@"Call Supplier"] && (text.length >= 14 && [[text substringToIndex:14] isEqual:@"Cancel Request"])) || text == nil)
    {
        if ([view isKindOfClass:[WhitePaymentCardCell class]])
        {
            return 52;
        }
        
        return 88;
    }
	else if ([view isKindOfClass:[GTTableViewCell class]])
	{
		return 52;
	}
    else
    {
        return 52;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//	NSInteger actualRowsCount = self.cells.count-1;
    GTTableViewCell *cell = (GTTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
//	if (self.listType == BlackOverlayListTypeCards || self.listType == BlackOverlayListTypeEnRouteOptions)
//	{
//		actualRowsCount = self.cells.count;
//	}
    
//	if (indexPath.row == actualRowsCount)
    if ([cell.textLabel.text isEqualToString:@"Other"])
	{
		// Other
		EnterTextViewController *textController = [EnterTextViewController new];
		
		textController.rightButtonText = @"Done";
		textController.leftButtonText = @"Cancel";
		textController.delegate = self;
		
		if (self.listType == BlackOverlayListTypeProblems)
		{
			textController.titleText = @"Other Problem";
			textController.textViewHintText = @"Please enter a description of your problem here.";
		}
		else
		{
			textController.titleText = @"Other Reason";
			textController.textViewHintText = @"Please enter a description of your reason here.";
		}
		
		UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:textController];
		[Flurry logAllPageViews:navigationController];
//		[self.navigationController presentViewController:navigationController animated:YES completion:nil];
		[self presentViewController:navigationController animated:YES completion:nil];
	}
	else
	{
		GTTableViewCell *cell = (GTTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
		[self.delegate cellSelected:(int)indexPath.row WithText:cell.textLabel.text fromController:self];
		
		[[NSNotificationCenter defaultCenter] removeObserver:self];
		
		[self.delegate dismissedController:self];
	}
	
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Delegate -
-(void)textEntered:(NSString *)text forController:(EnterTextViewController *)controllercontroller
{
//	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.cells.count-1 inSection:0];
	
	self.otherTextString = text;
	
//	GTTableViewCell *cell = (GTTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
//	cell.textLabel.text = self.otherTextString;
	
	//[self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
	
	[self.delegate cellSelected:-1 WithText:text fromController:self];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[self.delegate dismissedController:self];
}

#pragma mark - Notification -
- (void)updateTime:(NSNotification*)notification
{
	NSDictionary *dict = notification.userInfo;
	
	NSDate *timerDate = [dict objectForKey:@"time"];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"mm:ss"];
	[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
	
	NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:timerDate];
	NSInteger min = [components minute];
	NSInteger sec = [components second];
	
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    
    GTTableViewCell *cell = (GTTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:@"CancelCellMulti"];//cellForRowAtIndexPath:index];
    
	if (min == 0 && sec == 0)
	{
		cell.textLabel.text = @"Cancel request";
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.accessoryView = nil;
        [self.cells replaceObjectAtIndex:1 withObject:cell];
        [self.tableView reloadData];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
	}
	else
	{
		GTMultiLineTableViewCell *multiCell = (GTMultiLineTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//        GTMultiLineTableViewCell *multiCell = (GTMultiLineTableViewCell*)cell;//[self.tableView dequeueReusableCellWithIdentifier:@"CancelCellMulti"];
        [multiCell setupCellWithTitle:@"Cancel Request" timeText:[NSString stringWithFormat:@"(%@ min to cancel request for free)",[dateFormatter stringFromDate:timerDate]] andText:@"You will be charged a call out fee of R150.00 if you choose to cancel after this time."];
	}
}

- (void)stopTime
{
	GTTableViewCell *cell = [GTTableViewCell new];
	cell.textLabel.text = @"Cancel request";
	[self.cells replaceObjectAtIndex:1 withObject:cell];
	[self.tableView reloadData];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
