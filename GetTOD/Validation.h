//
//  Validation.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/23.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validation : NSObject

+ (BOOL) isStringNumbersOnly: (NSString*) aString;
+ (BOOL) isStringAnInteger: (NSString *) aString;
+ (BOOL) isStringNumbersAndLettersOnly:(NSString*) candidate;
+ (BOOL) isValidPhoneNumber: (NSString*) candidate;
+ (BOOL) isValidEmail: (NSString*) candidate;
+ (BOOL) isStringCharactersOnly: (NSString*) candidate;
+ (BOOL) isUsernameValid: (NSString*) aString;

@end
