//
//  WhitePaymentCardCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "GTTextField.h"
#import "Card.h"

@interface WhitePaymentCardCell : GTBoldTableViewCell

@property (nonatomic,strong) GTTextField *cardNumber;
@property (nonatomic,strong) GTTextField *nameField;

- (void)setupCellWithCard:(Card*)card;
- (void)getCardLogo:(NSString*)number;

@end
