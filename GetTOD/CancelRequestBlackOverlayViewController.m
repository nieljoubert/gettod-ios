//
//  CancelRequestBlackOverlayViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CancelRequestBlackOverlayViewController.h"
#import "APIManager.h"

@interface CancelRequestBlackOverlayViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end

@implementation CancelRequestBlackOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(confirm)];
	[self.confirmButton addGestureRecognizer:longPress];
	
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(dismiss)];
	singleTap.numberOfTapsRequired = 1;
	[self.closeButton addGestureRecognizer:singleTap];
	
	self.backgroundImageView.image = self.backgroundImage;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (void)dismiss
{
	[self.delegate dismissController:self didCancel:NO];
}

- (void)confirm
{
	self.confirmButton.enabled = NO;
	
	[[APIManager manager] cancelJob:[SettingsManager manager].currentJob.jobId
						   withType:JobCancelTypeDuringJob
							success:^(BOOL success) {
								[self.delegate dismissController:self didCancel:YES];
							} failure:^(NSError *error) {
								[self.delegate dismissController:self didCancel:NO];
							}];
}

@end
