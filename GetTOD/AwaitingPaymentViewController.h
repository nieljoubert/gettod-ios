//
//  AwaitingPaymentViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/05/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseWhiteOverlayElipsesViewController.h"

@interface AwaitingPaymentViewController : BaseWhiteOverlayElipsesViewController

@end
