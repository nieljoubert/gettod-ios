//
//  ServiceCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ServiceCell.h"
#import "ImageHelper.h"

@implementation ServiceCell

- (id)init
{
	self = [super init];
	
	if (self)
	{
		[self setupCellWithName:nil];
	}
	
	return self;
}

- (void)setupCellWithName:(Service*)service
{
	self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure"]];
	self.selectionStyle = UITableViewCellSelectionStyleDefault;
	
	if (self.providerNameLabel == nil)
	{
		self.providerNameLabel = [UILabel new];
		self.providerNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.providerNameLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.providerNameLabel.textColor = DARK_TEXT_COLOR;
		self.providerNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.contentView addSubview:self.providerNameLabel];
	}
	
	if (self.providerImage == nil)
	{
		self.providerImage = [UIImageView new];
		self.providerImage.translatesAutoresizingMaskIntoConstraints = NO;
		self.providerImage.contentMode = UIViewContentModeScaleAspectFit;
		self.providerImage.backgroundColor = [UIColor clearColor];
		[self.contentView addSubview:self.providerImage];
	}
	
	__weak ServiceCell *weakSelf = self;
	
	NSString *imageName = [NSString stringWithFormat:SERVICE_IMAGE_NAME,service.serviceId];
	
	[self.providerImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:service.pictureActiveURL]]
							  placeholderImage:nil
									   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
										   
										   weakSelf.imageView.image = image;
										   [weakSelf setNeedsLayout];
										   [ImageHelper saveImageToDisk:image withName:imageName];
										   
									   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
										   
										   
									   }];
	
	self.providerNameLabel.text = service.name;
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[image(==imageW)]-(padding)-[text]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING,@"imageW":[NSNumber numberWithInt:59]}
																			   views:@{@"text":self.providerNameLabel, @"image":self.providerImage}]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[text]|"
																			 options:0
																			 metrics:nil
																			   views:@{@"text":self.providerNameLabel, @"image":self.providerImage}]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|"
																			 options:0
																			 metrics:nil
																			   views:@{@"text":self.providerNameLabel, @"image":self.providerImage}]];
}

@end
