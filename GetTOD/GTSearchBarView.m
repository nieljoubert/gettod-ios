//
//  GTSeachBarView.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTSearchBarView.h"
#import "MapSearchModalViewController.h"

@implementation GTSearchBarView

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	
	if (self)
	{
		self.translatesAutoresizingMaskIntoConstraints = NO;
		self.backgroundColor = [UIColor whiteColor];
		self.layer.borderWidth = 0.5;
		self.layer.borderColor = GRAY_COLOR.CGColor;
		self.layer.cornerRadius = 5;
		self.layer.masksToBounds = YES;
		
		[self setupBar];
	}
	
	return self;
}


- (id)init
{
	self = [super init];
	
	if (self)
	{
		self.translatesAutoresizingMaskIntoConstraints = NO;
		self.backgroundColor = [UIColor whiteColor];
		self.layer.borderWidth = 0.5;
		self.layer.borderColor = GRAY_COLOR.CGColor;
		self.layer.cornerRadius = 5;
		self.layer.masksToBounds = YES;
		
		[self setupBar];
	}
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	
	if (self)
	{
		self.translatesAutoresizingMaskIntoConstraints = NO;
		self.backgroundColor = [UIColor whiteColor];
		self.layer.borderWidth = 0.5;
		self.layer.borderColor = GRAY_COLOR.CGColor;
		self.layer.cornerRadius = 5;
		self.layer.masksToBounds = YES;
		
		[self setupBar];
	}
	
	return self;
}

- (void)setupBar
{
	UIImageView *marker = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_marker_gray"]];
	marker.translatesAutoresizingMaskIntoConstraints = NO;
	marker.contentMode = UIViewContentModeCenter;
	
	UIImage *searchImage = [UIImage imageNamed:@"search_icon_pink"];
	
	UIImageView	*search = [[UIImageView	alloc] initWithImage:searchImage];
	search.userInteractionEnabled = YES;
	search.translatesAutoresizingMaskIntoConstraints = NO;
	search.contentMode = UIViewContentModeCenter;
	
	UITapGestureRecognizer *single = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchButtonTapped)];
	single.numberOfTapsRequired = 1;
	[search addGestureRecognizer:single];
	
	self.textField = [GTTextField new];
	self.textField.translatesAutoresizingMaskIntoConstraints = NO;
	self.textField.placeholder = @"Enter your address";
	self.textField.delegate = self;
	self.textField.userInteractionEnabled = YES;
	
	UIButton *cancelButton = [[UIButton alloc] init];
	cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
	[cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
	[cancelButton setTitleColor:PINK_COLOR forState:UIControlStateNormal];
	[cancelButton addTarget:self action:@selector(searchCancelled) forControlEvents:UIControlEventTouchUpInside];
	
	[self addSubview:marker];
	[self addSubview:search];
	[self addSubview:self.textField];
	
	NSDictionary *metrics = @{@"markerW":[NSNumber numberWithFloat:marker.frame.size.width],
							  @"searchW":[NSNumber numberWithFloat:searchImage.size.width],
							  @"textW":[NSNumber numberWithFloat:(self.frame.size.width - searchImage.size.width - marker.frame.size.width-40)]};
	
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[marker(==44)]|"
																 options:0
																 metrics:metrics
																   views:@{@"marker":marker, @"search":search, @"text":self.textField}]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[search(==44)]|"
																 options:0
																 metrics:metrics
																   views:@{@"marker":marker, @"search":search, @"text":self.textField}]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[text(==40)]|"
																 options:0
																 metrics:metrics
																   views:@{@"marker":marker, @"search":search, @"text":self.textField}]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[marker(==markerW)]-10-[text(==textW)]-10-[search(==searchW)]-10-|"
																 options:0
																 metrics:metrics
																   views:@{@"marker":marker, @"search":search, @"text":self.textField}]];
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	[self.delegate searchButtonTapped];
	return NO;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	[self.delegate searchTextEntered:(NSString*)textField.text];
	
	return YES;
}

- (void)searchCancelled
{
	
}

- (void)searchButtonTapped
{
	[self.delegate searchButtonTapped];
}


@end
