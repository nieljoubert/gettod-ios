//
//  ServicesListViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ServicesListViewController.h"
#import "Constants.h"
#import "ImageHelper.h"
#import "ServiceCell.h"
#import "ChooseSupplierViewController.h"
#import "NoSuppliersViewController.h"
#import "LoginViewController.h"
#import "BaseTableViewBlackOverlayViewController.h"
#import "Service.h"
#import "SupplierQuoteViewController.h"
#import "ArrivedViewController.h"
#import "NewRequestViewController.h"
#import "SupplierJobInProgressViewController.h"
#import "NavigationDirectionsViewController.h"
#import "AddEditCardDetailsViewController.h"

@interface ServicesListViewController ()

@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation ServicesListViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.title = @"getTOD";
	
	UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"gear"] style:UIBarButtonItemStylePlain target:self action:@selector(launchSettings)];
	self.navigationItem.leftBarButtonItem = settings;
	self.navigationItem.hidesBackButton = YES;
	
	[self.tableView registerClass:[ServiceCell class] forCellReuseIdentifier:@"ServiceCell"];
	
	self.tableView.tableHeaderView = [self createHeaderView];
	
	self.refreshControl = [[UIRefreshControl alloc]init];
	[self.tableView addSubview:self.refreshControl];
	[self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
	
//	[self getAllInfo];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = NO;
	
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		self.currentLocation = location;
	}];
	
	[self getServices];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	[SettingsManager manager].appState = AppStateHome;
	[SettingsManager save];
}

- (UILabel*)createHeaderView
{
	UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 115)];
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.backgroundColor = LIGHT_BLUE_COLOR;
	textLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:18];
	textLabel.textColor = DARK_BLUE_FONT_COLOR;
	textLabel.text = @"Select your service";
	
	return textLabel;
}

- (void)getAllInfo
{
	[[APIManager manager] getProvinces:^(NSMutableArray *results) {
	} failure:^(NSError *error) {}];
	
	[[APIManager manager] getEntities:^(NSMutableArray *results) {
	} failure:^(NSError *error) {}];
	
	[[APIManager manager] getCities:^(NSMutableArray *results) {
	} failure:^(NSError *error) {}];
}

#pragma mark - Actions -
- (void)launchSettings
{
	SettingsViewController *settings = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	settings.delegate = self;
	UINavigationController *settingsNavigationController = [[UINavigationController alloc] initWithRootViewController:settings];
	[Flurry logAllPageViews:settingsNavigationController];
	[self.navigationController presentViewController:settingsNavigationController animated:YES completion:nil];
}

-(void)settingsDismissed:(SettingsViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TableView -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	Service *service = [self.services objectAtIndex:indexPath.row];
	
	ServiceCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ServiceCell"];
	
	[cell setupCellWithName:service];
	
	return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.services.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.tableView.userInteractionEnabled = NO;
	
	Service *service = [self.services objectAtIndex:indexPath.row];
	
	[self showLoader];
	
	[[APIManager manager] getNearbySuppliersFromLocation:[SettingsManager manager].currentLocationCoords
												 andType:service.serviceId
												 success:^(NSMutableArray *results) {
													 [self hideLoader];
													 
													 if (results.count == 0)
													 {
														 NoSuppliersViewController *controller = [[NoSuppliersViewController alloc] initWithNibName:@"NoSuppliersViewController" bundle:nil];
														 controller.serviceTypeID = service.serviceId;
														 [self.navigationController pushViewController:controller animated:YES];
													 }
													 else
													 {
														 ChooseSupplierViewController *chooser = [[ChooseSupplierViewController alloc] init];
														 chooser.suppliers = results;
														 chooser.serviceTypeID = service.serviceId;
                                                         [self.navigationController pushViewController:chooser animated:YES];
													 }
													 self.tableView.userInteractionEnabled = YES;
													 
												 } failure:^(NSError *error) {
													 [self hideLoader];
													 NoSuppliersViewController *controller = [[NoSuppliersViewController alloc] initWithNibName:@"NoSuppliersViewController" bundle:nil];
													 controller.serviceTypeID = service.serviceId;
													 [self.navigationController pushViewController:controller animated:YES];
													 self.tableView.userInteractionEnabled = YES;
												 }];
	
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Data -
- (void)getServices
{
	[self showLoader];
	[self getServicesFromAPI];
}

- (void)refreshTable
{
	[self getServicesFromAPI];
}

- (void)getServicesFromAPI
{
	[[APIManager manager] getServices:^(NSMutableArray *results) {
		[self hideLoader];
		[self.refreshControl endRefreshing];
		self.services = results;
		[self.tableView reloadData];
		
	} failure:^(NSError *error) {
		[self hideLoader];
		[self.refreshControl endRefreshing];
	}];
}


@end
