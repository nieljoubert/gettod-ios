//
//  ClientQuoteViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BaseTableViewBlackOverlayViewController.h"
#import "Quote.h"

@interface ClientQuoteViewController : BaseTableViewController <BlackTableViewDelegate>

@property (nonatomic, strong) Quote *quote;
@property (nonatomic, assign) int serviceId;

@end
