//
//  LoginViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface LoginViewController : BaseTableViewController

@property (nonatomic, assign) BOOL isRegistration;

@end
