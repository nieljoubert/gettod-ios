//
//  API.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "API.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "HTTPClient.h"
#import "AsyncHTTPClient.h"
#import "SettingsManager.h"
#import "AlertviewManager.h"

@interface API()

@property (nonatomic, strong) NSMutableDictionary *requestData;
@property (nonatomic, strong) NSString *requestPath;

@end

@implementation API

- (id) init
{
    if ((self = [super init])) {}
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    return self;
}

- (void) createRequestData: (NSDictionary*) data
{
    self.requestData = [NSMutableDictionary dictionary];
    
    //Add the request specific data
    for (NSString *key in data)
    {
        [self.requestData setObject:[data objectForKey:key] forKey:key];
    }
    
    NSError *error;
    
    NSString *stringRequest = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:self.requestData options:NSJSONWritingPrettyPrinted error:&error] encoding:NSUTF8StringEncoding];
    
    NSLog(@"\n\n=============== Request: %@ ================\nTOKEN = %@\n%@\n==============================================================\n", self.requestPath, [SettingsManager manager].authenticationToken, stringRequest);
}

- (void)performPOSTRequestWithData:(NSDictionary*)data
                      inJSONFormat:(BOOL)isJSON
                           isAsync:(BOOL)isAsync
                           success:(SuccessHandler)success
                           failure:(FailureHandler)failure
{
    self.requestPath = [NSString stringWithFormat:@"%@/%@", self.path, self.function];
    
    [self createRequestData:data];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            case AFNetworkReachabilityStatusReachableViaWiFi:
            case AFNetworkReachabilityStatusReachableViaWWAN:
                // Our connection is fine
                // Resume our requests or do nothing
                [self doPOSTisJSON:isJSON
                           isAsync:isAsync
                       WithSuccess:success
                        andFailure:failure];
                break;
            case AFNetworkReachabilityStatusNotReachable:
                // We have no active connection - disable all requests and don’t let the user do anything
                [[NSNotificationCenter defaultCenter] postNotificationName:@"HideLoader" object:@""];
                
                [AlertViewManager showAlertWithTitle:@"No connection detected"
                                             message:@"Internet Connectivity lost, please try again"//You need an active internet connection to use this app."
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:@[]
                                buttonSelectionBlock:nil];
                return;
                break;
            default:
                // If we get here, we’re most likely timing out
                return;
                break;
        }
    }];
    
    // Set the reachabilityManager to actively wait for these events
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}

- (void)doPOSTisJSON:(BOOL)isJSON
             isAsync:(BOOL)isAsync
         WithSuccess:(SuccessHandler)success
          andFailure:(FailureHandler)failure
{
    HTTPClient *client = (HTTPClient*)[HTTPClient sharedClient];
    
    if (isAsync)
    {
        client = (AsyncHTTPClient*) [AsyncHTTPClient sharedAsyncClient];
    }
    
    [client doPOST:self.requestPath
      inJSONFormat:isJSON
        parameters:self.requestData
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               
               NSLog(@"\n\n=============== Reponse: %@ ================\n%@\n==============================================================\n", self.requestPath,responseObject);
               
               if (responseObject != nil)
               {
                   success(responseObject);
               }
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               NSLog(@"\n\n=============== Reponse: %@ ================\n%@\n==============================================================\n", self.requestPath,error.localizedDescription);
               
               if ([self.requestPath isEqualToString:@"http://suppliers.gettod.com/api/auth/login"] && operation.responseObject != nil)
               {
                   NSDictionary *userInfo = @{
                                              NSLocalizedDescriptionKey: [operation.responseObject objectForKey:@"Error"],
                                              NSLocalizedFailureReasonErrorKey: [operation.responseObject objectForKey:@"Error"],
                                              NSLocalizedRecoverySuggestionErrorKey: [operation.responseObject objectForKey:@"Error"]
                                              };
                   NSError *newError = [NSError errorWithDomain:error.domain
                                                           code:error.code
                                                       userInfo:userInfo];
                   
                   failure(newError);
               }
               else
               {
                   failure(error);
               }
               
           }];
}

- (void)performGETRequestWithData:(NSDictionary*)data
                          isAsync:(BOOL)isAsync
                          success:(SuccessHandler)success
                          failure:(FailureHandler)failure
{
    self.requestPath = [NSString stringWithFormat:@"%@/%@", self.path, self.function];
    
    [self createRequestData:data];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            case AFNetworkReachabilityStatusReachableViaWiFi:
            case AFNetworkReachabilityStatusReachableViaWWAN:
                // Our connection is fine
                // Resume our requests or do nothing
                [self doGETAsync:isAsync
                         success:success
                      andFailure:failure];
                break;
            case AFNetworkReachabilityStatusNotReachable:
                // We have no active connection - disable all requests and don’t let the user do anything
                [[NSNotificationCenter defaultCenter] postNotificationName:@"HideLoader" object:@""];
                
                [AlertViewManager showAlertWithTitle:@"No connection detected"
                                             message:@"Internet Connectivity lost, please try again"//You need an active internet connection to use this app."
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:@[]
                                buttonSelectionBlock:nil];
                return;
                break;
            default:
                // If we get here, we’re most likely timing out
                return;
                break;
        }
    }];
    
    // Set the reachabilityManager to actively wait for these events
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

- (void)doGETAsync:(BOOL)isAsync
           success:(SuccessHandler)success
        andFailure:(FailureHandler)failure
{
    HTTPClient *client = (HTTPClient*)[HTTPClient sharedClient];
    
    if (isAsync)
    {
        client = (AsyncHTTPClient*) [AsyncHTTPClient sharedAsyncClient];
    }
    
    [client doGET:self.requestPath
       parameters:self.requestData
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog(@"\n\n=============== Reponse: %@ ================\n%@\n==============================================================\n", self.requestPath,responseObject);
              
              if (responseObject != nil)
              {
                  success(responseObject);
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              NSLog(@"\n\n=============== Reponse: %@ ================\n%@\n==============================================================\n", self.requestPath,error.localizedDescription);
              
              //			  [AlertViewManager showAlertWithTitle:self.function
              //										   message:[error localizedDescription]
              //								 cancelButtonTitle:@"OK"
              //								 otherButtonTitles:nil
              //							  buttonSelectionBlock:nil];
              
              failure(error);
              
          }];
}

- (void)performDELETERequestWithData:(NSDictionary*)data
                             isAsync:(BOOL)isAsync
                             success:(SuccessHandler)success
                             failure:(FailureHandler)failure
{
    self.requestPath = [NSString stringWithFormat:@"%@/%@", self.path, self.function];
    
    [self createRequestData:data];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            case AFNetworkReachabilityStatusReachableViaWiFi:
            case AFNetworkReachabilityStatusReachableViaWWAN:
                // Our connection is fine
                // Resume our requests or do nothing
                [self doDELETEAsync:isAsync
                        WithSuccess:success
                         andFailure:failure];
                break;
            case AFNetworkReachabilityStatusNotReachable:
                // We have no active connection - disable all requests and don’t let the user do anything
                [[NSNotificationCenter defaultCenter] postNotificationName:@"HideLoader" object:@""];
                
                [AlertViewManager showAlertWithTitle:@"No connection detected"
                                             message:@"Internet Connectivity lost, please try again"//You need an active internet connection to use this app."
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:@[]
                                buttonSelectionBlock:nil];
                return;
                break;
            default:
                // If we get here, we’re most likely timing out
                return;
                break;
        }
    }];
    
    // Set the reachabilityManager to actively wait for these events
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

- (void)doDELETEAsync:(BOOL)isAsync
          WithSuccess:(SuccessHandler)success
           andFailure:(FailureHandler)failure
{
    HTTPClient *client = (HTTPClient*)[HTTPClient sharedClient];
    
    if (isAsync)
    {
        client = (AsyncHTTPClient*) [AsyncHTTPClient sharedAsyncClient];
    }
    
    [client doDELETE:self.requestPath
          parameters:self.requestData
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 
                 NSLog(@"\n\n=============== Reponse: %@ ================\n%@\n==============================================================\n", self.requestPath,responseObject);
                 
                 if (responseObject != nil)
                 {
                     success(responseObject);
                 }
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 
                 NSLog(@"\n\n=============== Reponse: %@ ================\n%@\n==============================================================\n", self.requestPath,error.localizedDescription);
                 
                 //				 [AlertViewManager showAlertWithTitle:self.function
                 //											  message:[error localizedDescription]
                 //									cancelButtonTitle:@"OK"
                 //									otherButtonTitles:nil
                 //								 buttonSelectionBlock:nil];
                 
                 failure(error);
                 
             }];
}

@end
