//
//  Supplier.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Supplier.h"

@implementation Supplier

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.supplierId = [decoder decodeObjectForKey:@"supplierId"];
		self.rating = [decoder decodeObjectForKey:@"rating"];
		self.cityOther = [decoder decodeObjectForKey:@"cityOther"];
		self.phoneNumber = [decoder decodeObjectForKey:@"phoneNumber"];
		self.profilePicURL = [decoder decodeObjectForKey:@"profilePicURL"];
		self.locationLat = [decoder decodeObjectForKey:@"locationLat"];
		self.locationLng = [decoder decodeObjectForKey:@"locationLng"];
		self.possibleProblems = [decoder decodeObjectForKey:@"pushToken"];
		self.labourRate = [decoder decodeObjectForKey:@"labourRate"];
		self.service = [decoder decodeObjectForKey:@"service"];
		self.province = [decoder decodeObjectForKey:@"province"];
		self.city = [decoder decodeObjectForKey:@"city"];
		self.company = [decoder decodeObjectForKey:@"company"];
		self.user = [decoder decodeObjectForKey:@"user"];
	}
	
	return self;
	
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.supplierId forKey:@"supplierId"];
	[encoder encodeObject:self.rating forKey:@"rating"];
	[encoder encodeObject:self.cityOther forKey:@"cityOther"];
	[encoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
	[encoder encodeObject:self.profilePicURL forKey:@"profilePicURL"];
	[encoder encodeObject:self.locationLat forKey:@"locationLat"];
	[encoder encodeObject:self.locationLng forKey:@"locationLng"];
	[encoder encodeObject:self.possibleProblems forKey:@"possibleProblems"];
	[encoder encodeObject:self.labourRate forKey:@"labourRate"];
	
	[encoder encodeObject:self.service forKey:@"service"];
	[encoder encodeObject:self.province forKey:@"province"];
	[encoder encodeObject:self.city forKey:@"city"];
	[encoder encodeObject:self.company forKey:@"company"];
	[encoder encodeObject:self.user forKey:@"user"];
}

- (CLLocationCoordinate2D)location
{
	return CLLocationCoordinate2DMake([self.locationLat doubleValue], [self.locationLng doubleValue]);
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nSupID: %@\nRating: %@\nCityOther: %@\nPhone: %@\nProfile: %@\nLocation: %@\nProblems: %@\nService: %@\nProvince: %@\nCity: %@\nCompany: %@\nUser: %@\n-------------------------------",
			self.supplierId,
			self.rating,
			self.cityOther,
			self.phoneNumber,
			self.profilePicURL,
			[NSString stringWithFormat:@"%f,%f",self.location.latitude,self.location.longitude],
			self.possibleProblems,
			self.service,
			self.province,
			self.city,
			self.company,
			self.user];
}

@end
