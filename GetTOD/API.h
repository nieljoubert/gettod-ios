//
//  API.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Handlers.h"

@interface API : NSObject

@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *function;
@property (nonatomic, assign) BOOL isAsync;

- (void)performPOSTRequestWithData:(NSDictionary*)data
                      inJSONFormat:(BOOL)isJSON
                           isAsync:(BOOL)isAsync
                           success:(SuccessHandler)success
                           failure:(FailureHandler)failure;

- (void)performGETRequestWithData:(NSDictionary*)data
                          isAsync:(BOOL)isAsync
                          success:(SuccessHandler)success
                          failure:(FailureHandler)failure;

- (void)performDELETERequestWithData:(NSDictionary*)data
                             isAsync:(BOOL)isAsync
                             success:(SuccessHandler)success
                             failure:(FailureHandler)failure;

@end
