//
//  ImageHelper.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageHelper : NSObject

+ (UIImage*)downloadImage:(NSString*)fromURL withName:(NSString*)name;
+ (void)saveImageToDisk:(UIImage*)image withName:(NSString*)name;
+ (UIImage*)getImageFromDisk:(NSString*)name;
+ (void)deleteImageNamed:(NSString*)name;

@end
