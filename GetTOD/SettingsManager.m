//
//  SettingsManager.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SettingsManager.h"
#import <CoreLocation/CoreLocation.h>

static SettingsManager *manager = nil;

@implementation SettingsManager

+ (SettingsManager*) manager
{
	return [self loadData];
}

+ (SettingsManager*) loadData
{
	if (manager == nil)
	{
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
		NSData *userData = [userDefaults objectForKey:@"GetTOD"];
		
		manager = (SettingsManager*) [NSKeyedUnarchiver unarchiveObjectWithData:userData];
		
		if (manager == nil)
		{
			manager = [[SettingsManager alloc] init];
			manager.authenticationToken = nil;
			manager.cardEncryptionKey = @"P1@t1num533d";
		}
	}
	
	return manager;
}

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.firstName = [decoder decodeObjectForKey:@"firstName"];
		self.lastName = [decoder decodeObjectForKey:@"lastName"];
		self.password = [decoder decodeObjectForKey:@"password"];
		self.phoneNumber = [decoder decodeObjectForKey:@"phoneNumber"];
		self.currentLocation = [decoder decodeObjectForKey:@"currentLocation"];
		self.authenticationToken = [decoder decodeObjectForKey:@"authenticationToken"];
		self.emailAddress = [decoder decodeObjectForKey:@"emailAddress"];
		self.username = [decoder decodeObjectForKey:@"username"];
		self.pushToken = [decoder decodeObjectForKey:@"pushToken"];
		self.profileImageURL = [decoder decodeObjectForKey:@"profileImageURL"];
		self.requestedLocationLat = [decoder decodeObjectForKey:@"requestedLocationLat"];
		self.requestedLocationLng = [decoder decodeObjectForKey:@"requestedLocationLng"];
		self.gotUserDetailsOnce = [decoder decodeBoolForKey:@"gotUserDetailsOnce"];
		self.loggedInWithFB = [decoder decodeBoolForKey:@"loggedInWithFB"];
		self.hasRegisteredForPushNotification = [decoder decodeBoolForKey:@"hasRegisteredForPushNotification"];
		self.currentLocationLat = [decoder decodeObjectForKey:@"currentLocationLat"];
		self.currentLocationLng = [decoder decodeObjectForKey:@"currentLocationLng"];
		self.currentHeading = [decoder decodeObjectForKey:@"currentHeading"];
        
		self.currentJob = [decoder decodeObjectForKey:@"job"];
		self.supplierStatus = [decoder decodeIntegerForKey:@"supplierStatus"];
		self.appState = [decoder decodeIntegerForKey:@"appState"];
		self.cancelType = [decoder decodeIntegerForKey:@"cancelType"];
		self.rejectReason = [decoder decodeObjectForKey:@"rejectReason"];
		self.currentJobAddress = [decoder decodeObjectForKey:@"currentJobAddress"];
		self.isInSA = [decoder decodeObjectForKey:@"isInSA"];
        
        self.pushInfo = [decoder decodeObjectForKey:@"pushInfo"];
        self.didHandlePush = [decoder decodeBoolForKey:@"didHandlePush"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.firstName forKey:@"firstName"];
	[encoder encodeObject:self.lastName forKey:@"lastName"];
	[encoder encodeObject:self.password forKey:@"password"];
	[encoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
	[encoder encodeObject:self.currentLocation forKey:@"currentLocation"];
	[encoder encodeObject:self.authenticationToken forKey:@"authenticationToken"];
	[encoder encodeObject:self.emailAddress forKey:@"emailAddress"];
	[encoder encodeObject:self.username forKey:@"username"];
	[encoder encodeObject:self.pushToken forKey:@"pushToken"];
	[encoder encodeObject:self.profileImageURL forKey:@"profileImageURL"];
	[encoder encodeObject:self.requestedLocationLat forKey:@"requestedLocationLat"];
	[encoder encodeObject:self.requestedLocationLng forKey:@"requestedLocationLng"];
	[encoder encodeBool:self.gotUserDetailsOnce forKey:@"gotUserDetailsOnce"];
	[encoder encodeBool:self.loggedInWithFB forKey:@"loggedInWithFB"];
	[encoder encodeBool:self.hasRegisteredForPushNotification forKey:@"hasRegisteredForPushNotification"];
	[encoder encodeObject:self.currentLocationLat forKey:@"currentLocationLat"];
	[encoder encodeObject:self.currentLocationLng forKey:@"currentLocationLng"];
    [encoder encodeObject:self.currentHeading forKey:@"currentHeading"];
    
	[encoder encodeObject:self.currentJob forKey:@"job"];
	[encoder encodeInteger:self.supplierStatus forKey:@"supplierStatus"];
	[encoder encodeInteger:self.appState forKey:@"appState"];
	[encoder encodeInteger:self.cancelType forKey:@"cancelType"];
	[encoder encodeObject:self.rejectReason forKey:@"rejectReason"];
	[encoder encodeObject:self.currentJobAddress forKey:@"currentJobAddress"];
	[encoder encodeInteger:self.isInSA forKey:@"isInSA"];
    
    [encoder encodeObject:self.pushInfo forKey:@"pushInfo"];
    [encoder encodeBool:self.didHandlePush forKey:@"didHandlePush"];
}

+ (void) save
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:manager];
	[userDefaults setObject:userData forKey:@"GetTOD"];
	[userDefaults synchronize];
}

+ (void) clear
{
	NSString *pushToken = [SettingsManager manager].pushToken;
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults removeObjectForKey:@"GetTOD"];
	[userDefaults synchronize];
	manager = nil;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
	if ([paths count] > 0)
	{
		NSError *error = nil;
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		NSString *directory = [paths objectAtIndex:0];
			
		for (NSString *file in [fileManager contentsOfDirectoryAtPath:directory error:&error])
		{
			NSString *filePath = [directory stringByAppendingPathComponent:file];
			NSLog(@"File : %@", filePath);
		  
			BOOL fileDeleted = [fileManager removeItemAtPath:filePath error:&error];
		  
			if (fileDeleted != YES || error != nil)
			{
				// Deal with the error...
			}
		}
	}
	
	[SettingsManager manager].pushToken = pushToken;
	[SettingsManager save];
}

- (CLLocationCoordinate2D)requestedLocationCoords
{
	return CLLocationCoordinate2DMake([self.requestedLocationLat floatValue], [self.requestedLocationLng floatValue]);
}

- (CLLocationCoordinate2D)currentLocationCoords
{
	return CLLocationCoordinate2DMake([self.currentLocationLat floatValue], [self.currentLocationLng floatValue]);
}
- (CLLocation*)currentLocationCL
{
	return [[CLLocation alloc] initWithLatitude:[self.currentLocationLat floatValue] longitude:[self.currentLocationLng floatValue]];
}

- (BOOL)isiOS8
{
	if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1)//8_4
	{
		return NO;
	}
	
	return YES;
}

- (BOOL)isClientApp
{
	if ([[[[NSProcessInfo processInfo] environment] objectForKey:@"TARGET"] isEqualToString:@"CLIENT"])
	{
		return YES;
	}
	
	return NO;
}

@end
