//
//  SearchResultCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "LPPlaceDetails.h"

@interface SearchResultCell : GTBoldTableViewCell

@property (nonatomic, strong) LPPlaceDetails *placeDetails;

- (void)setupCellWithPlaceDetails:(LPPlaceDetails*)placeDetails;

@end
