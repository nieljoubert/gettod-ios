//
//  AddNewMaterialCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/08.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AddNewItemCell.h"

@interface AddNewItemCell()

@property (nonatomic, strong) UIImageView *plusSign;
@property (nonatomic, strong) UILabel *addLabel;
@property (nonatomic, strong) UIView *containerView;

@end

@implementation AddNewItemCell

- (void)setupCellWithText:(NSString*)text showPlusSign:(bool)showPlus
{
	self.accessoryView = nil;
	
	if (self.containerView == nil)
	{
		self.containerView = [UIView new];
		self.containerView.translatesAutoresizingMaskIntoConstraints = NO;
		[self.contentView addSubview:self.containerView];
	}
	
	if (showPlus)
	{
		if (self.plusSign == nil)
		{
			self.plusSign = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"add_icon"]];
			self.plusSign.translatesAutoresizingMaskIntoConstraints = NO;
			self.plusSign.contentMode = UIViewContentModeScaleAspectFit;
			[self.containerView addSubview:self.plusSign];
		}
	}
	else
	{
		if (self.plusSign)
		{
			[self.plusSign removeFromSuperview];
		}
	}
	
	if (self.addLabel == nil)
	{
		self.addLabel = [UILabel new];
		self.addLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.addLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:13];
		self.addLabel.textColor = PINK_COLOR;
		self.addLabel.numberOfLines = 1;
		[self.containerView addSubview:self.addLabel];
	}
	
	self.addLabel.text = text;
	
	NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"text":self.addLabel,
																				 @"container":self.containerView}];
	
	NSDictionary *metrics = @{@"buttonW":[NSNumber numberWithFloat:[UIImage imageNamed:@"add_icon"].size.width]};
	
	if (showPlus)
	{
		[views addEntriesFromDictionary:@{@"image":self.plusSign}];
		
		[self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image(==buttonW)]-5-[text]|"
																				   options:0
																				   metrics:metrics
																					 views:views]];
		
		[self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|"
																				   options:0
																				   metrics:metrics
																					 views:views]];
		
		[self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[text]|"
																				   options:0
																				   metrics:metrics
																					 views:views]];
	}
	else
	{
		[self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[text]|"
																				   options:0
																				   metrics:metrics
																					 views:views]];
		
		[self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[text]|"
																				   options:0
																				   metrics:metrics
																					 views:views]];
	}
	
	
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[container]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.containerView
																 attribute:NSLayoutAttributeCenterX
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.contentView
																 attribute:NSLayoutAttributeCenterX
																multiplier:1
																  constant:0]];
	
	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.containerView
																 attribute:NSLayoutAttributeCenterY
																 relatedBy:NSLayoutRelationEqual
																	toItem:self.contentView
																 attribute:NSLayoutAttributeCenterY
																multiplier:1
																  constant:0]];
}

@end
