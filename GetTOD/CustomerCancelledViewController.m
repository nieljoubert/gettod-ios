//
//  CustomerCancelledViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CustomerCancelledViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "SettingsManager.h"

@interface CustomerCancelledViewController ()
@property (strong, nonatomic) IBOutlet UILabel *cancelledTextLabel;

@end

@implementation CustomerCancelledViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];
	
	self.cancelledTextLabel.numberOfLines = 0;
	self.cancelledTextLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"The customer will be charged a R%d call out fee for cancelling their request.",[[SettingsManager manager].currentJob.supplier.labourRate intValue]] attributes:attributes];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (IBAction)okayTapped:(id)sender
{
	self.navigationController.navigationBarHidden = NO;
	AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
	[appDelegate startAppWithPartnerHomeScreen];
}

@end
