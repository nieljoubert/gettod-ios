//
//  Job.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Job.h"

@implementation Job

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.jobId = [decoder decodeObjectForKey:@"jobId"];
		self.supplier = [decoder decodeObjectForKey:@"supplier"];
		self.problem = [decoder decodeObjectForKey:@"problem"];
		self.quote = [decoder decodeObjectForKey:@"quote"];
		self.customer = [decoder decodeObjectForKey:@"customer"];
		self.customerId = [decoder decodeObjectForKey:@"customerId"];
		self.additionalInstructions = [decoder decodeObjectForKey:@"additionalInstructions"];
		self.calculatedTime = [decoder decodeObjectForKey:@"calculatedTime"];
		self.calculatedDistance = [decoder decodeObjectForKey:@"calculatedDistance"];
		self.createdDate = [decoder decodeObjectForKey:@"createdDate"];
		self.jobLocationLat = [decoder decodeObjectForKey:@"jobLocationLat"];
		self.jobLocationLng = [decoder decodeObjectForKey:@"jobLocationLng"];
		self.cancelReasons = [decoder decodeObjectForKey:@"cancelReasons"];
	
		self.cancelledAtDate = [decoder decodeObjectForKey:@"cancelledAtDate"];
		self.arrivedAtDate = [decoder decodeObjectForKey:@"arrivedAtDate"];
		self.updatedDate = [decoder decodeObjectForKey:@"updatedDate"];
		self.quotePrice = [decoder decodeObjectForKey:@"quotePrice"];
	}
	
	return self;
	
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.jobId forKey:@"jobId"];
	[encoder encodeObject:self.supplier forKey:@"supplier"];
	[encoder encodeObject:self.problem forKey:@"problem"];
	[encoder encodeObject:self.quote forKey:@"quote"];
	[encoder encodeObject:self.customer forKey:@"customer"];
	[encoder encodeObject:self.customerId forKey:@"customerId"];
	[encoder encodeObject:self.additionalInstructions forKey:@"additionalInstructions"];
	[encoder encodeObject:self.calculatedTime forKey:@"calculatedTime"];
	[encoder encodeObject:self.calculatedDistance forKey:@"calculatedDistance"];
	[encoder encodeObject:self.createdDate forKey:@"createdDate"];
	[encoder encodeObject:self.jobLocationLat forKey:@"jobLocationLat"];
	[encoder encodeObject:self.jobLocationLng forKey:@"jobLocationLng"];
	[encoder encodeObject:self.cancelReasons forKey:@"cancelReasons"];
	
	[encoder encodeObject:self.cancelledAtDate forKey:@"cancelledAtDate"];
	[encoder encodeObject:self.arrivedAtDate forKey:@"arrivedAtDate"];
	[encoder encodeObject:self.updatedDate forKey:@"updatedDate"];
	[encoder encodeObject:self.quotePrice forKey:@"quotePrice"];
}

- (CLLocationCoordinate2D)jobLocationCoordinates
{
	return CLLocationCoordinate2DMake([self.jobLocationLat doubleValue], [self.jobLocationLng doubleValue]);
}

@end
