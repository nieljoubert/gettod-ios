//
//  City.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "City.h"

@implementation City

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.cityId = [decoder decodeObjectForKey:@"cityId"];
		self.name = [decoder decodeObjectForKey:@"name"];
		self.province = [decoder decodeObjectForKey:@"province"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.cityId forKey:@"cityId"];
	[encoder encodeObject:self.name forKey:@"name"];
	[encoder encodeObject:self.province forKey:@"province"];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nID: %@\nName: %@\nProv: %@\n-------------------------------",self.cityId,self.name,self.province];
}

@end
