//
//  EnRouteCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "Supplier.h"

@interface EnRouteCell : GTBoldTableViewCell

@property (nonatomic, strong) Supplier *supplier;

- (void)setupCellWithSupplier:(Supplier *)supplier andProblemDescription:(NSString*)problemDescription;

@end
