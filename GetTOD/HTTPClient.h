//
//  HTTPClient.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "Handlers.h"

@interface HTTPClient : AFHTTPRequestOperationManager

+ (HTTPClient *)sharedClient;
- (instancetype)initWithBaseURL:(NSURL *)url;

- (void)doPOST:(NSString *)URLString
  inJSONFormat:(BOOL)isJSON
	parameters:(id)parameters
	   success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
	   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)doGET:(NSString *)URLString
   parameters:(id)parameters
	  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
	  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)doDELETE:(NSString *)URLString
	  parameters:(id)parameters
		 success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
		 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end