//
//  Material.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Material : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *quantity;
@property (nonatomic, strong) NSNumber *costPerUnit;

- (NSNumber*)price;
@end
