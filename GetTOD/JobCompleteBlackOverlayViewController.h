//
//  JobCompleteBlackOverlayViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseBlackOverlayViewController.h"
#import "RateView.h"
#import "EnterTextViewController.h"

@interface JobCompleteBlackOverlayViewController : UIViewController <RateViewDelegate,EnterTextViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) UIImage *backgroundImage;

@property(nonatomic, weak) id delegate;

@end

@protocol JobCompleteBlackOverlayDelegate <NSObject>
- (void)dismissedController:(JobCompleteBlackOverlayViewController*)controller;
@end
