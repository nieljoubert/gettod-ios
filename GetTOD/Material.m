//
//  Material.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Material.h"

@implementation Material

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.name = [decoder decodeObjectForKey:@"name"];
		self.quantity = [decoder decodeObjectForKey:@"quantity"];
		self.costPerUnit = [decoder decodeObjectForKey:@"costPerUnit"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.name forKey:@"name"];
	[encoder encodeObject:self.quantity forKey:@"quantity"];
	[encoder encodeObject:self.costPerUnit forKey:@"costPerUnit"];
}

- (NSNumber*)price
{
	float total = [self.quantity intValue] * [self.costPerUnit floatValue];
	
	return [NSNumber numberWithFloat:total];
}
@end
