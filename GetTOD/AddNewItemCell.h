//
//  AddNewMaterialCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/08.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableViewCell.h"

@interface AddNewItemCell : GTTableViewCell

- (void)setupCellWithText:(NSString*)text showPlusSign:(bool)showPlus;

@end
