//
//  Card.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject

@property (nonatomic, strong) NSString *cardHolderName;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *lastDigits;
@property (nonatomic, strong) NSString *month;
@property (nonatomic, strong) NSString *year;
@property (nonatomic, strong) NSString *cvv;
@property (nonatomic, strong) NSString *brand;
@property (nonatomic, assign) int ident;
@property (nonatomic, strong) NSString *token;

@end
