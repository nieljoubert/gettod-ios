//
//  PaymentViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "PaymentViewController.h"
#import "AddEditCardDetailsViewController.h"
#import "JobCompleteBlackOverlayViewController.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.tableView.scrollEnabled = NO;
	
	self.title = @"Payment";
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleDone target:self action:@selector(addNew)];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[self getCards];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	self.navigationController.delegate = nil;
}

#pragma mark - Private -
- (void) getCards
{
	[self showLoader];
	
	[[APIManager manager] getCreditCards:^(NSMutableArray *cards) {
		[self hideLoader];
		self.cards = cards;
		[self.tableView reloadData];
		
	} failure:^(NSError *error) {
		[self hideLoader];
	}];
}

#pragma mark - Actions -
- (void)addNew
{
	AddEditCardDetailsViewController *addEdit = [[AddEditCardDetailsViewController alloc] init];
	addEdit.isAdding = YES;
	addEdit.isSettings = YES;
	[self.navigationController pushViewController:addEdit animated:YES];
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.cards.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PaymentCardCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"PaymentCardCell"];
	[cell setupCellWithCard:[self.cards objectAtIndex:indexPath.row] forScreen:NO];
	
	return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"CURRENT CARDS"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		Card *card = [self.cards objectAtIndex:indexPath.row];
		
		[[APIManager manager] deleteCreditCard:card
									   success:^(id result) {
										   
										   [AlertViewManager showAlertWithTitle:@""
																		message:[NSString stringWithFormat:@"Card named '%@' deleted successfully",card.name]
															  cancelButtonTitle:@"OK"
															  otherButtonTitles:nil
														   buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
															   
															   [[APIManager manager] getCreditCards:^(NSMutableArray *results) {
																   self.cards = results;
																   [self.tableView reloadData];
																   
															   } failure:^(NSError *error) {
																   
																   
															   }];
														   }];
										   
									   } failure:^(NSError *error) {
										   
										   
									   }];
	}
}

@end
