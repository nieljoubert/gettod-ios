//
//  NavigationDistanceTimeCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "NavigationDistanceTimeView.h"

@interface NavigationDistanceTimeView()

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) UIView *texts;
@property (nonatomic, strong) UIButton *myLocationButton;
@property (nonatomic, strong) UIImageView *speedometer;

@end

@implementation NavigationDistanceTimeView

- (id)init
{
    self = [super init];
    
    if (self) {}
    
    return self;
}

- (void)setupViewWithTime:(NSString*)timeString andDistance:(NSString*)distanceString
{
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    if (self.texts == nil)
    {
        self.texts = [UIView new];
        self.texts.translatesAutoresizingMaskIntoConstraints = NO;
        self.texts.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.texts];
    }
    
    if (self.timeLabel == nil)
    {
        self.timeLabel = [UILabel new];
        self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.timeLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:17];
        self.timeLabel.textColor = DARK_TEXT_COLOR;
        self.timeLabel.numberOfLines = 1;
        [self.texts addSubview:self.timeLabel];
    }
    
    self.timeLabel.text = timeString;
    
    if (self.distanceLabel == nil)
    {
        self.distanceLabel = [UILabel new];
        self.distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.distanceLabel.font = [UIFont fontWithName:FONT size:12];
        self.distanceLabel.textColor = GRAY_COLOR;
        self.distanceLabel.numberOfLines = 1;
        [self.texts addSubview:self.distanceLabel];
    }
    
    self.distanceLabel.text = distanceString;
    
    if (self.speedometer == nil)
    {
        self.speedometer = [UIImageView new];
        self.speedometer.translatesAutoresizingMaskIntoConstraints = NO;
        self.speedometer.image = [UIImage imageNamed:@"time_icon"];
        self.speedometer.contentMode = UIViewContentModeScaleAspectFit;
        self.speedometer.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.speedometer];
    }
    
    if (self.myLocationButton == nil)
    {
        //		self.myLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.myLocationButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.myLocationButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.myLocationButton.layer.cornerRadius = 5;
        self.myLocationButton.layer.borderWidth = 1;
        self.myLocationButton.layer.borderColor = PINK_COLOR.CGColor;
        
        
        //        [self.myLocationButton setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [self.myLocationButton addTarget:self action:@selector(triggerCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *iv = [UIImageView new];
        iv.translatesAutoresizingMaskIntoConstraints = NO;
        
//        UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        [color setFill];
//        CGContextTranslateCTM(context, 0, image.size.height);
//        CGContextScaleCTM(context, 1.0, -1.0);
//        CGContextClipToMask(context, CGRectMake(0, 0, image.size.width, image.size.height), [image CGImage]);
//        CGContextFillRect(context, CGRectMake(0, 0, image.size.width, image.size.height));
//        
//        coloredImg = UIGraphicsGetImageFromCurrentImageContext();
//        
//        UIGraphicsEndImageContext();
        
        
        iv.image = [[UIImage imageNamed:@"refresh"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        iv.contentMode = UIViewContentModeScaleAspectFill;
        iv.tintColor = PINK_COLOR;
        [self.myLocationButton addSubview:iv];
        
        [self.myLocationButton addConstraint:[NSLayoutConstraint constraintWithItem:iv
                                                                          attribute:NSLayoutAttributeCenterY
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.myLocationButton
                                                                          attribute:NSLayoutAttributeCenterY
                                                                         multiplier:1
                                                                           constant:-0.5]];
        
        [self.myLocationButton addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[image(20)]"
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:@{@"image":iv}]];
        
        [self.myLocationButton addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-3-[image(20)]-50-|"
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:@{@"image":iv}]];
        
        //[self.myLocationButton setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
        
        [self.myLocationButton setTitleColor:PINK_COLOR forState:UIControlStateNormal];
        [self.myLocationButton setTitle:@"          Reroute    " forState:UIControlStateNormal];
        self.myLocationButton.titleLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:12];
        self.myLocationButton.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.myLocationButton];
    }
    
    NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"time":self.timeLabel,
                                                                                 @"distance":self.distanceLabel,
                                                                                 @"texts":self.texts,
                                                                                 @"button":self.myLocationButton,
                                                                                 @"image":self.speedometer}];
    
    [self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[time]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views]];
    
    [self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[distance]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views]];
    
    [self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[time][distance]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[image(==imageW)]-10-[texts]->=15-[button]-10-|"//(==buttonW)
                                                                 options:0
                                                                 metrics:@{@"imageW":[NSNumber numberWithFloat:[UIImage imageNamed:@"time_icon"].size.width], @"buttonW":[NSNumber numberWithFloat:[UIImage imageNamed:@"my_location"].size.width]}
                                                                   views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-13-[image]-13-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[texts]-10-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[button]-10-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
}

- (void)triggerCurrentLocation
{
    [self.delegate mustGetCurrentLocationWithButtonTapped:self.myLocationButton];
}

@end
