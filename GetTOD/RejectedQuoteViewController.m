//
//  QuoteRejectedViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/02.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RejectedQuoteViewController.h"
#import "GTBoldTableViewCell.h"
#import "CancelationFeedbackViewController.h"

@interface RejectedQuoteViewController ()

@property (strong, nonatomic) IBOutlet UILabel *rejectProcessDescriptionLabel;

@end

@implementation RejectedQuoteViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	NSString *reason = [NSString stringWithFormat:@"Please wait to receive another quote from %@.\n\nPlease note that if you choose to cancel this job, a minimum call out fee of R%d will be charged.",[SettingsManager manager].currentJob.supplier.company.name,[[SettingsManager manager].currentJob.supplier.labourRate intValue]];
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];

	self.rejectProcessDescriptionLabel.numberOfLines = 0;
	self.rejectProcessDescriptionLabel.attributedText = [[NSAttributedString alloc] initWithString:reason attributes:attributes];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	self.navigationController.navigationBar.hidden = NO;
}

- (IBAction)cancelJob:(id)sender
{
	BaseTableViewBlackOverlayViewController *cancelList = [[BaseTableViewBlackOverlayViewController alloc] initWithNibName:@"BaseTableViewBlackOverlayViewController" bundle:nil];
	cancelList.listType = BlackOverlayListTypeCancelJob;
	cancelList.delegate = self;
	
	cancelList.cells = [NSMutableArray array];
	
	for (Reason *reason in [SettingsManager manager].currentJob.cancelReasons)
	{
		GTTableViewCell *cell = [GTTableViewCell new];
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.textColor = [UIColor whiteColor];
		cell.accessoryView = nil;
		cell.textLabel.text = reason.reasonDescription;
	
		[cancelList.cells addObject:cell];
	}
	
	cancelList.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	cancelList.backgroundImage = [self getBlurredBackgroundImage];
	
	[self.navigationController presentViewController:cancelList animated:YES completion:nil];
}


#pragma mark - BlackCellSelected -
- (void)cellSelected:(int)index WithText:(NSString *)text fromController:(BaseTableViewBlackOverlayViewController*)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
	
	if (text)
	{
		if (index == [SettingsManager manager].currentJob.cancelReasons.count)
		{
			[AlertViewManager showAlertWithTitle:@"Other Reason"
										 message:@"Other reason submission not implemented yet.\n\nPlease select an actual reason."
							   cancelButtonTitle:@"OK"
							   otherButtonTitles:nil
							buttonSelectionBlock:nil];
		}
		else
		{
			[[APIManager manager] cancelJob:[SettingsManager manager].currentJob.jobId
								   withType:JobCancelTypeDuringJob
									success:^(BOOL success) {
										
										[SettingsManager manager].appState = AppStateCancelledFromQuote;
										[SettingsManager save];
										
										if (success)
										{
											CancelationFeedbackViewController *feedback = [[CancelationFeedbackViewController alloc] initWithNibName:@"CancelationFeedbackViewController" bundle:nil];
											[self.navigationController pushViewController:feedback animated:NO];
										}
										
									} failure:^(NSError *error) {
										
									}];
		}
	}
}

- (void)dismissedController:(BaseTableViewBlackOverlayViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
}

@end
