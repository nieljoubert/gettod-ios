//
//  SupplierHomeMapViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/06.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"
#import "SettingsViewController.h"

@interface SupplierHomeMapViewController : RootViewController <SettingsViewControllerDelegate>

@end
