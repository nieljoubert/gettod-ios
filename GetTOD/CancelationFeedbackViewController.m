//
//  CancelationFeedbackViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/02.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CancelationFeedbackViewController.h"

@interface CancelationFeedbackViewController ()
@property (strong, nonatomic) IBOutlet UILabel *feedbackLabel;

@end

@implementation CancelationFeedbackViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationController.navigationBar.hidden = YES;
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];
	
	self.feedbackLabel.numberOfLines = 0;
	self.feedbackLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Your feedback helps us improve the service for you in the future." attributes:attributes];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBar.hidden = YES;
}

- (IBAction)okayTapped:(id)sender
{
	self.navigationController.navigationBarHidden = NO;
	AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
	[appDelegate startAppWithServicesList];
}

@end
