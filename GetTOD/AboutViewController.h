//
//  AboutViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"
#import <MessageUI/MessageUI.h>

@interface AboutViewController : BaseTableViewController  <MFMailComposeViewControllerDelegate>

@end
