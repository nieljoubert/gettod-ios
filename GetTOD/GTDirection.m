//
//  GTDirection.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/13.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTDirection.h"

@implementation GTDirection

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.directionDescription = [decoder decodeObjectForKey:@"directionDescription"];
		self.streetName = [decoder decodeObjectForKey:@"streetName"];
		self.directionImage = [decoder decodeObjectForKey:@"directionImage"];
		self.distance = [decoder decodeObjectForKey:@"distance"];
		self.distanceStringWithKM = [decoder decodeObjectForKey:@"distanceStringWithKM"];
		self.direction = [decoder decodeIntegerForKey:@"direction"];
		self.startLat = [decoder decodeObjectForKey:@"startLat"];
		self.startLng = [decoder decodeObjectForKey:@"startLng"];
		self.endLat = [decoder decodeObjectForKey:@"endLat"];
		self.endLng = [decoder decodeObjectForKey:@"endLng"];
	}
	
	return self;
	
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.directionDescription forKey:@"directionDescription"];
	[encoder encodeObject:self.streetName forKey:@"streetName"];
	[encoder encodeObject:self.directionImage forKey:@"directionImage"];
	[encoder encodeObject:self.distance forKey:@"distance"];
	[encoder encodeObject:self.distanceStringWithKM forKey:@"distanceStringWithKM"];
	[encoder encodeInteger:self.direction forKey:@"direction"];
	[encoder encodeObject:self.startLat forKey:@"startLat"];
	[encoder encodeObject:self.startLng forKey:@"startLng"];
	[encoder encodeObject:self.endLat forKey:@"endLat"];
	[encoder encodeObject:self.endLng forKey:@"endLng"];
}

- (CLLocationCoordinate2D)startLocation
{
	return CLLocationCoordinate2DMake([self.startLat doubleValue], [self.startLng doubleValue]);
}

- (CLLocationCoordinate2D)endLocation
{
	return CLLocationCoordinate2DMake([self.endLat doubleValue], [self.endLng doubleValue]);
}

@end
