//
//  ToBeSentToView.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ToBeSentToView.h"
#import "Constants.h"


@interface ToBeSentToView()

@property (nonatomic, strong) UILabel *toBeSentToLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UIImageView *downArrow;
@property (nonatomic, strong) UIView *backgroundView;

@end

@implementation ToBeSentToView

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	
	if (self)
	{
		[self setupViewWithAddress:@"Current Location"];
	}
	
	return self;
}

- (id)initWithAddress:(NSString*)address
{
	self = [super init];
	
	if (self)
	{
		if (address)
		{
			[self setupViewWithAddress:address];
		}
		else
		{
			[self setupViewWithAddress:@"Current Location"];
		}
	}
	
	return self;
}

- (void)setupViewWithAddress:(NSString*)address
{
	self.userInteractionEnabled = YES;
	self.backgroundColor = [UIColor clearColor];
	
	if (self.toBeSentToLabel == nil)
	{
		self.toBeSentToLabel = [UILabel new];
		self.toBeSentToLabel.backgroundColor = [UIColor clearColor];
		self.toBeSentToLabel.textColor = DARK_BLUE_FONT_COLOR;
		self.toBeSentToLabel.font = [UIFont fontWithName:FONT_BOLD size:10];
		self.toBeSentToLabel.text = @"T O  B E  S E N T  T O :";
		self.toBeSentToLabel.numberOfLines = 1;
		self.toBeSentToLabel.textAlignment = NSTextAlignmentCenter;
		[self.toBeSentToLabel sizeToFit];
		[self addSubview:self.toBeSentToLabel];
	}
	
	if (self.addressLabel == nil)
	{
		self.addressLabel = [UILabel new];
		self.addressLabel.backgroundColor = [UIColor clearColor];
		self.addressLabel.textColor = DARK_BLUE_FONT_COLOR;
		self.addressLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:15];
		self.addressLabel.numberOfLines = 1;
		self.addressLabel.textAlignment = NSTextAlignmentCenter;
		[self addSubview:self.addressLabel];
	}
	
	if (self.downArrow == nil)
	{
		self.downArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down"]];
		self.downArrow.contentMode = UIViewContentModeScaleAspectFit;
		self.downArrow.backgroundColor = [UIColor clearColor];
		[self addSubview:self.downArrow];
	}
	
	if (self.backgroundView == nil)
	{
		self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.frame.size.height)];
		self.backgroundView.backgroundColor = LIGHT_BLUE_COLOR;
		self.backgroundView.alpha = 0.8;
		
		[self addSubview:self.backgroundView];
		[self sendSubviewToBack:self.backgroundView];
	}
	
	self.addressLabel.text = address;
	[self.addressLabel sizeToFit];
	
	CGRect frame = self.toBeSentToLabel.frame;
	frame.origin.y = 28;
	frame.size.width = SCREEN_WIDTH;
	self.toBeSentToLabel.frame = frame;
	
	frame = self.addressLabel.frame;
	frame.origin.y = self.toBeSentToLabel.frame.origin.y + self.toBeSentToLabel.frame.size.height + 5;
	frame.size.width = SCREEN_WIDTH;
	self.addressLabel.frame = frame;
	
	self.downArrow.frame = CGRectMake(0, self.addressLabel.frame.origin.y + self.addressLabel.frame.size.height + 10, SCREEN_WIDTH, self.downArrow.frame.size.height);
}

@end
