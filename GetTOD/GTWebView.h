//
//  GTWebView.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/28.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"

@interface GTWebView : RootViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSString *urlRequest;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *bottomView;

- (id) initWithUrlRequest:(NSString *)urlRequest;
- (void) setupView;


@end
