//
//  GTWebView.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/28.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTWebView.h"

@interface GTWebView ()

@end

@implementation GTWebView

#pragma mark - Inits -

- (id) initWithUrlRequest:(NSString *)urlRequest
{
	if (self = [super init])
	{
		self.urlRequest = urlRequest;
	}
	return self;
}

#pragma mark - LifeCycle Methods -

- (void) viewDidLoad
{
	[super viewDidLoad];	
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) setupView
{
	NSURL *url = [NSURL URLWithString:self.urlRequest];
	
	self.webView = [[UIWebView alloc] init];
	self.webView.translatesAutoresizingMaskIntoConstraints = NO;
	[self.webView loadRequest:[NSURLRequest requestWithURL:url]];
	self.webView.delegate = self;
	self.webView.opaque = NO;
	self.webView.backgroundColor = [UIColor clearColor];
	
	[self.view addSubview:self.webView];
	
	
	NSMutableDictionary *views = [[NSMutableDictionary alloc] initWithDictionary:@{@"web":self.webView}];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[web]|"
																	  options:0
																	  metrics:nil
																		views:views]];
	NSString *verticalConstraint = @"V:|";
	
	if (self.topView)
	{
		[views addEntriesFromDictionary:@{@"top":self.topView}];
		[self.view addSubview:self.topView];
		verticalConstraint = [verticalConstraint stringByAppendingString:@"[top]"];
		
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[top]|"
																		  options:0
																		  metrics:nil
																			views:views]];
	}
	
	verticalConstraint = [verticalConstraint stringByAppendingString:@"[web]"];
	
	if (self.bottomView)
	{
		[views addEntriesFromDictionary:@{@"bottom":self.bottomView}];
		[self.view addSubview:self.bottomView];
		verticalConstraint = [verticalConstraint stringByAppendingString:@"[bottom]|"];
		
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[bottom]|"
																		  options:0
																		  metrics:nil
																			views:views]];
	}
	else
	{
		verticalConstraint = [verticalConstraint stringByAppendingString:@"|"];
	}
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:verticalConstraint
																	  options:0
																	  metrics:nil
																		views:views]];
}

#pragma mark - WebView Delegate -

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	if ([error code] != NSURLErrorCancelled)
	{
		
	}
}

@end
