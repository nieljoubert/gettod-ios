//
//  NewLocationManger.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/22.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

typedef void (^LocationRequestHandler)(BOOL success, CLLocation *location);
typedef void (^LocationSouthAfricaHandler)(BOOL inSA, NSString *message);

@protocol LocationManagerDelegate;

@interface LocationManager : NSObject

+ (LocationManager*) manager;

- (void) setDistanceFilter:(int)distance;
- (void) resetDistanceFilter;
- (void) requestCurrentLocation:(LocationRequestHandler) completionHandler;
- (void) checkIfUserInSouthAfrica:(LocationSouthAfricaHandler)completed;
- (void) startMonitoringSignificantLocationChanges: (LocationRequestHandler) completionHandler;
- (void) startMonitoringLocationChanges: (LocationRequestHandler) completionHandler;
- (void) stopMonitoringSignificantLocation;
- (void) startConstantUpdating;
- (void) stopUpdatingLocation;
- (void) checkforBackgroundPermissions;
- (BOOL) isLocationServicesActivated;
- (void) removeAllRegions;
- (void) addRegion:(CLCircularRegion*)region;
- (void) removeRegion:(CLCircularRegion*)region;

- (CLLocation*)getCurrentLocationValue;

@property(nonatomic, weak) id <LocationManagerDelegate> delegate;

@end


@protocol LocationManagerDelegate <NSObject>

@optional
- (void) locationMonitoringRequestLaunched;
- (void) locationMonitoringRequestCanceled;
- (void) locationMonitoringChangedSettings;
@end
