//
//  GTButtonDynamic.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTButtonDynamic.h"
#import "Constants.h"

@interface GTButtonDynamic()

@property (nonatomic, strong) UIColor *customBackgroundColour;
@property (nonatomic, strong) UIColor *customDisabledBackgroundColour;
@property (nonatomic, strong) UIColor *customHightlightedColor;
@property (nonatomic, strong) UIColor *customBorderColor;
@property (nonatomic, strong) UIColor *customTextColor;

@end

@implementation GTButtonDynamic

- (id) initWithType: (GTButtonType) buttonType
			 target: (id) target
			 action: (SEL) selector
		  titleText: (NSString*) title
{
	self = [super init];
	
	if (self)
	{
		[self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];

		[self.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
		[self setTitle:title forState:UIControlStateNormal];
		[self setAdjustsImageWhenHighlighted:NO];
		[self setTranslatesAutoresizingMaskIntoConstraints:NO];
		
		self.layer.borderWidth = .5;
		self.layer.cornerRadius = 2.2;
		
		[self setColorsForButtonType:buttonType];
	}
	
	return self;
}

- (void) setColorsForButtonType:(GTButtonType)type
{
	if (self.buttonType == GTButtonTypePink)
	{
		self.customBackgroundColour = PINK_COLOR;
		self.customDisabledBackgroundColour = [UIColor lightGrayColor];
		self.customHightlightedColor = NAVBAR_COLOR;
		self.customBorderColor = NAVBAR_COLOR;
		self.customTextColor = NAVBAR_COLOR;
		
	}
	else if (self.buttonType == GTButtonTypeWhite)
	{
		self.customBackgroundColour = NAVBAR_COLOR;
		self.customDisabledBackgroundColour = [UIColor lightGrayColor];
		self.customHightlightedColor = PINK_COLOR;
		self.customBorderColor = PINK_COLOR;
		self.customTextColor = PINK_COLOR;		
	}
	
	
	self.layer.borderColor = self.customBorderColor.CGColor;
	[self setBackgroundColor:self.customBackgroundColour];
	[self setTitleColor:self.customTextColor forState:UIControlStateNormal];
}

- (CGSize)intrinsicContentSize
{
	return CGSizeMake(UIViewNoIntrinsicMetric, 50);
}

- (void)setHighlighted:(BOOL)highlighted
{
	[super setHighlighted:highlighted];
	
	if (highlighted)
	{
		self.backgroundColor = self.customHightlightedColor;
	}
	else
	{
		self.backgroundColor = self.customBackgroundColour;
	}
}

- (void) setEnabled:(BOOL)enabled
{
	[super setEnabled:enabled];
	
	if (enabled)
	{
		self.backgroundColor = self.customBackgroundColour;
	}
	else
	{
		self.backgroundColor = self.customDisabledBackgroundColour;
	}
}

@end
