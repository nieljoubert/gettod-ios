//
//  CustomerCancelledViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerCancelledViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *calloutFeeTextLabel;

//@property(nonatomic, weak) id delegate;

@end

//@protocol CustomerCancelledDelegate <NSObject>
//- (void)customerCancelled:(CustomerCancelledViewController*)controller;
//@end
