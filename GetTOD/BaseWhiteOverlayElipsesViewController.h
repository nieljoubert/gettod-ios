//
//  BaseWhiteOverlayElipsesViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/05/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MONActivityIndicatorView.h"
#import "Constants.h"

@interface BaseWhiteOverlayElipsesViewController : UIViewController <MONActivityIndicatorViewDelegate>
@property (strong, nonatomic) IBOutlet MONActivityIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
