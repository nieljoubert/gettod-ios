//
//  RequestSupplierViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseMapViewController.h"
#import "BaseTableViewBlackOverlayViewController.h"
#import "EnterTextViewController.h"

@interface RequestSupplierViewController : RootViewController <UITableViewDataSource, UITableViewDelegate, BlackTableViewDelegate, EnterTextViewControllerDelegate>

@property (nonatomic, strong) Supplier *supplier;
@property (nonatomic, assign) CLLocationCoordinate2D requestCoordinates;
@property (nonatomic, strong) UIImage *mapImage;

@property (strong, nonatomic) NSNumber *timeToSupplier;
@property (strong, nonatomic) NSNumber *distanceToSupplier;

@end
