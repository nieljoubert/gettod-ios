//
//  EnRouteCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "EnRouteCell.h"

@implementation EnRouteCell

- (id)init
{
	self = [super init];
	
	if (self)
	{
	}
	
	return self;
}

- (void)setupCellWithSupplier:(Supplier *)supplier andProblemDescription:(NSString*)problemDescription
{
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	
	UILabel *companyName = [UILabel new];
	companyName.translatesAutoresizingMaskIntoConstraints = NO;
	companyName.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
	companyName.textColor = DARK_TEXT_COLOR;
	companyName.numberOfLines = 1;
	companyName.lineBreakMode = NSLineBreakByTruncatingTail;
	companyName.text = supplier.company.name;
	
	UILabel *problem = [UILabel new];
	problem.translatesAutoresizingMaskIntoConstraints = NO;
	problem.font = [UIFont fontWithName:FONT size:10];
	problem.textColor = GRAY_COLOR;
	problem.numberOfLines = 1;
	problem.lineBreakMode = NSLineBreakByTruncatingTail;
	problem.text = [NSString stringWithFormat:@"Problem: %@",problemDescription];
	
	UILabel *rating = [UILabel new];
	rating.translatesAutoresizingMaskIntoConstraints = NO;
	rating.font = [UIFont fontWithName:FONT size:10];
	rating.textColor = DARK_TEXT_COLOR;
	rating.backgroundColor = GREEN_COLOR;
	rating.textAlignment = NSTextAlignmentCenter;
	rating.layer.cornerRadius = 3;
	rating.layer.masksToBounds = YES;
	rating.text = [NSString stringWithFormat:@"%.1f",[supplier.rating floatValue]];
	
	UIView *ratingView = [UIView new];
	ratingView.translatesAutoresizingMaskIntoConstraints = NO;
	ratingView.backgroundColor = GREEN_COLOR;
	ratingView.layer.cornerRadius = 3;
	ratingView.layer.masksToBounds = YES;
	[ratingView addSubview:rating];
	
	UIView *ratingAndProblemView = [UIView new];
	ratingAndProblemView.translatesAutoresizingMaskIntoConstraints = NO;
	ratingAndProblemView.backgroundColor = [UIColor clearColor];
	[ratingAndProblemView addSubview:ratingView];
	[ratingAndProblemView addSubview:problem];
	
	[self.contentView addSubview:ratingAndProblemView];
	[self.contentView addSubview:companyName];
	
	NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"name":companyName,
																				 @"problem":problem,
																				 @"rating":rating,
																				 @"ratingView":ratingView,
																				 @"ratingProb":ratingAndProblemView}];
	
	
	[ratingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rating]|"
																			options:0
																			metrics:nil
																			  views:views]];
	
	[ratingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[rating]|"
																			options:0
																			metrics:nil
																			  views:views]];
	
	// Rating and problem
	[ratingAndProblemView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ratingView(==25)]-10-[problem]|"
																				 options:0
																				 metrics:nil
																				   views:views]];
	
	[ratingAndProblemView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ratingView(==17)]|"
																				 options:0
																				 metrics:nil
																				   views:views]];
	
	[ratingAndProblemView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[problem(==ratingView)]|"
																				 options:0
																				 metrics:nil
																				   views:views]];
	
	// Content
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[name]-(>=20)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[ratingProb]-(>=20)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(<=25)-[name]-(5)-[ratingProb]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:views]];
}

@end
