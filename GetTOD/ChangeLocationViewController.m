//
//  ChangeLocationViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ChangeLocationViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LocationManager.h"

@interface ChangeLocationViewController ()

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *currentLocationText;

@end

@implementation ChangeLocationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.title = @"Change Location";
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
		
	self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
	self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.backgroundView = nil;
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	self.tableView.scrollEnabled = NO;
	
	[self.tableView registerClass:[GTTableViewCell class] forCellReuseIdentifier:@"GTTableViewCell"];
	
	[self.view addSubview:self.tableView];
	self.tableView.tableFooterView = [self createFooterView];
	
	GraySeachBarView *searchView = [[GraySeachBarView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
	searchView.delegate = self;
	
	self.tableView.tableHeaderView = searchView;
}

- (UIView*)createFooterView
{
	UILabel *textLabel = [UILabel new];
	textLabel.font = [UIFont fontWithName:FONT size:13];
	textLabel.textColor = PINK_COLOR;
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.text = @"Use your current location";
	[textLabel sizeToFit];
	
	UIImageView *cameraImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_icon"]];
	cameraImage.contentMode = UIViewContentModeScaleAspectFit;
	
	CGRect frame = cameraImage.frame;
	frame.origin = CGPointMake(0, 0);
	frame.size = CGSizeMake(cameraImage.frame.size.width, cameraImage.frame.size.height);
	cameraImage.frame = frame;
	
	frame = textLabel.frame;
	frame.origin = CGPointMake(cameraImage.frame.size.width + 5, 0);
	frame.size = CGSizeMake(textLabel.frame.size.width, cameraImage.frame.size.height);
	textLabel.frame = frame;
	
	UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cameraImage.frame.size.width+5+textLabel.frame.size.width, 20)];
	[container addSubview:cameraImage];
	[container addSubview:textLabel];
	[container sizeToFit];
	
	UITapGestureRecognizer *locationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(useCurrentLocation)];
	locationTap.numberOfTapsRequired = 1;
	[container addGestureRecognizer:locationTap];
	
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
	[headerView addSubview:container];
	
	container.center = CGPointMake(SCREEN_WIDTH/2, headerView.center.y);
	
	return headerView;
}

- (void)useCurrentLocation
{
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		[self hideLoader];
		
		if (success)
		{
			GMSGeocoder *geocoder = [GMSGeocoder geocoder];
			[geocoder reverseGeocodeCoordinate:location.coordinate
							 completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
								 
								 if (response)
								 {
									 self.currentLocationText = [NSString stringWithFormat:@"%@, %@",response.firstResult.thoroughfare,response.firstResult.locality];
									 [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
								 }
							 }];
		}
	}];
}

- (void)save
{
	if (self.currentLocationText)
	{
		[self.delegate locationChanged:self.currentLocationText];
	}
	
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)searchButtonTapped
{
	MapSearchModalViewController *searchController = [[MapSearchModalViewController alloc] init];
	searchController.delegate = self;
	searchController.infoText.text = @"Enter your town or suburb where you are currently situated";
	[self.navigationController presentViewController:searchController animated:YES completion:nil];
}

#pragma mark - MapViewModalDelegate

- (void)locationSelected:(LPPlaceDetails *)locationDetails
{
	NSArray *components = locationDetails.addressComponents;
	
	NSString *town = @"";
	NSString *province = @"";
	NSString *country = @"";
	
	for (LPAddressComponent* com in components)
	{
		NSLog(@"Com : %@",com.longName);
		for (NSString *type in com.types)
		{
			NSLog(@"Type : %@",type);
			
			if ([type isEqualToString:@"sublocality"]) // Millerton
			{
				town = com.longName;
			}
			if ([type isEqualToString:@"locality"]) // Cape Town
			{
//				town = com.longName;
			}
			if ([type isEqualToString:@"administrative_area_level_1"]) // Western Cape
			{
				province = com.longName;
			}
			if ([type isEqualToString:@"country"]) // South Africa
			{
				country = com.longName;
			}
		}
	}
	
	self.currentLocationText = [NSString stringWithFormat:@"%@, %@, %@",town,province,country];
	[self.tableView reloadData];
}

#pragma mark - TableView -

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	GTTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTTableViewCell"];
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.accessoryView = nil;
	
	if (self.currentLocationText)
	{
		cell.textLabel.text = self.currentLocationText;
	}
	else
	{
		cell.textLabel.text = @"";
	}
	
	return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return [[GTTableSectionHeaderView alloc] initWithTitle:@"LOCATION"];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
