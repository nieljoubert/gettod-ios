//
//  SupplierQuoteViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BaseTableViewBlackOverlayViewController.h"
#import "AddMaterialViewController.h"
#import "Quote.h"
#import "EnterTextViewController.h"

@interface SupplierQuoteViewController : BaseTableViewController <QuantityCellDelegate, AddMaterialViewControllerDelegate, EnterTextViewControllerDelegate>

@property (nonatomic, strong) Quote *quote;
@property (nonatomic, assign) int serviceId;

@end
