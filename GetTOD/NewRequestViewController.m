//
//  NewRequestViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "NewRequestViewController.h"
#import "ProfileImageAndDetailsCell.h"
#import "ButtonCountDownAnimatedView.h"
#import "NavigationDirectionsViewController.h"
#import "LPGoogleFunctions.h"
#import "GTButton.h"
#import "GoogleMaps/GoogleMaps.h"

@interface NewRequestViewController ()

@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UILabel *kilometersLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLeftLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet GTButton *acceptRequestButton;

@property (nonatomic, strong) ProfileImageAndDetailsCell *profileCell;

@property (nonatomic, strong) CAShapeLayer *strokeLayer;
@property (nonatomic, strong) CAShapeLayer *circleLayer;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSDate *startDate;

@property (nonatomic, assign) CLLocationCoordinate2D currentLocation;

@property (nonatomic, strong) LPDirections *directions;

@end

@implementation NewRequestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self.tableView registerClass:[ProfileImageAndDetailsCell class] forCellReuseIdentifier:@"ProfileImageAndDetailsCell"];
	
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[SettingsManager manager].currentJob.jobLocationLat doubleValue]
															longitude:[[SettingsManager manager].currentJob.jobLocationLng doubleValue]
																 zoom:15];
	
	GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.innerView.frame.size.width, self.innerView.frame.size.height) camera:camera];
	mapView.backgroundColor = [UIColor clearColor];
	mapView.mapType = kGMSTypeNormal;
	
	mapView.accessibilityElementsHidden = NO;
	mapView.settings.tiltGestures = NO;
	mapView.settings.scrollGestures = NO;
	mapView.settings.rotateGestures = YES;
	mapView.settings.zoomGestures = YES;
	mapView.settings.compassButton = NO;
	mapView.settings.myLocationButton = NO;
	[self.innerView addSubview:mapView];
	
	GMSMarker *marker = [GMSMarker markerWithPosition:[SettingsManager manager].currentJob.jobLocationCoordinates];
	marker.icon = [UIImage imageNamed:@"location_marker_gray_large"];
	marker.appearAnimation = kGMSMarkerAnimationPop;
	marker.flat = YES;
	marker.map = mapView;
	
	self.acceptRequestButton.enabled = NO;
	
	[self showLoaderWithMessage:@"Calculating distance"];
	
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		self.currentLocation = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
		
		[self getDirections];
		
	}];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	[self.view layoutIfNeeded];
	self.innerView.layer.cornerRadius = self.innerView.frame.size.width/2;
}

#pragma mark - Actions -
- (IBAction)acceptRequest:(id)sender
{
	[self showLoader];
	[self stopTimer];
	[self cancelAnimation];
	
	[[APIManager manager] acceptJob:[SettingsManager manager].currentJob.jobId
							success:^(BOOL success) {

								[self hideLoader];
								
								[SettingsManager manager].appState = AppStateEnroute;
								[SettingsManager save];
								
								NavigationDirectionsViewController *directionsController = [[NavigationDirectionsViewController alloc] initWithNibName:@"NavigationDirectionsViewController" bundle:nil];
								directionsController.directions = self.directions;
								directionsController.supplierLocationCoordinates = self.currentLocation;
								[self.navigationController pushViewController:directionsController animated:YES];
								
							} failure:^(NSError *error) {
								[self hideLoader];
							}];
}

- (void)getDirections
{
	LPGoogleFunctions *mapsThing = [[LPGoogleFunctions alloc] init];
	
	[mapsThing loadDirectionsForOrigin:[LPLocation locationWithLatitude:self.currentLocation.latitude
															  longitude:self.currentLocation.longitude]
						forDestination:[LPLocation locationWithLatitude:[SettingsManager manager].currentJob.jobLocationCoordinates.latitude
															  longitude:[SettingsManager manager].currentJob.jobLocationCoordinates.longitude]
				  directionsTravelMode:LPGoogleDirectionsTravelModeDriving
				  directionsAvoidTolls:LPGoogleDirectionsAvoidNone
						directionsUnit:LPGoogleDirectionsUnitMetric
				directionsAlternatives:NO
						 departureTime:[NSDate date]
						   arrivalTime:nil
							 waypoints:nil
					   successfulBlock:^(LPDirections *directions) {
						   
						   [self hideLoader];
						   
						   self.directions = directions;
						   
						   LPRoute *route = [self.directions.routes objectAtIndex:0];
						   LPLeg *leg = [route.legs objectAtIndex:0];
						   
						   self.kilometersLabel.text = [NSString stringWithFormat:@"%.1f",((float)leg.distance.value)/1000];
						   
						   self.acceptRequestButton.enabled = YES;
						   
						   [[GMSGeocoder geocoder] reverseGeocodeCoordinate:[SettingsManager manager].currentJob.jobLocationCoordinates
												 completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
													 
													 if (response)
													 {
														 GMSAddress *address = response.firstResult;
														 
														 
														 NSString *string = address.lines[0];
														 
//														 if (address.lines.count > 1)
//														 {
//															 string = [string stringByAppendingString:address.lines[1]];
//														 }
														 
														 [SettingsManager manager].currentJobAddress = string;
														 [SettingsManager save];
													 }
													 
													 [self startTimer];
													 [self antiClockwiseAnimation];
												 }];
						   
						   
						   
						   
						   
						   
						   
						   
						   
						   
						   
						   
					   } failureBlock:^(LPGoogleStatus status) {
						   [self hideLoader];
					   }];
}

#pragma mark - TableView -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.profileCell = [tableView dequeueReusableCellWithIdentifier:@"ProfileImageAndDetailsCell"];
	
	[self.profileCell setupCellWithJob:[SettingsManager manager].currentJob forCustomer:YES];

	return self.profileCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 75;
}

#pragma mark - Timer -
- (void)startTimer
{
	if (self.timer)
	{
		[self.timer invalidate];
		self.timer = nil;
	}
	
	self.startDate = [NSDate date];
	
	self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5
												  target:self
												selector:@selector(updateTimer)
												userInfo:nil
												 repeats:YES];
}

- (void)updateTimer
{
	int seconds = 31;
	
	NSDate *currentDate = [NSDate date];
	NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:self.startDate];
	NSTimeInterval timeIntervalCountDown = seconds - timeInterval;
	
	NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeIntervalCountDown];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"mm:ss"];
	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	
	NSString *timeString = [dateFormatter stringFromDate:timerDate];
	
	self.timeLeftLabel.text = [NSString stringWithFormat:@"%@ to accept",timeString];
}

- (void)stopTimer
{
	[self.timer invalidate];
	self.timer = nil;
	[self updateTimer];
}

#pragma mark - Animation stuff -
- (void)antiClockwiseAnimation
{
	int width = 2;
	int radius = self.innerView.frame.size.width/2;
	int duration = 30;
	
	self.circleLayer = [CAShapeLayer layer];
	[self.circleLayer setPath:[[UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.innerView.frame.origin.x-2*width, self.innerView.frame.origin.y-2*width, 2*(radius+2*width), 2*(radius+2*width)) cornerRadius:radius] CGPath]];
	[self.circleLayer setFillColor:[[UIColor clearColor] CGColor]];
	[self.circleLayer setStrokeColor:[TIMING_BLUE_COLOR CGColor]];
	[self.circleLayer setLineWidth:width];
	self.circleLayer.lineDashPattern = @[@1, @3];
	[self.view.layer addSublayer:self.circleLayer];
	
	self.strokeLayer = [CAShapeLayer layer];
	[self.strokeLayer setPath:self.circleLayer.path];
	[self.strokeLayer setFillColor:[[UIColor clearColor] CGColor]];
	[self.strokeLayer setStrokeColor:[TIMING_BLUE_COLOR CGColor]];
	[self.strokeLayer setLineWidth:width];
	[self.strokeLayer setLineCap:kCALineCapRound];
	[self.view.layer addSublayer:self.strokeLayer];
	
	CABasicAnimation *strokeAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];//@"strokeStart"];
	[strokeAnimation setDelegate:self];
	[strokeAnimation setDuration:duration];
	[strokeAnimation setRepeatCount:1];
	[strokeAnimation setFromValue:@1];
	[strokeAnimation setToValue:@0];
	[self.strokeLayer addAnimation:strokeAnimation forKey:@"antiClockwiseAnimation"];
}

- (void)cancelAnimation
{
	[self.circleLayer removeFromSuperlayer];
	[self.strokeLayer removeFromSuperlayer];
}

- (void)finishAnimation
{
	[self cancelAnimation];
	
	self.timeLeftLabel.text = [NSString stringWithFormat:@"00:00 to accept"];
	[self stopTimer];

	self.view.userInteractionEnabled = NO;
	
    [self showLoader];
    
	[[APIManager manager] cancelJob:[SettingsManager manager].currentJob.jobId
						   withType:JobCancelTypeTimeOut
							success:^(BOOL success) {
                                [self hideLoader];
                                [self.navigationController popViewControllerAnimated:YES];
							} failure:^(NSError *error) {
                                [self hideLoader];
							}];
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	if (flag)
	{
		[self finishAnimation];
	}
	else
	{
		[self cancelAnimation];
	}
}
@end
