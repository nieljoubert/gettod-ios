//
//  JobInProgressViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/02.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "JobInProgressViewController.h"
#import "JobCompleteBlackOverlayViewController.h"
#import "MONActivityIndicatorView.h"
#import "Constants.h"

@interface JobInProgressViewController () <MONActivityIndicatorViewDelegate>
@property (strong, nonatomic) IBOutlet MONActivityIndicatorView *indicatorView;
@property (strong, nonatomic) IBOutlet UILabel *footerLabel;

@end

@implementation JobInProgressViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationController.navigationBarHidden = YES;

	self.indicatorView.delegate = self;
	self.indicatorView.numberOfCircles = 3;
	self.indicatorView.radius = 2.5;
	self.indicatorView.internalSpacing = 5;
	self.indicatorView.duration = 1.25;
	self.indicatorView.delay = 0.5;
	
	[self.indicatorView startAnimating];
	
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];
	
	self.footerLabel.numberOfLines = 0;
	self.footerLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Take it easy while the expert does their thing, we will let you know when the job is complete." attributes:attributes];

}

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
	  circleBackgroundColorAtIndex:(NSUInteger)index {
	return PINK_COLOR;
}

@end
