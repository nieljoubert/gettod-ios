//
//  MaterialQuantityCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableViewCell.h"

@interface QuantityAdjustmentCell : GTTableViewCell

@property(nonatomic, weak) id delegate;
- (void)setupCellWithSubtitle:(BOOL)hasSubTitle withQuantity:(int)quant;

@end

@protocol QuantityCellDelegate <NSObject>
- (void)quantityChanged:(int)quantity;
@end