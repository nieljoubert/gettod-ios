//
//  Validation.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/23.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Validation.h"

@implementation Validation

+ (BOOL) isStringNumbersOnly: (NSString*) aString
{
	//check if start with '.'
	if ([aString hasPrefix:@"."] == true)
	{
		return false;
	}
	
	//check if has more than two '.'
	NSString *str = [aString stringByReplacingOccurrencesOfString:@"." withString:@""];
	
	if (aString.length > str.length + 1)
	{
		return false;
	}
	
	//check if contains only numbers
	str = aString;
	str = [str stringByReplacingOccurrencesOfString:@"0" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"1" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"2" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"3" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"4" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"5" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"6" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"7" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"8" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"9" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"." withString:@""];
	
	if (str.length > 0)
	{
		return false;
	}
	
	return true;
}

+ (BOOL) isStringAnInteger: (NSString *) aString
{
	//check if start with '.'
	if ([aString hasPrefix:@"."] == true)
	{
		return false;
	}
	
	//check if has more than two '.'
	NSString *str = [aString stringByReplacingOccurrencesOfString:@"." withString:@""];
	
	if (aString.length > str.length + 1)
	{
		return false;
	}
	
	//check if contains only numbers
	str = aString;
	str = [str stringByReplacingOccurrencesOfString:@"0" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"1" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"2" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"3" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"4" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"5" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"6" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"7" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"8" withString:@""];
	str = [str stringByReplacingOccurrencesOfString:@"9" withString:@""];
	
	if (str.length > 0)
	{
		return false;
	}
	return true;
}

+ (BOOL) isStringNumbersAndLettersOnly:(NSString*) candidate
{
	NSString *stringRegex = @"[A-Z0-9a-z]*";
	NSPredicate *stringTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stringRegex];
	return [stringTest evaluateWithObject:candidate];
}

+ (BOOL) isValidPhoneNumber: (NSString*) candidate
{
	NSString *stringRegex = @"\\(?([0-9]{1,3})\\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";
	NSPredicate *stringTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stringRegex];
	return [stringTest evaluateWithObject:candidate];
}

+ (BOOL) isValidEmail: (NSString*) candidate
{
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	
	return [emailTest evaluateWithObject:candidate];
}

+ (BOOL) isStringCharactersOnly: (NSString*) candidate
{
	NSString *stringRegex = @"^[a-zA-Z']+((\\s|\\-)[a-zA-Z'\\s]+)?$";
	NSPredicate *stringTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stringRegex];
	return [stringTest evaluateWithObject:candidate];
}

+ (BOOL) isUsernameValid: (NSString*) aString
{
	BOOL isValid = NO;
	return isValid;
	
}
@end
