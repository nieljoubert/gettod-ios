//
//  TaskHistoryCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "TaskHistoryCell.h"
#import "Quote.h"

@interface TaskHistoryCell ()

@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *jobLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UIImageView *checkmark;
@property (nonatomic,strong) UIImageView *divider;

@property (nonatomic,strong) UIView *priceDateView;
@property (nonatomic,strong) UIView *texts;

@end


@implementation TaskHistoryCell


- (void)setupCellWithTask:(Job*)job
{
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	
	if (self.nameLabel == nil)
	{
		self.nameLabel = [UILabel new];
		self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.nameLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.nameLabel.textColor = DARK_TEXT_COLOR;
		self.nameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		self.nameLabel.backgroundColor = [UIColor clearColor];
	}
	
	if (self.jobLabel == nil)
	{
		self.jobLabel = [UILabel new];
		self.jobLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.jobLabel.font = [UIFont fontWithName:FONT size:12];
		self.jobLabel.textColor = GRAY_COLOR;
		self.jobLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		self.jobLabel.backgroundColor = [UIColor clearColor];
	}
	
	if (self.priceLabel == nil)
	{
		self.priceLabel = [UILabel new];
		self.priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.priceLabel.font = [UIFont fontWithName:FONT size:12];
		self.priceLabel.textColor = GRAY_COLOR;
		self.priceLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		self.priceLabel.backgroundColor = [UIColor clearColor];
	}
	
	if (self.dateLabel == nil)
	{
		self.dateLabel = [UILabel new];
		self.dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.dateLabel.font = [UIFont fontWithName:FONT size:12];
		self.dateLabel.textColor = GRAY_COLOR;
		self.dateLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		self.dateLabel.backgroundColor = [UIColor clearColor];
	}
	
	if (self.divider == nil)
	{
		self.divider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"task_history_price_date_divider"]];
		self.divider.translatesAutoresizingMaskIntoConstraints = NO;
		self.divider.contentMode = UIViewContentModeScaleAspectFit;
	}
	
	if (self.priceDateView == nil)
	{
		self.priceDateView = [UIView new];
		self.priceDateView.translatesAutoresizingMaskIntoConstraints = NO;
		self.priceDateView.backgroundColor = [UIColor clearColor];
		[self.priceDateView addSubview:self.priceLabel];
		[self.priceDateView addSubview:self.dateLabel];
		[self.priceDateView addSubview:self.divider];
	}
	
	if (self.texts == nil)
	{
		self.texts = [UIView new];
		self.texts.translatesAutoresizingMaskIntoConstraints = NO;
		self.priceDateView.backgroundColor = [UIColor clearColor];
		[self.texts addSubview:self.jobLabel];
		[self.texts addSubview:self.nameLabel];
		[self.texts addSubview:self.priceDateView];
		[self.contentView addSubview:self.texts];
	}
	
	if (self.checkmark == nil)
	{
		self.checkmark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"task_history_tickbox_enabled"]];
		self.checkmark.translatesAutoresizingMaskIntoConstraints = NO;
		self.priceDateView.backgroundColor = [UIColor clearColor];
		self.checkmark.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.checkmark];
	}
	
#ifdef CLIENT
	self.nameLabel.text = job.supplier.company.name;
#else
	self.nameLabel.text = job.customer.name;
#endif

	self.jobLabel.text = job.problem.problemDescription;
	self.priceLabel.text = [NSString stringWithFormat:@"R %d",[job.quotePrice intValue]];

	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	[dateFormatter setDateFormat:@"dd MMM YYYY"];
	
	self.dateLabel.text = [dateFormatter stringFromDate:job.createdDate];
	
	NSDictionary *views = @{@"name":self.nameLabel,
							@"job":self.jobLabel,
							@"price":self.priceLabel,
							@"date":self.dateLabel,
							@"priceDate":self.priceDateView,
							@"texts":self.texts,
							@"div":self.divider,
							@"check":self.checkmark};
	
	NSDictionary *metrics = @{@"checkW":[NSNumber numberWithFloat:self.checkmark.frame.size.width],
							  @"divW":[NSNumber numberWithFloat:self.divider.frame.size.width],
							  @"padding":@CELL_PADDING};
	
	
	[self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[name]-padding-|"
																	   options:0
																	   metrics:metrics
																		 views:views]];
	
	[self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[job]-padding-|"
																	   options:0
																	   metrics:metrics
																		 views:views]];
	
	[self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[priceDate]-padding-|"
																	   options:0
																	   metrics:metrics
																		 views:views]];
	
	[self.texts addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[name]-2-[job]-3-[priceDate]-12-|"
																	   options:0
																	   metrics:metrics
																		 views:views]];
	
	
	[self.priceDateView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[price(==date)]|"
																			   options:0
																			   metrics:metrics
																				 views:views]];
	
	[self.priceDateView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[div(==date)]|"
																			   options:0
																			   metrics:metrics
																				 views:views]];
	
	[self.priceDateView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[date]|"
																			   options:0
																			   metrics:metrics
																				 views:views]];
	
	[self.priceDateView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[price]-7-[div(==divW)]-7-[date]"
																			   options:0
																			   metrics:metrics
																				 views:views]];
	
	
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[check(==checkW)]-padding-[texts]-padding-|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[check(==texts)]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[texts]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
}

@end
