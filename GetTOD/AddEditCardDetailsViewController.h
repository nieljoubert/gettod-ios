//
//  AddEditCardDetailsViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"
#import "CardIO.h"
#import "Card.h"
#import "JobCompleteBlackOverlayViewController.h"
#import "ReferAFriendBlackOverlayViewController.h"
#import "CreatingAccountBlackOverlayViewController.h"


@interface AddEditCardDetailsViewController : BaseTableViewController <CardIOPaymentViewControllerDelegate, CardIOViewDelegate,UITextFieldDelegate, JobCompleteBlackOverlayDelegate, CreatingAccountBlackOverlayDelegate>

@property (nonatomic, assign) BOOL isAdding;
@property (nonatomic, assign) BOOL isSettings;
@property (nonatomic, strong) Card *card;

@end
