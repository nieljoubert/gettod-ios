//
//  SupplierSelectorViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseMapViewController.h"
#import "GTSearchBarView.h"
#import "MapSearchModalViewController.h"
#import "LPGoogleFunctions.h"

@interface ChooseSupplierViewController : BaseMapViewController <UITableViewDataSource, UITableViewDelegate, GTSearchBarViewDelegate, MapSearchModalDelegate, BaseMapViewDelegate, LPGoogleFunctionsDelegate>

@property (nonatomic, strong) NSMutableArray *suppliers;
@property (nonatomic, strong) NSNumber *serviceTypeID;

@end
