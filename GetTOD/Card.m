//
//  Card.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Card.h"
#import <CommonCrypto/CommonCryptor.h>

@implementation Card

- (void)encodeWithCoder:(NSCoder *)aCoder
{
	[aCoder encodeObject:self.cardHolderName forKey:@"name"];
	[aCoder encodeObject:self.name forKey:@"cardName"];
	[aCoder encodeObject:self.number forKey:@"number"];
	[aCoder encodeObject:self.lastDigits forKey:@"digits"];
	[aCoder encodeObject:self.cvv forKey:@"cvv"];
	[aCoder encodeObject:self.month forKey:@"month"];
	[aCoder encodeObject:self.year forKey:@"year"];
	[aCoder encodeObject:self.brand	forKey:@"brand"];
	[aCoder encodeInt:self.ident	forKey:@"id"];
	[aCoder encodeObject:self.token	forKey:@"token"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super init])
	{
		self.cardHolderName = [aDecoder decodeObjectForKey:@"name"];
		self.name = [aDecoder decodeObjectForKey:@"cardName"];
		self.number = [aDecoder decodeObjectForKey:@"number"];
		self.lastDigits = [aDecoder decodeObjectForKey:@"digits"];
		self.cvv = [aDecoder decodeObjectForKey:@"cvv"];
		self.month = [aDecoder decodeObjectForKey:@"month"];
		self.year = [aDecoder decodeObjectForKey:@"year"];
		self.brand = [aDecoder decodeObjectForKey:@"brand"];
		self.ident = [aDecoder decodeObjectForKey:@"id"];
		self.token = [aDecoder decodeObjectForKey:@"token"];
	}
	
	return self;
}


@end
