//
//  SupplierJobInProgressViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SupplierJobInProgressViewController.h"
#import "ButtonCountDownAnimatedView.h"
#import "JobCompleteBlackOverlayViewController.h"
#import "AwaitingPaymentViewController.h"
#import "MONActivityIndicatorView.h"
#import "SupplierQuoteViewController.h"

@interface SupplierJobInProgressViewController () <MONActivityIndicatorViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *endJobView;
@property (strong, nonatomic) IBOutlet MONActivityIndicatorView *indicatorView;

@property (strong, nonatomic) IBOutlet UIButton *editQuoteButton;
@end

@implementation SupplierJobInProgressViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
	
	self.indicatorView.delegate = self;
	self.indicatorView.numberOfCircles = 3;
	self.indicatorView.radius = 2.5;
	self.indicatorView.internalSpacing = 5;
	self.indicatorView.duration = 1.25;
	self.indicatorView.delay = 0.5;
	
	[self.indicatorView startAnimating];
	
	
	self.editQuoteButton.layer.borderColor = [[UIColor lightGrayColor] CGColor];
	self.editQuoteButton.layer.borderWidth = 0.5;
}

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
	  circleBackgroundColorAtIndex:(NSUInteger)index {
	return PINK_COLOR;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	[self.view layoutIfNeeded];
	ButtonCountDownAnimatedView *animatedView = [[ButtonCountDownAnimatedView alloc] initWithInnerView:self.endJobView
																							  duration:1.5
																						   strokeWidth:2
																						   strokeColor:PINK_COLOR
																							 clockwise:YES
																				   animantionCompleted:^{
																					   
																					   [self showLoader];
																					   
																					   [[APIManager manager] setJobCompleted:[SettingsManager manager].currentJob.jobId
																													 success:^(BOOL success) {
																														 [self hideLoader];
																														 
//																														 [SettingsManager manager].appState = AppStateAwaitingPayment;
//																														 [SettingsManager save];
																														 
																														 [SettingsManager manager].appState = AppStateJobComplete;
																														 [SettingsManager save];
																														 JobCompleteBlackOverlayViewController *complete = [[JobCompleteBlackOverlayViewController alloc] initWithNibName:@"JobCompleteBlackOverlayViewController" bundle:nil];
																														 [self.navigationController pushViewController:complete animated:NO];
//																														 AwaitingPaymentViewController *controller = [[AwaitingPaymentViewController alloc] initWithNibName:@"BaseWhiteOverlayElipsesViewController" bundle:nil];
//																														 [self.navigationController pushViewController:controller animated:YES];
																														 
																													 } failure:^(NSError *error) {
																														 [self hideLoader];
																													 }];
																				   }];
	[self.view addSubview:animatedView];
}

- (IBAction)didTapEditQuote:(UIButton *)sender
{
	SupplierQuoteViewController *quote = [[SupplierQuoteViewController alloc] initWithNibName:@"SupplierQuoteViewController" bundle:nil];
	[self.navigationController pushViewController:quote animated:NO];
}
@end
