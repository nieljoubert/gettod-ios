//
//  ExpiryDateCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTextFieldCell.h"
#import "GTTextField.h"

@interface ExpiryDateCell : GTTextFieldCell <UITextFieldDelegate>

@property (nonatomic,strong) GTTextField *monthField;
@property (nonatomic,strong) GTTextField *yearField;

- (void)setupCellWithYear:(NSString*)card andMonth:(NSString*)month;

@end
