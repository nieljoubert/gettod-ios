//
//  ServicesListViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"
#import "SettingsViewController.h"

@interface ServicesListViewController : BaseTableViewController <SettingsViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *services;

@end
