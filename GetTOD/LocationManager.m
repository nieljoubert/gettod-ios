//
//  NewLocationManger.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/22.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "LocationManager.h"
#import "SettingsManager.h"
#import "AlertViewManager.h"
#import "APIManager.h"

static LocationManager *sharedManager = nil;

@interface LocationManager() <CLLocationManagerDelegate>
{
    LocationRequestHandler locationCompletionHandler;
}

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, assign) BOOL isOnceOff;

@property (nonatomic, strong) CLLocation *currentLocation;

@end


@implementation LocationManager

+ (LocationManager*)manager
{
    if (sharedManager == nil)
    {
        sharedManager = [[LocationManager alloc] init];
        
        sharedManager.locationManager = [[CLLocationManager alloc] init];
        sharedManager.locationManager.delegate = sharedManager;
        
#ifdef CLIENT
        sharedManager.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        sharedManager.locationManager.distanceFilter = 1;
#else
        sharedManager.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        sharedManager.locationManager.distanceFilter = 5;
#endif
        
    }
    
    return sharedManager;
}

- (void)setDistanceFilter:(int)distance
{
    sharedManager.locationManager.distanceFilter = distance;
}

- (void)resetDistanceFilter
{
#ifdef CLIENT
    sharedManager.locationManager.distanceFilter = 1;
#else
    sharedManager.locationManager.distanceFilter = 5;
#endif
}
- (void)requestCurrentLocation:(LocationRequestHandler) completionHandler
{
    locationCompletionHandler = completionHandler;
    
    self.isOnceOff = YES;
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"Location services disabled in settings.");
        
        [self startMonitoringLocationChanges:^(BOOL success, CLLocation *location) {
            
            
        }];
    }
    else
    {
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        [self startConstantUpdating];
    }
}

- (void) checkIfUserInSouthAfrica:(LocationSouthAfricaHandler)completed
{
//    [SettingsManager manager].isInSA = @"YES";
//    [SettingsManager save];
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"InSACheck" object:@""];
    //    completed(YES,@"");
    
    [self requestCurrentLocation:^(BOOL success, CLLocation *location) {
        
        if (success)
        {
            [[[CLGeocoder alloc] init] reverseGeocodeLocation:location
                                            completionHandler:^(NSArray *placemarks, NSError *error) {
                                                CLPlacemark *placemark = placemarks.firstObject;
                                                
                                                NSString *country = [placemark.addressDictionary objectForKey:@"CountryCode"];
                                                NSString *province = [placemark.addressDictionary objectForKey:@"State"];
                                                
                                                if (([country isEqualToString:@"ZA"] && [province isEqualToString:@"WC"]))
                                                {
                                                    [SettingsManager manager].isInSA = @"YES";
                                                    [SettingsManager save];
                                                    
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"InSACheck" object:@""];
                                                    
                                                    completed(YES,@"");
                                                }
                                                else
                                                {
                                                    NSString *baseString = @"Not yet available in %@.";
                                                    NSString *region = @"your area";
                                                    
                                                    if (![country isEqualToString:@"ZA"])
                                                    {
                                                        if (![province isEqualToString:@"WC"])
                                                        {
                                                            region = [NSString stringWithFormat:@"%@, %@",province,[placemark.addressDictionary objectForKey:@"Country"]];
                                                        }
                                                    }
                                                    else
                                                    {
                                                        region = [placemark.addressDictionary objectForKey:@"Country"];
                                                    }
                                                    
                                                    [SettingsManager manager].isInSA = @"NO";
                                                    [SettingsManager save];
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"InSACheck" object:[NSString stringWithFormat:baseString,region]];
                                                    completed(NO,[NSString stringWithFormat:baseString,region]);
                                                }
                                            }];
        }
    }];
}

- (void) checkforBackgroundPermissions
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self showBackgroundLocationAlert];
    }
}


- (void) startConstantUpdating
{
    [self.locationManager startUpdatingLocation];
    [self.locationManager startUpdatingHeading];
}

- (void) startMonitoringLocationChanges: (LocationRequestHandler) completionHandler
{
    locationCompletionHandler = completionHandler;
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if ([self.delegate respondsToSelector:@selector(locationMonitoringRequestLaunched)])
    {
        [self.delegate locationMonitoringRequestLaunched];
    }
    
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self showBackgroundLocationAlert];
        
        locationCompletionHandler(NO, nil);
    }
    else
    {
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        [self startConstantUpdating];
    }
}

- (void) startMonitoringSignificantLocationChanges: (LocationRequestHandler) completionHandler
{
    locationCompletionHandler = completionHandler;
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusAuthorizedAlways)
    {
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
}

-(CLLocation*)getCurrentLocationValue
{
    return self.currentLocation;
}

#pragma mark - region -
- (void)removeAllRegions
{
    for (CLCircularRegion *region in self.locationManager.monitoredRegions)
    {
        [self removeRegion:region];
    }
}

- (void)addRegion:(CLCircularRegion*)region
{
    if ([CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]])
    {
        [self.locationManager startMonitoringForRegion:region];
        
        NSLog(@"added - %@ - Total = %lu",region,(unsigned long)self.locationManager.monitoredRegions.count);
    }
}

- (void)removeRegion:(CLCircularRegion*)region
{
    [self.locationManager stopMonitoringForRegion:region];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"Entered - %@",region);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EnteredRegion" object:region];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"Exit - %@",region);
}

- (BOOL) isLocationServicesActivated
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusAuthorized || status == kCLAuthorizationStatusAuthorizedAlways)
    {
        return YES;
    }
    
    return NO;
}

- (void) stopMonitoringSignificantLocation
{
    [self.locationManager stopMonitoringSignificantLocationChanges];
}

- (void) stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
}

- (void) showBackgroundLocationAlert
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if ([[SettingsManager manager] isiOS8])
    {
        [AlertViewManager showAlertWithTitle:(status == kCLAuthorizationStatusDenied) ? @"Location Service Required" : @"Background Location Service Required"
                                     message:@"To use this app we require the 'Always On' location setting to be activated"
                           cancelButtonTitle:@"Cancel"
                           otherButtonTitles:@[@"Settings"]
                        buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                            
                            if (buttonIndex == 1)
                            {
                                if ([self.delegate respondsToSelector:@selector(locationMonitoringChangedSettings)])
                                {
                                    [self.delegate locationMonitoringChangedSettings];
                                }
                                
                                NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                [[UIApplication sharedApplication] openURL:settingsURL];
                            }
                            else
                            {
                                if ([self.delegate respondsToSelector:@selector(locationMonitoringRequestCanceled)])
                                {
                                    [self.delegate locationMonitoringRequestCanceled];
                                }
                            }
                        }];
    }
    else
    {
        [AlertViewManager showAlertWithTitle:@"Location Service Required"
                                     message:@"To use Personal ATM we require access to your location. Please go to Settings > Privacy > Location to turn it on."
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil
                        buttonSelectionBlock:nil];
    }
    
    
}

- (void)updateLocationOnAPI:(CLLocationCoordinate2D)coordinates
{
#ifdef CLIENT
    
#else
    if ([SettingsManager manager].authenticationToken)
    {
        [[APIManager manager] setSupplierLocationCoordinates:coordinates
                                                     bearing:[[SettingsManager manager].currentHeading floatValue]
                                                   andStatus:[SettingsManager manager].supplierStatus
                                                     success:^(id result) {
                                                         
                                                     } failure:^(NSError *error) {
                                                         
                                                     }];
    }
#endif
}

#pragma mark - Location -

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    	[self checkIfUserInSouthAfrica:^(BOOL inSA, NSString *message) {
    
    
    	}];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //	NSLog(@"Got Location: %@ - %f - %f", [NSDate date], manager.location.coordinate.latitude, manager.location.coordinate.longitude);
    
    [SettingsManager manager].currentLocationLat = [NSNumber numberWithDouble:manager.location.coordinate.latitude];
    [SettingsManager manager].currentLocationLng = [NSNumber numberWithDouble:manager.location.coordinate.longitude];
    [SettingsManager save];
    
    if (self.isOnceOff)
    {
        NSLog(@"Got Once off Location: %@ - %f - %f", [NSDate date], manager.location.coordinate.latitude, manager.location.coordinate.longitude);
        
        [self stopUpdatingLocation]; //manager
        self.isOnceOff = NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationUpdated" object:nil];
        
        [self updateLocationOnAPI:manager.location.coordinate];
        locationCompletionHandler(YES, manager.location);
    }
    else
    {
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        {
            NSLog(@"Got Significant Location: %@ - %f - %f", [NSDate date], manager.location.coordinate.latitude, manager.location.coordinate.longitude);
            [self updateLocationOnAPI:manager.location.coordinate];
            locationCompletionHandler(YES, manager.location);
        }
        else
        {
            CLLocation *newLocation = locations.lastObject;
            
            NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
            if (locationAge > 5.0) return;
            
            if (newLocation.horizontalAccuracy < 0) return;
            
            // Needed to filter cached and too old locations
            //NSLog(@"Location updated to = %@", newLocation);
            CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:self.currentLocation.coordinate.latitude longitude:self.currentLocation.coordinate.longitude];
            CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
            double distance = [loc1 distanceFromLocation:loc2];
            self.currentLocation = newLocation;
            
            if(distance > manager.distanceFilter)
            {
                //significant location update
                //location updated
                
                //                [AlertViewManager showAlertWithTitle:[NSString stringWithFormat:@"%.2f > %.2f",distance,manager.distanceFilter]
                //                                             message:[NSString stringWithFormat:@"%f\n%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude]
                //                                   cancelButtonTitle:@"OK"
                //                                   otherButtonTitles:nil
                //                                buttonSelectionBlock:nil];
                
                NSLog(@"Got Updated Location: %@ - %f - %f", [NSDate date], manager.location.coordinate.latitude, manager.location.coordinate.longitude);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationUpdated" object:nil];
                locationCompletionHandler(YES, manager.location);
                [self updateLocationOnAPI:manager.location.coordinate];
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (self.isOnceOff)
    {
        [self stopUpdatingLocation];
        self.isOnceOff = NO;
    }
    
    locationCompletionHandler(NO, nil);
}

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    float trueHeading = newHeading.trueHeading;
    [SettingsManager manager].currentHeading = [NSNumber numberWithFloat:(trueHeading*M_PI/180)];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BearingUpdated" object:newHeading];
}

@end
