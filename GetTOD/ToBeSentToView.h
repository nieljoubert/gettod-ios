//
//  ToBeSentToView.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToBeSentToView : UIView

- (void)setupViewWithAddress:(NSString*)address;

@end
