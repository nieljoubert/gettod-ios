//
//  Service.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Service.h"
#import "Supplier.h"


@implementation Service

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.serviceId = [decoder decodeObjectForKey:@"serviceId"];
		self.name = [decoder decodeObjectForKey:@"name"];
		self.pictureActiveURL = [decoder decodeObjectForKey:@"pictureActiveURL"];
		self.pictureInactiveURL = [decoder decodeObjectForKey:@"pictureInactiveURL"];
		self.highlightColor = [decoder decodeObjectForKey:@"highlightColor"];
	}

	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.serviceId forKey:@"serviceId"];
	[encoder encodeObject:self.name forKey:@"name"];
	[encoder encodeObject:self.pictureActiveURL forKey:@"pictureActiveURL"];
	[encoder encodeObject:self.pictureInactiveURL forKey:@"pictureInactiveURL"];
	[encoder encodeObject:self.highlightColor forKey:@"highlightColor"];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nID: %@\nNA: %@\nAU: %@\nIU: %@\nHC: %@\n-------------------------------",self.serviceId,self.name,self.pictureActiveURL,self.pictureInactiveURL,self.highlightColor];
}

@end
