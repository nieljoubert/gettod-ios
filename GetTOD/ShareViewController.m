//
//  ShareViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ShareViewController.h"

@interface ShareViewController ()

@property (nonatomic, strong) GTTextField *emailField;
@end

@implementation ShareViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.title = @"Send To A Friend";
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStyleBordered target:self action:@selector(share)];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
	self.navigationItem.hidesBackButton = YES;
	
	self.emailField = [GTTextField new];
	self.emailField.translatesAutoresizingMaskIntoConstraints = NO;
	self.emailField.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
	self.emailField.placeholder = @"Enter your Friend's Email Address";
	self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
	self.emailField.delegate = self;
	
	[self.view addSubview:self.emailField];
	
	UILabel *textLabel = [UILabel new];
	textLabel.translatesAutoresizingMaskIntoConstraints = NO;
	textLabel.font = [UIFont fontWithName:FONT size:12];
	textLabel.textColor = GRAY_COLOR;
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.numberOfLines = 0;
	textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	textLabel.text = @"We will send your friend an email telling them you think they may find this service useful. Thanks for recommending us!";

	[self.view addSubview:textLabel];

	UIView *line = [UIView new];
	line.translatesAutoresizingMaskIntoConstraints = NO;
	line.backgroundColor = [UIColor lightGrayColor];
	
	[self.view addSubview:line];
	
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[email]-15-|"
																	  options:0
																	  metrics:nil
																		views:@{@"email":self.emailField}]];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-40-[text]-40-|"
																	  options:0
																	  metrics:nil
																		views:@{@"text":textLabel}]];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|"
																	  options:0
																	  metrics:nil
																		views:@{@"line":line}]];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[email(50)][line(0.5)]-30-[text]-(>=10)-|"
																	  options:0
																	  metrics:nil
																		views:@{@"email":self.emailField,@"text":textLabel,@"line":line}]];
	
	
	
}

- (BOOL)isEmailValid
{
	if ([Validation isValidEmail:self.emailField.text])
	{
		[self.emailField dismissError];
		return YES;
	}
	else
	{
		[self.emailField setErrorTextMessage:@"Please enter a valid email address"];
		return NO;
	}
}

- (void)share
{
	if ([self isEmailValid])
	{
		[[APIManager manager] referFriendWithEmail:self.emailField.text
										   success:^(id result) {
											   
										   } failure:^(NSError *error) {
											   
										   }];
		
		[self.delegate finishedSharing:self];
	}
	else
	{
		[self.emailField showErrorAlert:^(UIAlertView *alertView, NSInteger buttonIndex) {
			[self.emailField becomeFirstResponder];
		}];
	}
}

- (void)cancel
{
	[self.delegate finishedSharing:self];
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[textField resignFirstResponder];
}

@end
