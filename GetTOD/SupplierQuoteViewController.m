//
//  SupplierQuoteViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SupplierQuoteViewController.h"
#import "GTMultiLineTitleView.h"
#import "JobInProgressViewController.h"
#import "GTBoldTableViewCell.h"
#import "CancelationFeedbackViewController.h"
#import "RejectedQuoteViewController.h"
#import "JobCompleteBlackOverlayViewController.h"
#import "AddMaterialViewController.h"
#import "AwaitingApprovalViewController.h"
#import "QuoteRejectedViewController.h"
#import "SupplierJobInProgressViewController.h"

@interface SupplierQuoteViewController ()

@property (strong, nonatomic) IBOutlet UIView *subtotalView;
@property (strong, nonatomic) IBOutlet UILabel *labourPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *materialsTotalLabel;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UILabel *vatLabel;
@property (weak, nonatomic) IBOutlet UIView *labourContainerView;
@property (weak, nonatomic) IBOutlet UIView *materialsCostContainerView;
@property (strong, nonatomic) IBOutlet UILabel *labourPerHourLabel;

@property (nonatomic, strong) UILabel *descriptionTextLabel;

@property (nonatomic, strong) NSMutableArray *materials;

@property (nonatomic, assign) int numberOfHours;
@property (nonatomic, assign) float quoteTotal;
@property (nonatomic, assign) float vatTotal;
@property (nonatomic, assign) float labourTotal;
@property (nonatomic, assign) float materialsTotal;

@property (weak, nonatomic) IBOutlet UITableView *tableViewFromXib;
@property (strong, nonatomic) IBOutlet UILabel *addJobDescriptionView;

@property (nonatomic, strong) NSString *jobDescriptionText;
@end

@implementation SupplierQuoteViewController

- (void)viewDidLoad
{
	self.tableView = self.tableViewFromXib;
	[super viewDidLoad];
	
	self.navigationController.navigationBarHidden = NO;
	
	self.navigationItem.titleView = [[GTMultiLineTitleView alloc] initWithTitle:@"Quote to" andSubTitle:[SettingsManager manager].currentJob.customer.name smallTitle:YES];
	
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelQuote)];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStyleDone target:self action:@selector(sendQuote)];
	
	self.labourContainerView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.labourContainerView.layer.borderWidth = 0.5;
	
	self.materialsCostContainerView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.materialsCostContainerView.layer.borderWidth = 0.5;
	
	if ([SettingsManager manager].currentJob.quote)
	{
		self.materials = [SettingsManager manager].currentJob.quote.materials;
		self.numberOfHours = [[SettingsManager manager].currentJob.quote.labourHours intValue];
	}
	else
	{
		self.materials = [NSMutableArray array];
		self.numberOfHours = 1;
	}
	
	self.labourPerHourLabel.text = [NSString stringWithFormat:@"LABOUR AT R%d / HR",[[SettingsManager manager].currentJob.supplier.labourRate intValue]];
	
	[SettingsManager manager].appState = AppStateQuote;
	[SettingsManager save];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = NO;
	
	[self calculateMaterialTotal];
}

- (void)calculateMaterialTotal
{
	float total = 0.00;
	
	for (Material *mat in self.materials)
	{
		total += [[mat price] floatValue];
	}
	
	self.materialsTotal = total;
	self.materialsTotalLabel.text = [NSString stringWithFormat:@"%.2f",total];
	[self calculateLabourTotal];
}

- (void)calculateLabourTotal
{
	self.labourTotal = self.numberOfHours * [[SettingsManager manager].currentJob.supplier.labourRate intValue];
	self.labourPriceLabel.text = [NSString stringWithFormat:@"%.2f",self.labourTotal];
	[self calculateQuoteTotal];
}

- (void)calculateQuoteTotal
{
	self.quoteTotal = self.materialsTotal + self.labourTotal;
	self.totalLabel.text = [NSString stringWithFormat:@"%.2f",self.quoteTotal];
	[self calculateVatTotal];
}

- (void)calculateVatTotal
{
	float total = 0.00;
	total += self.materialsTotal + self.labourTotal;
	self.vatTotal = self.quoteTotal * 0.14;
	self.vatLabel.text = [NSString stringWithFormat:@"Inclusive of 14%% VAT at R%.2f",self.vatTotal];
	
	if ([SettingsManager manager].currentJob.supplier.company.isVatRegistered)
	{
		self.vatLabel.hidden = NO;
	}
	else
	{
		self.vatLabel.hidden = YES;
	}
}

- (float)caclulateInfoHeight
{
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, SCREEN_WIDTH-15, 100)];
	label.font = [UIFont fontWithName:FONT size:14];
	label.numberOfLines = 0;
	label.lineBreakMode = NSLineBreakByWordWrapping;
	label.text = self.jobDescriptionText;
	
	CGSize size = [label sizeThatFits:CGSizeMake(SCREEN_WIDTH-15, 1000)];
	return size.height;
}

- (void)launchJobDescriptionAddScreen
{
	EnterTextViewController *textController = [EnterTextViewController new];
	textController.titleText = @"Job Description";
	textController.rightButtonText = @"Done";
	textController.leftButtonText = @"Cancel";
	textController.textViewHintText = @"Please enter job description here.";
	textController.existingText = self.jobDescriptionText;
	textController.delegate = self;
	
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:textController];
	[Flurry logAllPageViews:navigationController];
	[self.navigationController presentViewController:navigationController animated:YES completion:nil];
}


#pragma mark - Actions -

- (void)sendQuote
{
	if (self.materials.count == 0)
	{
		[AlertViewManager showAlertWithTitle:@""
									 message:@"Are you sure you are not using any materials?"
						   cancelButtonTitle:@"No"
						   otherButtonTitles:@[@"Yes"]
						buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
							switch (buttonIndex) {
								case 1:
									[self submitQuote];
									break;
								default:
									break;
							}
						}];
	}
	else
	{
		[self submitQuote];
	}
}

- (void)cancelQuote
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)submitQuote
{
	[self showLoader];
	
	[[APIManager manager] createQuoteWithJobID:[SettingsManager manager].currentJob.jobId
										labour:[NSNumber numberWithInt:self.numberOfHours]
								   description:(self.jobDescriptionText) ? self.jobDescriptionText : @""
							   listOfMaterials:self.materials
									   success:^(id result) {
										   
										   [self hideLoader];
										   
										   [SettingsManager manager].appState = AppStateAwaitingApproval;
										   [SettingsManager save];
										   
										   [SettingsManager manager].currentJob.quote = result;
										   
										   AwaitingApprovalViewController *controller = [[AwaitingApprovalViewController alloc] initWithNibName:@"BaseWhiteOverlayElipsesViewController" bundle:nil];
										   [self.navigationController pushViewController:controller animated:NO];
										   
									   } failure:^(NSError *error) {
										   [self hideLoader];
									   }];
}

#pragma mark - Delegate -
- (void)quantityChanged:(int)quantity
{
	self.numberOfHours = quantity;
	
	[self calculateLabourTotal];
}

- (void)addedMaterial:(Material *)material wasEditing:(BOOL)wasEdit fromController:(AddMaterialViewController *)controller
{
	if (wasEdit)
	{
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationNone];
	}
	else
	{
		[self.materials addObject:material];
	}
	
	[self.tableView reloadData];
	
	[self calculateMaterialTotal];
	
	[controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)addMaterialCanceled:(AddMaterialViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)textEntered:(NSString *)text forController:(EnterTextViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
	self.jobDescriptionText = text;
	
	[self.tableView reloadData];
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
	{
		return 1;
	}
	else if (section == 1)
	{
		return 1;
	}
	else if (section == 2)
	{
		return self.materials.count+1;
	}
	else if (section == 3)
	{
		return 1;
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		GTBoldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTBoldTableViewCell"];
		[cell.contentView addSubview:self.subtotalView];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.accessoryView = nil;
		return cell;
	}
	else if (indexPath.section == 1)
	{
		QuantityAdjustmentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuantityAdjustmentCell"];
		cell.delegate = self;
		[cell setupCellWithSubtitle:YES withQuantity:self.numberOfHours];
		return cell;
	}
	else if (indexPath.section == 2)
	{
		if (self.materials.count > 0 && indexPath.row < self.materials.count)
		{
			QuoteMaterialCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuoteMaterialCell"];
			[cell setupCellWithMaterial:[self.materials objectAtIndex:indexPath.row]];
			return cell;
		}
		
		else if (indexPath.row == self.materials.count)
		{
			// add button
			AddNewItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddNewItemCell"];
			[cell setupCellWithText:@"Add Material Cost" showPlusSign:YES];
			return cell;
		}
	}
	else if (indexPath.section == 3)
	{
		if (self.jobDescriptionText.length > 0)
		{
			GTBoldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GTBoldTableViewCell"];
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			cell.accessoryType = UITableViewCellAccessoryNone;
			cell.accessoryView = nil;
			
			if (self.descriptionTextLabel == nil)
			{
				self.descriptionTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 2, SCREEN_WIDTH-30, 34)];
				self.descriptionTextLabel.font = [UIFont fontWithName:FONT size:14];
				self.descriptionTextLabel.textColor = DARK_TEXT_COLOR;
				self.descriptionTextLabel.numberOfLines = 0;
				self.descriptionTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
				[cell.contentView addSubview:self.descriptionTextLabel];
			}
			
			self.descriptionTextLabel.text = self.jobDescriptionText;
			
			self.descriptionTextLabel.frame = CGRectMake(15, 15, SCREEN_WIDTH-15, [self caclulateInfoHeight]);
			
			return cell;
		}
		else
		{
			AddNewItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddNewItemCell"];
			[cell setupCellWithText:@"Add Job Description" showPlusSign:YES];
			return cell;
		}
	}
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"SUBTOTAL"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"LABOUR"];
	}
	else if (section == 2)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"MATERIALS"];
	}
	else if (section == 3)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"DESCRIPTION"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return 24;
	}
	
	return 24;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if (indexPath.section == 2)
	{
		AddMaterialViewController *addMaterial = [[AddMaterialViewController alloc] init];
		addMaterial.delegate = self;
		
		if (indexPath.row < self.materials.count)
		{
			addMaterial.material = [self.materials objectAtIndex:indexPath.row];
		}
		
		UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addMaterial];
		[Flurry logAllPageViews:navigationController];
		[self.navigationController presentViewController:navigationController animated:YES completion:nil];
	}
	else if (indexPath.section == 3)
	{
		[self launchJobDescriptionAddScreen];
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		return self.subtotalView.frame.size.height;
	}
	else if (indexPath.section == 1)
	{
		return 67;
	}
	else if (indexPath.section == 2 || indexPath.section == 3)
	{
		return 58;
	}
	
	return 44;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 2)
	{
		return YES;
	}
	
	return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 2)
	{
		if (editingStyle == UITableViewCellEditingStyleDelete)
		{
			[self.materials removeObjectAtIndex:indexPath.row];
			
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
		}
	}
}


@end
