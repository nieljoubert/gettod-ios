//
//  QuoteMaterialCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "QuoteMaterialCell.h"
#import "Material.h"

@interface QuoteMaterialCell ()

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UILabel *quantityLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIView *detailsView;

@end

@implementation QuoteMaterialCell

- (void)setupCellWithMaterial:(Material*)material
{
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[formatter setPositiveFormat:@"###0.00"];
	[formatter setMinimumFractionDigits:2];
	[formatter setMaximumFractionDigits:2];
	
	NSDecimalNumber *decimalCost = [NSDecimalNumber decimalNumberWithDecimal:[material.costPerUnit decimalValue]];
	
	if (self.detailsView == nil)
	{
		self.detailsView = [UIView new];
		self.detailsView.translatesAutoresizingMaskIntoConstraints = NO;
		[self.contentView addSubview:self.detailsView];
	}
	
	if (self.nameLabel == nil)
	{
		self.nameLabel = [UILabel new];
		self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.nameLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.nameLabel.textColor = DARK_TEXT_COLOR;
		self.nameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.detailsView addSubview:self.nameLabel];
	}
	
	self.nameLabel.text = material.name;

	if (self.quantityLabel == nil)
	{
		self.quantityLabel = [UILabel new];
		self.quantityLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.quantityLabel.font = [UIFont fontWithName:FONT size:11];
		self.quantityLabel.textColor = GRAY_COLOR;
		self.quantityLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.detailsView addSubview:self.quantityLabel];
	}
	
	self.quantityLabel.text = [NSString stringWithFormat:@"%@ x R%@ ea",material.quantity,[formatter stringFromNumber:decimalCost]];

	if (self.priceLabel == nil)
	{
		self.priceLabel = [UILabel new];
		self.priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.priceLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.priceLabel.textColor = DARK_TEXT_COLOR;
		self.priceLabel.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.contentView addSubview:self.priceLabel];
	}
	
	self.priceLabel.text = [NSString stringWithFormat:@"R %@",[formatter stringFromNumber:[material price]]];
	
	NSDictionary *views = @{@"details":self.detailsView,@"price":self.priceLabel,@"name":self.nameLabel,@"quant":self.quantityLabel};
	
	[self.detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[name]|"
																	options:0
																	metrics:nil
																	  views:views]];
	
	[self.detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[quant]|"
																	options:0
																	metrics:nil
																	  views:views]];
	
	[self.detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[name][quant]-10-|"
																	options:0
																	metrics:nil
																	  views:views]];	
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[details]-(>=5)-[price]-15-|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[details]|"
																			 options:0
																			 metrics:nil
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[price]|"
																			 options:0
																			 metrics:nil
																			   views:views]];
}



@end
