//
//  GTDirection.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/13.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

typedef enum {
	NavigationDirectionTurnSharpLeft,
	NavigationDirectionUTurnRight,
	NavigationDirectionTurnSlightRight,
	NavigationDirectionMerge,
	
	NavigationDirectionRoundaboutStraight,
	NavigationDirectionRoundaboutLeft,
	NavigationDirectionRoundaboutRight,
	NavigationDirectionUTurnLeft,
	NavigationDirectionTurnSlightLeft,
	
	NavigationDirectionTurnLeft,
	NavigationDirectionRampRight,
	NavigationDirectionTurnRight,
	NavigationDirectionForkRight,
	
	NavigationDirectionStraight,
	NavigationDirectionForkLeft,
	NavigationDirectionTurnSharpRight,
	NavigationDirectionRampLeft,
	NavigationDirectionKeepLeft,
	NavigationDirectionKeepRight,
	
	NavigationDirectionEnd
} NavigationDirection;

@interface GTDirection : NSObject

@property (nonatomic, strong) NSString *directionDescription;
@property (nonatomic, strong) NSString *streetName;
@property (nonatomic, strong) UIImage *directionImage;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSString *distanceStringWithKM;
@property (nonatomic, assign) NavigationDirection direction;
@property (nonatomic, strong) NSNumber *startLat;
@property (nonatomic, strong) NSNumber *startLng;
@property (nonatomic, strong) NSNumber *endLat;
@property (nonatomic, strong) NSNumber *endLng;

- (CLLocationCoordinate2D)startLocation;
- (CLLocationCoordinate2D)endLocation;

@end
