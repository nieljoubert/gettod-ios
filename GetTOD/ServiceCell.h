//
//  ServiceCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "Service.h"

@interface ServiceCell : GTBoldTableViewCell

@property (nonatomic, strong) UIImageView *providerImage;
@property (nonatomic, strong) UILabel *providerNameLabel;

- (void)setupCellWithName:(Service*)service;

@end
