//
//  CancelRequestBlackOverlayViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseBlackOverlayViewController.h"

@interface CancelRequestBlackOverlayViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic)  UIImage *backgroundImage;
@property (nonatomic, assign) int jobID;

@property (nonatomic, weak) id delegate;
@end

@protocol CancelRequestDelegate <NSObject>
- (void)dismissController:(CancelRequestBlackOverlayViewController*)controller didCancel:(BOOL)didCancel;
@end