//
//  HTTPClient.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "HTTPClient.h"
#import "AFHTTPSessionManager.h"
#import "SettingsManager.h"
#import "Constants.h"


@implementation HTTPClient

+ (HTTPClient*)sharedClient
{
	static HTTPClient *_sharedClient = nil;
	
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		_sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:BASEURLSTRING]];
	});
	
	return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
	self = [super initWithBaseURL:url];
	
	if (self)
	{
		self.requestSerializer = [AFJSONRequestSerializer serializer];
		self.responseSerializer = [AFJSONResponseSerializer serializer];
	}
	
	return self;
}

- (void)doPOST:(NSString *)URLString
  inJSONFormat:(BOOL)isJSON
	parameters:(id)parameters
	   success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
	   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
{
	if (isJSON)
	{
		self.requestSerializer = [AFJSONRequestSerializer serializer];//change
	}
	else
	{
		self.requestSerializer = [AFHTTPRequestSerializer serializer];
	}
	
	self.responseSerializer = [AFJSONResponseSerializer serializer];
	
	if ([SettingsManager manager].authenticationToken)
	{
		[self.requestSerializer setValue:[NSString stringWithFormat:@"Token %@",[SettingsManager manager].authenticationToken] forHTTPHeaderField:@"Authorization"];
	}
	
	[self.requestSerializer setValue:@"IOS" forHTTPHeaderField:@"Platform"];
	
#ifdef CLIENT
	[self.requestSerializer setValue:@"CLIENT" forHTTPHeaderField:@"Authorization-Type"];
#else
	[self.requestSerializer setValue:@"PARTNER" forHTTPHeaderField:@"Authorization-Type"];
#endif
	
	
	NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST"
																   URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString]
																  parameters:parameters
																	   error:nil];
	
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request
																	  success:success
																	  failure:failure];
    
	[self.operationQueue addOperation:operation];
}

- (void)doGET:(NSString *)URLString
   parameters:(id)parameters
	  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
	  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
	
	if ([SettingsManager manager].authenticationToken)
	{
		[self.requestSerializer setValue:[NSString stringWithFormat:@"Token %@",[SettingsManager manager].authenticationToken] forHTTPHeaderField:@"Authorization"];
	}
	
	[self.requestSerializer setValue:@"IOS" forHTTPHeaderField:@"Platform"];
	
#ifdef CLIENT
	[self.requestSerializer setValue:@"CLIENT" forHTTPHeaderField:@"Authorization-Type"];
#else
	[self.requestSerializer setValue:@"PARTNER" forHTTPHeaderField:@"Authorization-Type"];
#endif
	
	NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET"
																   URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString]
																  parameters:parameters
																	   error:nil];
	
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request
																	  success:success
																	  failure:failure];
	
	[self.operationQueue addOperation:operation];
}

- (void)doDELETE:(NSString *)URLString
	  parameters:(id)parameters
		 success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
		 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
	
	if ([SettingsManager manager].authenticationToken)
	{
		[self.requestSerializer setValue:[NSString stringWithFormat:@"Token %@",[SettingsManager manager].authenticationToken] forHTTPHeaderField:@"Authorization"];
	}
	
	[self.requestSerializer setValue:@"IOS" forHTTPHeaderField:@"Platform"];
	
#ifdef CLIENT
	[self.requestSerializer setValue:@"CLIENT" forHTTPHeaderField:@"Authorization-Type"];
#else
	[self.requestSerializer setValue:@"PARTNER" forHTTPHeaderField:@"Authorization-Type"];
#endif
	
	NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"DELETE"
																   URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString]
																  parameters:parameters
																	   error:nil];
	
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request
																	  success:success
																	  failure:failure];
	
	[self.operationQueue addOperation:operation];
}

- (NSData *)encodePostParameters:(NSDictionary *)parameters
{
	NSMutableArray *results = [NSMutableArray array];
	
	for (NSString *key in parameters)
	{
		NSString *string;
		id value = parameters[key];
		
		if ([value isKindOfClass:[NSData class]])
		{
			string = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
		}
		else if ([value isKindOfClass:[NSString class]])
		{
			string = value;
		}
		else
		{
			string = [value description];
		}
		
		[results addObject:[NSString stringWithFormat:@"%@=%@", key, [self percentEscapeString:string]]];
	}
	
	NSString *postString = [results componentsJoinedByString:@"&"];
	
	return [postString dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)percentEscapeString:(NSString *)string
{
	NSString *result = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
																				 (CFStringRef)string,
																				 (CFStringRef)@" ",
																				 (CFStringRef)@":/?@!$&'()*+,;=",
																				 kCFStringEncodingUTF8));
	return [result stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}

@end
