//
//  PossibleProblem.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Problem.h"

@implementation Problem

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.problemId = [decoder decodeObjectForKey:@"problemId"];
		self.problemDescription = [decoder decodeObjectForKey:@"problemDescription"];
		self.estimatedPrice = [decoder decodeObjectForKey:@"estimatedPrice"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.problemId forKey:@"problemId"];
	[encoder encodeObject:self.problemDescription forKey:@"problemDescription"];
	[encoder encodeObject:self.estimatedPrice forKey:@"estimatedPrice"];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nID: %@\nDescription: %@\nEstimate: %@\n-------------------------------",self.problemId,self.problemDescription,self.estimatedPrice];
}

@end
