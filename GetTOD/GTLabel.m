//
//  GTLabel.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTLabel.h"
#import "Constants.h"

@implementation GTLabel

-(id)init
{
	self = [super init];
	
	if (self)
	{
		self.font = [UIFont fontWithName:@"AdelleSans" size:16];
	}
	
	return self;
}

@end
