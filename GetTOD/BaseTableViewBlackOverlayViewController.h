//
//  BaseTableViewBlackOverlayViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "EnterTextViewController.h"

typedef enum {
	BlackOverlayListTypeRejectQuote,
	BlackOverlayListTypeCancelJob,
	BlackOverlayListTypeProblems,
	BlackOverlayListTypeCards,
	BlackOverlayListTypeEnRouteOptions
} BlackOverlayListType;


@protocol BlackTableViewDelegate;

@interface BaseTableViewBlackOverlayViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, EnterTextViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *button;

@property (strong, nonatomic) NSMutableArray *cells;
@property(nonatomic, weak) id <BlackTableViewDelegate> delegate;

@property (nonatomic, assign) BlackOverlayListType listType;

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) UIImage *backgroundImage;

@end

@protocol BlackTableViewDelegate <NSObject>

- (void)cellSelected:(int)index WithText:(NSString*)text fromController:(BaseTableViewBlackOverlayViewController*)controller;
- (void)dismissedController:(BaseTableViewBlackOverlayViewController*)controller;
- (void)delayedNotification;

@end