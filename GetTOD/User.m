//
//  User.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "User.h"

@implementation User

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.userId = [decoder decodeObjectForKey:@"userId"];
		self.name = [decoder decodeObjectForKey:@"name"];
		self.phoneNumber = [decoder decodeObjectForKey:@"phoneNumber"];
		self.profilePictureURL = [decoder decodeObjectForKey:@"profilePictureURL"];
		self.email = [decoder decodeObjectForKey:@"email"];
		self.rating = [decoder decodeObjectForKey:@"rating"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.userId forKey:@"userId"];
	[encoder encodeObject:self.name forKey:@"name"];
	[encoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
	[encoder encodeObject:self.profilePictureURL forKey:@"profilePictureURL"];
	[encoder encodeObject:self.email forKey:@"email"];
	[encoder encodeObject:self.rating forKey:@"rating"];
}


- (NSString *)description
{
	return [NSString stringWithFormat:@"\nID: %@\nName: %@\nPhone: %@\nEmail: %@\n-------------------------------",self.userId,self.name,self.phoneNumber,self.email];
}

@end
