//
//  WebViewController.m
//  
//
//  Created by Niel Joubert on 2015/09/20.
//
//

#import "WebViewController.h"

@interface WebViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[self.webView loadRequest:self.urlRequestToLoad];
}

- (void)viewWillDisappear:(BOOL)animated
{
	self.webView.delegate = nil;
	[super viewWillDisappear:animated];
}

@end
