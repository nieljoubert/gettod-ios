//
//  AddEditCardDetailsViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AddEditCardDetailsViewController.h"
#import "GTMultiLineTitleView.h"
#import "ServicesListViewController.h"
#import "CreatingAccountBlackOverlayViewController.h"
#import "NSData+AES256.h"

@interface AddEditCardDetailsViewController ()

@property (nonatomic, strong) GTTextField *cardHolderName;
@property (nonatomic, strong) GTTextFieldCell *cardHolderNameCell;
@property (nonatomic, strong) GTTextField *cardName;
@property (nonatomic, strong) GTTextFieldCell *cardNameCell;
@property (nonatomic, strong) CardNumberCell *cardnumberCell;
@property (nonatomic, strong) GTTextField *cardNumber;
@property (nonatomic, strong) ExpiryDateCell *expiryCell;
@property (nonatomic, strong) GTTextField *cvv;
@property (nonatomic, strong) GTTextFieldCell *cvvCell;
@property (nonatomic, strong) GTTextField *month;
@property (nonatomic, strong) GTTextField *year;
@property (nonatomic, strong) GTTextField *dummyTextFieldExpiry;

@property (nonatomic, strong) NSString *holderNameString;
@property (nonatomic, strong) NSString *numberString;
@property (nonatomic, strong) NSString *monthString;
@property (nonatomic, strong) NSString *yearString;
@property (nonatomic, strong) NSString *cvvString;
@property (nonatomic, strong) NSString *cardNameString;

@property (nonatomic, strong) UIImage *screenshot;

@property (nonatomic, assign) BOOL didChangeYear;
@property (nonatomic, assign) BOOL didChangeMonth;

typedef void (^CompletedHandler)(BOOL completed, Card *card);

@end

@implementation AddEditCardDetailsViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	if (self.isAdding)
	{
		if (self.isSettings)
		{
			self.title = @"Enter Payment Details";
			self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
			//			self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Pay" style:UIBarButtonItemStyleDone target:self action:@selector(pay)];
		}
		else
		{
			self.navigationItem.titleView = [[GTMultiLineTitleView alloc] initWithTitle:@"Payment" andSubTitle:@"Step 3 of 3" smallTitle:NO];
			self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(finish)];
			self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Later" style:UIBarButtonItemStyleDone target:self action:@selector(completeLater)];
		}
		
		//		self.navigationItem.leftBarButtonItem = nil;
		//		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		self.title = @"Edit Payment";
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
	}
	
	self.tableView.tableFooterView = [self createFooterView];
	self.tableView.tableHeaderView = [self createHeaderView];
	
	self.didChangeYear = NO;
	self.didChangeMonth = NO;
	
	[self setupFields];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[CardIOUtilities preload];
    
//    [self setTextFieldDelegate];
     [self registerForKeyboardNotifications];
}

#pragma mark - Private -
- (void)setupFields
{
	self.cardHolderName = [GTTextField new];
	[self.cardHolderName setKeyboardToolbar:self.keyboardToolbar];
	
	self.cardNumber = [GTTextField new];
	[self.cardNumber setKeyboardToolbar:self.keyboardToolbar];
	
	self.month = [GTTextField new];
	self.month.keyboardType = UIKeyboardTypeNumberPad;
	[self.month setKeyboardToolbar:self.keyboardToolbar];
	
	self.year = [GTTextField new];
	self.year.keyboardType = UIKeyboardTypeNumberPad;
	[self.year setKeyboardToolbar:self.keyboardToolbar];
	
	self.dummyTextFieldExpiry = [GTTextField new];
	
	self.cvv = [GTTextField new];
	self.cvv.keyboardType = UIKeyboardTypeNumberPad;
	[self.cvv setKeyboardToolbar:self.keyboardToolbar];
	
	self.cardName = [GTTextField new];
	[self.cardName setKeyboardToolbar:self.keyboardToolbar];
	
	[self addFormField:self.cardHolderName];
	[self addFormField:self.cardNumber];
	[self addFormField:self.month];
	[self addFormField:self.year];
	[self addFormField:self.cvv];
	[self addFormField:self.cardName];
	
    [self setTextFieldDelegate];
    
	self.holderNameString = self.card.cardHolderName;
	self.numberString = self.card.number;
	self.monthString = self.card.month;
	self.yearString = self.card.year;
	self.cvvString = self.card.cvv;
	self.cardNameString = self.card.name;
}

- (void)setTextFieldDelegate
{
    self.cardHolderName.delegate = self;
    self.cardNumber.delegate = self;
    self.month.delegate = self;
    self.year.delegate = self;
    self.cvv.delegate = self;
    self.cardName.delegate = self;
}

- (UIView*)createHeaderView
{
	UILabel *textLabel = [UILabel new];
	textLabel.font = [UIFont fontWithName:FONT size:13];
	textLabel.textColor = PINK_COLOR;
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.text = @"Scan your card";
	[textLabel sizeToFit];
	
	UIImageView *cameraImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"camera_scan"]];
	cameraImage.contentMode = UIViewContentModeScaleAspectFit;
	
	CGRect frame = cameraImage.frame;
	frame.origin = CGPointMake(0, 0);
	frame.size = CGSizeMake(cameraImage.frame.size.width, 20);
	cameraImage.frame = frame;
	
	frame = textLabel.frame;
	frame.origin = CGPointMake(cameraImage.frame.size.width + 5, 0);
	frame.size = CGSizeMake(textLabel.frame.size.width, 20);
	textLabel.frame = frame;
	
	UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cameraImage.frame.size.width+5+textLabel.frame.size.width, 20)];
	[container addSubview:cameraImage];
	[container addSubview:textLabel];
	[container sizeToFit];
	
	UITapGestureRecognizer *scanTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scanCard)];
	scanTap.numberOfTapsRequired = 1;
	[container addGestureRecognizer:scanTap];
	
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
	[headerView addSubview:container];
	
	container.center = CGPointMake(SCREEN_WIDTH/2, headerView.center.y);
	
	return headerView;
}

- (UIView*)createFooterView
{
	UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CELL_PADDING*3, 10, SCREEN_WIDTH-2*(CELL_PADDING*3), 50)]; // y=65
	textLabel.font = [UIFont fontWithName:FONT size:12];
	textLabel.textColor = GRAY_COLOR;
	textLabel.numberOfLines = 0;
	textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.backgroundColor = [UIColor whiteColor];
	textLabel.text = @"Your card will only be charged once you have engaged in a transaction with a service supplier.";
	
	UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, textLabel.frame.size.height)];
	[footerView addSubview:textLabel];
	
	if (!self.isAdding)
	{
		UILabel *deleteText = [[UILabel alloc] initWithFrame:CGRectMake(CELL_PADDING, 0, SCREEN_WIDTH-CELL_PADDING*2, 20)];
		deleteText.font = [UIFont fontWithName:FONT_SEMI_BOLD size:13];
		deleteText.textColor = PINK_COLOR;
		deleteText.numberOfLines = 1;
		deleteText.textAlignment = NSTextAlignmentCenter;
		deleteText.text = @"Delete Card";
		[deleteText sizeToFit];
		
		UIImageView *deleteImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"delete_icon"]];
		deleteImage.contentMode = UIViewContentModeScaleAspectFit;
		
		UIView *container = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-260)/2, 10, 260, 50)];
		container.userInteractionEnabled = YES;
		[container addSubview:deleteImage];
		[container addSubview:deleteText];
		
		CGRect frame = deleteText.frame;
		frame.origin = CGPointMake((container.frame.size.width-deleteText.frame.size.width)/2, 15);
		frame.size = CGSizeMake(deleteText.frame.size.width, 20);
		deleteText.frame = frame;
		
		frame = deleteImage.frame;
		frame.origin = CGPointMake(deleteText.frame.origin.x-deleteImage.frame.size.width-5, deleteText.frame.origin.y);
		frame.size = CGSizeMake(deleteImage.frame.size.width, 20);
		deleteImage.frame = frame;
		
		UITapGestureRecognizer *deleteTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(deleteCard)];
		deleteTap.numberOfTapsRequired = 1;
		[container addGestureRecognizer:deleteTap];
		
		footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, footerView.frame.size.width + 20);
		[footerView addSubview:container];
	}
	
	
	return footerView;
}

#pragma mark - Actions -
- (void)finish
{
	[self dismissKeyboard];
	
	if ([self isScreenFieldsValid])
	{
		[self saveCardToBackend:NO
					  completed:^(BOOL completed, Card *card) {
						  
                          if (completed)
                          {
                              CreatingAccountBlackOverlayViewController *overlay = [[CreatingAccountBlackOverlayViewController alloc] initWithNibName:@"CreatingAccountBlackOverlayViewController" bundle:nil];
                              overlay.card = card;
                              overlay.delegate = self;
                              [self.navigationController presentViewController:overlay animated:YES completion:nil];
                          }
					  }];
	}
	else
	{
		[self showScreenError];
	}
}

- (void)completeLater
{
	CreatingAccountBlackOverlayViewController *overlay = [[CreatingAccountBlackOverlayViewController alloc] initWithNibName:@"CreatingAccountBlackOverlayViewController" bundle:nil];
	overlay.delegate = self;
	//	overlay.image = self.profileImageView.image;
	[self.navigationController presentViewController:overlay animated:YES completion:nil];
}

- (void)deleteCard
{
	[AlertViewManager showAlertWithTitle:@"Card"
								 message:@"Are you sure you want to delete this card?"
					   cancelButtonTitle:@"No"
					   otherButtonTitles:@[@"Yes"]
					buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
						if (buttonIndex == 1)
						{
							//						   [CacheManager deleteCard:self.card];
							[self.navigationController popViewControllerAnimated:YES];
						}
					}];
	
}

-(void)gotoJobCompleteScreen
{
	JobCompleteBlackOverlayViewController *controller = [[JobCompleteBlackOverlayViewController alloc] initWithNibName:@"JobCompleteBlackOverlayViewController" bundle:nil];
	controller.delegate = self;
	controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	self.screenshot = [self getBlurredBackgroundImage];
	controller.backgroundImage = self.screenshot;
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)save
{
	[self dismissKeyboard];
	
	if ([self isScreenFieldsValid])
	{
		[self saveCardToBackend:YES
					  completed:^(BOOL completed, Card *card) {
						  
						  if (completed)
						  {
							  [AlertViewManager showAlertWithTitle:@""
														   message:@"Card added successfully"
												 cancelButtonTitle:@"OK"
												 otherButtonTitles:nil
											  buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
												  [self.navigationController popViewControllerAnimated:YES];
											  }];
						  }
					  }];
	}
	else
	{
		[self showScreenError];
	}
}

-(void)saveCardToBackend:(bool)shouldSaveToBackend
			   completed:(CompletedHandler)completed
{
	NSString *stringWithoutSpaces = [self.cardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
	
	Card *tempCard = [Card new];
	tempCard.number = stringWithoutSpaces;
	tempCard.cvv = self.cvv.text;
	
	if (self.month.text.length == 2)
	{
		tempCard.month = self.month.text;
	}
	else
	{
		tempCard.month = [NSString stringWithFormat:@"0%@",self.month.text];
	}
	
	if (self.year.text.length == 2)
	{
		tempCard.year = [NSString stringWithFormat:@"20%@",self.year.text];
	}
	else
	{
		tempCard.year = [NSString stringWithFormat:@"201%@",self.year.text];
	}
	
	tempCard.cardHolderName = self.cardHolderName.text;
	tempCard.name = self.cardName.text;
	
	if ([[stringWithoutSpaces substringToIndex:1] isEqualToString:@"3"])
	{
		tempCard.brand = @"AMEX";
	}
	else if ([[stringWithoutSpaces substringToIndex:1] isEqualToString:@"4"])
	{
		tempCard.brand = @"VISA";
	}
	else if ([[stringWithoutSpaces substringToIndex:1] isEqualToString:@"5"])
	{
		tempCard.brand = @"MASTERCARD";
	}
	
	// DO API CALL
	//paymentBrand: AMEX
	//cardNumber: 377777777777770
	//cardHolder: Jane Jones
	//expiryMonth: 05
	//expiryYear: 2018
	//cvv: 1234
	//name: Private
	
//		tempCard.brand = @"EX";
//		tempCard.number = @"377777777777770";
//		tempCard.cardHolderName = @"Jane Jones";
//		tempCard.month = @"05";
//		tempCard.year = @"2018";
//		tempCard.cvv = @"1234";
//		tempCard.name = @"Private";
	
	if (shouldSaveToBackend)
	{
		[[APIManager manager] registerCreditCard:tempCard
										 success:^(id result) {
											 completed(YES,tempCard);
										 } failure:^(NSError *error) {
                                             [AlertViewManager showAlertWithTitle:@""
                                                                          message:error.localizedDescription
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil
                                                             buttonSelectionBlock:nil];
                                             
											 completed(NO,tempCard);
										 }];
	}
	else
	{
		completed(YES,tempCard);
	}
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		self.cardHolderNameCell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		self.cardHolderNameCell.textField = self.cardHolderName;
		self.cardHolderNameCell.textField.delegate = self;
		[self.cardHolderNameCell setupCellWithPlaceholder:@"Card Holder Name"];
		self.cardHolderName.text = self.holderNameString;
		
		return self.cardHolderNameCell;
	}
	
	else if (indexPath.section == 1)
	{
		self.cardnumberCell = [tableView dequeueReusableCellWithIdentifier:@"CardNumberCell"];
		self.cardnumberCell.cardNumber = self.cardNumber;
		self.cardnumberCell.cardNumber.delegate = self;
		[self.cardnumberCell setupCellWithCardNumber:self.numberString];
		
		return self.cardnumberCell;
	}
	
	else if (indexPath.section == 2)
	{
		self.expiryCell = [tableView dequeueReusableCellWithIdentifier:@"ExpiryDateCell"];
		self.expiryCell.monthField = self.month;
		self.expiryCell.yearField = self.year;
		self.expiryCell.textField = self.dummyTextFieldExpiry;
		self.expiryCell.monthField.delegate = self;
		self.expiryCell.yearField.delegate = self;
		
		[self.expiryCell setupCellWithYear:self.yearString andMonth:self.monthString];
		
		return self.expiryCell;
	}
	
	else if (indexPath.section == 3)
	{
		self.cvvCell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		self.cvvCell.textField = self.cvv;
		self.cvvCell.textField.delegate = self;
		[self.cvvCell setupCellWithPlaceholder:@"CVV Number"];
		self.cvv.text = self.cvvString;
		
		return self.cvvCell;
	}
	
	else if (indexPath.section == 4)
	{
		self.cardNameCell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		self.cardNameCell.textField = self.cardName;
		self.cardNameCell.textField.delegate = self;
		[self.cardNameCell setupCellWithPlaceholder:@"Card Description"];
		self.cardName.text = self.cardNameString;
		
		return self.cardNameCell;
	}
	
	
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"CARD HOLDER NAME"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"CARD NUMBER"];
	}
	else if (section == 2)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"EXPIRY DATE"];
	}
	else if (section == 3)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"CVV"];
	}
	else if (section == 4)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"CARD DESCRIPTION"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

#pragma mark - Card Scanning -

- (void)scanCard
{
	CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
	scanViewController.hideCardIOLogo = YES;
	scanViewController.disableManualEntryButtons = YES;
	[self presentViewController:scanViewController animated:YES completion:nil];
}

- (void)scanCardAsView
{
	CardIOView *cardIOView = [[CardIOView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
	cardIOView.delegate = self;
	
	[self.view addSubview:cardIOView];
}

- (IBAction)cancelScanCard:(id)sender
{
	//[cardIOView removeFromSuperview];
}

#pragma mark - Delegates -
- (void)dismissViewController:(CreatingAccountBlackOverlayViewController *)controller success:(BOOL)success onCard:(BOOL)cardFailed
{
    AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    
    if (!success && !cardFailed)
    {
        [SettingsManager clear];
        [appDelegate startAppWithLoginController];
    }
    else
    {
        if (success)
        {
            [appDelegate startAppWithServicesList];
        }
        
        if (cardFailed)
        {
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.hidesBackButton = YES;
            
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
        }
        
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - CardIO -
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
	NSLog(@"User canceled payment info");
	
	[scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
	NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
	
	//self.cardNumber.text = info.cardNumber;
	self.numberString = info.cardNumber;
	//	[self.cardnumberCell reformatAsCardNumber:self.cardNumber];
	
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
	
	self.month.text = [NSString stringWithFormat:@"%lu",(unsigned long)info.expiryMonth];
	self.year.text = [[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear] substringFromIndex:2];
	
	if (info.expiryMonth < 10)
	{
		NSString *zero = @"0";
		self.month.text = [zero stringByAppendingString:self.month.text];
	}
	
	if (info.expiryYear < 10)
	{
		NSString *zero = @"0";
		self.year.text = [zero stringByAppendingString:self.year.text];
	}
	
	self.cvv.text = [NSString stringWithFormat:@"%@",info.cvv];
	
	self.cvvString = self.cvv.text;
	self.monthString = self.month.text;
	self.yearString = self.year.text;
	
	[scanViewController dismissViewControllerAnimated:YES completion:nil];
}

//AS View
- (void)cardIOView:(CardIOView *)cardIOView didScanCard:(CardIOCreditCardInfo *)info
{
	if (info)
	{
		NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
	}
	else
	{
		NSLog(@"User cancelled payment info");
	}
	
	[cardIOView removeFromSuperview];
}

#pragma mark - TextField -

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
	GTTextField *field = (GTTextField*)textField;
	
	if (field == self.cardNumber)
	{
		field.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
	}
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	GTTextField *field = (GTTextField*)textField;
	
	if (field == self.cardNumber)
	{
		NSString *cardNumber = [field.text stringByReplacingOccurrencesOfString:@" " withString:@""];
		
		if (cardNumber.length >= 16)
		{
			if (![string isEqualToString:@""])
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		if (range.location != 0 && (field.text.length + 1) % 5 == 0 && string.length > 0)
		{
			field.text = [field.text stringByAppendingString:@" "];
		}
		
		return TRUE;
	}
	
	else if (field == self.cvv)
	{
		return field.text.length + (string.length - range.length) <= 3;
	}
	
	else if (field == self.month || field == self.year)
	{
		if (textField.text.length == 2 && ![string isEqualToString:@""])
		{
			return FALSE;
		}
		else if ([string isEqualToString:@""])
		{
			return TRUE;
		}
		return TRUE;
	}
	
	return TRUE;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
	GTTextField *field = (GTTextField*)textField;
	
	if (field == self.cardHolderName)
	{
		self.holderNameString = self.cardHolderName.text;
		
		if (self.holderNameString.length > 0)
		{
			[self isCardHolderNameValid];
		}
	}
	else if (field == self.cardName)
	{
		self.cardNameString = self.cardName.text;
		
		if (self.cardNameString.length > 0)
		{
			[self isCardNameValid];
		}
	}
	else if (field == self.cardNumber)
	{
		NSString *cardNumber = [field.text stringByReplacingOccurrencesOfString:@" " withString:@""];
		
		if (cardNumber.length == 16)
		{
			[self.cardnumberCell getCardLogo];
			[self.cardnumberCell reformatAsCardNumber:self.cardNumber];
		}
		
		self.numberString = cardNumber;
		
		if (self.numberString.length > 0)
		{
			[self isCardNumberValid];
		}
	}
	
	else if (field == self.cvv)
	{
		self.cvvString = self.cvv.text;
		
		if (self.cvvString.length > 0)
		{
			[self isCVVValid];
		}
	}
	
	else if (field == self.month)
	{
		self.monthString = self.month.text;
		
		if (self.monthString.length > 0 )
		{
			self.didChangeMonth = YES;
			
			if (self.didChangeYear)
			{
				[self isYearValid];
				[self isMonthValidForAnyYear:NO];
				self.didChangeYear = NO;
			}
			else
			{
				[self isMonthValidForAnyYear:YES];
			}
		}
	}
	else if(field == self.year)
	{
		self.yearString = self.year.text;
		
		if (self.yearString.length > 1)
		{
			self.didChangeYear = YES;
			
			[self isYearValid];
			
			if (self.didChangeMonth)
			{
				[self isMonthValidForAnyYear:NO];
				self.didChangeMonth = NO;
			}
		}
	}
}

#pragma mark - Validation -
- (BOOL)isScreenFieldsValid
{
	[super isScreenFieldsValid];
	
	BOOL isValid = YES;
	
	isValid = [self isCardHolderNameValid] && isValid;
	isValid = [self isCardNumberValid] && isValid;
	isValid = [self isMonthValidForAnyYear:NO] && isValid;
	isValid = [self isYearValid] && isValid;
	isValid = [self isCVVValid] && isValid;
	isValid = [self isCardNameValid] && isValid;
	
	return isValid;
}

- (BOOL)isCardHolderNameValid
{
	if (self.cardHolderName.text.length > 0)
	{
		[self.cardHolderNameCell dismissError];
		[self dismissScreenError:self.cardHolderName];
		return YES;
	}
	else
	{
		[self.cardHolderNameCell setError];
		[self setScreenError:self.cardHolderName message:@"Please enter the name on the card"];
		return NO;
	}
}

- (BOOL)isCardNameValid
{
	if (self.cardName.text.length > 0)
	{
		[self.cardNameCell dismissError];
		[self dismissScreenError:self.cardName];
		return YES;
	}
	else
	{
		[self.cardNameCell setError];
		[self setScreenError:self.cardName message:@"Please enter a description for the card"];
		return NO;
	}
}

- (BOOL)isCardNumberValid
{
	NSString *stringWithoutSpaces = [self.cardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
	
	if (stringWithoutSpaces.length == 16)
	{
		[self.cardnumberCell dismissError];
		[self dismissScreenError:self.cardNumber];
		return YES;
	}
	else
	{
		[self.cardnumberCell setError];
		[self setScreenError:self.cardNumber message:@"Please enter a valid 16 digit card number"];
		return NO;
	}
}

- (BOOL)isMonthValidForAnyYear:(BOOL)forAnyYear
{
	if (self.month.text.length > 0)
	{
		NSDateComponents *components = [[NSCalendar currentCalendar] components:NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
		
		NSInteger month = [components month];
		NSString *subYear = [[NSString stringWithFormat:@"%ld",[components year]] substringFromIndex:2];
		
		if (self.yearString.length > 1)
		{
			if ( [self.month.text intValue] < month && [self.year.text intValue] == [subYear intValue])
			{
				[self.expiryCell setError];
				[self setScreenError:self.expiryCell.textField message:@"Please enter a valid date"];//month later than last month"];
				return NO;
			}
			
			if ( [self.month.text integerValue] < 1 || [self.month.text intValue] > 12)
			{
				[self.self.expiryCell setError];
				[self setScreenError:self.expiryCell.textField message:@"Please enter a valid date"]; //Month
				return NO;
			}
			else
			{
				[self.expiryCell dismissError];
				[self dismissScreenError:self.expiryCell.textField];
				return YES;
			}
		}
		else
		{
			if ( [self.month.text integerValue] < 1 || [self.month.text intValue] > 12)
			{
				[self.self.expiryCell setError];
				[self setScreenError:self.expiryCell.textField message:@"Please enter a valid date"]; //Month
				return NO;
			}
			else
			{
				[self.expiryCell dismissError];
				[self dismissScreenError:self.expiryCell.textField];
				return YES;
			}
		}
	}
	else
	{
		[self.expiryCell setError];
		[self setScreenError:self.expiryCell.textField message:@"Please enter a valid date"];//month
		return NO;
	}
	
	return NO;
}

- (BOOL)isYearValid
{
	if (self.year.text.length > 1)
	{
		NSDateComponents *components = [[NSCalendar currentCalendar] components:NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
		
		NSInteger year = [components year];
		
		NSString *subYear = [[NSString stringWithFormat:@"%ld",year] substringFromIndex:2];
		
		if ([self.year.text intValue] < [subYear intValue])
		{
			[self.expiryCell setError];
			[self setScreenError:self.expiryCell.textField message:@"Please enter a valid date"];//[NSString stringWithFormat:@"Please enter a year later than %@",subYear]];
			return NO;
		}
		else
		{
			[self.expiryCell dismissError];
			[self dismissScreenError:self.expiryCell.textField];
			return YES;
		}
	}
	else
	{
		[self.expiryCell setError];
		[self setScreenError:self.expiryCell.textField message:@"Please enter a valid date"];//year
		return NO;
	}
}

- (BOOL)isCVVValid
{
	if (self.cvv.text.length == 3)
	{
		[self.cvvCell dismissError];
		[self dismissScreenError:self.cvv];
		return YES;
	}
	else
	{
		[self.cvvCell setError];
		[self setScreenError:self.cvv message:@"Please enter a 3 digit CVV number"];
		return NO;
	}
}

@end
