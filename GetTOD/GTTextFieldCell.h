//
//  GTTextFieldCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "GTTextField.h"

@interface GTTextFieldCell : GTBoldTableViewCell <UITextFieldDelegate>

@property (nonatomic, strong) GTTextField *textField;

- (void)setupCellWithPlaceholder:(NSString*)placeholder;

- (void)setError;
- (void)dismissError;

@end
