//
//  AppDelegate.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/05.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AppDelegate.h"
#import "Flurry.h"

#import <GoogleMaps/GoogleMaps.h>
#import "RootViewController.h"
#import "LoginViewController.h"
#import "ServicesListViewController.h"
#import "SupplierHomeMapViewController.h"
#import "Constants.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "UIApplication+SimulatorRemoteNotifications.h"
#import "LocationManager.h"

#import "SupplierEnRouteViewController.h"
#import "RequestingSupplierBlackOverlayViewController.h"
#import "SupplierArrivedViewController.h"
#import "ClientQuoteViewController.h"
#import "JobCompleteBlackOverlayViewController.h"

#import "NewRequestViewController.h"
#import "CustomerCancelledViewController.h"
#import "CustomerCancelledInTimeViewController.h"
#import "QuoteRejectedViewController.h"
#import "SupplierJobInProgressViewController.h"
#import "AddEditCardDetailsViewController.h"
#import "NoSuppliersViewController.h"
#import "ChooseSupplierViewController.h"

#import "JobInProgressViewController.h"
#import "NavigationDirectionsViewController.h"
#import "ArrivedViewController.h"
#import "SupplierQuoteViewController.h"
#import "AwaitingApprovalViewController.h"
#import "AwaitingPaymentViewController.h"
#import "RejectedQuoteViewController.h"
#import "CancelationFeedbackViewController.h"

typedef enum {
    NotificationTypeJob,
    NotificationTypeQuote,
    NotificationTypeSuppliers,
    NotificationTypeDirections
} NotificationType;

@interface AppDelegate () <CLLocationManagerDelegate>

@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, retain) MBProgressHUD *HUD;

@property (strong, nonatomic) NSTimer *timer;
//@property (strong, nonatomic) NSDictionary *currentPushNotification;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[GMSServices provideAPIKey:GOOGLE_MAPS_API_KEY];
	[AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
	
#ifdef CLIENT
	[Flurry startSession:@"3CG3S5658V74QHPY4BP8"];
#else
	[Flurry startSession:@"2T56ZM46FK8XXC22RFBH"];
#endif
	
	
#if TARGET_IPHONE_SIMULATOR
	[application listenForRemoteNotifications];
#else
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        
        UIUserNotificationType types = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [application registerUserNotificationSettings:mySettings];
        [application registerForRemoteNotifications];
    }else{
        [application registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
//	if ([SettingsManager manager].isiOS8)
//	{
//		UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound  categories:nil];
//		[[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//		[[UIApplication sharedApplication] registerForRemoteNotifications];
//	}
//	else
//	{
//		[[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
//	}
#endif
	
	if ([SettingsManager manager].isInSA == nil || [[SettingsManager manager].isInSA isEqualToString:@""])
	{
		[[LocationManager manager] checkIfUserInSouthAfrica:^(BOOL inSA, NSString *message) {}];
	}
	
	//When the app receives a significant change notification
	if (launchOptions[UIApplicationLaunchOptionsLocationKey])
	{
		[[LocationManager manager] startMonitoringSignificantLocationChanges:^(BOOL success, CLLocation *location) {
			
			[SettingsManager manager].currentLocationLat = [NSNumber numberWithDouble:location.coordinate.latitude];
			[SettingsManager manager].currentLocationLng = [NSNumber numberWithDouble:location.coordinate.longitude];
			[SettingsManager save];
		}];
	}
	else
	{
		if ([SettingsManager manager].authenticationToken)
		{
#ifdef CLIENT
			[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
				
				if (success)
				{
					[SettingsManager manager].currentLocationLat = [NSNumber numberWithDouble:location.coordinate.latitude];
					[SettingsManager manager].currentLocationLng = [NSNumber numberWithDouble:location.coordinate.longitude];
					[SettingsManager save];
				}
			}];
#else
			[[LocationManager manager] checkforBackgroundPermissions];
#endif
		}
	}
	
	NSLog(@"TOKEN  = %@",[SettingsManager manager].authenticationToken);
	NSLog(@"STATE  = %d",[SettingsManager manager].appState);
	
	[self startApp];
	
	return YES;
}

- (void)startApp
{
	if ([SettingsManager manager].authenticationToken)
	{
#ifdef CLIENT
		[self startAppWithServicesList];
		[self handleClientAppState];
#else
		[self startAppWithPartnerHomeScreen];
		[self startTimer];
		[self handlePartnerAppState];
#endif
	}
	else
	{
		[self startAppWithLoginController];
	}
}
- (void)startAppWithLoginController
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.window.backgroundColor = [UIColor whiteColor];
	[self.window makeKeyAndVisible];
	
	LoginViewController *login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
	self.navController = [[UINavigationController alloc] initWithRootViewController:login];
	self.navController.navigationBarHidden = NO;
	[Flurry logAllPageViews:self.navController];
	self.window.rootViewController = self.navController;
}

- (void)startAppWithServicesList
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.window.backgroundColor = [UIColor whiteColor];
	[self.window makeKeyAndVisible];
	
	ServicesListViewController *list = [[ServicesListViewController alloc] init];
	self.navController = [[UINavigationController alloc] initWithRootViewController:list];
	self.navController.navigationBarHidden = NO;
	[Flurry logAllPageViews:self.navController];
	self.window.rootViewController = self.navController;
}

- (void)startAppWithPartnerHomeScreen
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.window.backgroundColor = [UIColor whiteColor];
	[self.window makeKeyAndVisible];
	
	SupplierHomeMapViewController *map = [[SupplierHomeMapViewController alloc] init];
	self.navController = [[UINavigationController alloc] initWithRootViewController:map];
	self.navController.navigationBarHidden = NO;
	[Flurry logAllPageViews:self.navController];
	self.window.rootViewController = self.navController;
}

- (void)handleClientAppState
{
	switch ([SettingsManager manager].appState) {
		case AppStateLogin: {
			[self startAppWithLoginController];
			break;
		}
		case AppStateRegistration: {
			[self startAppWithLoginController];
			break;
		}
		case AppStateEnteringDetails: {
			
			break;
		}
		case AppStateCreatingAccount: {
			
			break;
		}
		case AppStateNone: {
			break;
		}
		case AppStateRequestingSupplier: {
			
			break;
		}
		case AppStateEnroute: {
			[self gotoEnRouteForCurrentJob];
			break;
		}
		case AppStateCancelled: {
			
			break;
		}
		case AppStateArrived: {
			[self gotoPartnerArrivedScreen];
			break;
		}
		case AppStateQuote: {
			[self gotoClientQuoteScreen];
			break;
		}
		case AppStateQuoteRejected: {
			[self gotoRejectedQuoteScreen];
			break;
		}
		case AppStateCancelledFromQuote: {
			[self gotoThanksForFeedbackScreen];
			break;
		}
		case AppStateJobInProgress: {
			[self gotoClientJobInProgressScreen];
			break;
		}
		case AppStatePayment: {
			[self gotoPaymentScreen];
			break;
		}
		case AppStateJobComplete: {
			[self gotoJobCompleteScreen];
			break;
		}
		case AppStateRecommend: {
			[self gotoShareScreen];
			break;
		}
		case AppStateReportAProblem: {
			
			break;
		}
		case AppStateCancelling: {
			
			break;
		}
		case AppStateQuoteRejecting: {
			
			break;
		}
		case AppStateProblemReported: {
			
			break;
		}
		default: {
			break;
		}
	}
}

- (void)handlePartnerAppState
{
	switch ([SettingsManager manager].appState) {
		case AppStateLogin: {
			[self startAppWithLoginController];
			break;
		}
		case AppStateNone: {
			
			break;
		}
		case AppStateRequestingSupplier: {
			
			break;
		}
		case AppStateOffline: {
			[self startAppWithPartnerHomeScreen];
			break;
		}
		case AppStateEnroute: {
			[self gotoNavigationScreen];
			break;
		}
		case AppStateCancelled: {
			[self clientCancelled];
			break;
		}
		case AppStateArrived: {
			[self gotoArrivedScreen];
			break;
		}
		case AppStateQuote: {
			[self gotoSupplierQuoteScreen];
			break;
		}
		case AppStateAwaitingApproval: {
			[self gotoAwaitingApprovalScreen];
			break;
		}
		case AppStateQuoteRejected: {
			[self clientRejectedQuote];
			break;
		}
		case AppStateCancelledFromQuote: {
			[self clientCancelled];
			break;
		}
		case AppStateJobInProgress: {
			[self gotoSupplierJobInProgressScreen];
			break;
		}
		case AppStateAwaitingPayment: {
			[self gotoAwaitingPaymentScreen];
			break;
		}
		case AppStateJobComplete: {
			[self gotoJobCompleteScreen];
			break;
		}
		case AppStateReportAProblem: {
			
			break;
		}
		case AppStateProblemReported: {
			
			break;
		}
		default: {
			break;
		}
	}
}


- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	[self stopTimer];
	
	[[LocationManager manager] stopUpdatingLocation];
	
#ifdef CLIENT
	
#else
	[[LocationManager manager] startMonitoringSignificantLocationChanges:^(BOOL success, CLLocation *location) {
		
	}];
#endif
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
//    [SettingsManager manager].didHandlePush = NO;
//    [SettingsManager save];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    

    [self processPushNotification:[SettingsManager manager].pushInfo];

#ifdef CLIENT
	
#else
	[[LocationManager manager] stopMonitoringSignificantLocation];
	
	[[LocationManager manager] startMonitoringLocationChanges:^(BOOL success, CLLocation *location) {
		
	}];
	
	[self startTimer];
#endif
}

- (void)applicationWillTerminate:(UIApplication *)application
{
//	[self saveContext];
	
#ifdef CLIENT
	
#else
	if ([SettingsManager manager].authenticationToken)
	{
		[[APIManager manager] setSupplierLocationCoordinates:[SettingsManager manager].currentLocationCoords
                                                     bearing:[[SettingsManager manager].currentHeading floatValue]
												   andStatus:SupplierStatusOffline
													 success:^(id result) {
														 
														 
													 } failure:^(NSError *error) {
														 
														 
													 }];
	}
#endif
	
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
	NSString * strToken = [NSString stringWithFormat:@"%@",deviceToken];
	
	strToken = [[[strToken	stringByReplacingOccurrencesOfString: @"<" withString: @""]
				 stringByReplacingOccurrencesOfString: @">" withString: @""]
				stringByReplacingOccurrencesOfString: @" " withString: @""];
	
	[SettingsManager manager].pushToken = [NSString stringWithFormat:@"%@",strToken];
	[SettingsManager save];
	
	NSLog(@"PUSH TOKEN: %@",strToken);
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	NSLog(@"Did Fail to Register for Remote Notifications");
	NSLog(@"%@, %@", error, error.localizedDescription);
	
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
	
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self handleReceivedPushForApplicationState:application.applicationState andUserInfo:userInfo];
}

-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler
{
	
}

-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
	
}
// Foreground
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [self handleReceivedPushForApplicationState:application.applicationState andUserInfo:userInfo];
}

- (void)handleReceivedPushForApplicationState:(UIApplicationState)applicationState andUserInfo:(NSDictionary*)userInfo
{
    [SettingsManager manager].pushInfo = userInfo;
    [SettingsManager manager].didHandlePush = NO;
    [SettingsManager save];
    
    if(applicationState == UIApplicationStateInactive)
    {
        NSLog(@"Inactive");
        
        //Show the view with the content of the push
    }
    else if (applicationState == UIApplicationStateBackground)
    {
        NSLog(@"Background:\n%@",userInfo);
        
        //Refresh the local model
    }
    else
    {
        NSLog(@"Active");
        
        //Show an in-app banner
        [self processPushNotification:[SettingsManager manager].pushInfo];
    }
}
//#pragma mark - Core Data stack -
//
//@synthesize managedObjectContext = _managedObjectContext;
//@synthesize managedObjectModel = _managedObjectModel;
//@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
//
//- (NSURL *)applicationDocumentsDirectory {
//	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
//}
//
//- (NSManagedObjectModel *)managedObjectModel {
//	if (_managedObjectModel != nil) {
//		return _managedObjectModel;
//	}
//	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"GetTOD" withExtension:@"momd"];
//	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
//	return _managedObjectModel;
//}
//
//- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
//	if (_persistentStoreCoordinator != nil) {
//		return _persistentStoreCoordinator;
//	}
//	
//	// Create the coordinator and store
//	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
//	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"GetTOD.sqlite"];
//	NSError *error = nil;
//	NSString *failureReason = @"There was an error creating or loading the application's saved data.";
//	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
//		// Report any error we got.
//		NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//		dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
//		dict[NSLocalizedFailureReasonErrorKey] = failureReason;
//		dict[NSUnderlyingErrorKey] = error;
//		error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
//		// Replace this with code to handle the error appropriately.
//		// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//		abort();
//	}
//	
//	return _persistentStoreCoordinator;
//}
//
//
//- (NSManagedObjectContext *)managedObjectContext {
//	// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
//	if (_managedObjectContext != nil) {
//		return _managedObjectContext;
//	}
//	
//	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
//	if (!coordinator) {
//		return nil;
//	}
//	_managedObjectContext = [[NSManagedObjectContext alloc] init];
//	[_managedObjectContext setPersistentStoreCoordinator:coordinator];
//	return _managedObjectContext;
//}
//
//#pragma mark - Core Data Saving support
//
//- (void)saveContext {
//	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//	if (managedObjectContext != nil) {
//		NSError *error = nil;
//		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
//			// Replace this implementation with code to handle the error appropriately.
//			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//			abort();
//		}
//	}
//}

#pragma mark - LocationTimer -
- (void)startTimer
{
	if (self.timer)
	{
		[self.timer invalidate];
		self.timer = nil;
	}
	
	self.timer = [NSTimer scheduledTimerWithTimeInterval:TIME_TO_UPDATE_LOCATION*60
												  target:self
												selector:@selector(updateSupplierLocationFromTimer)
												userInfo:nil
												 repeats:YES];
}

- (void)stopTimer
{
	[self.timer invalidate];
	self.timer = nil;
}

- (void)updateSupplierLocationFromTimer
{
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		if ([SettingsManager manager].authenticationToken)
		{
			if (success)
			{
				[[APIManager manager] setSupplierLocationCoordinates:location.coordinate
                                                             bearing:[[SettingsManager manager].currentHeading floatValue]
                                                           andStatus:[SettingsManager manager].supplierStatus
                                                             success:^(id result) {
                                                                 
                                                                 
                                                             } failure:^(NSError *error) {
                                                                 
                                                                 
                                                             }];
			}
			else
			{
				[[APIManager manager] setSupplierLocationCoordinates:[SettingsManager manager].currentLocationCoords
                                                             bearing:[[SettingsManager manager].currentHeading floatValue]
                                                           andStatus:[SettingsManager manager].supplierStatus
															 success:^(id result) {
																 
															 } failure:^(NSError *error) {
																 
															 }];
			}
		}
		
	}];
	
}

#pragma mark - Notification -
- (void)sendNotificationForLoading:(NotificationType)type
{
    NSString *message;
    
    if (type == NotificationTypeJob)
    {
        message = @"Loading job details";
    }
    else if (type == NotificationTypeQuote)
    {
        message = @"Loading quote details";
    }
    else if (type == NotificationTypeSuppliers)
    {
        message = @"Loading supplier details";
    }
    else if (type == NotificationTypeDirections)
    {
        message = @"Loading directions";
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowLoaderWithMessage" object:self userInfo:@{@"message":message}];
}

//- (void)hideLoader
//{
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"HideLoader" object:self];
//}

#pragma mark - Push Handling -
- (void)processPushNotification:(NSDictionary*)info
{
    if ([SettingsManager manager].didHandlePush)
    {
        return;
    }
    
	ActionType action = [[info objectForKey:@"action"] integerValue];
	
	NSNumber *job = [info objectForKey:@"job_id"];
	NSNumber *quote = [info objectForKey:@"quote_id"];
	NSNumber *service = [info objectForKey:@"service_id"];
	
	NSString *rejectReason = [info objectForKey:@"rejection_reason"];
	NSInteger cancelType = [[info objectForKey:@"cancel_type"] integerValue];
	
	NSLog(@"\nPUSH ----- \n\n %@",info);
	
//	if (![self.currentPushNotification isEqualToDictionary:info])
//	{
//    NSLog(@"\n PUSH");//DIFFERENT");
//		self.currentPushNotification = info;
		
		switch (action) {
			case ActionTypeClientRequestedPartner: {
				[self clientSubmittedRequest:job];
				break;
			}
			case ActionTypePartnerAcceptedRequest: {
				[self partnerIsEnRoute:job];
				break;
			}
			case ActionTypeRequestCancelled: {
				[SettingsManager manager].cancelType = cancelType;
				[SettingsManager manager];
#ifdef CLIENT
				[self partnerCancelled];
#else
				[self clientCancelled];
#endif
				break;
			}
			case ActionTypePartnerQuoteSubmitted: {
				[self partnerSentQuote:quote];
				break;
			}
			case ActionTypeClientAcceptedQuote: {
				[self clientAcceptedQuote];
				break;
			}
			case ActionTypeClientRejectedQuote: {
				[SettingsManager manager].rejectReason = rejectReason;
				[SettingsManager save];
				
				[self clientRejectedQuote];
				break;
			}
			case ActionTypePartnerArrived: {
				[self partnerArrived];
				break;
			}
			case ActionTypePartnerJobCompleted: {
				[self partnerCompletedJob];
				break;
			}
			case ActionTypePartnerCameOnline: {
#ifdef CLIENT
				[self partnerCameOnline:service];
#else
				
#endif
				break;
			}
			case ActionTypeClientCompletedPayment: {
				[self clientCompletedPayment];
				break;
			}
			default: {
				break;
			}
		}
//	}
//	else
//	{
//		NSLog(@"\n SAME");
//		self.currentPushNotification = info;
//	}
    
    [SettingsManager manager].didHandlePush = YES;
    [SettingsManager save];
}

#pragma mark - ClientSideActions
- (void)partnerCancelled
{
	if ([SettingsManager manager].cancelType == JobCancelTypeTimeOut)
	{
		[SettingsManager manager].appState = AppStateHome;
		[SettingsManager save];
		
		[self.navController popViewControllerAnimated:YES];
	}
}

- (void)partnerIsEnRoute:(NSNumber*)jobId
{
    [self sendNotificationForLoading:NotificationTypeJob];
    
	[[APIManager manager] getJobForID:jobId
							  success:^(Job *job) {
								  
								  [SettingsManager manager].currentJob = job;
								  [SettingsManager save];
								  
								  [self gotoEnRouteForCurrentJob];
								  
							  } failure:^(NSError *error) {
								  
							  }];
}

- (void)gotoEnRouteForCurrentJob
{
	[SettingsManager manager].appState = AppStateEnroute;
	[SettingsManager save];
	
	AppDelegate *appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
	
	for (UIViewController *controller in appDelegate.navController.viewControllers)
	{
		if ([controller isKindOfClass:[RequestingSupplierBlackOverlayViewController class]])
		{
			[self.navController popViewControllerAnimated:NO];
		}
	}
	
	SupplierEnRouteViewController *controller = [[SupplierEnRouteViewController alloc] initWithNibName:@"SupplierEnRouteViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)partnerArrived
{
	[SettingsManager manager].appState = AppStateArrived;
	[SettingsManager save];
	
	[self gotoPartnerArrivedScreen];
}

- (void)gotoPartnerArrivedScreen
{
	SupplierArrivedViewController *controller = [[SupplierArrivedViewController alloc] initWithNibName:@"SupplierArrivedViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)partnerSentQuote:(NSNumber*)quoteId
{
    [self sendNotificationForLoading:NotificationTypeQuote];
    
	[[APIManager manager] getQuoteForID:quoteId
								success:^(Quote *quote) {
									
									[SettingsManager manager].currentJob.quote = quote;
									[SettingsManager save];
									
									[self gotoClientQuoteScreen];
									
								} failure:^(NSError *error) {
									
								}];
}

- (void)gotoClientQuoteScreen
{
	[SettingsManager manager].appState = AppStateQuote;
	[SettingsManager save];
	
	ClientQuoteViewController *controller = [[ClientQuoteViewController alloc] initWithNibName:@"ClientQuoteViewController" bundle:nil];
	controller.quote = [SettingsManager manager].currentJob.quote;
	
	[self.navController pushViewController:controller animated:NO];
}

- (void)gotoRejectedQuoteScreen
{
	[SettingsManager manager].appState = AppStateQuoteRejected;
	[SettingsManager save];
	
	RejectedQuoteViewController *controller = [[RejectedQuoteViewController alloc] initWithNibName:@"RejectedQuoteViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)gotoThanksForFeedbackScreen
{
	[self.navController popToRootViewControllerAnimated:NO];
	
	CancelationFeedbackViewController *controller = [[CancelationFeedbackViewController alloc] initWithNibName:@"CancelationFeedbackViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)gotoClientJobInProgressScreen
{
	[self.navController popToRootViewControllerAnimated:NO];
	
	JobInProgressViewController *inProgress = [[JobInProgressViewController alloc] initWithNibName:@"JobInProgressViewController" bundle:nil];
	[self.navController pushViewController:inProgress animated:NO];
}

- (void)gotoShareScreen
{
	[self.navController popToRootViewControllerAnimated:NO];
	
	ReferAFriendBlackOverlayViewController *controller = [[ReferAFriendBlackOverlayViewController alloc] initWithNibName:@"ReferAFriendBlackOverlayViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)partnerCompletedJob
{
	[SettingsManager manager].appState = AppStatePayment;
	[SettingsManager save];
	
	[self gotoJobCompleteScreen];
}

- (void)gotoPaymentScreen
{
	
	
	
	
//	[self gotoJobCompleteScreen];
	
	
	
	
//	[self.navController popToRootViewControllerAnimated:NO];
//	
//		JobCompleteBlackOverlayViewController *controller = [[JobCompleteBlackOverlayViewController alloc] initWithNibName:@"JobCompleteBlackOverlayViewController" bundle:nil];
//		controller.delegate = self;
//		controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//		self.screenshot = [self getBlurredBackgroundImage];
//		controller.backgroundImage = self.screenshot;
//		[self.navigationController pushViewController:controller animated:YES];

	
//	AddEditCardDetailsViewController *controller = [[AddEditCardDetailsViewController alloc] init];
//	controller.isAdding = YES;
//	controller.isSettings = YES;
//	[self.navController pushViewController:controller animated:NO];
}

- (void)partnerCameOnline:(NSNumber*)serviceID
{
	[self.navController popToRootViewControllerAnimated:NO];
	
    [self sendNotificationForLoading:NotificationTypeSuppliers];
    
	[[APIManager manager] getNearbySuppliersFromLocation:[SettingsManager manager].currentLocationCoords
												 andType:serviceID
												 success:^(NSMutableArray *results) {
													 
													 if (results.count == 0)
													 {
														 NoSuppliersViewController *controller = [[NoSuppliersViewController alloc] initWithNibName:@"NoSuppliersViewController" bundle:nil];
														 controller.serviceTypeID = serviceID;
														 [self.navController pushViewController:controller animated:NO];
													 }
													 else
													 {
														 ChooseSupplierViewController *chooser = [[ChooseSupplierViewController alloc] init];
														 chooser.suppliers = results;
														 [self.navController pushViewController:chooser animated:NO];
													 }
													 
												 } failure:^(NSError *error) {
													 
													 NoSuppliersViewController *controller = [[NoSuppliersViewController alloc] initWithNibName:@"NoSuppliersViewController" bundle:nil];
													 controller.serviceTypeID = serviceID;
													 [self.navController pushViewController:controller animated:NO];
												 }];
}

#pragma mark - PartnerSideActions
- (void)clientSubmittedRequest:(NSNumber*)jobId
{
    [self sendNotificationForLoading:NotificationTypeJob];
    
	[[APIManager manager] getJobForID:jobId
							  success:^(Job *job) {
								  
								  [SettingsManager manager].currentJob = job;
								  [SettingsManager save];
								  
								  [self gotoNewRequestScreen];
								  
							  } failure:^(NSError *error) {
								  
							  }];
}

- (void)gotoNewRequestScreen
{
	[SettingsManager manager].appState = AppStateRequestingSupplier;
	[SettingsManager save];
	
	NewRequestViewController *controller = [[NewRequestViewController alloc] initWithNibName:@"NewRequestViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)gotoNavigationScreen
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
    [SettingsManager save];
    
	LPGoogleFunctions *mapsThing = [[LPGoogleFunctions alloc] init];
	
    [self sendNotificationForLoading:NotificationTypeDirections];
    
	[mapsThing loadDirectionsForOrigin:[LPLocation locationWithLatitude:[SettingsManager manager].currentJob.supplier.location.latitude
															  longitude:[SettingsManager manager].currentJob.supplier.location.longitude]
						forDestination:[LPLocation locationWithLatitude:[SettingsManager manager].currentJob.jobLocationCoordinates.latitude
															  longitude:[SettingsManager manager].currentJob.jobLocationCoordinates.longitude]
				  directionsTravelMode:LPGoogleDirectionsTravelModeDriving
				  directionsAvoidTolls:LPGoogleDirectionsAvoidNone
						directionsUnit:LPGoogleDirectionsUnitMetric
				directionsAlternatives:NO
						 departureTime:[NSDate date]
						   arrivalTime:nil
							 waypoints:nil
					   successfulBlock:^(LPDirections *directions) {
						   
						   NavigationDirectionsViewController *directionsController = [[NavigationDirectionsViewController alloc] initWithNibName:@"NavigationDirectionsViewController" bundle:nil];
						   directionsController.directions = directions;
						   
						   __block bool requestedLocationOnce = NO;
						   
						   [[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
							   
							   if (!requestedLocationOnce)
							   {
								   requestedLocationOnce = YES;
								   directionsController.supplierLocationCoordinates = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
								   [self.navController pushViewController:directionsController animated:NO];
							   }
						   }];
						   
					   } failureBlock:^(LPGoogleStatus status) {
						   
						   if (status == LPGoogleStatusZeroResults)
						   {
							   
						   }
						   
					   }];
}

- (void)gotoArrivedScreen
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
    [SettingsManager save];
    
	ArrivedViewController *controller = [[ArrivedViewController alloc] initWithNibName:@"ArrivedViewController" bundle:nil];
	[self.navController pushViewController:controller animated:YES];
}

- (void)gotoAwaitingApprovalScreen
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
    [SettingsManager save];
    
	AwaitingApprovalViewController *controller = [[AwaitingApprovalViewController alloc] initWithNibName:@"BaseWhiteOverlayElipsesViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)gotoSupplierQuoteScreen
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
    [SettingsManager save];
    
	ArrivedViewController *controller = [[ArrivedViewController alloc] initWithNibName:@"ArrivedViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
	SupplierQuoteViewController *quote = [[SupplierQuoteViewController alloc] initWithNibName:@"SupplierQuoteViewController" bundle:nil];
	[self.navController pushViewController:quote animated:NO];
}

- (void)clientCancelled
{
	[SettingsManager manager].appState = AppStateCancelled;
	[SettingsManager save];
	
	if ([SettingsManager manager].cancelType == JobCancelTypeBeforeJob)
	{
		[self.navController popToRootViewControllerAnimated:NO];
		CustomerCancelledInTimeViewController *controller = [[CustomerCancelledInTimeViewController alloc] initWithNibName:@"CustomerCancelledInTimeViewController" bundle:nil];
		[self.navController pushViewController:controller animated:NO];
	}
	else
	{
		[self.navController popToRootViewControllerAnimated:NO];
		
		if ([SettingsManager manager].cancelType == JobCancelTypeFree)
		{
			CustomerCancelledInTimeViewController *controller = [[CustomerCancelledInTimeViewController alloc] initWithNibName:@"CustomerCancelledInTimeViewController" bundle:nil];
			[self.navController pushViewController:controller animated:NO];
		}
		else if ([SettingsManager manager].cancelType == JobCancelTypeDuringJob)
		{
			CustomerCancelledViewController *controller = [[CustomerCancelledViewController alloc] initWithNibName:@"CustomerCancelledViewController" bundle:nil];
			[self.navController pushViewController:controller animated:NO];
		}
	}
}

- (void)clientRejectedQuote
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
    
	[SettingsManager manager].appState = AppStateQuoteRejected;
	[SettingsManager save];
	
	[self.navController popToRootViewControllerAnimated:NO];
	
	QuoteRejectedViewController *controller = [[QuoteRejectedViewController alloc] initWithNibName:@"QuoteRejectedViewController" bundle:nil];
	controller.reasonString = [SettingsManager manager].rejectReason;
	[self.navController pushViewController:controller animated:NO];
}

- (void)clientAcceptedQuote
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
	[SettingsManager manager].appState = AppStateJobInProgress;
	[SettingsManager save];
	
	[self gotoSupplierJobInProgressScreen];
}

- (void)gotoSupplierJobInProgressScreen
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
    [SettingsManager save];
    
	[self.navController popToRootViewControllerAnimated:NO];
	
	SupplierJobInProgressViewController *controller = [[SupplierJobInProgressViewController alloc] initWithNibName:@"SupplierJobInProgressViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)gotoAwaitingPaymentScreen
{
    [SettingsManager manager].supplierStatus = SupplierStatusBusy;
    [SettingsManager save];
    
	AwaitingPaymentViewController *controller = [[AwaitingPaymentViewController alloc] initWithNibName:@"BaseWhiteOverlayElipsesViewController" bundle:nil];
	[self.navController pushViewController:controller animated:NO];
}

- (void)clientCompletedPayment
{
	[SettingsManager manager].appState = AppStateJobComplete;
	[SettingsManager save];
	
	[self gotoJobCompleteScreen];
}

- (void)gotoJobCompleteScreen
{
    [SettingsManager manager].appState = AppStateJobComplete;
    [SettingsManager save];
    
//	[self.navController popToRootViewControllerAnimated:NO];
	
	JobCompleteBlackOverlayViewController *complete = [[JobCompleteBlackOverlayViewController alloc] initWithNibName:@"JobCompleteBlackOverlayViewController" bundle:nil];
	[self.navController pushViewController:complete animated:NO];
}

//#pragma mark - LOADER -
//- (void) showLoader
//{
//    [self.navController.navigationBar setUserInteractionEnabled:NO];
//    
//    if ([self.navController respondsToSelector:@selector(interactivePopGestureRecognizer)])
//    {
//        self.navController.interactivePopGestureRecognizer.enabled = NO;
//    }
//    
//    self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    self.HUD.opacity = 0.6;
//    self.HUD.detailsLabelFont = [UIFont fontWithName:FONT_BOLD size:14];
//    [self.HUD show:YES];
//}
//
//- (void) showLoaderWithMessage:(NSString*)message
//{
//    [self showLoader];
//    self.HUD.detailsLabelText = message;
//}
//
//- (void) hideLoader
//{
//    [self.navController.navigationBar setUserInteractionEnabled:YES];
//    
//    if ([self.navController respondsToSelector:@selector(interactivePopGestureRecognizer)])
//    {
//        self.navController.interactivePopGestureRecognizer.enabled = YES;
//    }
//    
//    [self.HUD hide:YES];
//    self.HUD.detailsLabelText = nil;
//}


@end
