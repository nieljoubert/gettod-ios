//
//  TaskHistoryCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "Job.h"

@interface TaskHistoryCell : GTBoldTableViewCell

- (void)setupCellWithTask:(Job*)job;

@end
