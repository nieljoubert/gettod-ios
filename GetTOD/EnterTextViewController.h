//
//  EnterTextViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"

@protocol EnterTextViewControllerDelegate;

@interface EnterTextViewController : RootViewController <UITextViewDelegate>

@property (nonatomic, strong) NSString *titleText;
@property (nonatomic, strong) NSString *textViewHintText;
@property (nonatomic, strong) NSString *leftButtonText;
@property (nonatomic, strong) NSString *rightButtonText;

@property (nonatomic, strong) NSString *existingText;

@property(nonatomic, weak) id <EnterTextViewControllerDelegate> delegate;

@end

@protocol EnterTextViewControllerDelegate <NSObject>

- (void)textEntered:(NSString*)text forController:(EnterTextViewController*)controller;
- (void)sendText:(NSString*)text forController:(EnterTextViewController*)controller;

@end
