//
//  EditEmailAddressViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/17.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "EditEmailAddressViewController.h"

@interface EditEmailAddressViewController ()

@property (nonatomic, strong) GTTextField *email;
@property (nonatomic, strong) GTTextField *password;
@property (nonatomic, strong) GTTextFieldCell *emailCell;
@property (nonatomic, strong) GTTextFieldCell *passwordCell;

@property (nonatomic, assign) bool didChangePassword;

@end

@implementation EditEmailAddressViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
	
	self.title = @"Edit Email Address";
	
	UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CELL_PADDING*2+10, 0, SCREEN_WIDTH-2*(CELL_PADDING*2+10), 50)];
	textLabel.font = [UIFont fontWithName:FONT size:12];
	textLabel.textColor = GRAY_COLOR;
	textLabel.numberOfLines = 0;
	textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.backgroundColor = [UIColor clearColor];
	textLabel.text = @"In the interest of your security, please enter your password to make this change.";
	
	UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, textLabel.frame.size.height)];
	[footerView addSubview:textLabel];
	
	self.tableView.tableFooterView = footerView;
	
	self.email = [GTTextField new];
	[self.email setKeyboardToolbar:self.keyboardToolbar];
	self.password = [GTTextField new];
	self.password.autocapitalizationType = UITextAutocapitalizationTypeNone;
	[self.password setKeyboardToolbar:self.keyboardToolbar];
	
	[self addFormField:self.email];
	[self addFormField:self.password];
}

- (void)goBack
{
	[super goBack];
}

#pragma mark - Validation -

- (BOOL)isScreenFieldsValid
{
	[super isScreenFieldsValid];
	
	BOOL isValid = YES;

	isValid =  [self isEmailValid] && isValid;
	isValid =  [self isPasswordValid] && isValid;

	return isValid;
}

- (BOOL)isEmailValid
{
	if ([Validation isValidEmail:self.email.text])
	{
		[self.emailCell dismissError];
		[self dismissScreenError:self.email];
		return YES;
	}
	else
	{
		[self.emailCell setError];
		[self setScreenError:self.email message:@"Please enter a valid email address"];
		return NO;
	}
}

- (BOOL)isPasswordValid
{
	if (self.password.text.length >= 8 && ![self.password.text isEqualToString:@"****"])
	{
		[self.passwordCell dismissError];
		[self dismissScreenError:self.password];
		return YES;
	}
	else
	{
		[self.passwordCell setError];
		[self setScreenError:self.password message:@"Please enter a password with more than 8 characters"];
		return NO;
	}
}

#pragma mark - Actions -
- (void)save
{
	if ([self isScreenFieldsValid])
	{
		[[APIManager manager] changeUsername:self.email.text
								withPassword:self.password.text
									 success:^(id result) {
										 
										 [SettingsManager manager].emailAddress = self.email.text;
										 [SettingsManager save];
										 
										 [self.navigationController popViewControllerAnimated:YES];
										 
									 } failure:^(NSError *error) {
										 
										 
									 }];
	}
	else
	{
		[self showScreenError];
	}
}

#pragma mark - Textfield -
- (void)textFieldDidEndEditing:(UITextField *)textField
{
	GTTextField *field = (GTTextField*)textField;
	
	if (field == self.email)
	{
		[self isEmailValid];
	}
	else if (field == self.password)
	{
		[self isPasswordValid];
	}
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		self.emailCell = cell;
		
		self.emailCell.textField = self.email;
		[self.emailCell setupCellWithPlaceholder:@"Email Address"];
		self.email.delegate = self;
		self.email.text = [SettingsManager manager].emailAddress;
		
		return self.emailCell;
	}
	
	else if (indexPath.section == 1)
	{
		GTTextFieldCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
		self.passwordCell = cell;
		
		self.passwordCell.textField = self.password;
		[self.passwordCell setupCellWithPlaceholder:@"Password"];
		self.password.secureTextEntry = YES;
		self.password.delegate = self;
		self.password.text = @"****";
		
		return self.passwordCell;
	}
	
	return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"YOUR EMAIL ADDRESS"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"PASSWORD"];
	}
	
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

@end
