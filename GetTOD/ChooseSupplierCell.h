//
//  ChooseSupplierCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "Supplier.h"

@interface ChooseSupplierCell : GTBoldTableViewCell

@property (nonatomic, strong) NSString *provider;
@property (nonatomic, strong) NSNumber *distanceValue;

- (void)setupCellWithSupplier:(Supplier*)supplier withDistance:(NSNumber*)distance;

@end
