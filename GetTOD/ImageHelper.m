//
//  ImageHelper.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageHelper.h"
#import "AFNetworking.h"
#import "SettingsManager.h"
#import "Constants.h"

@implementation ImageHelper


+ (UIImage*)downloadImage:(NSString*)fromURL withName:(NSString*)name
{
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:fromURL]];
	
	AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
	
	[requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
		[self saveImageToDisk:responseObject withName:name];
		[[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@-ImageDownloaded",name] object:responseObject];
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
	
	}];
	
	[requestOperation start];
	
	return nil;
}

+ (void)saveImageToDisk:(UIImage*)image withName:(NSString*)name;
{
	// Write to disk
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:name];
	NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
	[imageData writeToFile:savedImagePath atomically:NO];
}

+ (UIImage*)getImageFromDisk:(NSString*)name;
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:name];
	
	NSData *imageData = [NSData dataWithContentsOfFile:savedImagePath
											   options:NSDataReadingUncached
												 error:nil];
	
	if ([name isEqualToString:PROFILE_IMAGE_NAME] && imageData == nil)
	{
		return [UIImage imageNamed:@"default_profile_photo"];
	}
	
	return [UIImage imageWithData:imageData];
}

+ (void)deleteImageNamed:(NSString*)name;
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",name]];
	
	[fileManager removeItemAtPath:savedImagePath error:nil];
}

@end
