//
//  AlertviewManager.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AlertviewManager.h"
#import "UIAlertView+Blocks.h"

static AlertViewManager *sharedManager;

@interface AlertViewManager()

@property (nonatomic, strong) UIAlertView *alertView;

@end

@implementation AlertViewManager

+ (instancetype) createSharedManager
{
	if (sharedManager == nil)
	{
		sharedManager = [[AlertViewManager alloc] init];
	}
	return sharedManager;
}

+ (void)showAlertWithTitle:(NSString *)title
				   message:(NSString *)message
		 cancelButtonTitle:(NSString *)cancelButtonTitle
		 otherButtonTitles:(NSArray *)otherButtonTitles
	 buttonSelectionBlock:(AlertViewManagerCompletionBlock)buttonSelectionBlock
{
	[self createSharedManager];
	
	[self dismissAlert];
	
	sharedManager.alertView = [UIAlertView showWithTitle:title
												 message:message
												   style:UIAlertViewStyleDefault
									   cancelButtonTitle:cancelButtonTitle
									   otherButtonTitles:otherButtonTitles
												tapBlock:buttonSelectionBlock];
	
}

+ (void) dismissAlert
{	
	[self createSharedManager];
	
	if (sharedManager.alertView != nil)
	{
		sharedManager.alertView.delegate = nil;
		[sharedManager.alertView dismissWithClickedButtonIndex:0 animated:NO];
		sharedManager.alertView = nil;
	}
}


@end
