//
//  CacheManager.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"
#import "Supplier.h"
#import "Entity.h"

extern NSString *cacheKeyToken;
extern NSString *cacheKeyProvinces;
extern NSString *cacheKeySuppliers;
extern NSString *cacheKeyCompanies;
extern NSString *cacheKeyServices;
extern NSString *cacheKeyCompanyEntities;
extern NSString *cacheKeyCities;
extern NSString *cacheKeyCards;

@interface CacheManager : NSObject

+(void)cacheUserToken:(NSString*)token;
+(NSString*)tokenFromCache;

+(void)cacheProvinces:(NSArray*)data;
+(NSMutableArray*)provincesFromCache;
+(Province*)provinceFromCacheWithID:(int)ident;

+(void)cacheSuppliers:(NSArray*)data;
+(NSMutableArray*)suppliersFromCache;
+(NSMutableArray*)suppliersFromCacheForService:(NSNumber*)serviceId;
+(void)updateSupplier:(Supplier*)updatedSupplier;
+(Supplier*)supplierFromCacheWithID:(int)ident;

+(void)cacheCompanies:(NSArray*)data;
+(NSMutableArray*)companiesFromCache;
+(Company*)companyFromCacheWithID:(int)ident;

+(void)cacheServices:(NSArray*)data;
+(NSMutableArray*)servicesFromCache;
+(Service*)serviceFromCacheWithID:(int)ident;

+(void)cacheCompanyEntities:(NSArray*)data;
+(NSMutableArray*)companyEntitiesFromCache;
+(Entity*)entityFromCacheWithID:(int)ident;

+(void)cacheCities:(NSArray*)data;
+(NSMutableArray*)citiesFromCache;
+(City*)cityFromCacheWithID:(int)ident;

+(void)cacheCard:(Card *)card;
+(void)deleteCard:(Card *)card;
+(void)cacheCards:(NSArray*)data;
+(NSMutableArray*)cardsFromCache;


+(void)cacheRejectReasons:(NSArray*)data;
+(NSMutableArray*)rejectResonsFromCache;

@end
