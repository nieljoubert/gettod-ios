//
//  CostTableViewCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CostTableViewCell.h"
#import "GTTextField.h"

@interface CostTableViewCell()
@property (strong, nonatomic) UIImageView *divider;
@property (strong, nonatomic) UILabel *randLabel;
@end

@implementation CostTableViewCell

- (void)setupCell
{
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	self.selectionStyle = UITableViewCellSelectionStyleNone;
		
	if (self.randLabel == nil)
	{
		self.randLabel = [UILabel new];
		self.randLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.randLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:8];
		self.randLabel.textColor = GRAY_COLOR;
		[self.contentView addSubview:self.randLabel];
		
		self.randLabel.text = @"R";
	}
	
	if (self.amountField == nil)
	{
		self.amountField = [GTTextField new];
		self.amountField.placeholder = @"Price";
		self.amountField.delegate = self;
		self.amountField.keyboardType = UIKeyboardTypeDecimalPad;
		[self.contentView addSubview:self.amountField];
	}

	if (self.divider == nil)
	{
		self.divider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_pipe_card"]];
		self.divider.translatesAutoresizingMaskIntoConstraints = NO;
		self.divider.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.divider];
	}
	
	NSDictionary *views = @{@"rand":self.randLabel,
							@"div":self.divider,
							@"amount":self.amountField};
	
	NSDictionary *metrics = @{@"divW":[NSNumber numberWithFloat:self.divider.frame.size.width]};
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[rand(==10)]-5-[div(==divW)]-10-[amount]-15-|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[rand]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[div]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[amount]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
}

- (void)setError
{
	self.backgroundColor = ERROR_BACKGROUND_COLOR;
}

- (void)dismissError
{
	self.backgroundColor = [UIColor whiteColor];
}

@end
