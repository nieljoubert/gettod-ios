//
//  Entity.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Company.h"

@interface Entity : NSObject

@property (nonatomic, strong) NSNumber * entityId;
@property (nonatomic, strong) NSString * name;

@end
