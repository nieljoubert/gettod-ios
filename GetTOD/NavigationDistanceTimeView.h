//
//  NavigationDistanceTimeCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableViewCell.h"

@interface NavigationDistanceTimeView : UIView

- (void)setupViewWithTime:(NSString*)timeString andDistance:(NSString*)distanceString;

@property(nonatomic, weak) id delegate;

@end

@protocol NavigationDistanceTimeViewDelegate <NSObject>
- (void)mustGetCurrentLocationWithButtonTapped:(UIButton*)button;
@end
