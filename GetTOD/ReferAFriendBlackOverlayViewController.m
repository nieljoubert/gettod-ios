//
//  ReferAFriendBlackOverlayViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/02.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ReferAFriendBlackOverlayViewController.h"
#import "ServicesListViewController.h"

@interface ReferAFriendBlackOverlayViewController ()

@property (weak, nonatomic) IBOutlet UIButton *referButton;

@end

@implementation ReferAFriendBlackOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationController.navigationBar.hidden = YES;
	self.view.backgroundColor = [UIColor blackColor];
	
	self.referButton.layer.borderColor = [UIColor whiteColor].CGColor;
	self.referButton.layer.borderWidth = 1;
	self.referButton.layer.cornerRadius = self.referButton.frame.size.height/2;
	
	self.backgroundImageView.image = self.backgroundImage;
}

-(void)viewWillDisappear:(BOOL)animated
{
	self.navigationController.navigationBarHidden = NO;
	[super viewWillDisappear:animated];
}

- (IBAction)tellAFriendButtonTapped:(id)sender
{
	ShareViewController *shareController = [ShareViewController new];
	shareController.delegate = self;
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:shareController];
	[Flurry logAllPageViews:navigationController];
	[self.navigationController presentViewController:navigationController animated:YES completion:nil];
}


- (IBAction)closeButtonTapped:(id)sender
{
	self.navigationController.navigationBarHidden = NO;
	AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
	[appDelegate startAppWithServicesList];
}


#pragma mark - Delegates - 

-(void)finishedSharing:(ShareViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];

	AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
	[appDelegate startAppWithServicesList];
}

@end
