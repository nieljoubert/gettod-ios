//
//  GTTableViewCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableViewCell.h"

@implementation GTTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
	if (self)
	{
		self.textLabel.font = [UIFont fontWithName:FONT size:16];
		self.textLabel.textColor = DARK_TEXT_COLOR;
		self.selectionStyle = UITableViewCellSelectionStyleDefault;
		self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure"]];
	}
	
	return self;
}

@end
