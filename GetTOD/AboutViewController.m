//
//  AboutViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AboutViewController.h"
#import "WebViewController.h"

@interface AboutViewController ()

@property (weak, nonatomic) IBOutlet UILabel *websiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIView *deleteContainer;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewFromXib;

@end

@implementation AboutViewController

- (void)viewDidLoad
{
    self.tableView = self.tableViewFromXib;
    
	[super viewDidLoad];
	
	self.tableView.scrollEnabled = NO;
	
	self.title = @"About";
    
    self.tableView.tableFooterView = self.footerView;
  
    CGRect frame = self.footerView.frame;
    
    if (IS_IPHONE_4_OR_LESS)
    {
        frame.size.height = 160;
    }
    else
    {
        frame.size.height = 245;
    }
    
    self.footerView.frame = frame;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(gotoWebsite)];
    singleTap.numberOfTapsRequired = 1;
    [self.websiteLabel addGestureRecognizer:singleTap];
    
    UITapGestureRecognizer *deleteTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(deleteAccount)];
    deleteTap.numberOfTapsRequired = 1;
    [self.deleteContainer addGestureRecognizer:deleteTap];
    
    
}

- (void) viewWillDisappear:(BOOL) animated
{
	[super viewWillDisappear:animated];
	
	if ([self isMovingFromParentViewController])
	{
        if (self.navigationController)
        {
            self.navigationController.delegate = nil;
        }
	}
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

#pragma mark - Private -


#pragma mark - Actions -
- (void)gotoWebsite
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.gettod.com"]];
}

- (void)deleteAccount
{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Email Delegate -
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	controller.delegate = nil;
	
	if (error)
	{
		[AlertViewManager showAlertWithTitle:@""
									 message:@"There was a problem sending your email. Please try again."
						   cancelButtonTitle:@"OK"
						   otherButtonTitles:nil
						buttonSelectionBlock:nil];
	}
	
	if (result == MFMailComposeResultCancelled)
	{
		
	}
	else if (result == MFMailComposeResultSent)
	{
		[AlertViewManager showAlertWithTitle:@""
									 message:@"Your email has been sent. We will respond as soon as possible."
						   cancelButtonTitle:@"OK"
						   otherButtonTitles:nil
						buttonSelectionBlock:nil];
	}
	

	
	[controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0)
	{
		return 1;
	}
	else
	{
		return 3;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		if (indexPath.row == 0)
		{
			GTBoldTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"GTBoldTableViewCell"];
			cell.textLabel.text = @"1.03";
			cell.accessoryView = nil;
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			return cell;
		}
	}
	
	else if (indexPath.section == 1)
	{
		GTBoldTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"GTBoldTableViewCell"];
		
		if (indexPath.row == 0)
		{
			cell.textLabel.text = @"Privacy Policy";
		}
		else if (indexPath.row == 1)
		{
			cell.textLabel.text = @"Terms Of Services";
		}
		else if (indexPath.row == 2)
		{
			cell.textLabel.text = @"Support";
		}
		
		return cell;
	}
	
	return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0)
	{
		if (indexPath.row == 0)
		{
			// Version
		}
	}
	
	else if (indexPath.section == 1)
	{
		WebViewController *controller = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
		
		if (indexPath.row == 0 || indexPath.row == 1)
		{
			NSString *webTitle = @"";
			
			if (indexPath.row == 0)
			{
				webTitle = @"Privacy Policy";
			}
			else
			{
				webTitle = @"Terms Of Services";
			}
			
			controller.title = webTitle;
			controller.urlRequestToLoad = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.gettod.com/termsconditions.html"]];
			
			[self.navigationController pushViewController:controller animated:YES];
		}
		else if (indexPath.row == 2)
		{
			// Support Mailto
			MFMailComposeViewController *comp= [[MFMailComposeViewController alloc] init];
			[comp setMailComposeDelegate:self];
			
			if ([MFMailComposeViewController canSendMail])
			{
				[comp setToRecipients:[NSArray arrayWithObjects:@"support@gettod.com", nil]];
				[comp setSubject:@"GetTOD Support"];
//				[comp setMessageBody:@"Hello bro" isHTML:NO];
//				[comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
				[self.navigationController presentViewController:comp animated:YES completion:nil];
			}
			else
			{
				[AlertViewManager showAlertWithTitle:@""
											 message:@"Your phone has not been setup to send email"
								   cancelButtonTitle:@"OK"
								   otherButtonTitles:nil
								buttonSelectionBlock:nil];
			}
		}
	}

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 24;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	if (section == 0)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"VERSION"];
	}
	else if (section == 1)
	{
		return [[GTTableSectionHeaderView alloc] initWithTitle:@"LEGAL"];
	}
	
	return nil;
}

//- (UIView *)createTableFooterView
//{
//    if (self.websiteLabel == nil)
//    {
//        self.websiteLabel = [UILabel new];
//        self.websiteLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        self.websiteLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:13];
//        self.websiteLabel.textColor = PINK_COLOR;
//        self.websiteLabel.numberOfLines = 1;
//        self.websiteLabel.textAlignment = NSTextAlignmentCenter;
//        self.websiteLabel.userInteractionEnabled = YES;
//        self.websiteLabel.text = @"Visit Website";
//        
//        
//    }
//    
//    if (self.textLabel == nil)
//    {
//        self.textLabel = [UILabel new];
//        self.textLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        self.textLabel.font = [UIFont fontWithName:FONT size:12];
//        self.textLabel.textColor = GRAY_COLOR;
//        self.textLabel.numberOfLines = 1;
//        self.textLabel.textAlignment = NSTextAlignmentCenter;
//        self.textLabel.text = @"© 2015 getTOD. All rights reserved.";
//    }
//    
//    if (self.deleteContainer == nil)
//    {
//        self.deleteContainer = [UIView new];
//        self.deleteContainer.translatesAutoresizingMaskIntoConstraints = NO;
//        self.deleteContainer.userInteractionEnabled = YES;
//        
//        
//        
//        UILabel *deleteText = [UILabel new];
//        deleteText.translatesAutoresizingMaskIntoConstraints = NO;
//        deleteText.font = [UIFont fontWithName:FONT_SEMI_BOLD size:13];
//        deleteText.textColor = PINK_COLOR;
//        deleteText.numberOfLines = 1;
//        deleteText.textAlignment = NSTextAlignmentCenter;
//        deleteText.text = @"Delete Account";
//        
//        UIImageView *deleteImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"delete_icon"]];
//        deleteImage.translatesAutoresizingMaskIntoConstraints = NO;
//        deleteImage.contentMode = UIViewContentModeScaleAspectFit;
//        
//        [self.deleteContainer addSubview:deleteImage];
//        [self.deleteContainer addSubview:deleteText];
//        
//        NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"text": deleteText,
//                                                                                     @"image": deleteImage}];
//        
//        NSDictionary *metrics = @{@"imageW":[NSNumber numberWithFloat:[UIImage imageNamed:@"delete_icon"].size.width]};
//        
//        [self.deleteContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image(==imageW)]-5-[text]|"
//                                                                                     options:0
//                                                                                     metrics:metrics
//                                                                                       views:views]];
//        
//        [self.deleteContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|"
//                                                                                     options:0
//                                                                                     metrics:metrics
//                                                                                       views:views]];
//        
//        [self.deleteContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[text]|"
//                                                                                     options:0
//                                                                                     metrics:metrics
//                                                                                       views:views]];
//    }
//    
//    if (self.footerView == nil)
//    {
//        self.footerView = [UIView new];
//        self.footerView.translatesAutoresizingMaskIntoConstraints = NO;
//        [self.footerView addSubview:self.websiteLabel];
//        [self.footerView addSubview:self.textLabel];
//        [self.footerView addSubview:self.deleteContainer];
//    }
//    
//    
//    self.footerView.backgroundColor = [UIColor yellowColor];
//    self.deleteContainer.backgroundColor = [UIColor redColor];
//    self.websiteLabel.backgroundColor = [UIColor cyanColor];
//    self.textLabel.backgroundColor = [UIColor greenColor];
//    
//    NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"website": self.websiteLabel,
//                                                                                 @"copyright": self.textLabel,
//                                                                                 @"delete": self.deleteContainer}];
//    
//    NSDictionary *metrics = @{@"imageW":[NSNumber numberWithFloat:[UIImage imageNamed:@"delete_icon"].size.width]};
//    
//    
////    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.websiteLabel
////                                                                attribute:NSLayoutAttributeCenterX
////                                                                relatedBy:NSLayoutRelationEqual
////                                                                   toItem:self.footerView
////                                                                attribute:NSLayoutAttributeCenterX
////                                                               multiplier:1
////                                                                 constant:0]];
////    
////    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.textLabel
////                                                                attribute:NSLayoutAttributeCenterX
////                                                                relatedBy:NSLayoutRelationEqual
////                                                                   toItem:self.footerView
////                                                                attribute:NSLayoutAttributeCenterX
////                                                               multiplier:1
////                                                                 constant:0]];
////    
////    [self.footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.deleteContainer
////                                                                attribute:NSLayoutAttributeCenterX
////                                                                relatedBy:NSLayoutRelationEqual
////                                                                   toItem:self.footerView
////                                                                attribute:NSLayoutAttributeCenterX
////                                                               multiplier:1
////                                                                 constant:0]];
//    
////    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[website]|"
////                                                                            options:0
////                                                                            metrics:metrics
////                                                                              views:views]];
////    
////    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[copyright]|"
////                                                                            options:0
////                                                                            metrics:metrics
////                                                                              views:views]];
////    
////    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[delete]|"
////                                                                            options:0
////                                                                            metrics:metrics
////                                                                              views:views]];
////    
////    [self.footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[website]-5-[copyright]-(>=5)-[delete]|"
////                                                                            options:0
////                                                                            metrics:metrics
////                                                                              views:views]];
//    
//    return self.deleteContainer;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    
//    
//    if (section == 1)
//    {
//        return SCREEN_HEIGHT-240;
//    }
//    
//    return 1;
//}


@end
