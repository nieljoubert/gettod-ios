//
//  NoSuppliesViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/05/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "NoSuppliersViewController.h"
#import "LocationManager.h"

@interface NoSuppliersViewController ()
@property (strong, nonatomic) IBOutlet UILabel *notifyLabel;

@end

@implementation NoSuppliersViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.title = @"Choose Supplier";
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];
	
	self.notifyLabel.numberOfLines = 0;
	self.notifyLabel.attributedText = [[NSAttributedString alloc] initWithString:@"We can notify you when a supplier comes online, but hurry, first come, first serve." attributes:attributes];
}


- (IBAction)notifyButtonTapped:(id)sender
{
    [self showLoader];
    
	[[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
		
		[[APIManager manager] setReminderForServiceType:self.serviceTypeID
										   fromLocation:location.coordinate
												success:^(BOOL success) {
                                                    [self hideLoader];
                                                    [self.navigationController popViewControllerAnimated:YES];
												} failure:^(NSError *error) {
                                                    [self hideLoader];
												}];
	}];
}

- (IBAction)noThanksTapped:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

@end
