//
//  EnterTextViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "EnterTextViewController.h"

@interface EnterTextViewController ()

@property (nonatomic, strong) UITextView *textView;

@end

@implementation EnterTextViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.title = self.titleText;
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:self.rightButtonText style:UIBarButtonItemStyleBordered target:self action:@selector(rightButtonTapped)];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:self.leftButtonText style:UIBarButtonItemStyleBordered target:self action:@selector(lefttButtonTapped)];
	self.navigationItem.hidesBackButton = YES;
	
	self.textView = [[UITextView alloc] initWithFrame:self.view.bounds];
	self.textView.translatesAutoresizingMaskIntoConstraints = NO;
	self.textView.font = [UIFont fontWithName:FONT size:15];
	self.textView.backgroundColor = [UIColor whiteColor];
	self.textView.keyboardType = UIKeyboardTypeDefault;
	self.textView.delegate = self;
	
	if (self.existingText.length > 0)
	{
		self.textView.textColor = DARK_TEXT_COLOR;
		self.textView.text = self.existingText;
	}
	else
	{
		self.textView.textColor = [UIColor lightGrayColor];
		self.textView.text = self.textViewHintText;
	}
	
	[self.view addSubview:self.textView];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[text]-15-|"
																	  options:0
																	  metrics:nil
																		views:@{@"text":self.textView}]];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[text]|"
																	  options:0
																	  metrics:nil
																		views:@{@"text":self.textView}]];
	
}

- (void)rightButtonTapped
{
	if ([self.rightButtonText isEqualToString:@"Done"])
	{
		if (![self.textView.text isEqualToString:self.textViewHintText])
		{
			[self.delegate textEntered:self.textView.text forController:self];
		}
		[self dismissViewControllerAnimated:YES completion:nil];
	}
	else if ([self.rightButtonText isEqualToString:@"Send"])
	{
		[self.delegate sendText:self.textView.text forController:self];
	}
}

- (void)lefttButtonTapped
{
	if ([self.leftButtonText isEqualToString:@"Cancel"])
	{
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	if ([textView.text isEqualToString:self.textViewHintText])
	{
		textView.textColor = DARK_TEXT_COLOR;
		textView.text = @"";
	}
	
	return  YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
	if (textView.text.length == 0)
	{
		textView.textColor = [UIColor lightGrayColor];
		textView.text = self.textViewHintText;
	}
}

@end
