//
//  GTButton.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTButton.h"
#import "Constants.h"

@implementation GTButton

- (void)setupView
{
	self.backgroundColor = LIGHT_BUTTON_BACKGROUND_COLOR;
	[self setTitleColor:PINK_COLOR forState:UIControlStateNormal];

	self.titleLabel.font = [UIFont fontWithName:FONT size:18];
	self.layer.cornerRadius = 5;
}

@end
