//
//  ChooseSupplierCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ChooseSupplierCell.h"
#import "Company.h"

@interface ChooseSupplierCell()

@property (nonatomic, strong) UILabel *supplierName;
@property (nonatomic, strong) UILabel *company;
@property (nonatomic, strong) UILabel *rating;
@property (nonatomic, strong) UILabel *distance;

@property (nonatomic, strong) UIView *distanceView;
@property (nonatomic, strong) UIView *top;
@property (nonatomic, strong) UIView *bot;
@property (nonatomic, strong) UILabel *distanceIndicator;
@property (nonatomic, strong) UIView *ratingView;
@property (nonatomic, strong) UIView *ratingAndCompanyView;
@property (nonatomic, strong) UIView *nameAndStarView;

@property (nonatomic, strong) UIImageView *starImageView;

@end

@implementation ChooseSupplierCell

- (id)init
{
	self = [super init];
	
	if (self)
	{
	}
	
	return self;
}

- (void)setupCellWithSupplier:(Supplier*)supplier withDistance:(NSNumber*)distance
{
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	
	if (self.nameAndStarView == nil)
	{
		self.nameAndStarView = [UIView new];
		self.nameAndStarView.translatesAutoresizingMaskIntoConstraints = NO;
		[self.contentView addSubview:self.nameAndStarView];
	}
	
	if (self.supplierName == nil)
	{
		self.supplierName = [UILabel new];
		self.supplierName.translatesAutoresizingMaskIntoConstraints = NO;
		self.supplierName.font = [UIFont fontWithName:FONT_SEMI_BOLD size:16];
		self.supplierName.textColor = DARK_TEXT_COLOR;
		self.supplierName.numberOfLines = 1;
		self.supplierName.adjustsFontSizeToFitWidth = YES;
		[self.nameAndStarView addSubview:self.supplierName];
	}
	
	self.supplierName.text = supplier.user.name;
	
	if (self.starImageView == nil)
	{
		self.starImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"supplier_list_star"]];
		self.starImageView.translatesAutoresizingMaskIntoConstraints = NO;
		self.starImageView.contentMode = UIViewContentModeCenter;
		[self.nameAndStarView addSubview:self.starImageView];
	}
	
	self.starImageView.hidden = YES;
	
	if (self.company == nil)
	{
		self.company = [UILabel new];
		self.company.translatesAutoresizingMaskIntoConstraints = NO;
		self.company.font = [UIFont fontWithName:FONT size:11];
		self.company.textColor = GRAY_COLOR;
		self.company.numberOfLines = 1;
		self.company.lineBreakMode = NSLineBreakByTruncatingTail;
	}
	
	self.company.text = supplier.company.name;
	
	if (self.rating == nil)
	{
		self.rating = [UILabel new];
		self.rating.translatesAutoresizingMaskIntoConstraints = NO;
		self.rating.font = [UIFont fontWithName:FONT size:10];
		self.rating.textColor = DARK_TEXT_COLOR;
		self.rating.backgroundColor = [UIColor clearColor];
		self.rating.textAlignment = NSTextAlignmentCenter;
	}
	
	self.rating.text = [NSString stringWithFormat:@"%.1f",[supplier.rating floatValue]];
	
	if (self.ratingView == nil)
	{
		self.ratingView = [UIView new];
		self.ratingView.translatesAutoresizingMaskIntoConstraints = NO;
		self.ratingView.backgroundColor = GREEN_COLOR;
		self.ratingView.layer.cornerRadius = 3;
		self.ratingView.layer.masksToBounds = YES;
		[self.ratingView addSubview:self.rating];
	}
	
	if (self.ratingAndCompanyView == nil)
	{
		self.ratingAndCompanyView = [UIView new];
		self.ratingAndCompanyView.translatesAutoresizingMaskIntoConstraints = NO;
		self.ratingAndCompanyView.backgroundColor = [UIColor clearColor];
		[self.ratingAndCompanyView addSubview:self.ratingView];
		[self.ratingAndCompanyView addSubview:self.company];
		[self.contentView addSubview:self.ratingAndCompanyView];
	}
	
	if (self.distance == nil)
	{
		self.distance = [UILabel new];
		self.distance.translatesAutoresizingMaskIntoConstraints = NO;
		self.distance.font = [UIFont fontWithName:FONT size:12];
		self.distance.textColor = GRAY_COLOR;
		self.distance.backgroundColor = [UIColor clearColor];
	}
	
	if (distance)
	{
		self.distance.text = [NSString stringWithFormat:@"%@",distance];
	}
	else
	{
		self.distance.text = @"...";
	}
	
	if (self.distanceIndicator == nil)
	{
		self.distanceIndicator = [UILabel new];
		self.distanceIndicator.translatesAutoresizingMaskIntoConstraints = NO;
		self.distanceIndicator.font = [UIFont fontWithName:FONT size:8];
		self.distanceIndicator.textColor = GRAY_COLOR;
		self.distanceIndicator.backgroundColor = [UIColor clearColor];
		self.distanceIndicator.text = @"KM";
	}
	
	if (self.distanceView == nil)
	{
		self.distanceView = [UIView new];
		self.distanceView.translatesAutoresizingMaskIntoConstraints = NO;
		self.distanceView.backgroundColor = LIGHT_BACKGROUND_COLOR;
		self.distanceView.layer.cornerRadius = 12;
		self.distanceView.layer.masksToBounds = YES;
		[self.distanceView addSubview:self.distance];
		[self.distanceView addSubview:self.distanceIndicator];
		[self.contentView addSubview:self.distanceView];
	}
	
	if (self.top == nil)
	{
		self.top = [UIView new];
		self.top.translatesAutoresizingMaskIntoConstraints = NO;
		self.top.backgroundColor = [UIColor clearColor];
		[self.contentView addSubview:self.top];
	}
	
	if (self.bot == nil)
	{
		self.bot = [UIView new];
		self.bot.translatesAutoresizingMaskIntoConstraints = NO;
		self.bot.backgroundColor = [UIColor clearColor];
		[self.contentView addSubview:self.bot];
	}
	
	NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"name":self.supplierName,
																				 @"star":self.starImageView,
																				 @"nameStar":self.nameAndStarView,
																				 @"company":self.company,
																				 @"rating":self.rating,
																				 @"ratingView":self.ratingView,
																				 @"distance":self.distanceView,
																				 @"ratingComp":self.ratingAndCompanyView,
																				 @"dist":self.distance,
																				 @"km":self.distanceIndicator,
																				 @"top":self.top,
																				 @"bot":self.bot}];
	
	// Distance
	[self.distanceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[dist(==25)]|"
																		 options:0
																		 metrics:nil
																		   views:views]];
	
	[self.distanceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[km(==dist)]|"
																		 options:0
																		 metrics:nil
																		   views:views]];
	
	[self.distanceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[dist]-2-[km]-10-|"
																			  options:0
																			  metrics:nil
																				views:views]];
	
	// NameStar
	[self.nameAndStarView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[name(==star)]|"
																			  options:0
																			  metrics:nil
																				views:views]];

	[self.nameAndStarView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[star]|"
																			  options:0
																			  metrics:nil
																				   views:views]];
	
	[self.nameAndStarView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[name]-5-[star(starW)]|"
																			  options:0
																			  metrics:@{@"starW":[NSNumber numberWithFloat:[UIImage imageNamed:@"supplier_list_star"].size.width]}
																				views:views]];
	
	// Rating and Company
	[self.ratingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rating]|"
																			options:0
																			metrics:nil
																			  views:views]];
	
	[self.ratingView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[rating]|"
																			options:0
																			metrics:nil
																			  views:views]];
	
	
	[self.ratingAndCompanyView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ratingView(==25)]-5-[company]|"
																					  options:0
																					  metrics:nil
																						views:views]];
	
	[self.ratingAndCompanyView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ratingView(==17)]|"
																					  options:0
																					  metrics:nil
																						views:views]];
	
	[self.ratingAndCompanyView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[company(14)]|"
																					  options:0
																					  metrics:nil
																						views:views]];
	
	// Content
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[nameStar]-(>=20)-[distance]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[ratingComp]-(>=20)-[distance]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:views]];

	[self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.distanceView
																  attribute:NSLayoutAttributeCenterY
																  relatedBy:NSLayoutRelationEqual
																	 toItem:self.contentView
																  attribute:NSLayoutAttributeCenterY
																 multiplier:1
																   constant:0]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(<=25)-[nameStar]-(5)-[ratingComp]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:views]];
}


@end
