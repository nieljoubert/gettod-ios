//
//  CreatingAccountBlackOverlayViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CreatingAccountBlackOverlayViewController.h"
#import "MONActivityIndicatorView.h"
#import "ImageHelper.h"

@interface CreatingAccountBlackOverlayViewController () <MONActivityIndicatorViewDelegate>
@property (strong, nonatomic) IBOutlet MONActivityIndicatorView *indicatorView;
@end

@implementation CreatingAccountBlackOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self registerUser];

	self.indicatorView.delegate = self;
	self.indicatorView.numberOfCircles = 3;
	self.indicatorView.radius = 2.5;
	self.indicatorView.internalSpacing = 5;
	self.indicatorView.duration = 1.25;
	self.indicatorView.delay = 0.5;
	
	[self.indicatorView startAnimating];
}

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
	  circleBackgroundColorAtIndex:(NSUInteger)index {
	return [UIColor whiteColor];
}

- (void)registerUser
{
	[[APIManager manager] registerWithUsername:[SettingsManager manager].username
										 email:[SettingsManager manager].emailAddress
								   andPassword:[SettingsManager manager].password
									   success:^(id result) {

                                           if (self.card)
                                           {
                                               [[APIManager manager] registerCreditCard:self.card
                                                                                success:^(id result) {
                                                                                    
                                                                                    [self registerForPushNotifications];
                                                                                    
                                                                                } failure:^(NSError *error) {
                                                                                    [self.delegate dismissViewController:self success:NO onCard:YES];
                                                                                }];
                                           }
                                           else
                                           {
                                               [self registerForPushNotifications];
                                           }

									   } failure:^(NSError *error) {
                                           [self.delegate dismissViewController:self success:NO onCard:NO];
									   }];
}

- (void)registerForPushNotifications
{
#if	TARGET_IPHONE_SIMULATOR
	[self navigateToHomeScreen];
#else
	[[APIManager manager] registerForPushNotifications:^(BOOL success) {
		
		[self navigateToHomeScreen];
		
	} failure:^(NSError *error) {
		
	}];
#endif
}

- (void)navigateToHomeScreen
{
	[[APIManager manager] updateUserDetails:[SettingsManager manager].firstName
									surname:[SettingsManager manager].lastName
							 profilePicture:[ImageHelper getImageFromDisk:PROFILE_IMAGE_NAME]
							 andPhoneNumber:[SettingsManager manager].phoneNumber
									success:^(id result) {
										
										[SettingsManager manager].profileImageURL = result;
										[SettingsManager manager].gotUserDetailsOnce = YES;
										[SettingsManager save];
										
										[self.delegate dismissViewController:self success:YES onCard:NO];
										
									} failure:^(NSError *error) {
										
										[self.delegate dismissViewController:self success:NO onCard:NO];
										
									}];
}

@end
