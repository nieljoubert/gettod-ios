//
//  ArrivedViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ArrivedViewController.h"
#import "SupplierQuoteViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface ArrivedViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivalInstructionsLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *problemLabel;

@end

@implementation ArrivedViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.title = @"You have arrived";
	self.navigationItem.hidesBackButton = YES;
	self.navigationItem.leftBarButtonItem = nil;
	
	self.arrivalInstructionsLabel.text = [SettingsManager manager].currentJob.additionalInstructions;
	self.ratingLabel.text = [NSString stringWithFormat:@"%.1f",[[SettingsManager manager].currentJob.customer.rating floatValue]];
	self.customerNameLabel.text = [SettingsManager manager].currentJob.customer.name;
	self.problemLabel.text = [SettingsManager manager].currentJob.problem.problemDescription;
	
	__weak ArrivedViewController *weakSelf = self;
	
	[self.profileImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEURLSTRING,[SettingsManager manager].currentJob.customer.profilePictureURL]]]
								 placeholderImage:[UIImage imageNamed:@"default_profile_photo"]
									   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
										   
										   weakSelf.profileImageView.image = image;
										   weakSelf.profileImageView.layer.cornerRadius = weakSelf.profileImageView.frame.size.width/2;
										   weakSelf.profileImageView.clipsToBounds = YES;
										   
									   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
										   
										   
									   }];
	
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[SettingsManager manager].currentJob.jobLocationCoordinates.latitude
															longitude:[SettingsManager manager].currentJob.jobLocationCoordinates.longitude
																 zoom:16];
	
	GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height) camera:camera];
	mapView.backgroundColor = [UIColor clearColor];
	mapView.mapType = kGMSTypeNormal;
	
	mapView.accessibilityElementsHidden = NO;
	mapView.settings.tiltGestures = NO;
	mapView.settings.scrollGestures = NO;
	mapView.settings.rotateGestures = YES;
	mapView.settings.zoomGestures = YES;
	mapView.settings.compassButton = NO;
	mapView.settings.myLocationButton = NO;
	[self.mapView addSubview:mapView];
	
	GMSMarker *marker = [GMSMarker markerWithPosition:[SettingsManager manager].currentJob.jobLocationCoordinates];
	marker.icon = [UIImage imageNamed:@"location_marker_gray_large"];
	marker.appearAnimation = kGMSMarkerAnimationPop;
	marker.flat = YES;
	marker.map = mapView;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

#pragma mark - Actions -
- (IBAction)quoteCustomer:(id)sender
{
	[SettingsManager manager].appState = AppStateQuote;
	[SettingsManager save];
	
	SupplierQuoteViewController *quote = [[SupplierQuoteViewController alloc] initWithNibName:@"SupplierQuoteViewController" bundle:nil];
	[self.navigationController pushViewController:quote animated:YES];
}

- (IBAction)callCustomer:(id)sender
{
	NSString *cleanedString = [[[SettingsManager manager].currentJob.customer.phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
	
	// Prompt but return
	NSURL *phoneNumberURL = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:cleanedString]];
	
	// Direct call
	NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
	
	[[UIApplication sharedApplication] openURL:phoneNumberURL];
	
}
@end
