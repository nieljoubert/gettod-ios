//
//  RequestingSupplierBlackOverlayViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseBlackOverlayViewController.h"

@interface RequestingSupplierBlackOverlayViewController : RootViewController //BaseBlackOverlayViewController
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) UIImage *backgroundImage;
@end
