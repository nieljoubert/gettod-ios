//
//  LoginTextFieldCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "LoginTextFieldCell.h"

@implementation LoginTextFieldCell

- (void)setupCellWithTextFieldView:(LoginTextFieldView*)textfieldView
{
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	
	self.textField = textfieldView.textField;
	
	[self.contentView addSubview:textfieldView];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(padding)-[text]-(padding)-|"
																			 options:0
																			 metrics:@{@"padding":@CELL_PADDING}
																			   views:@{@"text":textfieldView}]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[text]-4-|"
																			 options:0
																			 metrics:nil
																			   views:@{@"text":textfieldView}]];
}

- (void)setErrorTo:(NSString*)text
{
	self.backgroundColor = ERROR_BACKGROUND_COLOR;
	[self.textField setErrorTo:text];
}

- (void)dismissError
{
	[self.textField dismissError];
	self.backgroundColor = [UIColor whiteColor];
}

@end
