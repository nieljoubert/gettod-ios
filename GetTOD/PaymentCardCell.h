//
//  PaymentCardCell.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "GTTextField.h"
#import "Card.h"

@interface PaymentCardCell : GTBoldTableViewCell

@property (nonatomic,strong) UILabel *cardNumber;
@property (nonatomic,strong) UILabel *nameLabel;

- (void)setupCellWithCard:(Card*)card forScreen:(BOOL)forScreen;
- (void)getCardLogo:(NSString*)number;

@end
