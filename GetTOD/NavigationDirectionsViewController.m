//
//  NavigationDirectionsViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "NavigationDirectionsViewController.h"
#import "DirectionsDetailView.h"
#import "LPGoogleFunctions.h"
#import "GTDirection.h"
#import "DirectionCollectionViewCell.h"
#import "LocationManager.h"
#import "SupplierQuoteViewController.h"
#import "ArrivedViewController.h"

@interface NavigationDirectionsViewController ()

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UICollectionView *directionsCollectionView;

@property (nonatomic, retain) UIActivityIndicatorView *loader;
@property (nonatomic, strong) GMSMarker *currentStartPointMarker;
@property (nonatomic, strong) GMSMarker *customerLocationMarker;
@property (nonatomic, strong) GMSMutablePath *currentPath;

@property (nonatomic, strong) NSMutableArray *directionsInfo;
@property (nonatomic, strong) NSMutableArray *stepRegions;

@property (nonatomic, strong) AVSpeechSynthesizer *synth;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *flippingView;

@property (nonatomic, assign) BOOL flip;
@property (nonatomic, assign) BOOL shouldAnimateToCurrentLocation;
@property (nonatomic, strong) UIButton *rerouteButton;

@end

@implementation NavigationDirectionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.distanceTimeView.delegate = self;
    
    [self.directionsCollectionView registerClass:[DirectionCollectionViewCell class] forCellWithReuseIdentifier:@"DirectionCell"];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(flipView:)];
    
    [self.addressView addGestureRecognizer:singleTap];
    [self.directionsCollectionView addGestureRecognizer:singleTap];
    
    self.addressView.tag = 99;
    self.directionsCollectionView.tag = 100;
    self.flip = NO;
    
    self.addressLabel.textColor = DARK_TEXT_COLOR;
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animateMap) name:@"LocationUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteredRegion:) name:@"EnteredRegion" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotateMap:) name:@"BearingUpdated" object:nil];
    
    [[LocationManager manager] setDistanceFilter:20];
   	[[LocationManager manager] startConstantUpdating];
    self.shouldAnimateToCurrentLocation = YES;
    
    [self getDirectionsAndUpdatePath:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[LocationManager manager] resetDistanceFilter];
}


#pragma mark - Private -

- (void)flipView:(UIGestureRecognizer *)singleTap
{
    UIView *view = singleTap.view;
    
    [UIView transitionWithView:self.flippingView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        if (!self.flip) {
                            [self.flippingView sendSubviewToBack:self.directionsCollectionView];
                            [self.flippingView bringSubviewToFront:self.addressView];
                            [self.addressView addGestureRecognizer:singleTap];
                            
                            self.flip = YES;
                        } else {
                            [self.flippingView sendSubviewToBack:self.addressView];
                            [self.flippingView bringSubviewToFront:self.directionsCollectionView];
                            [self.directionsCollectionView addGestureRecognizer:singleTap];
                            
                            self.flip = NO;
                        }
                    } completion:nil];
}

- (void)setupMap:(CLLocationCoordinate2D)coords
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coords.latitude
                                                            longitude:coords.longitude
                                                                 zoom:16];
    self.mapView.backgroundColor = [UIColor clearColor];
    self.mapView.camera = camera;
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.accessibilityElementsHidden = NO;
    self.mapView.settings.tiltGestures = YES;
    self.mapView.settings.scrollGestures = YES;
    self.mapView.settings.rotateGestures = YES;
    self.mapView.settings.zoomGestures = YES;
    self.mapView.settings.compassButton = NO;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    
    [self.mapView animateToLocation:coords];
}

- (void)drawPath
{
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:self.currentPath];
    polyline.strokeWidth = 5;
    polyline.strokeColor = TIMING_BLUE_COLOR;
    polyline.map = self.mapView;
}

- (void)addMarkerToLocation:(CLLocationCoordinate2D)coordinates withAnimation:(BOOL)animate
{
    [self.mapView clear];
    
    self.customerLocationMarker = [GMSMarker markerWithPosition:coordinates];
    self.customerLocationMarker.icon = [UIImage imageNamed:@"location_marker_gray_large"];
    
    self.customerLocationMarker.flat = NO;
    self.customerLocationMarker.map = self.mapView;
    
    if (animate)
    {
        self.customerLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
        GMSCameraUpdate *camera = [GMSCameraUpdate setTarget:coordinates];
        [self.mapView animateWithCameraUpdate:camera];
    }
}

- (void)animateMap
{
    if (self.shouldAnimateToCurrentLocation)
    {
//        __block NavigationDirectionsViewController *weakSelf = self;
//        
//        [[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
    
//            [AlertViewManager showAlertWithTitle:@"Animate to"
//                                         message:[NSString stringWithFormat:@"%f\n%f",[[LocationManager manager] getCurrentLocationValue].coordinate.latitude,[[LocationManager manager] getCurrentLocationValue].coordinate.longitude]
//                               cancelButtonTitle:@"OK"
//                               otherButtonTitles:nil
//                            buttonSelectionBlock:nil];
        
            GMSCameraUpdate *camera = [GMSCameraUpdate setTarget:[[LocationManager manager] getCurrentLocationValue].coordinate];
            [self.mapView animateWithCameraUpdate:camera];
            NSLog(@"%@",[[LocationManager manager] getCurrentLocationValue]);
//        }];
    }
    //    [self getDirections];
    
}

- (void)addCircleForRegion:(CLCircularRegion*)region
{
    GMSCircle *circ = [GMSCircle circleWithPosition:region.center
                                             radius:region.radius];
    
    circ.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.05];
    circ.strokeColor = [UIColor redColor];
    circ.strokeWidth = 5;
    circ.map = self.mapView;
}

- (void) getDirectionsAndUpdatePath:(BOOL)update
{
//    [AlertViewManager showAlertWithTitle:@"Directions"
//                                 message:[NSString stringWithFormat:@"%f\n%f",[SettingsManager manager].currentLocationCoords.latitude,[SettingsManager manager].currentLocationCoords.longitude]
//                       cancelButtonTitle:@"OK"
//                       otherButtonTitles:nil
//                    buttonSelectionBlock:nil];
    LPGoogleFunctions *mapsThing = [[LPGoogleFunctions alloc] init];
    
    [mapsThing loadDirectionsForOrigin:[LPLocation locationWithLatitude:[SettingsManager manager].currentLocationCL.coordinate.latitude
                                                              longitude:[SettingsManager manager].currentLocationCL.coordinate.longitude]
                        forDestination:[LPLocation locationWithLatitude:[SettingsManager manager].currentJob.jobLocationCoordinates.latitude
                                                              longitude:[SettingsManager manager].currentJob.jobLocationCoordinates.longitude]
                  directionsTravelMode:LPGoogleDirectionsTravelModeDriving
                  directionsAvoidTolls:LPGoogleDirectionsAvoidNone
                        directionsUnit:LPGoogleDirectionsUnitMetric
                directionsAlternatives:NO
                         departureTime:[NSDate date]
                           arrivalTime:nil
                             waypoints:nil
                       successfulBlock:^(LPDirections *directions) {
                           
                           if (update)
                           {
                               self.directions = directions;
                               [self handleDirectionData];
                               [[LocationManager manager] startConstantUpdating];
                           }
                           else
                           {
                               LPRoute *route = [self.directions.routes objectAtIndex:0];
                               
                               for (LPLeg *leg in route.legs)
                               {
                                   [self.distanceTimeView setupViewWithTime:leg.duration.text andDistance:leg.distance.text];
                               }
                           }
                           
                       } failureBlock:^(LPGoogleStatus status) {
                           [self hideLoader];
                           [[LocationManager manager] startConstantUpdating];
                           [self.rerouteButton setEnabled:YES];
                       }];
}

- (void)handleDirectionData
{
    [self addMarkerToLocation:[SettingsManager manager].currentJob.jobLocationCoordinates withAnimation:YES];
    [self setupMap:self.supplierLocationCoordinates];
    
    [[LocationManager manager] removeAllRegions];
    
    [[LocationManager manager] addRegion:[[CLCircularRegion alloc] initWithCenter:[SettingsManager manager].currentJob.jobLocationCoordinates radius:25 identifier:@"JobLocation"]];
    
    self.directionsInfo = [NSMutableArray array];
    
    LPRoute *route = [self.directions.routes objectAtIndex:0];
    
    self.currentPath = [GMSMutablePath path];
    
    for (LPLocation *loc in route.overviewPolyline.pointsArray)
    {
        [self.currentPath addCoordinate:CLLocationCoordinate2DMake(loc.latitude,loc.longitude)];
    }
    
    int stepNumber = 0 ;
    self.stepRegions = [NSMutableArray array];
    
    for (LPLeg *leg in route.legs)
    {
        [self.distanceTimeView setupViewWithTime:leg.duration.text andDistance:leg.distance.text];
        NSArray *strings = [leg.endAddress componentsSeparatedByString:@","];
        self.addressLabel.text = [NSString stringWithFormat:@"%@,\n%@",[strings objectAtIndex:0],[strings objectAtIndex:1]];
        
        for (int s=0 ; s < leg.steps.count; s++)//LPStep *step in leg.steps)
        {
            LPStep *step = [leg.steps objectAtIndex:s];
            NSString *finalString = step.htmlInstructions;
            
            NSRange startDiv = [finalString rangeOfString:@"<div "];
            
            if (startDiv.location != NSNotFound)
            {
                finalString = [finalString substringToIndex:startDiv.location];
            }
            
            // Get bold placeholder values
            NSMutableArray *placeHolderValues = [[NSMutableArray alloc] init];
            
            NSRange start = [finalString rangeOfString:@"<b>"];
            NSRange end = [finalString rangeOfString:@"</b>"];
            NSString *boldText = @"";
            
            bool hasBold = YES;
            
            while (hasBold)
            {
                start = [finalString rangeOfString:@"<b>"];
                end = [finalString rangeOfString:@"</b>"];
                
                if (start.location != NSNotFound && end.location != NSNotFound && end.location > start.location)
                {
                    hasBold = YES;
                    
                    // save value
                    boldText = [finalString substringWithRange:NSMakeRange(start.location+3, end.location-(start.location+3))];
                    [placeHolderValues addObject:boldText];
                    
                    // remove <b> and </b>
                    finalString = [finalString stringByReplacingCharactersInRange:start withString:@""];
                    end = [finalString rangeOfString:@"</b>"];
                    finalString = [finalString stringByReplacingCharactersInRange:end withString:@""];
                }
                else
                {
                    hasBold = NO;
                }
            }
            
            NSString *direction;
            NSString *street;
            
            if (placeHolderValues.count > 1)
            {
                direction = [finalString substringToIndex:[finalString rangeOfString:[placeHolderValues objectAtIndex:1]].location];
                street = [placeHolderValues objectAtIndex:1];
                
                NSLog(@"\n%@ ->\n---\n%@\n%@\n---\n",step.maneuver,direction,street);
            }
            else if (placeHolderValues.count > 0)
            {
                direction = [finalString substringToIndex:[finalString rangeOfString:[placeHolderValues objectAtIndex:0]].location];
                street = [placeHolderValues objectAtIndex:0];
                
                NSLog(@"\n%@ ->\n---\n%@\n%@\n---\n",step.maneuver,direction,street);
            }
            else
            {
                direction = finalString;
                street = @"";
                NSLog(@"\n%@ ->\n---\nNONE\n%@\n---\n",step.maneuver,finalString);
            }
            
            GTDirection *directionObject = [GTDirection new];
            directionObject.directionDescription = direction;
            directionObject.streetName = street;
            directionObject.distance = [NSNumber numberWithInt:step.distance.value];
            directionObject.distanceStringWithKM = step.distance.text;
            directionObject.startLat = [NSNumber numberWithDouble:step.startLocation.latitude];
            directionObject.startLng = [NSNumber numberWithDouble:step.startLocation.longitude];
            directionObject.endLat = [NSNumber numberWithDouble:step.endLocation.latitude];
            directionObject.endLng = [NSNumber numberWithDouble:step.endLocation.longitude];
            
            CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:directionObject.startLocation
                                                                         radius:50
                                                                     identifier:[NSString stringWithFormat:@"Step-%d",stepNumber]];
            
            [self.stepRegions addObject:region];
            stepNumber++;
            [[LocationManager manager] addRegion:region];
            
            if (s == 0)
            {
                [self.mapView animateToLocation:[SettingsManager manager].currentLocationCoords];
            }
            
            if ([step.maneuver isEqualToString:@"turn-sharp-left"])
            {
                directionObject.direction = NavigationDirectionTurnSharpLeft;
            }
            else if ([step.maneuver isEqualToString:@"uturn-right"])
            {
                directionObject.direction = NavigationDirectionUTurnRight;
            }
            else if ([step.maneuver isEqualToString:@"turn-slight-right"])
            {
                directionObject.direction = NavigationDirectionTurnSlightRight;
            }
            else if ([step.maneuver isEqualToString:@"merge"])
            {
                directionObject.direction = NavigationDirectionTurnSharpLeft;
            }
            else if ([step.maneuver isEqualToString:@"roundabout-left"])
            {
                directionObject.direction = NavigationDirectionRoundaboutLeft;
            }
            else if ([step.maneuver isEqualToString:@"roundabout-right"])
            {
                directionObject.direction = NavigationDirectionRoundaboutRight;
            }
            else if ([step.maneuver isEqualToString:@"uturn-left"])
            {
                directionObject.direction = NavigationDirectionUTurnLeft;
            }
            else if ([step.maneuver isEqualToString:@"turn-slight-left"])
            {
                directionObject.direction = NavigationDirectionTurnSlightLeft;
            }
            else if ([step.maneuver isEqualToString:@"turn-left"])
            {
                directionObject.direction = NavigationDirectionTurnLeft;
            }
            else if ([step.maneuver isEqualToString:@"ramp-right"])
            {
                directionObject.direction = NavigationDirectionRampRight;
            }
            else if ([step.maneuver isEqualToString:@"turn-right"])
            {
                directionObject.direction = NavigationDirectionTurnRight;
            }
            else if ([step.maneuver isEqualToString:@"fork-right"])
            {
                directionObject.direction = NavigationDirectionForkRight;
            }
            else if ([step.maneuver isEqualToString:@"straight"])
            {
                directionObject.direction = NavigationDirectionStraight;
            }
            else if ([step.maneuver isEqualToString:@"fork-left"])
            {
                directionObject.direction = NavigationDirectionForkLeft;
            }
            else if ([step.maneuver isEqualToString:@"turn-sharp-right"])
            {
                directionObject.direction = NavigationDirectionTurnSharpRight;
            }
            else if ([step.maneuver isEqualToString:@"ramp-left"])
            {
                directionObject.direction = NavigationDirectionRampLeft;
            }
            else
            {
                directionObject.direction = -1;
                NSLog(@"NOT-FOUND: %@",step.maneuver);
            }
            
            [self.directionsInfo addObject:directionObject];
        }
    }
    
    GTDirection *directionObject = [GTDirection new];
    directionObject.directionDescription = @"You have arrrived at";
    directionObject.streetName = [SettingsManager manager].currentJobAddress;//@"Your destination";
    directionObject.distance = @0;
    directionObject.distanceStringWithKM = @"0m";
    directionObject.direction = NavigationDirectionEnd;
    
    [self.directionsInfo addObject:directionObject];
    [self.directionsCollectionView reloadData];
    [self.directionsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    
    [self voiceOverForDirection:[self.directionsInfo firstObject]];
    
    [self.rerouteButton setEnabled:YES];
    [self drawPath];
}

- (void)showDirectionsLoader
{
    self.loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    self.loader.center = CGPointMake(self.directionsCollectionView.center.x, self.directionsCollectionView.center.y + 10);
    [self.loader startAnimating];
    [self.view addSubview:self.loader];
    
    self.directionsCollectionView.hidden = YES;
}

- (void)hideDirectionsLoader
{
    [self.loader stopAnimating];
    [self.loader removeFromSuperview];
    
    self.directionsCollectionView.hidden = NO;
}

- (void)enteredRegion:(NSNotification*)regionNotification
{
    CLCircularRegion *region = regionNotification.object;
    
    [self getDirectionsAndUpdatePath:NO];
    
    if ([region.identifier isEqualToString:@"JobLocation"])
    {
        [self supplierArrived];
        [[LocationManager manager] removeAllRegions];
    }
    else
    {
        int stepNumber = [[region.identifier substringFromIndex:5] intValue];
        
        [self.directionsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:stepNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    }
}

- (void)voiceOverForDirection:(GTDirection*)direction
{
    //	NSString *directionString = @"";
    //
    //	switch (direction.direction) {
    //		case NavigationDirectionTurnSharpLeft:
    //			directionString = @"Take a sharp left turn";
    //			break;
    //		case NavigationDirectionTurnSharpRight:
    //			directionString = @"Take a sharp right turn";
    //			break;
    //		case NavigationDirectionTurnLeft:
    //			directionString = @"Turn left";
    //			break;
    //		case NavigationDirectionTurnRight:
    //			directionString = @"Turn right";
    //			break;
    //		case NavigationDirectionTurnSlightLeft:
    //			directionString = @"Take a slight left";
    //			break;
    //		case NavigationDirectionTurnSlightRight:
    //			directionString = @"Take a slight right";
    //			break;
    //		case NavigationDirectionUTurnLeft:
    //			directionString = @"Make a left u-turn";
    //			break;
    //		case NavigationDirectionUTurnRight:
    //			directionString = @"Make a right u-turn";
    //			break;
    //		case NavigationDirectionMerge:
    //			directionString = @"Merge";
    //			break;
    //		case NavigationDirectionEnd:
    //			directionString = @"You have reached your destination";
    //			break;
    //		case NavigationDirectionForkLeft:
    //			directionString = @"Take the left fork";
    //			break;
    //		case NavigationDirectionKeepLeft:
    //			directionString = @"Keep left";
    //			break;
    //		case NavigationDirectionRampLeft:
    //			directionString = @"Take the ramp to the left";
    //			break;
    //		case NavigationDirectionStraight:
    //			directionString = @"Continue straight";
    //			break;
    //		case NavigationDirectionForkRight:
    //			directionString = @"Take the right fork";
    //			break;
    //		case NavigationDirectionKeepRight:
    //			directionString = @"Keep right";
    //			break;
    //		case NavigationDirectionRampRight:
    //			directionString = @"Take the ramp to the right";
    //			break;
    //		case NavigationDirectionRoundaboutLeft:
    //			directionString = @"Exit the roundabout to the left";
    //			break;
    //		case NavigationDirectionRoundaboutRight:
    //			directionString = @"Exit the roundabout to the right";
    //			break;
    //		case NavigationDirectionRoundaboutStraight:
    //			directionString = @"Exit the roundabout straight";
    //			break;
    //
    //		default:
    //			break;
    //	}
    
    //33.889177, 18.511322
    
    NSString *distanceString = direction.distanceStringWithKM;
    NSString *directionDescription = direction.directionDescription;
    NSString *streetName = direction.streetName;
    
    if ([streetName containsString:@"Dr"])
    {
        streetName = [streetName stringByReplacingOccurrencesOfString:@"Dr" withString:@"drive"];
    }
    else if ([streetName containsString:@"Rd"])
    {
        streetName = [streetName stringByReplacingOccurrencesOfString:@"Rd" withString:@"road"];
    }
    else if ([streetName containsString:@"St"])
    {
        streetName = [streetName stringByReplacingOccurrencesOfString:@"St" withString:@"street"];
    }
    else if ([streetName localizedCaseInsensitiveContainsString:@"Cres"])
    {
        streetName = [streetName stringByReplacingOccurrencesOfString:@"Cres" withString:@"crescent"];
    }
    else if ([streetName localizedCaseInsensitiveContainsString:@"Cl"])
    {
        streetName = [streetName stringByReplacingOccurrencesOfString:@"Cl" withString:@"close"];
    }
    
    AVSpeechUtterance *utter = [[AVSpeechUtterance alloc] initWithString:[NSString stringWithFormat:@"In %@, %@ %@",distanceString,directionDescription,streetName]];
    
    
    //    for (AVSpeechSynthesisVoice *voice in [AVSpeechSynthesisVoice speechVoices]) {
    //        NSLog(@" %@", voice.language);
    //    }
    //    
    //    AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc] init];
    //    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:@"Localized -text"];
    //    utterance.rate = AVSpeechUtteranceMinimumSpeechRate; // Tell it to me slowly
    //    [synthesizer speakUtterance:utterance];
    
//    utter.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-ZA"];
//    
//    if (utter.voice == nil)
//    {
//        utter.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"GB"];
//    }
    
    if ([SettingsManager manager].isiOS8)
    {
        utter.rate = .15;
    }
    else
    {
        utter.rate = .52;
    }
    
    if (self.synth == nil)
    {
        self.synth = [[AVSpeechSynthesizer alloc] init];
    }
    
    [self.synth stopSpeakingAtBoundary:AVSpeechBoundaryWord];
    [self.synth speakUtterance:utter];
}

- (void)rotateMap:(NSNotification*)rotateNotification
{
    CLHeading *heading = rotateNotification.object;
    
    float trueHeading = heading.trueHeading;
    float headingDegrees = (trueHeading*M_PI/180);
    
    [self.mapView animateToBearing:headingDegrees];
}

- (void)supplierArrived
{
    [self showLoader];
    
    [[APIManager manager] supplierArrivedForJob:[SettingsManager manager].currentJob.jobId
                                        success:^(id result) {
                                            [self hideLoader];
                                            
                                            [SettingsManager manager].appState = AppStateArrived;
                                            [SettingsManager save];
                                            
                                            ArrivedViewController *controller = [[ArrivedViewController alloc] initWithNibName:@"ArrivedViewController" bundle:nil];
                                            [self.navigationController pushViewController:controller animated:YES];
                                            
                                        } failure:^(NSError *error) {
                                            [self hideLoader];
                                        }];
}

#pragma mark - CollectionView -
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DirectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DirectionCell" forIndexPath:indexPath];
    
    [cell setupViewWithDirection:[self.directionsInfo objectAtIndex:indexPath.row]];
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.directionsInfo.count;
}

#pragma mark - ScrollView -

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    for (UICollectionViewCell *cell in [self.directionsCollectionView visibleCells])
    {
        NSIndexPath *indexPath = [self.directionsCollectionView indexPathForCell:cell];
        
        GTDirection *direction = [self.directionsInfo objectAtIndex:indexPath.row];
        
        [self voiceOverForDirection:direction];
        [self.mapView clear];
        [self addMarkerToLocation:[SettingsManager manager].currentJob.jobLocationCoordinates withAnimation:NO];
        [self drawPath];
        
        UIImage *image = [UIImage imageNamed:@"circle_map"];
        
        self.currentStartPointMarker = [GMSMarker markerWithPosition:direction.startLocation];
        self.currentStartPointMarker.icon = image;//[GMSMarker markerImageWithColor:[UIColor blackColor]];
        self.currentStartPointMarker.groundAnchor = CGPointMake(0.5,0.5);
        self.currentStartPointMarker.map = self.mapView;
        
        if (indexPath.row < self.directionsInfo.count-1)
        {
            [self.mapView animateToLocation:direction.startLocation];
            self.shouldAnimateToCurrentLocation = NO;
        }
        else
        {
            [self.mapView animateToLocation:[SettingsManager manager].currentJob.jobLocationCoordinates];
        }
    }
}
#pragma mark - delegates -

-(void)mustGetCurrentLocationWithButtonTapped:(UIButton *)button
{
    self.rerouteButton = button;
    [self.rerouteButton setEnabled:NO];
    [self getDirectionsAndUpdatePath:YES];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    [self supplierArrived];
    
    return YES;
}

-(BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView
{
    self.shouldAnimateToCurrentLocation = YES;
    [self animateMap];
//    self.shouldAnimateToCurrentLocation = NO;
    
    [[LocationManager manager] startConstantUpdating];
    return YES;
}

@end
