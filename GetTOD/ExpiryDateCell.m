//
//  ExpiryDateCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ExpiryDateCell.h"

@interface ExpiryDateCell()

@property (nonatomic,strong) UIImageView *divider;

@end

@implementation ExpiryDateCell

- (void)setupCellWithPlaceholder:(NSString *)placeholder
{
	[super setupCellWithPlaceholder:@""];
}

- (void)setupCellWithYear:(NSString*)year andMonth:(NSString*)month
{
	self.textField = [GTTextField new];
	self.textField.userInteractionEnabled = YES;
	self.textField.delegate = self;
	[self.contentView addSubview:self.textField];
	
	self.accessoryView = nil;
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	
	self.monthField.textAlignment = NSTextAlignmentCenter;
	self.monthField.placeholder = @"MM";
	self.monthField.text = month;
	[self.contentView addSubview:self.monthField];

	self.divider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_pipe_card"]];
	self.divider.translatesAutoresizingMaskIntoConstraints = NO;
	self.divider.contentMode = UIViewContentModeScaleAspectFit;
	[self.contentView addSubview:self.divider];

	self.yearField.textAlignment = NSTextAlignmentRight;
	self.yearField.placeholder = @"YY";
	self.yearField.text = year;
	[self.contentView addSubview:self.yearField];

	NSDictionary *views = @{@"month":self.monthField,
							@"div":self.divider,
							@"year":self.yearField,
							@"textField":self.textField};
	
	NSDictionary *metrics = @{@"divW":[NSNumber numberWithFloat:self.divider.frame.size.width],
							  @"padding":@CELL_PADDING};
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[month(46)][div(==divW)]-5-[year(45)]-(>=5)-[textField(==50)]-padding-|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[month]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[div]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[year]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[textField]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	[textField resignFirstResponder];
	
	return FALSE;
}
@end
