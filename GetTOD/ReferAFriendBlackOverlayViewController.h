//
//  ReferAFriendBlackOverlayViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/02.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"
#import "ShareViewController.h"

@interface ReferAFriendBlackOverlayViewController : RootViewController <ShareViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, strong) UIImage *backgroundImage;

@end
