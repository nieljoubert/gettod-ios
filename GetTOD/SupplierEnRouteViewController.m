//
//  SupplierEnRouteViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/05.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "SupplierEnRouteViewController.h"
#import "EnRouteCell.h"
#import "SupplierArrivedViewController.h"
#import "CancelRequestBlackOverlayViewController.h"
#import "GTMultiLineTableViewCell.h"
#import "LPGoogleFunctions.h"

@interface SupplierEnRouteViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (weak, nonatomic) IBOutlet UIView *costView;
@property (weak, nonatomic) IBOutlet UIView *timeView;

@property (strong, nonatomic) GMSMapView *map;

@property (strong, nonatomic) NSTimer *locationUpdateTimer;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSString *timeString;

@property (nonatomic, assign) int currentTime;

@property (nonatomic, strong) GMSMarker *supplierMarker;

@end

@implementation SupplierEnRouteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Your Supplier Is En Route";
    
    [self.tableView registerClass:[EnRouteCell class] forCellReuseIdentifier:@"EnRouteCell"];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    
    self.timeView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
    self.timeView.layer.borderWidth = 0.5;
    
    self.costView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
    self.costView.layer.borderWidth = 0.5;
    
    self.tableView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
    self.tableView.layer.borderWidth = 0.5;
    
    self.timeLabel.text = [NSString stringWithFormat:@"%d",[[SettingsManager manager].currentJob.calculatedTime intValue]];
    self.costLabel.text = [NSString stringWithFormat:@"%d",[[SettingsManager manager].currentJob.problem.estimatedPrice intValue]];
    
    if ([[SettingsManager manager].currentJob.problem.estimatedPrice floatValue] == -1)
    {
        self.costLabel.text = @"N/A";
    }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[SettingsManager manager].currentJob.supplier.location.latitude
                                                            longitude:[SettingsManager manager].currentJob.supplier.location.longitude
                                                                 zoom:16];
    
    self.map = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height-NAVBAR_HEIGHT) camera:camera];
    self.map.backgroundColor = [UIColor clearColor];
    self.map.camera = camera;
    self.map.myLocationEnabled = YES;
    self.map.mapType = kGMSTypeNormal;
    self.map.accessibilityElementsHidden = NO;
    self.map.settings.tiltGestures = YES;
    self.map.settings.scrollGestures = YES;
    self.map.settings.rotateGestures = YES;
    self.map.settings.zoomGestures = YES;
    self.map.settings.compassButton = NO;
    self.map.settings.myLocationButton = YES;
    
    [self.mapView addSubview:self.map];
    
    self.supplierMarker = [GMSMarker markerWithPosition:[SettingsManager manager].currentJob.supplier.location];
    self.supplierMarker.icon = [UIImage imageNamed:@"nav_arrow"];
    self.supplierMarker.appearAnimation = kGMSMarkerAnimationNone;
    self.supplierMarker.flat = YES;
    self.supplierMarker.map = self.map;
    
    self.currentTime = [[SettingsManager manager].currentJob.calculatedTime intValue] * 60;
    
    if ([[SettingsManager manager].currentJob.calculatedTime intValue] >= 5)
    {
        self.currentTime = self.currentTime / 10;
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(newUpdate)
                                                    userInfo:nil
                                                     repeats:YES];
    }
    
    [self startLocationTimer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    
    [self updateSupplierLocationFromTimer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkTimer:) name:@"TimerUpdated" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)newUpdate
{
    self.currentTime--;
    
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:[NSDate date]];
    NSTimeInterval timeIntervalCountDown = self.currentTime - timeInterval;
    
    NSDate *dateToUse = [NSDate dateWithTimeIntervalSince1970:timeIntervalCountDown];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    self.timeString = [dateFormatter stringFromDate:dateToUse];
    
    NSLog(@"%@",self.timeString);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TimerUpdated" object:self userInfo:@{@"time":dateToUse}];
    
    if (self.currentTime == 0)
    {
        [self stopTimer];
    }
}

#pragma mark - LocationTimer -
- (void)startLocationTimer
{
    if (self.locationUpdateTimer)
    {
        [self.locationUpdateTimer invalidate];
        self.locationUpdateTimer = nil;
    }
    
    self.locationUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:5
                                                                target:self
                                                              selector:@selector(updateSupplierLocationFromTimer)
                                                              userInfo:nil
                                                               repeats:YES];
}

- (void)stopLocationTimer
{
    [self.locationUpdateTimer invalidate];
    self.locationUpdateTimer = nil;
}

- (void)updateSupplierLocationFromTimer
{
    [[APIManager manager] getSupplierLocation:[SettingsManager manager].currentJob.supplier.supplierId
                                      success:^(id result) {
                                          
                                          CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake([[result objectForKey:@"lat"] floatValue],
                                                                                                          [[result objectForKey:@"lng"] floatValue]);
                                          
                                          float heading = [[result objectForKey:@"bearing"] floatValue];
                                    
                                          LPGoogleFunctions *mapsThing = [[LPGoogleFunctions alloc] init];
                                          
                                          [mapsThing loadDirectionsForOrigin:[LPLocation locationWithLatitude:coordinates.latitude
                                                                                                    longitude:coordinates.longitude]
                                                              forDestination:[LPLocation locationWithLatitude:[[SettingsManager manager].currentJob.jobLocationLat doubleValue]
                                                                                                    longitude:[[SettingsManager manager].currentJob.jobLocationLng doubleValue]]
                                                        directionsTravelMode:LPGoogleDirectionsTravelModeDriving
                                                        directionsAvoidTolls:LPGoogleDirectionsAvoidNone
                                                              directionsUnit:LPGoogleDirectionsUnitMetric
                                                      directionsAlternatives:NO
                                                               departureTime:[NSDate date]
                                                                 arrivalTime:nil
                                                                   waypoints:nil
                                                             successfulBlock:^(LPDirections *directions) {
                                                                 
                                                                 NSArray *routes = directions.routes;
                                                                 
                                                                 LPRoute *route = [routes objectAtIndex:0];
                                                                 
                                                                 NSNumber *distance = [NSNumber numberWithFloat:route.getRouteDistance/1000];
                                                                 NSNumber *duration = [NSNumber numberWithInt:route.getRouteDuration/60];
                                                                 
                                                                 NSLog(@"\n-- DISTANCE: %@   DURATION: %@",distance,duration);
                                                                 
                                                                 [SettingsManager manager].currentJob.calculatedTime = duration;
                                                                 [SettingsManager save];
                                                                 
                                                                 self.timeLabel.text = [NSString stringWithFormat:@"%d",[[SettingsManager manager].currentJob.calculatedTime intValue]];
                                                                 
                                                                 [self.map moveCamera:[GMSCameraUpdate setCamera:[GMSCameraPosition cameraWithLatitude:coordinates.latitude
                                                                                                                                             longitude:coordinates.longitude
                                                                                                                                                  zoom:16]]];
                                                                 
                                                                 [self.map clear];
                                                                 
                                                                 self.supplierMarker = [GMSMarker markerWithPosition:coordinates];
                                                                 self.supplierMarker.icon = [UIImage imageNamed:@"nav_arrow"];
                                                                 self.supplierMarker.appearAnimation = kGMSMarkerAnimationNone;
                                                                 self.supplierMarker.flat = YES;
                                                                 self.supplierMarker.rotation = heading;
                                                                 self.supplierMarker.map = self.map;
                                                                 
                                                             } failureBlock:^(LPGoogleStatus status) {
                                                                 
                                                                 if (status == LPGoogleStatusZeroResults)
                                                                 {
                                                                     
                                                                 }
                                                                 
                                                             }];
                                          
                                      } failure:^(NSError *error) {
                                          
                                          
                                          
                                      }];
}


- (IBAction)optionsButtonPressed:(id)sender
{
    BaseTableViewBlackOverlayViewController *problemList = [[BaseTableViewBlackOverlayViewController alloc] initWithNibName:@"BaseTableViewBlackOverlayViewController" bundle:nil];
    problemList.listType = BlackOverlayListTypeEnRouteOptions;
    problemList.delegate = self;
    
    GTTableViewCell *callCell = [GTTableViewCell new];
    callCell.backgroundColor = [UIColor clearColor];
    callCell.textLabel.textColor = [UIColor whiteColor];
    callCell.accessoryView = nil;
    callCell.textLabel.text = @"Call Supplier";
    
    problemList.cells = [[NSMutableArray alloc] initWithObjects:callCell, nil];
    
    if (self.currentTime <= 1 || self.timeString == nil)
    {
        GTTableViewCell *cancelCell = [GTTableViewCell new];
        cancelCell.backgroundColor = [UIColor clearColor];
        cancelCell.textLabel.textColor = [UIColor whiteColor];
        cancelCell.accessoryView = nil;
        cancelCell.textLabel.text = @"Cancel request";
        [problemList.cells addObject:cancelCell];
    }
    else
    {
        GTMultiLineTableViewCell *cancelCell = [GTMultiLineTableViewCell new];
        cancelCell.backgroundColor = [UIColor clearColor];
        cancelCell.textLabel.textColor = [UIColor whiteColor];
        cancelCell.accessoryView = nil;
        [cancelCell setupCellWithTitle:@"Cancel Request" timeText:[NSString stringWithFormat:@"(%@ min to cancel request for free)",self.timeString] andText:@"You will be charged a call out fee of R150.00 if you choose to cancel after this time."];
        [problemList.cells addObject:cancelCell];
    }
    
    problemList.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    problemList.backgroundImage = [self getBlurredBackgroundImage];
    [self.navigationController presentViewController:problemList animated:NO completion:nil];
}

#pragma mark - BlackList selection delegate -
- (void)cellSelected:(int)index WithText:(NSString *)text fromController:(BaseTableViewBlackOverlayViewController*)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    if ((text.length >= 6 && [[text substringToIndex:6] isEqual:@"Cancel"]) || text == nil)
    {
        if (self.currentTime <= 1 || self.timeString == nil)
        {
            [self launchCancelOverlay];
        }
        else
        {
            [self stopTimer];
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            
            [[APIManager manager] cancelJob:[SettingsManager manager].currentJob.jobId
                                   withType:JobCancelTypeFree
                                    success:^(BOOL success) {
                                        
                                    } failure:^(NSError *error) {
                                        
                                    }];
        }
    }
    else
    {
        NSString *cleanedString = [[[SettingsManager manager].currentJob.supplier.phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        
        // Prompt but return
        NSURL *phoneNumberURL = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:cleanedString]];
        
        // Direct call
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        
        [[UIApplication sharedApplication] openURL:phoneNumberURL];
    }
}

- (void)dismissedController:(BaseTableViewBlackOverlayViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)delayedNotification
{
    [self launchCancelOverlay];
}

- (void)launchCancelOverlay
{
    [SettingsManager manager].appState = AppStateCancelling;
    [SettingsManager save];
    
    CancelRequestBlackOverlayViewController *cancel = [[CancelRequestBlackOverlayViewController alloc] initWithNibName:@"CancelRequestBlackOverlayViewController" bundle:nil];
    cancel.delegate = self;
    cancel.jobID = [[SettingsManager manager].currentJob.jobId intValue];
    
    cancel.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    cancel.backgroundImage = [self getBlurredBackgroundImage];
    
    [self.navigationController presentViewController:cancel animated:YES completion:nil];
}

- (void)dismissController:(CancelRequestBlackOverlayViewController *)controller didCancel:(BOOL)didCancel
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    if (didCancel)
    {
        [self stopTimer];
        [self stopLocationTimer];
        
        AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
        [appDelegate startAppWithServicesList];
            
//        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

#pragma mark - TableView -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EnRouteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EnRouteCell"];
    [cell setupCellWithSupplier:[SettingsManager manager].currentJob.supplier andProblemDescription:[SettingsManager manager].currentJob.problem.problemDescription];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Timer -

- (void)stopTimer
{
    [self.timer invalidate];
    self.timer = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TimerStopped" object:self userInfo:nil];
}

- (void)checkTimer:(NSNotification*)notification
{
    
}

@end
