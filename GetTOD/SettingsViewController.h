//
//  SettingsViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/17.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"

@interface SettingsViewController : BaseTableViewController

@property(nonatomic, weak) id delegate;

@end

@protocol SettingsViewControllerDelegate <NSObject>
- (void)settingsDismissed:(SettingsViewController*)controller;
@end
