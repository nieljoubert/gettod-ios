//
//  TaskHistoryViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/18.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "TaskHistoryViewController.h"

@interface TaskHistoryViewController ()

@end

@implementation TaskHistoryViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.title = @"Task History";
	
	[self showLoader];
	
	[[APIManager manager] getTaskHistory:^(NSMutableArray *results) {
		[self hideLoader];
		
		self.tasks = results;
		[self.tableView reloadData];
		
	} failure:^(NSError *error) {
		[self hideLoader];
		
	}];
}

#pragma mark - Private -


#pragma mark - TableView -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.tasks.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	TaskHistoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TaskHistoryCell"];
	[cell setupCellWithTask:[self.tasks objectAtIndex:indexPath.row]];
	return cell;
}

@end
