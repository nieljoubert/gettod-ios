//
//  QuoteRejectedViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "QuoteRejectedViewController.h"
#import "SupplierQuoteViewController.h"
#import "Reason.h"

@interface QuoteRejectedViewController ()

@property (weak, nonatomic) IBOutlet UILabel *quoteRejectReason;

@end

@implementation QuoteRejectedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
	style.lineSpacing = 7;
	style.lineBreakMode = NSLineBreakByWordWrapping;
	style.alignment = NSTextAlignmentCenter;
	
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{}];
	[attributes addEntriesFromDictionary:@{NSParagraphStyleAttributeName:style}];
	[attributes addEntriesFromDictionary:@{NSFontAttributeName:[UIFont fontWithName:FONT size:14]}];
	
	self.quoteRejectReason.numberOfLines = 0;
	self.quoteRejectReason.attributedText = [[NSAttributedString alloc] initWithString:self.reasonString attributes:attributes];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (IBAction)requote:(id)sender
{
	SupplierQuoteViewController *controller = [[SupplierQuoteViewController alloc] initWithNibName:@"SupplierQuoteViewController" bundle:nil];
	[self.navigationController pushViewController:controller animated:NO];
//	[self.delegate performRequote:self];
}

@end
