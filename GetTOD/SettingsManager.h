//
//  SettingsManager.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"
#import "Job.h"

@interface SettingsManager : NSObject

+ (SettingsManager*) manager;
+ (void) save;
+ (void) clear;
- (CLLocationCoordinate2D)requestedLocationCoords;
- (CLLocationCoordinate2D)currentLocationCoords;
- (CLLocation*)currentLocationCL;
- (BOOL)isiOS8;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *currentLocation;
@property (nonatomic, strong) NSString *authenticationToken;
@property (nonatomic, strong) NSString *emailAddress;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *pushToken;
@property (nonatomic, strong) NSString *cardEncryptionKey;
@property (nonatomic, assign) BOOL isRegistration;
@property (nonatomic, assign) BOOL calledFBOnce;
@property (nonatomic, assign) BOOL loggedInWithFB;
@property (nonatomic, assign) BOOL hasRegisteredForPushNotification;
@property (nonatomic, assign) SupplierStatus supplierStatus;
@property (nonatomic, strong) NSString *profileImageURL;
@property (nonatomic, strong) NSNumber *requestedLocationLat;
@property (nonatomic, strong) NSNumber *requestedLocationLng;

@property (nonatomic, strong) NSNumber *currentLocationLat;
@property (nonatomic, strong) NSNumber *currentLocationLng;
@property (nonatomic, strong) NSNumber *currentHeading;

@property (nonatomic, assign) BOOL gotUserDetailsOnce;

@property (nonatomic, strong) Job *currentJob;
@property (nonatomic, assign) AppState appState;
@property (nonatomic, assign) JobCancelType cancelType;
@property (nonatomic, strong) NSString *rejectReason;
@property (nonatomic, strong) NSString *currentJobAddress;
@property (nonatomic, strong) NSString *isInSA;

@property (nonatomic, strong) NSDictionary *pushInfo;
@property (nonatomic, assign) BOOL didHandlePush;

@end
