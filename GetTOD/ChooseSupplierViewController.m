//
//  SupplierSelectorViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "ChooseSupplierViewController.h"
#import "ToBeSentToView.h"
#import "ChooseSupplierCell.h"
#import "RequestSupplierViewController.h"
#import "AddEditCardDetailsViewController.h"
#import "Supplier.h"
#import "ImageHelper.h"
#import "LPLocation.h"

@interface ChooseSupplierViewController ()

@property (nonatomic, strong) NSMutableDictionary *supplierDistancesAndTimes;

@property (strong, nonatomic) UISearchDisplayController *searchBarController;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *tappableHeaderView;
@property (strong, nonatomic) ToBeSentToView *headerView;
@property (assign, nonatomic) BOOL fadingOut;

@property (assign, nonatomic) BOOL cameFromCardsView;

@property (strong, nonatomic) GTSearchBarView *searchBarView;
@property (strong, nonatomic) NSString *addressString;
@property (assign, nonatomic) CLLocationCoordinate2D selectedCoordinate;

@property (assign, nonatomic) int selectedIndex;

@property (nonatomic, strong) UILabel *noDataLabel;
@property (nonatomic, strong) UIButton *myLocationButton;

@end

@implementation ChooseSupplierViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Choose A Supplier";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"location_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(getCurrentLocation)];
    
    self.tappableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 123)];
    
    self.headerView = [[ToBeSentToView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 123)];
    
    UIButton *fakeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fakeButton.frame = self.headerView.frame;
    [fakeButton addTarget:self action:@selector(fade) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tappableHeaderView addSubview:self.headerView];
    [self.tappableHeaderView addSubview:fakeButton];
    
    int offset = self.tappableHeaderView.frame.origin.y + self.tappableHeaderView.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, offset, SCREEN_WIDTH, SCREEN_HEIGHT-offset) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.tableView registerClass:[ChooseSupplierCell class] forCellReuseIdentifier:@"ChooseSupplierCell"];
    
    self.searchBarView = [[GTSearchBarView alloc] initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, 44)];
    self.searchBarView.delegate = self;
    self.searchBarView.alpha = 0;
    self.delegate = self;
    self.selectedIndex = -1;
    
    if (self.noDataLabel == nil)
    {
        self.noDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, self.tableView.tableHeaderView.frame.size.height+15, SCREEN_WIDTH - 50, 50)];
        self.noDataLabel.font = [UIFont fontWithName:FONT size:15];
        self.noDataLabel.textColor = DARK_TEXT_COLOR;
        self.noDataLabel.textAlignment = NSTextAlignmentCenter;
        self.noDataLabel.numberOfLines = 0;
        self.noDataLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.noDataLabel.text = @"No suppliers are currently online, please try again soon";
        self.noDataLabel.hidden = YES;
        [self.tableView addSubview:self.noDataLabel];
        self.tableView.scrollEnabled = NO;
    }
    
    if (self.myLocationButton == nil)
    {
        self.myLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.myLocationButton.frame = CGRectMake(self.view.frame.size.width-[UIImage imageNamed:@"my_location"].size.width-10,
                                                 self.view.frame.size.height-offset-26-[UIImage imageNamed:@"my_location"].size.height,
                                                 [UIImage imageNamed:@"my_location"].size.width,
                                                 [UIImage imageNamed:@"my_location"].size.height);
        [self.myLocationButton addTarget:self action:@selector(getCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
        [self.myLocationButton setImage:[UIImage imageNamed:@"my_location"] forState:UIControlStateNormal];
        self.myLocationButton.hidden = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view bringSubviewToFront:self.HUD];
    
    self.navigationController.navigationBarHidden = NO;
    
    if (self.selectedIndex != -1 && [CacheManager cardsFromCache].count > 0 && self.cameFromCardsView)
    {
        [self gotoRequestScreen];
        self.cameFromCardsView = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.delegate = nil;
    self.navigationController.delegate = nil;
}

#pragma mark - Private -
- (void)calculateDistances
{
    for(int i=0; i < self.tableView.visibleCells.count; i++)
    {
        [self calculateDistance:i];
    }
}

- (void)calculateDistance:(int)row
{
    LPGoogleFunctions *mapsThing = [[LPGoogleFunctions alloc] init];
    mapsThing.delegate = self;
    
    Supplier *supplier = [self.suppliers objectAtIndex:row];
    
    [mapsThing loadDirectionsForOrigin:[LPLocation locationWithLatitude:supplier.location.latitude longitude:supplier.location.longitude]
                        forDestination:[LPLocation locationWithLatitude:self.selectedCoordinate.latitude longitude:self.selectedCoordinate.longitude]
                  directionsTravelMode:LPGoogleDirectionsTravelModeDriving
                  directionsAvoidTolls:LPGoogleDirectionsAvoidNone
                        directionsUnit:LPGoogleDirectionsUnitMetric
                directionsAlternatives:NO
                         departureTime:[NSDate date]
                           arrivalTime:nil
                             waypoints:nil
                       successfulBlock:^(LPDirections *directions) {
                           
                           NSArray *routes = directions.routes;
                           
                           LPRoute *route = [routes objectAtIndex:0];
                           
                           NSNumber *distance = [NSNumber numberWithFloat:route.getRouteDistance/1000];
                           NSNumber *duration = [NSNumber numberWithInt:route.getRouteDuration/60];
                           
                           NSLog(@"\n-- DISTANCE: %@   DURATION: %@",distance,duration);
                           
                           self.supplierDistancesAndTimes[[NSNumber numberWithInt:row]] = @{@"distance":distance,@"time":duration};
                           
                           [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                           
                           
                       } failureBlock:^(LPGoogleStatus status) {
                           
                           self.supplierDistancesAndTimes[[NSNumber numberWithInt:row]] = @{@"distance":@0,@"time":@0};
                           
                           [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];

                           
                           if (status == LPGoogleStatusZeroResults)
                           {
                               
                           }
                           
                       }];
}

- (void)gotoRequestScreen
{
    Supplier *selectedSupplier = [self.suppliers objectAtIndex:self.selectedIndex];
    
    self.tableView.userInteractionEnabled = NO;
    
    [[APIManager manager] getSupplierLocation:selectedSupplier.supplierId
                                      success:^(id result) {
                                          
                                          CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake([[result objectForKey:@"lat"] floatValue],
                                                                                                          [[result objectForKey:@"lng"] floatValue]);
                                          SupplierStatus status = [[result objectForKey:@"status"] integerValue];
                                          
                                          if (status == SupplierStatusOnline)
                                          {
                                              [self showLoader];
                                              
                                              [self getStaticMap:self.selectedIndex
                                                     withSuccess:^(UIImage *image) {
                                                         
                                                         RequestSupplierViewController *request = [[RequestSupplierViewController alloc] initWithNibName:@"RequestSupplierViewController" bundle:nil];
                                                         request.requestCoordinates = self.selectedCoordinate;
                                                         request.timeToSupplier = [[self.supplierDistancesAndTimes objectForKey:[NSNumber numberWithInt:self.selectedIndex]] objectForKey:@"time"];
                                                         request.distanceToSupplier = [[self.supplierDistancesAndTimes objectForKey:[NSNumber numberWithInt:self.selectedIndex]] objectForKey:@"distance"];
                                                         //			   request.supplier = [self.suppliers objectAtIndex:self.selectedIndex];
                                                         request.mapImage = image;
                                                         
                                                         [SettingsManager manager].requestedLocationLat = [NSNumber numberWithFloat:self.selectedCoordinate.latitude];
                                                         [SettingsManager manager].requestedLocationLng = [NSNumber numberWithFloat:self.selectedCoordinate.longitude];
                                                         
                                                         [[APIManager manager] getSupplierSummaryForID:selectedSupplier.supplierId
                                                                                               success:^(Supplier *supplier) {
                                                                                                   [self hideLoader];
                                                                                                   
                                                                                                   request.supplier = supplier;
                                                                                                   [self.navigationController pushViewController:request animated:YES];
                                                                                                   
                                                                                               } failure:^(NSError *error) {
                                                                                                   [self hideLoader];
                                                                                               }];
                                                         
                                                         
                                                         
                                                     } failureBlock:^(NSError *error) {
                                                         [self hideLoader];
                                                     }];
                                          }
                                          else if (status == SupplierStatusOffline)
                                          {
                                              [AlertViewManager showAlertWithTitle:@"Supplier Offline"
                                                                           message:@"Selected supplier is currently offline. Please select another." cancelButtonTitle:@"OK"
                                                                 otherButtonTitles:nil
                                                              buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                              }];
                                          }
                                          else if (status == SupplierStatusBusy)
                                          {
                                              [AlertViewManager showAlertWithTitle:@"Supplier Busy"
                                                                           message:@"Selected supplier is currently on a job. Please select another." cancelButtonTitle:@"OK"
                                                                 otherButtonTitles:nil
                                                              buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                              }];
                                          }
                                          self.tableView.userInteractionEnabled = YES;
                                      } failure:^(NSError *error) {
                                          self.tableView.userInteractionEnabled = YES;
                                      }];
    
}

#pragma mark - LPGoogleFunctions -
- (void)googleFunctions:(LPGoogleFunctions *)googleFunctions didLoadDistanceMatrix:(LPDistanceMatrix *)distanceMatrix
{
    
}

- (void)googleFunctions:(LPGoogleFunctions *)googleFunctions errorLoadingDistanceMatrixWithStatus:(LPGoogleStatus)status
{
    
}

#pragma mark - Actions -
- (void)getCurrentLocation
{
    [[LocationManager manager] requestCurrentLocation:^(BOOL success, CLLocation *location) {
        
        if (success)
        {
            self.addressString = nil;
            self.currentAddress = nil;
            self.currentLocation = nil;
            self.searchBarView.textField.text = @"";
            self.selectedCoordinate = location.coordinate;
            
            [self fadeOut];
            
            if (self.mapView.myLocation)
            {
                [self.mapView animateToLocation:self.mapView.myLocation.coordinate];
            }
            
            NSLog(@"%@",self.mapView.myLocation);
        }
        else
        {
            
        }
    }];
}

- (void)searchButtonTapped
{
    MapSearchModalViewController *searchController = [[MapSearchModalViewController alloc] init];
    searchController.delegate = self;
    [self.navigationController presentViewController:searchController animated:YES completion:nil];
}

- (void)closeExpandedMap
{
    if (self.addressString == nil)
    {
        [self.headerView setupViewWithAddress:@"Current Location"];
    }
    [self fadeIn];
}


-(void)fade
{
    if (self.fadingOut)
    {
        [self fadeIn];
    }
    else
    {
        [self fadeOut];
    }
}

-(void)fadeOut
{
    self.fadingOut = YES;
    
    self.searchBarView.frame = CGRectMake(10, 10, SCREEN_WIDTH-20, 44);
    
    UIView *otherTapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(searchButtonTapped)];
    singleTap.numberOfTapsRequired = 1;
    [otherTapView addGestureRecognizer:singleTap];
    [self.view addSubview:otherTapView];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.tappableHeaderView.alpha = 0;
                         self.searchBarView.alpha = 1;
                         
                         CGRect frame = self.tableView.frame;
                         frame.origin.y = self.view.frame.size.height-75;
                         self.tableView.frame = frame;
                         self.tableView.scrollEnabled = NO;
                         
                         if (CLLocationCoordinate2DIsValid(self.selectedCoordinate))
                         {
                             [self.mapView animateToLocation:self.selectedCoordinate];
                         }
                         else
                         {
                             if (self.mapView.myLocation)
                             {
                                 [self.mapView animateToLocation:self.mapView.myLocation.coordinate];
                             }
                         }
                     }
                     completion:^(BOOL finished) {
                         
                         self.searchBarView.frame = CGRectMake(10, 10, SCREEN_WIDTH-20, 44);
                         
                         self.marker.opacity = 1;
                         
                         self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(closeExpandedMap)];
                     }];
}

-(void)fadeIn
{
    self.fadingOut = NO;
    
    self.searchBarView.frame = CGRectMake(10, 10, SCREEN_WIDTH-20, 44);
    
    self.marker.opacity = 0;
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.tappableHeaderView.alpha = 1;
                         self.searchBarView.alpha = 0;
                         
                         int offset = self.tappableHeaderView.frame.origin.y + self.tappableHeaderView.frame.size.height;
                         
                         self.tableView.frame = CGRectMake(0, offset, SCREEN_WIDTH, SCREEN_HEIGHT-offset);
                         self.tableView.scrollEnabled = YES;
                     }
                     completion:^(BOOL finished) {
                         self.searchBarView.frame = CGRectMake(10, 10, SCREEN_WIDTH-20, 44);
                         
                         self.firstRunDone = NO;
                         [self.mapView clear];
                         
                         GMSCameraUpdate *downwards = [GMSCameraUpdate scrollByX:0 Y:SCREEN_HEIGHT/4+20];
                         [self.mapView animateWithCameraUpdate:downwards];
                         
                         if (self.addressString)
                         {
                             [self.headerView setupViewWithAddress:self.addressString];//[NSString stringWithFormat:@"%@, %@",self.currentAddress.lines[0],self.currentAddress.lines[1]]];
                         }
                         else
                         {
                             [self.headerView setupViewWithAddress:@"Current Location"];
                         }
                         
                         self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"location_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(getCurrentLocation)];
                     }];
}


- (void)getStaticMap:(int)row withSuccess:(void (^)(UIImage *image))successful failureBlock:(void (^)(NSError *error))failure
{
    
    Supplier *supplier = [self.suppliers objectAtIndex:row];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFImageResponseSerializer serializer];
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    [parameters setObject:[NSString stringWithFormat:@"%f,%f", supplier.location.latitude, supplier.location.longitude] forKey:@"center"];
    [parameters setObject:@"true" forKey:@"sensor"];
    [parameters setObject:[NSNumber numberWithInt:16] forKey:@"zoom"];
    [parameters setObject:[NSNumber numberWithInt:1] forKey:@"scale"];
    [parameters setObject:[NSString stringWithFormat:@"%dx%d", 320, 160] forKey:@"size"];
    [parameters setObject:[LPGoogleFunctions getMapType:LPGoogleMapTypeRoadmap] forKey:@"maptype"];
    
    NSMutableSet *parametersMarkers = [[NSMutableSet alloc] init];
    
    LPMapImageMarker *marker = [[LPMapImageMarker alloc] init];
    [marker setLocation:[LPLocation locationWithLatitude:supplier.location.latitude longitude:supplier.location.longitude]];
    [marker setColor:PINK_COLOR];
    [marker setSize:LPGoogleMapImageMarkerSizeNormal];
    
    [parametersMarkers addObject:[marker getMarkerURLString]];
    
    [parameters setObject:parametersMarkers forKey:@"markers"];
    
    [manager GET:@"http://maps.googleapis.com/maps/api/staticmap?"
      parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             if(successful)
             {
                 successful(responseObject);
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             if(failure)
             {
                 failure(error);
             }
         }];
}
#pragma mark - TableView -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseSupplierCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ChooseSupplierCell"];
    
    if (self.suppliers.count > 0)
    {
        if (![self.supplierDistancesAndTimes objectForKey:[NSNumber numberWithInteger:indexPath.row]])
        {
            [cell setupCellWithSupplier:[self.suppliers objectAtIndex:indexPath.row] withDistance:0];
        }
        else
        {
            NSNumber *distance = [[self.supplierDistancesAndTimes objectForKey:[NSNumber numberWithInteger:indexPath.row]] objectForKey:@"distance"];
            [cell setupCellWithSupplier:[self.suppliers objectAtIndex:indexPath.row] withDistance:distance];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self.supplierDistancesAndTimes objectForKey:[NSNumber numberWithInteger:indexPath.row]])
    {
        [self calculateDistance:(int)indexPath.row];
    }
}

//-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.suppliers.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = (int)indexPath.row;
    
    if ([[self.supplierDistancesAndTimes objectForKey:[NSNumber numberWithInt:self.selectedIndex]] objectForKey:@"time"] != nil)
    {
        [self gotoRequestScreen];
    }
    else
    {
        [AlertViewManager showAlertWithTitle:@""
                                     message:@"Still obtaining supplier information"
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil
                        buttonSelectionBlock:nil];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - MapViewModalDelegate

- (void)locationSelected:(LPPlaceDetails *)locationDetails
{
    [self.mapView moveCamera:[GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(locationDetails.geometry.location.latitude, locationDetails.geometry.location.longitude)]];
    
    NSArray *components = locationDetails.addressComponents;
    
    NSString *town = @"";
    
    for (LPAddressComponent* com in components)
    {
        for (NSString *type in com.types)
        {
            if ([type isEqualToString:@"locality"]) // Cape Town
            {
                town = com.longName;
            }
        }
    }
    
    self.selectedCoordinate = CLLocationCoordinate2DMake(locationDetails.geometry.location.latitude, locationDetails.geometry.location.longitude);
    self.supplierDistancesAndTimes = [NSMutableDictionary dictionary];
    [self.tableView reloadData];
    
    self.currentLocation = [[CLLocation alloc] initWithLatitude:locationDetails.geometry.location.latitude longitude:locationDetails.geometry.location.longitude];
    self.addressString = [NSString stringWithFormat:@"%@, %@",locationDetails.name,[NSString stringWithFormat:@"%@",town]];//,province]];
    
    self.searchBarView.textField.text = self.addressString;
}

#pragma mark - BaseMapViewDelegate -

- (void)addressSelectedFromMap:(GMSAddress *)address withCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.addressString = [NSString stringWithFormat:@"%@, %@",address.thoroughfare,address.locality];//,address.lines[0],address.lines[1]];
    self.currentAddress = address;
    self.selectedCoordinate = coordinate;
    
    self.supplierDistancesAndTimes = [NSMutableDictionary dictionary];
    
    self.searchBarView.textField.text = self.addressString;
    
    if (self.firstRunDone)
    {
        [self.tableView reloadData];
    }
}

- (void)updatedLocation:(CLLocation *)location
{
    self.currentLocation = location;
    self.selectedCoordinate = location.coordinate;
    self.supplierDistancesAndTimes = [NSMutableDictionary dictionary];
    [self.tableView reloadData];
    
    GMSCameraUpdate *downwards = [GMSCameraUpdate scrollByX:0 Y:SCREEN_HEIGHT/4-5];
    [self.mapView animateWithCameraUpdate:downwards];
}

-(void)mapViewFinishedLoading
{
    CGRect frame = self.mapView.frame;
    frame.origin.y = 0;
    frame.size.height = SCREEN_HEIGHT - 75;
    self.mapView.frame = frame;
    
    self.mapView.settings.myLocationButton = NO;
    
    [self.view addSubview:self.mapView];
    [self.view addSubview:self.tappableHeaderView];
    [self.view addSubview:self.myLocationButton];
    [self.view addSubview:self.tableView];
    [self.mapView addSubview:self.searchBarView];
    [self.mapView bringSubviewToFront:self.searchBarView];
    
    [self.view bringSubviewToFront:self.HUD];
    
    self.tableView.scrollEnabled = YES;
}

@end
