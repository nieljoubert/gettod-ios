//
//  QuoteMaterialCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldTableViewCell.h"
#import "Material.h"

@interface QuoteMaterialCell : GTBoldTableViewCell

- (void)setupCellWithMaterial:(Material*)material;

@end
