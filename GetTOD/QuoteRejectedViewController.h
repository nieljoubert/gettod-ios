//
//  QuoteRejectedViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuoteRejectedViewController : UIViewController

@property (nonatomic, strong) NSString *reasonString;
//@property(nonatomic, weak) id delegate;

@end

//@protocol QuoteRejectedDelegate <NSObject>
//- (void)rejectTapped:(QuoteRejectedViewController*)controller;
//- (void)performRequote:(QuoteRejectedViewController*)controller;
//@end