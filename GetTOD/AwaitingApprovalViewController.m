//
//  AwaitingApprovalViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "AwaitingApprovalViewController.h"

@interface AwaitingApprovalViewController ()

@end

@implementation AwaitingApprovalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.textLabel.text = @"Awaiting Approval";
	self.indicatorView.delegate = self;
	self.indicatorView.numberOfCircles = 3;
	self.indicatorView.radius = 2.5;
	self.indicatorView.internalSpacing = 5;
	self.indicatorView.duration = 1.25;
	self.indicatorView.delay = 0.5;
	
	[self.indicatorView startAnimating];
}

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
	  circleBackgroundColorAtIndex:(NSUInteger)index {
	return PINK_COLOR;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

@end
