//
//  RequestingSupplierBlackOverlayViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RequestingSupplierBlackOverlayViewController.h"
#import "MONActivityIndicatorView.h"

@interface RequestingSupplierBlackOverlayViewController () <MONActivityIndicatorViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet MONActivityIndicatorView *indicatorView;

@end

@implementation RequestingSupplierBlackOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;
	self.cancelButton.layer.borderWidth = 1;
	self.cancelButton.layer.cornerRadius = self.cancelButton.frame.size.height/2;
	
	self.backgroundImageView.image = self.backgroundImage;
	
	self.indicatorView.delegate = self;
	self.indicatorView.numberOfCircles = 3;
	self.indicatorView.radius = 2.5;
	self.indicatorView.internalSpacing = 5;
	self.indicatorView.duration = 1.25;
	self.indicatorView.delay = 0.5;

	[self.indicatorView startAnimating];
}

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
	  circleBackgroundColorAtIndex:(NSUInteger)index {
	return [UIColor whiteColor];
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (IBAction)cancelRequest:(id)sender
{
    [self showLoader];
    
	[[APIManager manager] cancelJob:[SettingsManager manager].currentJob.jobId
						   withType:JobCancelTypeBeforeJob
							success:^(BOOL success) {
                                [self hideLoader];
								[self.navigationController popViewControllerAnimated:NO];
							} failure:^(NSError *error) {
                                [self hideLoader];
							}];
}


@end
