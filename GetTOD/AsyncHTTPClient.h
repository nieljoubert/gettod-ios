//
//  AsyncHTTPClient.h
//  getTOD
//
//  Created by Niel Joubert on 2015/11/24.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPClient.h"

@interface AsyncHTTPClient : HTTPClient

+ (AsyncHTTPClient *)sharedAsyncClient;

@end
