//
//  GTMultiLineTableViewCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/05/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableViewCell.h"

@interface GTMultiLineTableViewCell : GTTableViewCell

- (void)setupCellWithTitle:(NSString*)title timeText:(NSString*)time andText:(NSString*)text;

@end
