//
//  BaseBlackOverlayViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/25.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseBlackOverlayViewController.h"
#import "Constants.h"
#import "ServicesListViewController.h"

@interface BaseBlackOverlayViewController ()

@property (nonatomic,strong) NSMutableDictionary *views;

@end

@implementation BaseBlackOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH)];
	blackView.translatesAutoresizingMaskIntoConstraints = NO;
	blackView.backgroundColor = [UIColor blackColor];
	
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(dismiss)];
	singleTap.numberOfTapsRequired = 1;
	[blackView addGestureRecognizer:singleTap];
	
	self.views = [NSMutableDictionary dictionaryWithDictionary:@{@"blackView":blackView}];
	
	[self.view addSubview:blackView];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[blackView]|"
																	 options:0
																	 metrics:nil
																		views:self.views]];

	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[blackView]|"
																	  options:0
																	  metrics:nil
																		views:self.views]];
}

- (void)dismiss
{
	[self dismissViewControllerAnimated:NO completion:nil];
}

@end
