//
//  WhitePaymentCardCell.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "WhitePaymentCardCell.h"

@interface WhitePaymentCardCell()

@property (nonatomic,strong) UIImageView *dots;
@property (nonatomic,strong) UIImageView *cardImage;
@property (nonatomic,strong) UIImageView *divider;

@end

@implementation WhitePaymentCardCell

- (id)init
{
	self = [super init];
	
	if (self)
	{
		self.backgroundColor = [UIColor clearColor];
		self.accessoryType = UITableViewCellAccessoryNone;
		self.accessoryView = nil;
	}
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	
	if (self)
	{
		self.backgroundColor = [UIColor clearColor];
		self.accessoryType = UITableViewCellAccessoryNone;
		self.accessoryView = nil;
	}
	
	return self;
}

- (void)setupCellWithCard:(Card*)card
{
	if (self.cardImage == nil)
	{
		self.cardImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"visa_white"]];
		self.cardImage.translatesAutoresizingMaskIntoConstraints = NO;
		self.cardImage.backgroundColor = [UIColor clearColor];
		self.cardImage.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.cardImage];
	}
	
	if (self.divider == nil)
	{
		self.divider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_pipe_card"]];
		self.divider.translatesAutoresizingMaskIntoConstraints = NO;
		self.divider.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.divider];
	}
	
	if (self.nameField == nil)
	{
		self.nameField = [GTTextField new];
		self.nameField.translatesAutoresizingMaskIntoConstraints = NO;
		self.nameField.backgroundColor = [UIColor clearColor];
		self.nameField.textColor = [UIColor whiteColor];
		self.nameField.enabled = NO;
		self.nameField.adjustsFontSizeToFitWidth = YES;
		[self.contentView addSubview:self.nameField];
	}
	
	if (self.cardNumber == nil)
	{
		self.cardNumber = [GTTextField new];
		self.cardNumber.translatesAutoresizingMaskIntoConstraints = NO;
		self.cardNumber.enabled = NO;
		self.cardNumber.backgroundColor = [UIColor clearColor];
		self.cardNumber.textColor = [UIColor whiteColor];
		[self.contentView addSubview:self.cardNumber];
	}
	
	if (self.dots == nil)
	{
		self.dots = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dots4_white"]];
		self.dots.translatesAutoresizingMaskIntoConstraints = NO;
		self.dots.contentMode = UIViewContentModeScaleAspectFit;
		[self.contentView addSubview:self.dots];
	}
	
	self.nameField.text = card.name;
	self.cardNumber.text = card.lastDigits;
	
	[self getCardLogo:card.brand];
	
	NSDictionary *views = @{@"image":self.cardImage,
							@"div":self.divider,
							@"name":self.nameField,
							@"dots":self.dots,
							@"number":self.cardNumber};
	
	NSDictionary *metrics = @{@"imageW":[NSNumber numberWithFloat:self.cardImage.frame.size.width],
							  @"divW":[NSNumber numberWithFloat:self.divider.frame.size.width],
  							  @"dotsW":[NSNumber numberWithFloat:self.dots.frame.size.width],
							  @"padding":@CELL_PADDING};
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[image(==imageW)]-10-[div(==divW)]-10-[name(>=10)]-10-[dots(==dotsW)]-10-[number(==60)]-10-|"
																			 options:0
																			 metrics:metrics
																			   views:views]];

	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];

	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[div]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];

	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[name]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];

	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dots]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];

	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[number]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
}

- (void)getCardLogo:(NSString*)brand
{
	if ([brand isEqualToString:@"VISA"])
	{
		self.cardImage.image = [UIImage imageNamed:@"visa_white"];
	}
	else if ([brand isEqualToString:@"MASTERCARD"])
	{
		self.cardImage.image = [UIImage imageNamed:@"mastercard_white"];
	}
	else
	{
		self.cardImage.image = [UIImage imageNamed:@"plain_card"];
	}
}
@end
