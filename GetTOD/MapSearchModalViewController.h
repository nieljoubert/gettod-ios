//
//  MapSearchModalViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LPGoogleFunctions.h"

@protocol MapSearchModalDelegate;

@interface MapSearchModalViewController : UIViewController <LPGoogleFunctionsDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property(nonatomic, weak) id <MapSearchModalDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *infoText;

@end

@protocol MapSearchModalDelegate <NSObject>
-(void) locationSelected:(LPPlaceDetails*)locationDetails;
@end