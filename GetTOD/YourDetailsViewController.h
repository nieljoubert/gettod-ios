//
//  YourDetailsViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"
#import "IBActionSheet.h"
#import "ChangeLocationViewController.h"

@interface YourDetailsViewController : BaseTableViewController <IBActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ChangeLocationViewDelegate>

@property (nonatomic, assign) BOOL isSettings;

@end
