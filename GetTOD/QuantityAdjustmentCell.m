//
//  MaterialQuantityCell.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "QuantityAdjustmentCell.h"

@interface QuantityAdjustmentCell()

@property (nonatomic, strong) UIButton *plusButton;
@property (nonatomic, strong) UIButton *minusButton;
@property (nonatomic, strong) UILabel *quantity;
@property (nonatomic, strong) UILabel *subTitle;
@property (nonatomic, assign) int quantityValue;

@end

@implementation QuantityAdjustmentCell

- (void)setupCellWithSubtitle:(BOOL)hasSubTitle withQuantity:(int)quant
{
	self.accessoryType = UITableViewCellAccessoryNone;
	self.accessoryView = nil;
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	
	if (self.quantity == nil)
	{
		self.quantity = [UILabel new];
		self.quantity.translatesAutoresizingMaskIntoConstraints = NO;
		self.quantity.font = [UIFont fontWithName:FONT_LIGHT size:30];
		self.quantity.textColor = DARK_TEXT_COLOR;
		self.quantity.textAlignment = NSTextAlignmentCenter;
		[self.contentView addSubview:self.quantity];
	}

	self.quantity.text = [NSString stringWithFormat:@"%d",quant];
	self.quantityValue = quant;
	
	if (self.subTitle == nil && hasSubTitle)
	{
		self.subTitle = [UILabel new];
		self.subTitle.translatesAutoresizingMaskIntoConstraints = NO;
		self.subTitle.font = [UIFont fontWithName:FONT_SEMI_BOLD size:8];
		self.subTitle.textColor = GRAY_COLOR;
		self.subTitle.textAlignment = NSTextAlignmentCenter;
		[self.contentView addSubview:self.subTitle];
		
		self.subTitle.text = @"HOURS";
	}
	
	if (self.minusButton == nil)
	{
		self.minusButton = [UIButton buttonWithType:UIButtonTypeCustom];
		self.minusButton.translatesAutoresizingMaskIntoConstraints = NO;
		[self.minusButton setImage:[UIImage imageNamed:@"material_minus_button"] forState:UIControlStateNormal];
		[self.minusButton addTarget:self action:@selector(decreaseQuantity) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:self.minusButton];
	}
	
	if (self.plusButton == nil)
	{
		self.plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
		self.plusButton.translatesAutoresizingMaskIntoConstraints = NO;
		[self.plusButton setImage:[UIImage imageNamed:@"material_plus_button"] forState:UIControlStateNormal];
		[self.plusButton addTarget:self action:@selector(increaseQuantity) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:self.plusButton];
	}
	
	NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"minus":self.minusButton,
																				 @"quant":self.quantity,
																				 @"plus":self.plusButton}];

	NSDictionary *metrics = @{@"buttonW":[NSNumber numberWithFloat:[UIImage imageNamed:@"material_plus_button"].size.width]};
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[minus(==buttonW)]-10-[quant]-10-[plus(==buttonW)]-15-|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[minus]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-3-[quant]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[plus]|"
																			 options:0
																			 metrics:metrics
																			   views:views]];
	
	if (hasSubTitle)
	{
		[views addEntriesFromDictionary:@{@"text":self.subTitle}];
		
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[minus(==buttonW)]-10-[text]-10-[plus(==buttonW)]-15-|"
																				 options:0
																				 metrics:metrics
																				   views:views]];
		
		[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=10)-[text(==10)]-10-|"
																				 options:0
																				 metrics:metrics
																				   views:views]];
	}
}

- (void)decreaseQuantity
{
	if (self.quantityValue != 1)
	{
		self.quantityValue--;
	}
	
	self.quantity.text = [NSString stringWithFormat:@"%d",self.quantityValue];
	
	[self.delegate quantityChanged:self.quantityValue];
}

- (void)increaseQuantity
{
	self.quantityValue++;
	
	self.quantity.text = [NSString stringWithFormat:@"%d",self.quantityValue];
	
	[self.delegate quantityChanged:self.quantityValue];
}

@end
