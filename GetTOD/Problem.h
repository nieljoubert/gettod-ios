//
//  PossibleProblem.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/19.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Problem : NSObject

@property (nonatomic, strong) NSNumber *problemId;
@property (nonatomic, strong) NSString *problemDescription;
@property (nonatomic, strong) NSNumber *estimatedPrice;

@end
