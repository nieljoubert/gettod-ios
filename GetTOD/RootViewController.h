//
//  RootViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Validation.h"
#import "APIManager.h"
#import "SettingsManager.h"
#import "GTTextField.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "CacheManager.h"
#import "AlertviewManager.h"
#import "MBProgressHUD.h"
#import "Flurry.h"

@interface RootViewController : UIViewController <UITextFieldDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIToolbar *keyboardToolbar;

@property (nonatomic, strong) GTTextField *currentField;
@property (nonatomic, strong) GTTextField *errorField;
@property (nonatomic, strong) NSMutableArray *fields;
@property (nonatomic, retain) MBProgressHUD *HUD;

- (void) removeField:(GTTextField*)field;
- (void) addFormField:(GTTextField*)formField;
- (void) showScreenError;
- (void) dismissScreenError:(GTTextField*)field;
- (void) setScreenError:(GTTextField*)field message:(NSString*)message;
- (BOOL) isScreenFieldsValid;

- (void) showLoader;
- (void) showLoaderWithMessage:(NSString*)message;
- (void) hideLoader;

- (void) goBack;
- (void) dismissKeyboard;
- (UIImage*)getBlurredBackgroundImage;

@end
