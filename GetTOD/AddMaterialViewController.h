//
//  AddMaterialViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"

@interface AddMaterialViewController : BaseTableViewController <QuantityCellDelegate>

@property(nonatomic, strong) Material *material;
@property(nonatomic, weak) id delegate;

@end

@protocol AddMaterialViewControllerDelegate <NSObject>
- (void)addMaterialCanceled:(AddMaterialViewController*)controller;
- (void)addedMaterial:(Material*)material wasEditing:(BOOL)wasEdit fromController:(AddMaterialViewController*)controller;
@end