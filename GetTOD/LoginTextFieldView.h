//
//  LoginTextFieldView.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTTextField.h"
#import "Constants.h"

@interface LoginTextFieldView : UIView

@property (nonatomic, strong) GTTextField *textField;

-(id)initWithTexField:(GTTextField*)textField withPlaceholder:(NSString*)placeholder hasLockIcon:(BOOL)hasLock;

- (void)setError;
- (void)dismissError;

@end
