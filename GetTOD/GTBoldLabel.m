//
//  GTBoldLabel.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTBoldLabel.h"
#import "Constants.h"

@implementation GTBoldLabel

-(id)init
{
	self = [super init];
	
	if (self)
	{
		self.font = [UIFont fontWithName:@"AdelleSans-SemiBold" size:16];
	}
	
	return self;
}

@end
