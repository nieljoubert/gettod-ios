//
//  ProfileImageAndDetailsCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableViewCell.h"
#import "Job.h"

@interface ProfileImageAndDetailsCell : GTTableViewCell

@property (nonatomic, strong) NSString *user;

- (void)setupCellWithJob:(Job*)job forCustomer:(BOOL)isCustomer;

@end
