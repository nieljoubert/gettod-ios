//
//  GTSeachBarView.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/16.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTTextField.h"

@protocol GTSearchBarViewDelegate;

@interface GTSearchBarView : UIView <UITextFieldDelegate>

@property(nonatomic, weak) id <GTSearchBarViewDelegate> delegate;
@property(nonatomic, strong) GTTextField *textField;

@end

@protocol GTSearchBarViewDelegate <NSObject>

-(void) searchTextEntered:(NSString*)searchText;
-(void) searchButtonTapped;

@end