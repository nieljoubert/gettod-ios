//
//  Company.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Company.h"

@implementation Company

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.companyId = [decoder decodeObjectForKey:@"companyId"];
		self.name = [decoder decodeObjectForKey:@"name"];
		self.address = [decoder decodeObjectForKey:@"address"];
		self.suburb = [decoder decodeObjectForKey:@"suburb"];
		self.postCode = [decoder decodeObjectForKey:@"postCode"];
		self.registeredName = [decoder decodeObjectForKey:@"registeredName"];
		self.registrationNumber = [decoder decodeObjectForKey:@"registrationNumber"];
		self.contactNumber = [decoder decodeObjectForKey:@"contactNumber"];
		self.vatNumber = [decoder decodeObjectForKey:@"vatNumber"];
		self.isVatRegistered = [decoder decodeObjectForKey:@"isVatRegistered"];
		self.city = [decoder decodeObjectForKey:@"city"];
		self.entity = [decoder decodeObjectForKey:@"entity"];
	}
	
	return self;
	
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.companyId forKey:@"companyId"];
	[encoder encodeObject:self.name forKey:@"name"];
	[encoder encodeObject:self.address forKey:@"address"];
	[encoder encodeObject:self.suburb forKey:@"suburb"];
	[encoder encodeObject:self.postCode forKey:@"postCode"];
	[encoder encodeObject:self.registeredName forKey:@"registeredName"];
	[encoder encodeObject:self.registrationNumber forKey:@"registrationNumber"];
	[encoder encodeObject:self.contactNumber forKey:@"contactNumber"];
	[encoder encodeObject:self.vatNumber forKey:@"vatNumber"];
	[encoder encodeBool:self.isVatRegistered forKey:@"isVatRegistered"];
	[encoder encodeObject:self.city forKey:@"city"];
	[encoder encodeObject:self.entity forKey:@"entity"];
}


- (NSString *)description
{
	return [NSString stringWithFormat:@"\nID: %@\nNA: %@\nAD: %@\nSU: %@\nPC: %@\nRN: %@\nRN: %@\nCN: %@\nVN: %@\nCI: %@\nEN: %@\n-------------------------------",self.companyId,self.name,self.address,self.suburb,self.postCode,self.registeredName,self.registrationNumber,self.contactNumber,self.vatNumber,self.city,self.entity];
}

@end
