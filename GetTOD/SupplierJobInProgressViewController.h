//
//  SupplierJobInProgressViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/10.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"

@interface SupplierJobInProgressViewController : RootViewController

//@property(nonatomic, weak) id delegate;

@end

//@protocol SupplierJobInProgressDelegate <NSObject>
//- (void)jobCancelled:(SupplierJobInProgressViewController*)controller;
//@end