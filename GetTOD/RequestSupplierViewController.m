//
//  RequestSupplierViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/21.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RequestSupplierViewController.h"
#import "PaymentCardCell.h"
#import "BaseTableViewBlackOverlayViewController.h"
#import "WhitePaymentCardCell.h"
#import "RequestingSupplierBlackOverlayViewController.h"
#import "SupplierEnRouteViewController.h"
#import "ImageHelper.h"
#import "Problem.h"
#import "Job.h"
#import "GTTableSectionHeaderView.h"
#import "AddNewItemCell.h"
#import "AddEditCardDetailsViewController.h"

@interface RequestSupplierViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *mapImageView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@property (weak, nonatomic) IBOutlet UIView *timeCostView;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *costView;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIImageView *costDotsImageView;

@property (weak, nonatomic) IBOutlet UIView *problemView;
@property (weak, nonatomic) IBOutlet UILabel *whatIsProblemLabel;
@property (weak, nonatomic) IBOutlet UILabel *letUsKnowLabel;
@property (weak, nonatomic) IBOutlet UILabel *chooseLabel;

@property (strong, nonatomic) UILabel *instructionsTextLabel;

@property (strong, nonatomic) IBOutlet UITableView *cardView;

@property (strong, nonatomic) NSString *additionalInstructionsText;
@property (strong, nonatomic) Problem *selectedProblem;
@property (strong, nonatomic) NSArray *problems;
@property (strong, nonatomic) IBOutlet UIScrollView *headerview;

@property (strong, nonatomic) Card *selectedCard;
@property (strong, nonatomic) NSArray *cards;

@end

@implementation RequestSupplierViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.title = @"Request Supplier";
	
	[self.cardView registerClass:[PaymentCardCell class] forCellReuseIdentifier:@"PaymentCardCell"];
	[self.cardView registerClass:[GTBoldTableViewCell class] forCellReuseIdentifier:@"GTBoldTableViewCell"];
	[self.cardView registerClass:[AddNewItemCell class] forCellReuseIdentifier:@"AddNewItemCell"];
	
	self.problemView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.problemView.layer.borderWidth = 0.5;
	
	self.timeView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.timeView.layer.borderWidth = 0.5;
	
	self.costView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
	self.costView.layer.borderWidth = 0.5;
	
//	self.cardView.layer.borderColor = [SEPARATOR_LINE_COLOR CGColor];
//	self.cardView.layer.borderWidth = 0.5;
	
	self.mapImageView.image = self.mapImage;
	
	UITapGestureRecognizer *probTap = [[UITapGestureRecognizer alloc] init];
	probTap.numberOfTapsRequired = 1;
	[probTap addTarget:self action:@selector(launchProblemList)];
	[self.problemView addGestureRecognizer:probTap];
	
	self.nameLabel.text = self.supplier.user.name;
	self.companyLabel.text = self.supplier.company.name;
	
	[self.photoImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEURLSTRING,self.supplier.profilePicURL]]]
							   placeholderImage:[UIImage imageNamed:@"default_profile_photo"]
										success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
											
											[ImageHelper saveImageToDisk:image withName:[NSString stringWithFormat:SUPPLIER_IMAGE_NAME,self.supplier.supplierId]];
											self.photoImageView.image = image;
											self.photoImageView.layer.cornerRadius = self.photoImageView.frame.size.width / 2;
											self.photoImageView.clipsToBounds = YES;
											
										} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
											
											
										}];
	
	self.ratingLabel.text = [NSString stringWithFormat:@"%.1f",[self.supplier.rating floatValue]];
	
	self.timeLabel.text = [NSString stringWithFormat:@"%d",[self.timeToSupplier intValue]];
	
	self.headerview.backgroundColor = [UIColor redColor];
	self.cardView.tableHeaderView = self.headerview;
    
    //	[self showLoader];
    
    [[APIManager manager] getCreditCards:^(NSMutableArray *results) {
        
        //		[self hideLoader];
        
        self.cards = results;
        
        if (results.count > 0)
        {
            [self.cardView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else
        {
            [AlertViewManager showAlertWithTitle:@"No credit card added"
                                         message:@"Do you want to add a card now?"
                               cancelButtonTitle:@"No"
                               otherButtonTitles:@[@"Yes"]
                            buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                
                                if (buttonIndex == 1)
                                {
                                    AddEditCardDetailsViewController *addEdit = [[AddEditCardDetailsViewController alloc] init];
                                    addEdit.isAdding = YES;
                                    addEdit.isSettings = YES;
                                    [self.navigationController pushViewController:addEdit animated:YES];
                                }
                            }];
        }
        
    } failure:^(NSError *error) {
        [self hideLoader];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = NO;
	
	if ([SettingsManager manager].cancelType == JobCancelTypeTimeOut)
	{
		[AlertViewManager showAlertWithTitle:@""
									 message:@"Your supplier didn't accept your request in time. Would you like to request a new supplier?"
						   cancelButtonTitle:@"No"
						   otherButtonTitles:@[@"Yes"]
						buttonSelectionBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
							
							[SettingsManager manager].cancelType = -1;
							[SettingsManager save];
							
							if (buttonIndex == 1)
							{
								[self.navigationController popViewControllerAnimated:YES];
							}
						}];
	}
}

#pragma mark - Private
- (void)launchProblemList
{
	BaseTableViewBlackOverlayViewController *problemList = [[BaseTableViewBlackOverlayViewController alloc] initWithNibName:@"BaseTableViewBlackOverlayViewController" bundle:nil];
	problemList.listType = BlackOverlayListTypeProblems;
	problemList.delegate = self;
	
	NSMutableArray *cells = [NSMutableArray array];
	
	for (Problem *problem in self.supplier.possibleProblems)
	{
		GTTableViewCell *cell = [GTTableViewCell new];
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.textColor = [UIColor whiteColor];
		cell.accessoryView = nil;
		cell.textLabel.text = problem.problemDescription;
		
		[cells addObject:cell];
	}
	
	problemList.cells = cells;
	problemList.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	problemList.backgroundImage = [self getBlurredBackgroundImage];
	[self.navigationController presentViewController:problemList animated:YES completion:nil];
}

- (void)launchCardList
{
	BaseTableViewBlackOverlayViewController *cardList = [[BaseTableViewBlackOverlayViewController alloc] initWithNibName:@"BaseTableViewBlackOverlayViewController" bundle:nil];
	cardList.listType = BlackOverlayListTypeCards;
	cardList.delegate = self;
	
	NSMutableArray *cells = [NSMutableArray array];
	
	for (Card *card in self.cards)
	{
		WhitePaymentCardCell *cell = [WhitePaymentCardCell new];
		[cell setupCellWithCard:card];
		
		[cells addObject:cell];
	}
	
	cardList.cells = cells;
	cardList.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	cardList.backgroundImage = [self getBlurredBackgroundImage];
	[self.navigationController presentViewController:cardList animated:YES completion:nil];
}

- (void)launchInstructions
{
	EnterTextViewController *textController = [EnterTextViewController new];
	textController.titleText = @"Arrival Instructions";
	textController.rightButtonText = @"Done";
	textController.leftButtonText = @"Cancel";
	textController.textViewHintText = @"Add arrival instructions for your supplier such as Apartment Block name and apartment number.";
	textController.existingText = self.additionalInstructionsText;
	textController.delegate = self;
	
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:textController];
	[Flurry logAllPageViews:navigationController];
	[self.navigationController presentViewController:navigationController animated:YES completion:nil];
}


#pragma mark - Delegates -

- (void)cellSelected:(int)index WithText:(NSString *)text fromController:(BaseTableViewBlackOverlayViewController*)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
	
	if (text)
	{
        if (index == -1)//self.supplier.possibleProblems.count)
		{
			self.whatIsProblemLabel.text = text;
			self.costLabel.text = @"N/A";
            
            Problem *problemToReturn = [Problem new];
            
            for (Problem *p in self.supplier.possibleProblems)
            {
                if ([p.problemDescription isEqualToString:@"Other"])
                {
                    problemToReturn.problemId = p.problemId;
                    problemToReturn.estimatedPrice = p.estimatedPrice;
                    break;
                }
            }
            
            problemToReturn.problemDescription = text;
            self.selectedProblem = problemToReturn;
        }
		else
		{
			self.selectedProblem = [self.supplier.possibleProblems objectAtIndex:index];
			self.whatIsProblemLabel.text = self.selectedProblem.problemDescription;
			self.costLabel.text = [NSString stringWithFormat:@"%@",self.selectedProblem.estimatedPrice];
		}
		
		self.chooseLabel.text = @"Change";
		
		self.letUsKnowLabel.text = @"Y O U R   P R O B L E M";
		self.letUsKnowLabel.font = [UIFont fontWithName:FONT_BOLD size:9];
		self.costDotsImageView.hidden = YES;
		
	}
	else
    {
        self.selectedCard = [self.cards objectAtIndex:index];
        [self.cardView reloadData];
    }
}

- (void)dismissedController:(BaseTableViewBlackOverlayViewController *)controller
{
    [controller dismissViewControllerAnimated:NO completion:nil];
}

- (void)textEntered:(NSString *)text forController:(EnterTextViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    self.additionalInstructionsText = text;
    
    [self.cardView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)requestTOD:(id)sender
{
    if (self.selectedProblem)
    {
        if (self.selectedCard)
        {
            [self showLoader];
            
            [[APIManager manager] requestSupplier:self.supplier.supplierId
                                          problem:self.selectedProblem
                                          isOther:([self.whatIsProblemLabel.text isEqualToString:self.selectedProblem.problemDescription]) ? YES : NO
                                   jobCoordinates:self.requestCoordinates
                           additionalInstructions:self.additionalInstructionsText
                                        cardToken:self.selectedCard.token
                                       travelTime:[NSString stringWithFormat:@"%@",self.timeToSupplier]
                                   travelDistance:[NSString stringWithFormat:@"%@",self.distanceToSupplier]
                                          success:^(Job *job) {
                                              
                                              [self hideLoader];
                                              
                                              [SettingsManager manager].appState = AppStateRequestingSupplier;
                                              [SettingsManager save];
                                              
                                              [SettingsManager manager].currentJob = job;
                                              
                                              RequestingSupplierBlackOverlayViewController *requestOverlay = [[RequestingSupplierBlackOverlayViewController alloc] initWithNibName:@"RequestingSupplierBlackOverlayViewController" bundle:nil];
                                              requestOverlay.backgroundImage = [self getBlurredBackgroundImage];
                                              [self.navigationController pushViewController:requestOverlay animated:NO];
                                              
                                          } failure:^(NSError *error) {
                                              
                                              [self hideLoader];
                                          }];
        }
        else
        {
            [AlertViewManager showAlertWithTitle:@""
                                         message:@"Please add or select a card before continuing"
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil
                            buttonSelectionBlock:nil];
        }
    }
    else
    {
        [AlertViewManager showAlertWithTitle:@""
                                     message:@"Please select a problem before continuing"
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil
                        buttonSelectionBlock:nil];
    }
}

- (float)caclulateInfoHeight
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, SCREEN_WIDTH-15, 100)];
    label.font = [UIFont fontWithName:FONT size:14];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = self.additionalInstructionsText;
    
	CGSize size = [label sizeThatFits:CGSizeMake(SCREEN_WIDTH-15, 1000)];
	return size.height;
}

#pragma mark - TableView -
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0)
	{
		if (self.selectedCard == nil)
		{
			if (self.cards.count > 0)
			{
				self.selectedCard = [self.cards objectAtIndex:0];
			}
		}
		
		PaymentCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCardCell"];
		[cell setupCellWithCard:self.selectedCard forScreen:YES];
		return cell;
	}
	else if (indexPath.row == 1)
	{
		AddNewItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddNewItemCell"];
		
		if (self.additionalInstructionsText.length > 0)
		{
			[cell setupCellWithText:@"Edit arrival instructions" showPlusSign:NO];
		}
		else
		{
			[cell setupCellWithText:@"Add arrival instructions" showPlusSign:YES];
		}
	
		return cell;
	}
	
	return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0)
	{
		return 50;
	}
	else
	{
		return 45;
	}
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//	return nil;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0)
	{
		if (self.selectedCard != nil)
		{
			[self launchCardList];
		}
		else
		{
			AddEditCardDetailsViewController *addEdit = [[AddEditCardDetailsViewController alloc] init];
			addEdit.isAdding = YES;
			addEdit.isSettings = YES;
			[self.navigationController pushViewController:addEdit animated:YES];
		}
	}
	if (indexPath.row == 1)
	{
		[self launchInstructions];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
