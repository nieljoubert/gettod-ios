//
//  FooterWithButtonsView.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "FooterWithButtonsView.h"
#import "GTButton.h"

@implementation FooterWithButtonsView

- (void)setupViewWithButtons:(NSArray*)buttons andView:(UIView*)view
{
	self.translatesAutoresizingMaskIntoConstraints = NO;
	
	GTButton *topButton = ([buttons objectAtIndex:0]) ? (GTButton*)[buttons objectAtIndex:0] : nil;
	GTButton *botButton = ([buttons objectAtIndex:1]) ? (GTButton*)[buttons objectAtIndex:1] : nil;

	NSMutableDictionary *views = [NSMutableDictionary new];

	if (buttons.count == 1)
	{
		[self addSubview:topButton];
		[views setObject:topButton forKey:@"topButton"];
		
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[topButton]-10-|"
																	 options:0
																	 metrics:nil
																	   views:views]];
		if (view)
		{
			[self addSubview:view];
			[views setObject:view forKey:@"view"];
			
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
																		 options:0
																		 metrics:nil
																		   views:views]];
			
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]-5-[topButton]-10-|"
																		 options:0
																		 metrics:nil
																		   views:views]];
		}
		else
		{
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[topButton]-10-|"
																		 options:0
																		 metrics:nil
																		   views:views]];
		}
	}
	else if (buttons.count == 2)
	{
		[self addSubview:topButton];
		[self addSubview:botButton];
		[views setObject:topButton forKey:@"topButton"];
		[views setObject:botButton forKey:@"botButton"];
		
		
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[topButton]-10-|"
																	 options:0
																	 metrics:nil
																	   views:views]];
		
		[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[botButton]-10-|"
																	 options:0
																	 metrics:nil
																	   views:views]];
		
		if (view)
		{
			[self addSubview:view];
			[views setObject:view forKey:@"view"];
			
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
																		 options:0
																		 metrics:nil
																		   views:views]];
			
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]-5-[topButton]-5-[botButton]-10-|"
																		 options:0
																		 metrics:nil
																		   views:views]];
		}
		else
		{
			[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[topButton]-5-[botButton]-10-|"
																		 options:0
																		 metrics:nil
																		   views:views]];
		}
	}
}

@end
