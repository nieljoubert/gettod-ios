//
//  FooterWithButtonsView.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterWithButtonsView : UIView

- (void)setupViewWithButtons:(NSArray*)buttons andView:(UIView*)view;

@end
