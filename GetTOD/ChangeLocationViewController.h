//
//  ChangeLocationViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseTableViewController.h"
#import "GraySeachBarView.h"
#import "MapSearchModalViewController.h"

@interface ChangeLocationViewController : RootViewController <GraySeachBarViewDelegate, UITableViewDataSource, UITableViewDelegate, MapSearchModalDelegate>

@property(nonatomic, weak) id delegate;
@end

@protocol ChangeLocationViewDelegate <NSObject>
- (void)locationChanged:(NSString*)newLocationString;
@end
