//
//  Supplier.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/04.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Company.h"
#import "User.h"
#import "City.h"
#import "Province.h"
#import "Service.h"
#import "Problem.h"


@interface Supplier : NSObject

- (CLLocationCoordinate2D)location;

@property (nonatomic, strong) NSNumber *supplierId;
@property (nonatomic, strong) NSNumber *rating;
@property (nonatomic, strong) NSNumber *labourRate;
@property (nonatomic, strong) NSString *cityOther;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *profilePicURL;
@property (nonatomic, strong) NSNumber *locationLat;
@property (nonatomic, strong) NSNumber *locationLng;
@property (nonatomic, strong) NSMutableArray *possibleProblems;

@property (nonatomic, strong) Service *service;
@property (nonatomic, strong) Province *province;
@property (nonatomic, strong) City *city;
@property (nonatomic, strong) Company *company;
@property (nonatomic, strong) User *user;


@end
