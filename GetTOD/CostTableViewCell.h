//
//  CostTableViewCell.h
//  getTOD
//
//  Created by Niel Joubert on 2015/04/07.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "GTTableViewCell.h"
#import "GTTextField.h"

@interface CostTableViewCell : GTTableViewCell <UITextFieldDelegate>

@property (strong, nonatomic) GTTextField *amountField;

- (void)setupCell;
- (void)setError;
- (void)dismissError;

@end
