//
//  CustomerCancelledInTimeViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/05/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "CustomerCancelledInTimeViewController.h"
#import "AppDelegate.h"

@interface CustomerCancelledInTimeViewController ()

@end

@implementation CustomerCancelledInTimeViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (IBAction)okayTapped:(id)sender
{
	self.navigationController.navigationBarHidden = NO;
	AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
	[appDelegate startAppWithPartnerHomeScreen];
}

@end
