//
//  AlertviewManager.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/12.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^AlertViewManagerCompletionBlock) (UIAlertView *alertView, NSInteger buttonIndex);

@interface AlertViewManager : NSObject

+ (void)showAlertWithTitle:(NSString *)title
				   message:(NSString *)message
		 cancelButtonTitle:(NSString *)cancelButtonTitle
		 otherButtonTitles:(NSArray *)otherButtonTitles
	 buttonSelectionBlock:(AlertViewManagerCompletionBlock)buttonSelectionBlock;
@end
