//
//  Constants.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#ifndef GetTOD_Constants_h
#define GetTOD_Constants_h

#define GOOGLE_MAPS_API_KEY @"AIzaSyC95pJm9dzF7zoKxdS-pM6wOhW7NIRwB1U"//@"AIzaSyCTfPd5AGDgch_yJyN2_t-GR0D6VZHe0EQ"
#define GOOGLE_MAPS_BROWSER_API_KEY @"AIzaSyDOWhQE3t85R3jgrQrfWliJBQSO2Np-g5o"//@"AIzaSyDgWYcrTepcuIzzIUPZamtmjCYGlX42y_8"

#define BASEURLSTRING @"http://suppliers.gettod.com"
#define DBPATH @"/tmp/getTOD.db"

#define NAVBAR_COLOR					[UIColor colorWithRed:0.960784 green:0.960784 blue:0.960784 alpha:1.0]
#define FB_BLUE_COLOR					[UIColor colorWithRed:0.176471 green:0.270588 blue:0.525490 alpha:1.0]
#define GRAY_COLOR						[UIColor colorWithRed:0.549020 green:0.580392 blue:0.611765 alpha:1.0]
#define LIGHT_BACKGROUND_COLOR			[UIColor colorWithRed:0.909804 green:0.929412 blue:0.937255 alpha:1.0]
#define PINK_COLOR						[UIColor colorWithRed:0.882353 green:0.341176 blue:0.419608 alpha:1.0]
#define PINK_BACKGROUND_COLOR			[UIColor colorWithRed:1.000000 green:0.901961 blue:0.917647 alpha:1.0]
#define DARK_BACKGROUND_COLOR			[UIColor colorWithRed:0.074510 green:0.074510 blue:0.074510 alpha:1.0]
#define DARK_TEXT_COLOR					[UIColor colorWithRed:0.211765 green:0.223529 blue:0.235294 alpha:1.0]
#define SECTION_HEADER_COLOR			[UIColor colorWithRed:0.835294 green:0.854902 blue:0.858824 alpha:1.0]
#define LIGHT_BUTTON_BACKGROUND_COLOR	[UIColor colorWithRed:0.945098 green:0.945098 blue:0.945098 alpha:1.0]
#define GREEN_COLOR						[UIColor colorWithRed:0.541176 green:0.721569 blue:0.258824 alpha:1.0]
#define ONLINE_GREEN_COLOR				[UIColor colorWithRed:0.545098 green:0.713725 blue:0.282353 alpha:1.0]
#define LOGIN_TEXT_BACKGROUND_COLOR		[UIColor colorWithRed:0.913725 green:0.933333 blue:0.937255 alpha:1.0]
#define SEPARATOR_LINE_COLOR			[UIColor colorWithRed:0.792157 green:0.815686 blue:0.815686 alpha:1.0]
#define ERROR_BACKGROUND_COLOR			[UIColor colorWithRed:0.941176 green:0.874510 blue:0.882353 alpha:1.0]
#define ERROR_TEXT_COLOR				[UIColor colorWithRed:0.564706 green:0.250980 blue:0.294118 alpha:1.0]
#define LIGHT_BLUE_COLOR				[UIColor colorWithRed:0.776471 green:0.890196 blue:0.949020 alpha:1.0]
#define DARK_BLUE_FONT_COLOR			[UIColor colorWithRed:0.278431 green:0.411765 blue:0.533333 alpha:1.0]
#define TIMING_BLUE_COLOR				[UIColor colorWithRed:0.411765 green:0.678431 blue:0.819608 alpha:1.0]

#define CELL_PADDING 15
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define NAVBAR_HEIGHT 64
#define TIME_TO_UPDATE_LOCATION 2

#define FONT_BOLD @"AdelleSans-Bold"
#define FONT_LIGHT @"AdelleSans-Light"
#define FONT_THIN @"AdelleSans-Thin"
#define FONT @"AdelleSans"
#define FONT_SEMI_BOLD @"AdelleSans-SemiBold"


#define PROFILE_IMAGE_NAME @"profile.png"
#define SERVICE_IMAGE_NAME @"service_%@.png"
#define SUPPLIER_IMAGE_NAME @"supplier_%@.png"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

typedef NS_ENUM(NSInteger,SupplierStatus)
{
	SupplierStatusOffline = 1,
	SupplierStatusOnline = 2,
	SupplierStatusBusy = 3
};

typedef NS_ENUM(NSInteger,JobCancelType)
{
	JobCancelTypeBeforeJob = 1,
	JobCancelTypeTimeOut = 2,
	JobCancelTypeFree = 3,
	JobCancelTypeDuringJob = 4
};

typedef NS_ENUM(NSInteger,ActionType)
{
	// 2, 7, 4, 8
    // 1, 5/6
	ActionTypeNone = 0,
	ActionTypeClientRequestedPartner = 1,
	ActionTypePartnerAcceptedRequest = 2,
	ActionTypeRequestCancelled = 3,
	ActionTypePartnerQuoteSubmitted = 4,
	ActionTypeClientAcceptedQuote = 5,
	ActionTypeClientRejectedQuote = 6,
	ActionTypePartnerArrived = 7,
	ActionTypePartnerJobCompleted = 8,
	ActionTypePartnerCameOnline = 9,
	ActionTypeClientCompletedPayment = 10
};


typedef NS_ENUM(NSInteger,AppState)
{
	AppStateLogin = 1,
	AppStateRegistration = 2,
	AppStateEnteringDetails = 3,
	AppStateCreatingAccount = 4,
	AppStateNone = 5,
	AppStateRequestingSupplier = 6,
	AppStateOffline = 7,
	AppStateEnroute = 8,
	AppStateCancelled = 9,
	AppStateArrived = 10,
	AppStateQuote = 11,
	AppStateAwaitingApproval = 12,
	AppStateQuoteRejected = 13,
	AppStateCancelledFromQuote = 14,
	AppStateJobInProgress = 15,
	AppStatePayment = 16,
	AppStateAwaitingPayment = 17,
	AppStateJobComplete = 18,
	AppStateRecommend = 19,
	AppStateReportAProblem = 20,
	AppStateCancelling = 21,
	AppStateQuoteRejecting = 22,
	AppStateProblemReported = 23,
	AppStateHome = 24
};


#endif
