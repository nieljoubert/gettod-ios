//
//  Quote.m
//  getTOD
//
//  Created by Niel Joubert on 2015/03/24.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Quote.h"

@implementation Quote

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.quoteID = [decoder decodeObjectForKey:@"quoteID"];
		self.jobID = [decoder decodeObjectForKey:@"jobID"];
		self.createdDate = [decoder decodeObjectForKey:@"createdDate"];
		self.labourRate = [decoder decodeObjectForKey:@"labourRate"];
		self.labourHours = [decoder decodeObjectForKey:@"labourHours"];
		self.jobDescription = [decoder decodeObjectForKey:@"jobDescription"];
		self.materials = [decoder decodeObjectForKey:@"materials"];
		self.rejectReasons = [decoder decodeObjectForKey:@"rejectReasons"];
	}
	
	return self;
	
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.quoteID forKey:@"quoteID"];
	[encoder encodeObject:self.jobID forKey:@"jobID"];
	[encoder encodeObject:self.createdDate forKey:@"createdDate"];
	[encoder encodeObject:self.labourRate forKey:@"labourRate"];
	[encoder encodeObject:self.labourHours forKey:@"labourHours"];
	[encoder encodeObject:self.jobDescription forKey:@"jobDescription"];
	[encoder encodeObject:self.materials forKey:@"materials"];
	[encoder encodeObject:self.rejectReasons forKey:@"rejectReasons"];
}

- (NSNumber*)quoteAmount
{
	float materialsAmount = 0.00;
	
	for (Material *mat in self.materials)
	{
		materialsAmount += [[mat price] floatValue];
	}
	
	float labourAmount = [self.labourHours floatValue] * [self.labourRate floatValue];
	float quoteAmountExVAT = materialsAmount + labourAmount;
	float quoteAmountIncVAT = quoteAmountExVAT*1.14;

	return [NSNumber numberWithFloat:quoteAmountIncVAT];
}

@end
