//
//  ShareViewController.h
//  getTOD
//
//  Created by Niel Joubert on 2015/03/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"

@protocol ShareViewDelegate;

@interface ShareViewController : RootViewController <UITextFieldDelegate>

@property(nonatomic, weak) id <ShareViewDelegate> delegate;

@end

@protocol ShareViewDelegate <NSObject>

- (void)finishedSharing:(ShareViewController*)controller;

@end