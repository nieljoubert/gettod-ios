//
//  JobCompleteBlackOverlayViewController.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/27.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "JobCompleteBlackOverlayViewController.h"
#import "EnterTextViewController.h"
#import "ReferAFriendBlackOverlayViewController.h"
#import "APIManager.h"
#import "ProblemReportedViewController.h"


@interface JobCompleteBlackOverlayViewController ()
@property (weak, nonatomic) IBOutlet UIView *reportProblemView;
@property (retain, nonatomic) IBOutlet RateView *rateView;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *rateTitleText;

@end

@implementation JobCompleteBlackOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.navigationController.navigationBar.hidden = YES;
	
	self.rateView.notSelectedImage = [UIImage imageNamed:@"star_rating_empty"];
	self.rateView.halfSelectedImage = [UIImage imageNamed:@"star_rating"];
	self.rateView.fullSelectedImage = [UIImage imageNamed:@"star_rating"];
	self.rateView.rating = 3;
	self.rateView.editable = YES;
	self.rateView.maxRating = 5;
	self.rateView.delegate = self;
	
	self.backgroundImageView.image = self.backgroundImage;
	
#ifdef CLIENT
	self.rateTitleText.text = @"PLEASE RATE YOUR SERVICE WITH";
	self.companyNameLabel.text = [SettingsManager manager].currentJob.supplier.company.name;
#else
	self.rateTitleText.text = @"PLEASE RATE YOUR EXPERIENCE WITH YOUR CLIENT";
	self.companyNameLabel.text = [SettingsManager manager].currentJob.customer.name;
#endif
	
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(reportProblem)];
	singleTap.numberOfTapsRequired = 1;
	[self.reportProblemView addGestureRecognizer:singleTap];
	
	[SettingsManager manager].appState = AppStateJobComplete;
	[SettingsManager save];
}

-(void) viewWillDisappear:(BOOL) animated
{
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController])
    {
        if (self.navigationController)
        {
            self.navigationController.delegate = nil;
        }
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)reportProblem
{
	[SettingsManager manager].appState = AppStateReportAProblem;
	[SettingsManager save];
	
	EnterTextViewController *textController = [EnterTextViewController new];
	textController.titleText = @"Report A Problem";
	textController.rightButtonText = @"Send";
	textController.leftButtonText = @"Cancel";
	textController.textViewHintText = @"Please enter a description of your problem here.";
	textController.delegate = self;
	
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:textController];
	[Flurry logAllPageViews:navigationController];
	[self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - Delegates -

-(void)rateView:(RateView *)rateView ratingDidChange:(float)rating
{

#ifdef CLIENT
	
	[SettingsManager manager].appState = AppStateRecommend;
	[SettingsManager save];
	
	ReferAFriendBlackOverlayViewController *referController = [[ReferAFriendBlackOverlayViewController alloc] initWithNibName:@"ReferAFriendBlackOverlayViewController" bundle:nil];
	
	referController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	referController.backgroundImage = self.backgroundImage;
	
	[self.navigationController pushViewController:referController animated:NO];
	
	[[APIManager manager] rateSupplierForJob:[SettingsManager manager].currentJob.jobId
									   andRating:[NSNumber numberWithFloat:rating]
										 success:^(BOOL success) {
											 
										 } failure:^(NSError *error) {
											 
										 }];
#else
	[[APIManager manager] rateUserForJob:[SettingsManager manager].currentJob.jobId
							   andRating:[NSNumber numberWithFloat:rating]
								 success:^(BOOL success) {
									 
								 } failure:^(NSError *error) {
									 
								 }];
	
    [SettingsManager manager].supplierStatus = SupplierStatusOnline;
    [SettingsManager save];
    
	self.navigationController.navigationBarHidden = NO;
	AppDelegate *appDelegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
	[appDelegate startAppWithPartnerHomeScreen];
#endif

}

-(void)sendText:(NSString *)text forController:(EnterTextViewController *)controller
{
	[controller dismissViewControllerAnimated:YES completion:nil];
    
	[[APIManager manager] reportProblemForJob:[SettingsManager manager].currentJob.jobId
								   andProblem:text
									  success:^(BOOL success) {
										  
										  [SettingsManager manager].appState = AppStateProblemReported;
										  [SettingsManager save];
										  
										  if (success)
										  {
											  ProblemReportedViewController *reported = [[ProblemReportedViewController alloc] initWithNibName:@"ProblemReportedViewController" bundle:nil];
											  [self.navigationController pushViewController:reported animated:NO];
										  }
										  
									  } failure:^(NSError *error) {
										  
									  }];
}

@end
