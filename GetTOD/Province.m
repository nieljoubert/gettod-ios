//
//  Province.m
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/01.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "Province.h"

@implementation Province

- (id)initWithCoder:(NSCoder *)decoder
{
	if ((self = [super init]))
	{
		self.provinceId = [decoder decodeObjectForKey:@"provinceId"];
		self.name = [decoder decodeObjectForKey:@"name"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
	[encoder encodeObject:self.provinceId forKey:@"provinceId"];
	[encoder encodeObject:self.name forKey:@"name"];
}


- (NSString *)description
{
	return [NSString stringWithFormat:@"\nID: %@\nName: %@\n-------------------------------",self.provinceId,self.name];
}

@end
