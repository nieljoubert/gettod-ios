//
//  CardScanViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/02/09.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"
#import "CardIO.h"

@interface CardScanViewController : RootViewController <CardIOPaymentViewControllerDelegate, CardIOViewDelegate>

@end
