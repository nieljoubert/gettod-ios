//
//  DirectionsDetailView.m
//  getTOD
//
//  Created by Niel Joubert on 2015/04/11.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "DirectionsDetailView.h"
#import "Constants.h"

@interface DirectionsDetailView()

@property (nonatomic, strong) UILabel *directionDescriptionLabel;
@property (nonatomic, strong) UILabel *streetNameLabel;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) UIView *directionAndDistanceView;
@property (nonatomic, strong) UIView *descriptionAndStreetView;
@property (nonatomic, strong) UIImageView *directionImageView;
@property (nonatomic, strong) UIImageView *divider;
@property (nonatomic, retain) UIActivityIndicatorView *loader;
@end

@implementation DirectionsDetailView

- (id)init
{
	self = [super init];
	
	if (self) {}
	
	return self;
}

- (void)setupViewWithDirection:(NavigationDirection)direction distanceInMeters:(int)distanceInMeters directionDescription:(NSString*)directionDescription streetName:(NSString*)streetName
{
	if (self.descriptionAndStreetView == nil)
	{
		self.descriptionAndStreetView = [UIView new];
		self.descriptionAndStreetView.translatesAutoresizingMaskIntoConstraints = NO;
		self.descriptionAndStreetView.backgroundColor = [UIColor whiteColor];
		[self addSubview:self.descriptionAndStreetView];
	}
	
	if (self.directionDescriptionLabel == nil)
	{
		self.directionDescriptionLabel = [UILabel new];
		self.directionDescriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.directionDescriptionLabel.font = [UIFont fontWithName:FONT size:23];
		self.directionDescriptionLabel.textColor = GRAY_COLOR;
		self.directionDescriptionLabel.numberOfLines = 1;
		[self.descriptionAndStreetView addSubview:self.directionDescriptionLabel];
	}
	
	self.directionDescriptionLabel.text = directionDescription;
	
	if (self.streetNameLabel == nil)
	{
		self.streetNameLabel = [UILabel new];
		self.streetNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.streetNameLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:23];
		self.streetNameLabel.textColor = DARK_TEXT_COLOR;
		self.streetNameLabel.numberOfLines = 1;
		self.streetNameLabel.adjustsFontSizeToFitWidth = YES;
		[self.descriptionAndStreetView addSubview:self.streetNameLabel];
	}
	
	self.streetNameLabel.text = streetName;
	
	if (self.directionAndDistanceView == nil)
	{
		self.directionAndDistanceView = [UIView new];
		self.directionAndDistanceView.translatesAutoresizingMaskIntoConstraints = NO;
		self.directionAndDistanceView.backgroundColor = [UIColor whiteColor];
		[self addSubview:self.directionAndDistanceView];
	}
	
	if (self.distanceLabel == nil)
	{
		self.distanceLabel = [UILabel new];
		self.distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
		self.distanceLabel.font = [UIFont fontWithName:FONT_SEMI_BOLD size:13];
		self.distanceLabel.textColor = DARK_TEXT_COLOR;
		self.distanceLabel.backgroundColor = [UIColor whiteColor];
		self.distanceLabel.textAlignment = NSTextAlignmentCenter;
		[self.directionAndDistanceView addSubview:self.distanceLabel];
	}
	
	self.distanceLabel.text = [NSString stringWithFormat:@"%dm",distanceInMeters];
	
	if (self.directionImageView == nil)
	{
		self.directionImageView = [UIImageView new];
		self.directionImageView.translatesAutoresizingMaskIntoConstraints = NO;
		self.directionImageView.contentMode = UIViewContentModeScaleAspectFit;
		[self.directionAndDistanceView addSubview:self.directionImageView];
	}
	
	UIImage *directionImage = [UIImage new];
	
	switch (direction)
	{
		case NavigationDirectionForkLeft:
			directionImage = [UIImage imageNamed:@"route_fork_left"];
			break;
		case NavigationDirectionForkRight:
			directionImage = [UIImage imageNamed:@"route_fork_right"];
			break;
		case NavigationDirectionTurnLeft:
			directionImage = [UIImage imageNamed:@"route_left"];
			break;
		case NavigationDirectionMerge:
			directionImage = [UIImage imageNamed:@"route_merge"];
			break;
		case NavigationDirectionRampLeft:
			directionImage = [UIImage imageNamed:@"route_ramp_left"];
			break;
		case NavigationDirectionRampRight:
			directionImage = [UIImage imageNamed:@"route_ramp_right"];
			break;
		case NavigationDirectionTurnRight:
			directionImage = [UIImage imageNamed:@"route_right"];
			break;
		case NavigationDirectionRoundaboutLeft:
			directionImage = [UIImage imageNamed:@"route_roundabout_left"];
			break;
		case NavigationDirectionRoundaboutRight:
			directionImage = [UIImage imageNamed:@"route_roundabout_right"];
			break;
		case NavigationDirectionRoundaboutStraight:
			directionImage = [UIImage imageNamed:@"route_roundabout_straight"];
			break;
		case NavigationDirectionTurnSharpLeft:
			directionImage = [UIImage imageNamed:@"route_sharp_right"];
			break;
		case NavigationDirectionTurnSharpRight:
			directionImage = [UIImage imageNamed:@"route_sharp_left"];
			break;
		case NavigationDirectionTurnSlightLeft:
			directionImage = [UIImage imageNamed:@"route_slight_left"];
			break;
		case NavigationDirectionTurnSlightRight:
			directionImage = [UIImage imageNamed:@"route_slight_right"];
			break;
		case NavigationDirectionStraight:
			directionImage = [UIImage imageNamed:@"route_straight"];
			break;
		case NavigationDirectionUTurnLeft:
			directionImage = [UIImage imageNamed:@"route_u_left"];
			break;
		case NavigationDirectionUTurnRight:
			directionImage = [UIImage imageNamed:@"route_u_right"];
			break;
		default:
			break;
	}
	
	self.directionImageView.image = directionImage;
	
	if (self.divider == nil)
	{
		self.divider = [UIImageView new];
		self.divider.translatesAutoresizingMaskIntoConstraints = NO;
		self.divider.image = [UIImage imageNamed:@"route_divider"];
		self.divider.contentMode = UIViewContentModeScaleAspectFit;
		[self addSubview:self.divider];
	}
	
	
	NSMutableDictionary *views = [NSMutableDictionary dictionaryWithDictionary:@{@"image":self.directionImageView,
																				 @"distance":self.distanceLabel,
																				 @"imageDistance":self.directionAndDistanceView,
																				 @"desc":self.directionDescriptionLabel,
																				 @"street":self.streetNameLabel,
																				 @"texts":self.descriptionAndStreetView,
																				 @"div":self.divider}];
	
	[self.directionAndDistanceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image(==distance)]|"
																						  options:0
																						  metrics:nil
																							views:views]];

	[self.directionAndDistanceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[distance]|"
																						  options:0
																						  metrics:nil
																							views:views]];

	[self.directionAndDistanceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image][distance]|"
																						  options:0
																						  metrics:nil
																							views:views]];
	
	[self.descriptionAndStreetView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[desc]|"
																						  options:0
																						  metrics:nil
																							views:views]];
	
	[self.descriptionAndStreetView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[street]|"
																						  options:0
																						  metrics:nil
																							views:views]];
	
	[self.descriptionAndStreetView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[desc][street]|"
																						  options:0
																						  metrics:nil
																							views:views]];
	
	
	// Content
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[imageDistance]-10-[div(==divW)]-10-[texts]-10-|"
																 options:0
																 metrics:@{@"divW":[NSNumber numberWithFloat:[UIImage imageNamed:@"route_divider"].size.width]}
																   views:views]];	
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[imageDistance]|"
																 options:0
																 metrics:nil
																   views:views]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-30-[div]|"
																 options:0
																 metrics:nil
																   views:views]];
	
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-35-[texts]|"
																 options:0
																 metrics:nil
																   views:views]];
}

- (void)showLoader
{
	self.loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	
	self.loader.center = CGPointMake(self.center.x, self.center.y + 10);
	[self.loader startAnimating];
	[self addSubview:self.loader];
	
	self.divider.hidden = YES;
	self.directionAndDistanceView.hidden = YES;
	self.descriptionAndStreetView.hidden = YES;
	
}

- (void)hideLoader
{
	[self.loader stopAnimating];
	[self.loader removeFromSuperview];
	
	self.divider.hidden = NO;
	self.directionAndDistanceView.hidden = NO;
	self.descriptionAndStreetView.hidden = NO;
}

@end
