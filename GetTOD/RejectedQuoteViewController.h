//
//  QuoteRejectedViewController.h
//  GetTOD
//
//  Created by Niel Joubert on 2015/03/02.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "RootViewController.h"
#import "BaseTableViewBlackOverlayViewController.h"

@interface RejectedQuoteViewController : RootViewController <BlackTableViewDelegate>

@end
