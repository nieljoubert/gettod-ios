//
//  CircularLock.m
//  CirucularLock
//
//  Created by Cem Olcay on 10/06/14.
//  Copyright (c) 2014 studionord. All rights reserved.
//

#import "ButtonCountDownAnimatedView.h"

@implementation ButtonCountDownAnimatedView


#pragma mark Lifecycle

- (instancetype)initWithInnerView:(UIView *)innerView
						 duration:(CGFloat)d
					  strokeWidth:(CGFloat)width
					  strokeColor:(UIColor *)strokeColor
						clockwise:(BOOL)clockwise
			  animantionCompleted:(animantionCompleted)animationCompleted;
{
    if ((self = [super initWithFrame:innerView.frame]))
	{
        [self.layer setCornerRadius:innerView.frame.size.width/2];
		
        self.radius = innerView.frame.size.height/2;
        self.duration = d;
		
        self.strokeWidth = width;
        self.strokeColor = strokeColor;
		self.innerView = innerView;
		
		self.clockwise = clockwise;
		self.completed = animationCompleted;
    }
	
    return self;
}


#pragma mark Interaction

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (self.clockwise)
	{
		[self clockwiseAnimation];
	}
	else
	{
		[self antiClockwiseAnimation];
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self cancelAnimation];
}


#pragma mark AnimationLayers

- (void)initLayers
{
    self.circleLayer = [CAShapeLayer layer];
    [self.circleLayer setPath:[[UIBezierPath bezierPathWithRoundedRect:CGRectMake(-2*self.strokeWidth, -2*self.strokeWidth, 2*(self.radius+2*self.strokeWidth), 2*(self.radius+2*self.strokeWidth)) cornerRadius:self.radius] CGPath]];
    [self.circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    [self.circleLayer setStrokeColor:[self.strokeColor CGColor]];
    [self.circleLayer setLineWidth:self.strokeWidth];
	self.circleLayer.lineDashPattern = @[@1, @3];
    [self.layer addSublayer:self.circleLayer];
    
    self.strokeLayer = [CAShapeLayer layer];
    [self.strokeLayer setPath:self.circleLayer.path];
    [self.strokeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [self.strokeLayer setStrokeColor:[self.strokeColor CGColor]];
    [self.strokeLayer setLineWidth:self.strokeWidth];
    [self.strokeLayer setLineCap:kCALineCapRound];
    [self.layer addSublayer:self.strokeLayer];
}

- (void)clockwiseAnimation
{
    [self initLayers];
    
    CABasicAnimation *strokeAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    [strokeAnimation setDelegate:self];
    [strokeAnimation setDuration:self.duration];
    [strokeAnimation setRepeatCount:1];
    [strokeAnimation setFromValue:@0];
    [strokeAnimation setToValue:@1];
    [self.strokeLayer addAnimation:strokeAnimation forKey:@"clockwiseAnimation"];
}

- (void)antiClockwiseAnimation
{
    [self initLayers];
    
	CABasicAnimation *strokeAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];//@"strokeStart"];
    [strokeAnimation setDelegate:self];
    [strokeAnimation setDuration:self.duration];
    [strokeAnimation setRepeatCount:1];
    [strokeAnimation setFromValue:@1];
    [strokeAnimation setToValue:@0];
    [self.strokeLayer addAnimation:strokeAnimation forKey:@"antiClockwiseAnimation"];
}


#pragma mark AnimationStates

- (void)cancelAnimation
{
    [self.circleLayer removeFromSuperlayer];
    [self.strokeLayer removeFromSuperlayer];
}

- (void)finishAnimation
{
	self.completed();
    [self cancelAnimation];
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	if (flag)
	{
        [self finishAnimation];
	}
	else
	{
		[self cancelAnimation];
	}
}

@end
