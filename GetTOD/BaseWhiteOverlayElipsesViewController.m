//
//  BaseWhiteOverlayElipsesViewController.m
//  getTOD
//
//  Created by Niel Joubert on 2015/05/15.
//  Copyright (c) 2015 NielJoubert. All rights reserved.
//

#import "BaseWhiteOverlayElipsesViewController.h"

@interface BaseWhiteOverlayElipsesViewController ()

@end

@implementation BaseWhiteOverlayElipsesViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	//	self.view.userInteractionEnabled = YES;
	//
	//	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(tapped)];
	//	singleTap.numberOfTapsRequired = 1;
	//	[self.view addGestureRecognizer:singleTap];
	
	self.indicatorView.delegate = self;
	self.indicatorView.numberOfCircles = 3;
	self.indicatorView.radius = 2.5;
	self.indicatorView.internalSpacing = 5;
	self.indicatorView.duration = 1.25;
	self.indicatorView.delay = 0.5;
	
	[self.indicatorView startAnimating];
}

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
	  circleBackgroundColorAtIndex:(NSUInteger)index {
	return PINK_COLOR;
}

//- (void)tapped
//{
//	[self.delegate awaitingTapped:self];
//}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}
@end
